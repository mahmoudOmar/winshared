﻿using Microsoft.AspNet.Identity.Owin;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Controllers
{
    public class ConfirmController : Controller
    {
        // GET: Confirm
        public ActionResult ConfirmLogin(string userid)
        {

            AspNetUsersManager usr = new AspNetUsersManager();
            var model=usr.GetById(userid);
            return View(model);
        }
        public ActionResult ConfirmLoginuser(string userid)
        {
            SecurityManager securityManager = new SecurityManager();
            securityManager.SignInauto(HttpContext.GetOwinContext().Get<ApplicationSignInManager>(), userid, false);
            return RedirectToAction("index", "home");
        }

    }
}