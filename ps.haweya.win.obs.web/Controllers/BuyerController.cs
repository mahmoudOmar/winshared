﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.viewModels;
using ps.haweya.win.obs.security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ps.haweya.win.obs.common;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.web.Models;
using Rotativa;
using ps.haweya.win.obs.business;
using System.Net;
using System.IO;
using RazorPDF;

namespace ps.haweya.win.obs.web.Controllers
{
    [Authorize(Roles = "Buyer")]
    public class BuyerController : BaseController
    {
        int buyerReportId = 2;
        // GET: Buyer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MyAccount()
        {
            string aspnetid = User.Identity.GetUserId();

            ApplicationUser u = new SecurityManager().UserManager.FindById(aspnetid);
            
            BuyerViewModel b = new BuyerViewModel() { Email = u.Email, Name = u.FullName, Country = Convert.ToInt32(u.CountryId), State = Convert.ToInt32(u.StateId), City = Convert.ToInt32(u.CityId),POBox=u.POBox, ASPNetUserID = User.Identity.GetUserId(),ID="0",Mobile=u.PhoneNumber };

            return View(b);
        }

        // POST: Buyers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MyAccount(BuyerViewModel buyer, HttpPostedFileBase img)
        {
            buyer.ASPNetUserID = User.Identity.GetUserId();
            buyer.Agreepolicies = true;
            buyer.ID = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                if (img != null)
                {
                    if (img.ContentType.Contains("image"))
                    {
                        //اسم عشوائي جديد لصورة مع الامتداد تاعها
                        buyer.ProfilePhoto = Guid.NewGuid() + System.IO.Path.GetExtension(img.FileName);
                        //تم حفظ الصورة بالاسم الجديد بمجلد الاوريجنال
                        img.SaveAs(Server.MapPath("/Content/DocFiles/" + buyer.ProfilePhoto));
                    }

                }

                SecurityManager securityManager = new SecurityManager();
                string result = securityManager.UpdateBuyer(buyer);


                Guid guid = Guid.Empty;
                Guid.TryParse(result, out guid);


                if (guid == Guid.Empty)
                {
                    ModelState.AddModelError("type", "Please Fill All Data");
                    return View(buyer);
                }
                //BuyerManager buyerManager= new BuyerManager();
                //BuyerViewModel itmdb = buyerManager.GetByid(buyer.ASPNetUserID);
                //itmdb.Name = buyer.Name;
                //itmdb.POBox = buyer.POBox;
                //itmdb.Country = buyer.Country;
                //itmdb.City = buyer.City;
                //itmdb.State = buyer.State;
                //itmdb.Mobile = buyer.Mobile;
                //itmdb.Email = buyer.Email;
              
                TempData["msg"] = "s: Data Updated Successfully";
            }

            return View(buyer);
        }


        public ActionResult MyInvoices()
        {
            string aspnetid = User.Identity.GetUserId();

            var ordeers = new BuyerOrderManager().GetByBuyerID(aspnetid).Where(x=>x.IsSentToAdmin!=null&&x.IsSentToAdmin==true).OrderByDescending(x => x.InsertDate);
            return View(ordeers);
        }

        public ActionResult MyOrders()
        {
            string aspnetid = User.Identity.GetUserId();
           
            var ordeers = new BuyerOrderManager().GetByBuyerID(aspnetid).Where(x => x.IsSentToAdmin != null && x.IsSentToAdmin == true).OrderByDescending(x => x.InsertDate);
            return View(ordeers);
        }
        public ActionResult OrderDetails(int id)
        {

            var ordeers = new BuyerOrderManager().GetByid(id);
            return View(ordeers);
        }

        public ActionResult MyBids()
        {
            string aspnetid = User.Identity.GetUserId();

            var Bids = new OBSBidDetailsManager().GetByUserid(aspnetid);
            return View(Bids);
        }
        public ActionResult BidDetails(long id)
        {

            var ordeers = new OBSBidDetailsManager().Get(id);
            return View(ordeers);
        }

        public ActionResult BidInvoic(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            OBSBidDetailViewModel bid = new OBSBidDetailsManager().Get(Convert.ToInt64(id));

            if (bid == null)
            {
                return HttpNotFound();
            }
            ViewBag.Bank = new BankAccountsManager().Get().FirstOrDefault();
            return View(bid);
        }
        public ActionResult MyFavorit()
        {
            string aspnetid = User.Identity.GetUserId();
          
            var ordeers = new FavoriteManager().GetByUserId(aspnetid).OrderByDescending(x => x.Insertdate).ToList();
            return View(ordeers);
        }
        public ActionResult DeleteFAvoritItem(long ProductID)
        {
            string aspnetid = User.Identity.GetUserId();
            FavoriteManager favoriteManager = new FavoriteManager();
            favoriteManager.Delete(favoriteManager.GetByProductId(aspnetid, ProductID).ID);
            TempData["msg"] = "s:The Ittem delete Successfully";
            return RedirectToAction("MyFavorit");
        }

        public ActionResult BuyerAddresses()
        {

            string buyerid = User.Identity.GetUserId() ;
          
            var itmes = new ShippingDataManager().GetbyBuyerID(buyerid).ToList();
            return View(itmes);

        }
        public ActionResult EndOrdershippingDataEdit(int id)
        {
            var itmdb = new ShippingDataManager().GetByid(id);

            return View(itmdb);

        }
   
        public ActionResult EndOrdershippingData()
        {

            return View();
        }

        public JsonResult SelectedAddress(int id)
        {
            ShippingDataManager shippingDataManager = new ShippingDataManager();
            var ship = shippingDataManager.GetByid(id);
            if (ship != null)
            {
                var shiplist = shippingDataManager.GetbyBuyerID(ship.UserID).ToList();
                foreach (var s in shiplist)
                {
                    s.IsPrimary = false;
                    shippingDataManager.UpdateShipData(s);
                }
                ship.IsPrimary = true;
        shippingDataManager.UpdateShipData(ship);
                return Json(new { satatus = 1 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { satatus = 0 }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EndOrdershippingDataEdit(ShippingDataViewModel model)
        {
            if (ModelState.IsValid)
            {
                ShippingDataManager SM = new ShippingDataManager();
                string buyerid = User.Identity.GetUserId();
                
                var itmes = new ShippingDataManager().GetbyBuyerID(buyerid).ToList();
                model.UserID = buyerid;
                var shiplist = SM.GetbyBuyerID(buyerid);
                if (model.IsPrimary == true && shiplist.Where(x => x.IsPrimary == true && x.ID != model.ID).Count() > 0)
                {
                    foreach (ShippingDataViewModel s in shiplist)
                    {
                        s.IsPrimary = false;
                        SM.UpdateShipData(s);
                    }

                    SM.UpdateShipData(model);
                }
                else
                {
                    SM.UpdateShipData(model);
                }
                BuyerOrderManager OM = new BuyerOrderManager();
                BuyerOrderViewModel O = OM.GetLastPinddingByBuyerid(buyerid);
                if (O != null)
                {
                    O.ShippingDataID = model.ID;
                    O.StageID = 2;
                    OM.UpdateOrder(O);
                }
                TempData["msg"] = "s: Data Saved Successfully ";
                return RedirectToAction("BuyerAddresses");
            }
            return View(model);



        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EndOrdershippingData(ShippingDataViewModel model)
        {
                if (ModelState.IsValid)
                {

                    ShippingDataManager SM = new ShippingDataManager();

                    string buyerid = User.Identity.GetUserId();
                   
                    var itmes = new ShippingDataManager().GetbyBuyerID(buyerid).ToList();
                    var shiplist = SM.GetbyBuyerID(buyerid);

                model.UserID = buyerid;
                    if (model.IsPrimary == true && shiplist.Where(x => x.IsPrimary == true && x.ID != model.ID).Count() > 0)
                    {

                        foreach (ShippingDataViewModel s in shiplist)
                        {
                            s.IsPrimary = false;
                            SM.UpdateShipData(s);
                        }
                    model.ID = SM.AddShipingData(model);
                }
                else
                {
                 model.ID=SM.AddShipingData(model);
                }
                BuyerOrderManager OM = new BuyerOrderManager();
                    BuyerOrderViewModel O = OM.GetLastPinddingByBuyerid(buyerid);
                    if (O != null)
                    {
                        O.ShippingDataID = model.ID;
                        O.StageID = 2;
                        OM.UpdateOrder(O);
                    }
                    TempData["msg"] = "s: Data Saved Successfully ";
                    return RedirectToAction("BuyerAddresses");
                }
                return View(model);
        }


        [HttpPost]
        public JsonResult UpdateOrderStatus(int orderId, int newStatus, string note, int oldStatusid)
        {

            int ID = new OrderHistoryManager().Add(orderId, newStatus, note, User.Identity.GetUserId(), oldStatusid);
            if (ID > 0)
            {
                new BuyerOrderManager().UpdateOrderStatus(orderId, newStatus);


            }

            OrderHistoryViewModel model = new OrderHistoryManager().GetByOrderId(orderId).OrderByDescending(x => x.InsertDate).FirstOrDefault();
            return Json(new { status = ID,StatusName=model.Status.Name,StatusClass=model.Status.Class,Oldclass=model.Status1.Class });
        }

        [HttpPost]
        public JsonResult UpdatebidStatus(int orderId, int newStatus, string note, int oldStatusid)
        {

            int ID = new BidHistoryManager().Add(orderId, newStatus, note, User.Identity.GetUserId(), oldStatusid);
            if (ID > 0)
            {
                new OBSBidDetailsManager().UpdateBidStatus(orderId, newStatus);


            }

            BidsHistoryViewModel model = new BidHistoryManager().GetByBidId(orderId).OrderByDescending(x => x.InsertDate).FirstOrDefault();
            return Json(new { status = ID, StatusName = model.Status.Name, StatusClass = model.Status.Class, Oldclass = model.Status1.Class });
        }
        public JsonResult bidHistory(int id)
        {
            var history = new BidHistoryManager().GetByBidId(id).ToList();

            return Json(history.Select(x => new { FullName = x.AspNetUser.FullName, Status1 = x.Status1, x.InsertDate, x.Notes }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult OrderHistory(int id)
        {
            var history = new OrderHistoryManager().GetByOrderId(id).ToList();
            if (history.Count() == 0)
            {
                var order = new BuyerOrderManager().GetByid(id);
                history.Add(new OrderHistoryViewModel() { OrderID = id, InsertDate = order.InsertDate, NewStatusID = order.StatusID, Status1 = order.Status, Notes = "New Order" });
            }


            return Json(history.Select(x => new { FullName = x.AspNetUser.FullName, Status1 = x.Status1, x.InsertDate, x.Notes }), JsonRequestBehavior.AllowGet);
        }
      
        public ActionResult Invoice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BuyerOrderViewModel buyerOrder = new BuyerOrderManager().GetByid(Convert.ToInt32(id));
            if (buyerOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.Bank = new BankAccountsManager().Get().FirstOrDefault();
            return View(buyerOrder);
        }


        [HttpPost]
        public JsonResult SavePaymentRecipt()
        {
            int Orderid = Convert.ToInt32(Request.Form["Orderid"]);
            int typeid = Convert.ToInt32(Request.Form["typeid"]);

            if (Request.Files.Count == 0 && Orderid == 0)
            {
                return Json(new { status = 0, msg = "please select image to role !!" });
            }

            BuyerOrderManager buyerOrderManager = new BuyerOrderManager();

            string filename = "";
            if (Request.Files.Count == 0||typeid==0)
                {
                    return Json(new { status = 0, msg = "Please Add Recipt" });

                }
          

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                   
                    filename = Orderid +(typeid==1?"_PaymentRecipt":typeid==2?"_InspectionRecipt":typeid==3? "_DeliveryRecipt":"test")+ Path.GetExtension(file.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\ReciptCopies\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    file.SaveAs(filePath);
                }

            


            BuyerOrderViewModel b = buyerOrderManager.GetByid(Orderid);
            if (typeid == 1)
            {
                b.PaymentRceiptCopy = filename;
                b.PaymentRceiptCopyTime = DateTime.Now;
            }
            if (typeid == 2)
            {
                b.InspictionReciptCopy = filename;
                b.InspictionReciptCopyTime = DateTime.Now;
            }
            if (typeid == 3)
            {
                b.DeliveryReciptCopy = filename;
                b.DeliveryReciptCopyTime = DateTime.Now;
            }
            int result = buyerOrderManager.UpdateOrder(b);

            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Payment Saved Success " });

        }

        [HttpPost]
        public JsonResult SaveBidPaymentRecipt()
        {
            int Orderid = Convert.ToInt32(Request.Form["Orderid"]);
            int typeid = Convert.ToInt32(Request.Form["typeid"]);

            if (Request.Files.Count == 0 && Orderid == 0)
            {
                return Json(new { status = 0, msg = "please select image to Recipt !!" });
            }

            OBSBidDetailsManager bidmanager = new OBSBidDetailsManager();

            string filename = "";
            if (Request.Files.Count == 0 || typeid == 0)
            {
                return Json(new { status = 0, msg = "Please Add Recipt" });

            }


            HttpPostedFileBase file = Request.Files[0];
            if (file != null && file.ContentLength > 0)
            {

                filename = Orderid + (typeid == 1 ? "_PaymentRecipt" : typeid == 2 ? "_InspectionRecipt" : typeid == 3 ? "_DeliveryRecipt" : "test") + Path.GetExtension(file.FileName);
                var path = Server.MapPath("~\\Content\\UploadData\\ReciptCopies\\");
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                var filePath = Path.Combine(path, filename);
                file.SaveAs(filePath);
            }




            OBSBidDetailViewModel b = bidmanager.Get(Orderid);
            if (typeid == 1)
            {
                b.PaymentRceiptCopy = filename;
                b.PaymentRceiptCopyTime = DateTime.Now;
            }
            if (typeid == 2)
            {
                b.InspictionReciptCopy = filename;
                b.InspictionReciptCopyTime = DateTime.Now;
            }
            if (typeid == 3)
            {
                b.PercentRceiptCopy = filename;
                b.PercentRceiptCopyTime = DateTime.Now;
            }

            long result = bidmanager.UpdateBid(b);

            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Payment Saved Success " });

        }

        public ActionResult Invoices()
        {
            return View();
        }

        //public ActionResult InvoicesDt()
        //{
        //    var purchaseInvoices = new OBSPurchaseDetailsManager().GetInvoiceDetailsByBuyer(User.Identity.Name);
        //    var BidInvoices = new OBSBidDetailsManager().GetInvoiceDetailsByBuyer(User.Identity.Name);
        //    List<Object> data = new List<object>();
        //    data.Add(purchaseInvoices);
        //    data.Add(BidInvoices);
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult PurchaseTracking()
        {
            return View();
        }

        public ActionResult PurchaseTrackingDt()
        {
            OBSPurchaseDetailsManager purchaseMgr = new OBSPurchaseDetailsManager();
            // TODO: change Get to GetByUser
            return Json(purchaseMgr.Get(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult BidTracking()
        {
            return View();
        }

        //public ActionResult BidTrackingDt()
        //{
        //    OBSBidDetailsManager purchaseMgr = new OBSBidDetailsManager();
        //    // TODO: change GetTrackingDetails to GetTrackingDetailsByUser
        //    return Json(purchaseMgr.GetTrackingDetails(), JsonRequestBehavior.AllowGet);
        //}

        public ActionResult DownloadInvoice(int? id)
        {
            ViewData["id"] = id;
            var invoice = new OBSPurchaseDetailsManager().GetInvoiceDetailsByPurchaseID(id.Value);
            var product = new ProductDetailsManager().GetByid((int)invoice.ProductId);
            //var CityData = Utility.GetCity();
            //var purchaseData = OBSDataManager.GetAllPurchaseData().Where(x => x.PurchaseId == id).FirstOrDefault();
           // var productDeatil = OBSDataManager.GetProductDetailsData().Where(x => x.ProductId == purchaseData.ProductId).FirstOrDefault();
            var total = Convert.ToDouble(invoice.BuyQuantity) * Convert.ToDouble(product.MinimumSellingPrice);
            var Grandtotal = total + Convert.ToDouble(invoice.ShipmentCharges);
            var Path = Request.Url.Authority;
            var ImagePath = Path + "/Content/OBSHome/images/logo-light1.png";
            var pageData = new PDFPagesData();
            pageData.UserName = invoice.OBSUserDetail.UserName;
            pageData.Address = invoice.OBSUserDetail.Address;
           // pageData.City = CityData[Convert.ToInt32(purchaseData.OBSUserDetail.City)];
            pageData.Phone = invoice.OBSUserDetail.Phone;
            pageData.Email = invoice.OBSUserDetail.Email;
            pageData.PurchaseOn = invoice.PurchaseOn.Date.ToString("MM/dd/yyyy");
            pageData.ProductTitle = invoice.ProductInformation.ProductTitle;
            pageData.MinimumSellingPrice = product.MinimumSellingPrice.ToString();
            pageData.BuyQuantity = invoice.BuyQuantity.ToString();
            pageData.SubTotals = total.ToString();
            pageData.Totals = Grandtotal.ToString();
            pageData.InvoiceNo = id.ToString();
            pageData.ImagePath = ImagePath;
            pageData.ShipmentCost = invoice.ShipmentCharges.ToString();


            return View(pageData);
        }

        public ActionResult InvoicePDF(int? id)
        {
            ViewData["id"] = id;
            var invoice = new OBSPurchaseDetailsManager().GetInvoiceDetailsByPurchaseID(id.Value);
            var product = new ProductDetailsManager().GetByid((int)invoice.ProductId);
            //var CityData = Utility.GetCity();
            //var purchaseData = OBSDataManager.GetAllPurchaseData().Where(x => x.PurchaseId == id).FirstOrDefault();
            // var productDeatil = OBSDataManager.GetProductDetailsData().Where(x => x.ProductId == purchaseData.ProductId).FirstOrDefault();
            var total = Convert.ToDouble(invoice.BuyQuantity) * Convert.ToDouble(product.MinimumSellingPrice);
            var Grandtotal = total + Convert.ToDouble(invoice.ShipmentCharges);
            var Path = Request.Url.Authority;
            var ImagePath = Path + "/Content/OBSHome/images/logo-light1.png";
            var pageData = new PDFPagesData();
            pageData.UserName = invoice.OBSUserDetail.UserName;
            pageData.Address = invoice.OBSUserDetail.Address;
            // pageData.City = CityData[Convert.ToInt32(purchaseData.OBSUserDetail.City)];
            pageData.Phone = invoice.OBSUserDetail.Phone;
            pageData.Email = invoice.OBSUserDetail.Email;
            pageData.PurchaseOn = invoice.PurchaseOn.Date.ToString("MM/dd/yyyy");
            pageData.ProductTitle = invoice.ProductInformation.ProductTitle;
            pageData.MinimumSellingPrice = product.MinimumSellingPrice.ToString();
            pageData.BuyQuantity = invoice.BuyQuantity.ToString();
            pageData.SubTotals = total.ToString();
            pageData.Totals = Grandtotal.ToString();
            pageData.InvoiceNo = id.ToString();
            pageData.ImagePath = ImagePath;
            pageData.ShipmentCost = invoice.ShipmentCharges.ToString();
            Dictionary<string, string> cookieCollection = new Dictionary<string, string>();
            foreach (var key in Request.Cookies.AllKeys)
            {
                cookieCollection.Add(key, Request.Cookies.Get(key).Value);
            }

            return new ViewAsPdf("DownloadInvoice", pageData)
            {
                FileName = invoice.OBSUserDetail.UserName + ".pdf",
                Cookies = cookieCollection
            };
        }

        public ActionResult DownloadBidInvoice(int id)
        {
            ViewData["id"] = id;
            var bidData = new OBSBidDetailsManager().Get().Where(x => x.BidId == id).FirstOrDefault();
            //var productDeatil = OBSDataManager.GetProductDetailsData().Where(x => x.ProductId == bidData.ProductId).FirstOrDefault();
            //var CityData = Utility.GetCity();
            var Path = Request.Url.Authority;
            var ImagePath = Path + "/Content/OBSHome/images/logo-light1.png";
            var pageData = new PDFPagesData();
            pageData.UserName = bidData.AspNetUser.UserName;
           // pageData.Address = bidData.AspNetUser.Address;
            //pageData.City = CityData[Convert.ToInt32(bidData.OBSUserDetail.City)];
           // pageData.Phone = bidData.AspNetUser.Phone;
            pageData.Email = bidData.AspNetUser.Email;
            pageData.PurchaseOn = bidData.AddedOn.Date.ToString("MM/dd/yyyy");
            pageData.ProductTitle = bidData.ProductInformation.ProductTitle;
            pageData.MinimumSellingPrice = bidData.BidPrice.ToString();
            pageData.BuyQuantity = bidData.ProductInformation.Quantity.ToString();
            var total = bidData.ProductInformation.ShipmentCharges + bidData.BidPrice;
            pageData.SubTotals = bidData.BidPrice.ToString();
            pageData.Totals = total.ToString();
            pageData.InvoiceNo = id.ToString();
            pageData.ImagePath = ImagePath;
            pageData.ShipmentCost = bidData.ProductInformation.ShipmentCharges.ToString();
            return View(pageData);
        }

        public ActionResult BidInvoicePDF(int id)
        {
            var bidData = new OBSBidDetailsManager().Get().Where(x => x.BidId == id).FirstOrDefault();
            //var productDeatil = OBSDataManager.GetProductDetailsData().Where(x => x.ProductId == bidData.ProductId).FirstOrDefault();
            //var CityData = Utility.GetCity();
            var Path = Request.Url.Authority;
            var ImagePath = Path + "/Content/OBSHome/images/logo-light1.png";
            var pageData = new PDFPagesData();
            pageData.UserName = bidData.AspNetUser.UserName;
            // pageData.Address = bidData.AspNetUser.Address;
            //pageData.City = CityData[Convert.ToInt32(bidData.OBSUserDetail.City)];
            // pageData.Phone = bidData.AspNetUser.Phone;
            pageData.Email = bidData.AspNetUser.Email;
            pageData.PurchaseOn = bidData.AddedOn.Date.ToString("MM/dd/yyyy");
            pageData.ProductTitle = bidData.ProductInformation.ProductTitle;
            pageData.MinimumSellingPrice = bidData.BidPrice.ToString();
            pageData.BuyQuantity = bidData.ProductInformation.Quantity.ToString();
            var total = bidData.ProductInformation.ShipmentCharges + bidData.BidPrice;
            pageData.SubTotals = bidData.BidPrice.ToString();
            pageData.Totals = total.ToString();
            pageData.InvoiceNo = id.ToString();
            pageData.ImagePath = ImagePath;
            pageData.ShipmentCost = bidData.ProductInformation.ShipmentCharges.ToString();

            Dictionary<string, string> cookieCollection = new Dictionary<string, string>();
            foreach (var key in Request.Cookies.AllKeys)
            {
                cookieCollection.Add(key, Request.Cookies.Get(key).Value);
            }
            return new ViewAsPdf("DownloadBidInvoice", pageData)
            {
                FileName = bidData.AspNetUser.UserName + ".pdf",
                Cookies = cookieCollection
            };

        }
        public ActionResult MyPreferences()
        {
            string aspnetid = User.Identity.GetUserId();

            ApplicationUser u = new SecurityManager().UserManager.FindById(aspnetid);

            BuyerViewModel b = new BuyerViewModel() { Email = u.Email, Name = u.FullName, Country = Convert.ToInt32(u.CountryId), State = Convert.ToInt32(u.StateId), City = Convert.ToInt32(u.CityId), POBox = u.POBox, ASPNetUserID = User.Identity.GetUserId(), ID = "0", Mobile = u.PhoneNumber };

            ViewBag.CUser = new AspNetUsersManager().GetByUserWithIncludes(User.Identity.GetUserId());
            ViewBag.Countries = CacheManager.Countries;
            ViewBag.Categories = new CategoryManager().Get();
            ViewBag.Manufacturers = new ManufacturerDetailsManager().Get();

            return View(b);
        }
        [HttpPost]
        public ActionResult MyPreferences(AspNetUserViewModel model)
        {
            try
            {
                var aspNetUsersManager = new AspNetUsersManager();
                var reportId = new UserReportsManager().AddOrUpdate(User.Identity.GetUserId(), buyerReportId, model.Frequency);
                new UserPreferedCategoryManager().AddRange(model.CategoriesPreferences, User.Identity.GetUserId());
                new UserPreferedManufacturerManager().AddRange(model.ManufacturersPreferences, User.Identity.GetUserId());
                new UserPreferedCountryManager().AddRange(model.CountryPreferences, User.Identity.GetUserId());

                TempData["msg"] = "s: Data Updated Successfully";
                return RedirectToAction("MyPreferences");
            }
            catch (Exception)
            {
                TempData["msg"] = "e: Please Check Data You Entered";
                return RedirectToAction("MyPreferences");
            }
        }

        [AllowAnonymous]
        public ActionResult DownloadOrderInvoice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BuyerOrderViewModel buyerOrder = new BuyerOrderManager().GetByid(Convert.ToInt32(id));
            if (buyerOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.Bank = new BankAccountsManager().Get().FirstOrDefault();
            return View(buyerOrder);
            // return new PdfResult(buyerOrder, "DownloadOrderInvoice");
        }
        public ActionResult GenerateInvoicePDF(int id)
        {
             return new Rotativa.ActionAsPdf("DownloadOrderInvoice", new { id=id});
            //return new PdfResult( "Index");
        }
    }
}