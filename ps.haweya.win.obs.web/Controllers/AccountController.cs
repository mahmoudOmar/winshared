﻿using ps.haweya.win.obs.viewModels;
using ps.haweya.win.obs.security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ps.haweya.win.obs.common;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.business;
using System.Threading.Tasks;
using ps.haweya.win.obs.viewModels.CustomModel;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;
using ps.haweya.win.obs.web.Utils;
using Microsoft.AspNet.Identity.Owin;
using System.Configuration;

namespace ps.haweya.win.obs.web.Controllers
{
    public class AccountController : BaseController
    {
        // GET: Register
        private string ClientId_Google = ConfigurationManager.AppSettings["Google.ClientID"];
        private string SecretKey_Google = ConfigurationManager.AppSettings["Google.SecretKey"];
        private string RedirectUrl_Google = ConfigurationManager.AppSettings["Google.RedirectUrl"];
        public ActionResult Register(string id)
        {
            if (string.Equals(id.ToLower(), "seller"))
                return View("SellerRegister");
            

            return View("BuyerRegister");

        }

        public ActionResult Verify()
        {
            if (TempData["UserId"] == null || string.IsNullOrEmpty(TempData["UserId"].ToString()))
            {
                return RedirectToAction("Index", "Home");
            }

            var userRoles = new SecurityManager().GetUserRolesById(TempData["UserId"].ToString());
            if (userRoles.Count() == 0)
            {
                throw new BusinessException("Invalid User Information, Please contact the system administrator");
            }

            var Verifymodel = new VerifyViewModel() { UserId = TempData["UserId"].ToString(), TypeID = userRoles.Contains("Buyer") ? "Buyer" : "Seller" };
            // we have valid user id.
            return View(Verifymodel);

          
        }

        [Authorize(Roles ="Seller")]

        public ActionResult AccountVerification()
        {
          
            return View();

        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BuyerRegister(RegisterViewModel model)
        {
            if (model.Agreepolicies == false)
            {
                //AddErrors()
                ModelState.AddModelError("Agreepolicies", "You Must Agree on the Terms & Policies!");
            }

            if (ModelState.IsValid)
            {
                SecurityManager securityManager = new SecurityManager();
                string result = securityManager.CreateUser(model, "Buyer");


                Guid guid = Guid.Empty;
                Guid.TryParse(result, out guid);


                if (guid != Guid.Empty)
                {
                    new VerficationPOTManager().Add(guid.ToString(), securityManager.GetByUserId(guid.ToString()).POT);
                    TempData["UserId"] = guid.ToString();
                    return RedirectToAction("Verify", "Account");
                }
                ModelState.AddModelError("ERROR", result);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SellerRegister(RegisterViewModel model)
        {
            if (model.Agreepolicies == false)
            {
                //AddErrors()
                ModelState.AddModelError("Agreepolicies", "You Must Agree on the Terms & Policies!");
            }

            if (ModelState.IsValid)
            {
                SecurityManager securityManager = new SecurityManager();
                string result = securityManager.CreateUser(model, "Seller");


                Guid guid = Guid.Empty;
                Guid.TryParse(result, out guid);


                if (guid != Guid.Empty)
                {

                    new SellerManager().Add(new SellerViewModel() { 
                        AspNetUserID=guid.ToString(),
                        //IsDeleted=false,
                        //IsVerified=false,
                        //EmailConfirmed=false,
                        //Agreepolicies=true,
                        //IsActive=true
                    });
                    new VerficationPOTManager().Add(guid.ToString(), securityManager.GetByUserId(guid.ToString()).POT);
                    TempData["UserId"] = guid;
                    return RedirectToAction("Verify", "Account");
                }
                ModelState.AddModelError("ERROR", result);
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Verify(VerifyViewModel model )
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "Invalid OTP ");
                return View(model) ;
            }

          
            
            var ValidOTP = new VerficationPOTManager().GetByUserId(model.UserId, model.OTP);
            if (ValidOTP == null)
            {
                // throw new BusinessException("Invalid User Information, Please contact the system administrator");
                ModelState.AddModelError("Error", "Invalid OTP ");
                return View(model);
            }
            SecurityManager securityManager = new SecurityManager();
            securityManager.VerifyUser(model.UserId);
            securityManager.SignIn(HttpContext.GetOwinContext().Authentication, model.UserId, false);


            if (securityManager.UserManager.IsInRole(model.UserId, "Buyer"))
            {

                // user signed in succ.

                var productsValue = Request.Cookies.Get("products");

                var products = new List<CartProductModel>();

                if (productsValue != null)
                {
                    products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
                }

                var cartManager = new UserCartManager();

                cartManager.UpdateUserCart(model.UserId, products);

                var userCart = cartManager.GetByUserId(User.Identity.GetUserId());

                // UpdateCookieCartValues(Response.Cookies, userCart.Count(), userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice));

                ResetCartValues();
            }
            // we have valid user id.
            return model.TypeID.Equals("Buyer") ? RedirectToAction("Index", "Home") : RedirectToAction("SellerDocumentID", "SellerSite");

        }




        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {

                SecurityManager sm = new SecurityManager();
                var user = await sm.UserManager.FindByEmailAsync(model.Email);
                //if (user == null || !(await sm.UserManager.IsEmailConfirmedAsync(user.Id)))
                //{
                    if (user != null)
                    {
                    // var code = await sm.UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    string guid = Guid.NewGuid().ToString();
                  
                        var record = new ChangePasswordRandomLinkManager().CheckValidcode(user.Id , false , true);
                        if (record> 0)
                        {
                        TempData["msg"]= "e:The code is requested before ... Pleas check your email ";
                        return View(model);
                        }
                        ChangePasswordRandomLinkViewModel cpass = new ChangePasswordRandomLinkViewModel();
                    cpass.UserID = user.Id;
                    cpass.Link = guid;
                    cpass.isValid = true;
                    cpass.MaxValidDateTime = DateTime.Now;
                    cpass.IsUsed = false;
                    new ChangePasswordRandomLinkManager().Add(cpass);
                    string Subject = String.Format("Reset Password");
                        string Message = string.Format("Hi {0}<BR/>Reset Password, Please reset your password by clicking here: <a href=\"{1}\">CLICK HERE</a>", user.UserName, Url.Action("ResetPassword", "Account", new {code = guid }, Request.Url.Scheme));
                        new MailManager().SendMessage(model.Email,Subject,Message);
                    TempData["msg"] = "s: Please check your email to reset your password.";
                    return View();
                    }
                    else
                    {
                    TempData["msg"] = "e:The Email Id is not found";

                    }
                }

            //}
            return View(model);
        }

        //
        //// GET: /Account/ForgotPasswordConfirmation
        //[AllowAnonymous]
        //public ActionResult ForgotPasswordConfirmation()
        //{
        //    return View();
        //}

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string Code)
        {
            return Code == null ? View("Unauthorized") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public  ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            SecurityManager sm = new SecurityManager();

            ChangePasswordRandomLinkViewModel chang = new ChangePasswordRandomLinkManager().GetByKey(model.Code);
            if (chang != null)
            {
                if (chang.isValid == true && chang.IsUsed == false)
                {
                    ApplicationUser usr = new SecurityManager().GetByUserId(chang.UserID);
                    
                    if (usr != null)
                    {
                        sm.ResetPassword(usr.UserName, model.Password);
                        
                        chang.isValid = false;
                        chang.IsUsed = true;

                        new ChangePasswordRandomLinkManager().Updated(chang);

                        TempData["msg"] = "s: Your password has been reset. Please <a href='/Home/Login' >click here to login</a>";
                        return View();
                    }
                }

                else
                {
                    TempData["msg"] = "e: Your Code is Invalid";
                    return View();
                }
            }
            //var user = await sm.UserManager.FindByEmailAsync(model.Email);
            //if (user == null)
            //{
            //    ModelState.AddModelError("","The Email Id is not found");
            //    return View();
            //}
            //var result = await sm.UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            //if (result.Succeeded)
            //{
            //    return RedirectToAction("ResetPasswordConfirmation", "Account");
            //}
            //ModelState.AddModelError("",string.Join(",", result.Errors));
           // AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }




        //


      







        //////linkedin
       // [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> SaveLinkedinUser(string code, string state, string error, string error_description)
        {
            if (string.IsNullOrEmpty(code))
            {
                return View("Error");
            }
            string type = state.Split('_')[1];
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://www.linkedin.com/")
            };
            var requestUrl = $"oauth/v2/accessToken?grant_type=authorization_code&code={code}&redirect_uri={AppConfig.Get("Linkedin.RedirectUrl")}&client_id={AppConfig.Get("Linkedin.ClientID")}&client_secret={AppConfig.Get("Linkedin.SecretKey")}";
            var response = await httpClient.GetAsync(requestUrl);
            var token = JsonConvert.DeserializeObject<TokenResponseLinkedin>(await response.Content.ReadAsStringAsync());
            //    Session["user"] = token.Access_token;
            var userinfo = await GetUserFromAccessTokenAsync(token.Access_token);

            AspNetUsersManager usr = new AspNetUsersManager();
         
            SecurityManager securityManager = new SecurityManager();
            var usrfound = usr.GetByEmail(userinfo.EmailAddress);
            if (usrfound != null)
            {
                usrfound.EmailConfirmed = true;
                usrfound.ConfirmedEmail = true;
                usr.Update(usrfound);
               securityManager.SignInauto(HttpContext.GetOwinContext().Get<ApplicationSignInManager>(), usrfound.Id, false);
                return RedirectToAction("index", "Home");
            }
            string result = securityManager.CreateUserExternal(userinfo.FirstName+" "+userinfo.LastName,userinfo.EmailAddress,"","" , type);

            if (result.Length > 0 && !result.Contains(','))
            {
              //  securityManager.SignInauto(HttpContext.GetOwinContext().Get<ApplicationSignInManager>(), result, false);
                return RedirectToAction("ConfirmLogin", "confirm",new {userid=result });
            }
            else
            { return RedirectToAction("Register", "Account",new { id=type}); }
        }
        private async Task<UserInfoLinkedoin> GetUserFromAccessTokenAsync(string token)
        {
            var apiClient = new HttpClient
            {
                BaseAddress = new Uri("https://api.linkedin.com")
            };
            // https://api.linkedin.com/v2/me
            //var url = $"/v1/people/~:({LinkedinUserInfoParameters})?oauth2_access_token={token}&format=json";
            var url = $"/v2/me";
            apiClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            var response = await apiClient.GetAsync(url);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var url1 = $"/v2/emailAddress?q=members&projection=(elements*(handle~))";
            var response1 = await apiClient.GetAsync(url1);
            var jsonResponse1 = await response1.Content.ReadAsStringAsync();


            dynamic names = JsonConvert.DeserializeObject(jsonResponse);
            dynamic emails = JsonConvert.DeserializeObject(jsonResponse1);
            dynamic elements = emails.elements[0];
            dynamic e = elements["handle~"];
            var usr = new UserInfoLinkedoin();
            usr.FirstName = names.localizedFirstName;
            usr.LastName = names.localizedLastName;
            //  usr.Countrycode = names[0].preferredLocale.country;
            usr.EmailAddress =e.emailAddress;

            //usr.EmailAddress=
            return usr;
        }


        //googlelogin
        public void LoginUsingGoogle(string type)
        {
            Response.Redirect($"https://accounts.google.com/o/oauth2/v2/auth?client_id={ClientId_Google}&response_type=code&scope=openid%20email%20profile&redirect_uri={RedirectUrl_Google}&state=abcdef_"+type);
        }
        /// <summary>
        /// Listen response from Google API after user authorization
        /// </summary>
        /// <param name="code">access code returned from Google API</param>
        /// <param name="state">A value passed by application to prevent Cross-site request forgery attack</param>
        /// <param name="session_state">session state</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> SaveGoogleUser(string code, string state, string session_state)
        {
            if (string.IsNullOrEmpty(code))
            {
                return View("Error");
            }

            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://www.googleapis.com")
            };
            var requestUrl = $"oauth2/v4/token?code={code}&client_id={ClientId_Google}&client_secret={SecretKey_Google}&redirect_uri={RedirectUrl_Google}&grant_type=authorization_code";

            var dict = new Dictionary<string, string>
            {
                { "Content-Type", "application/x-www-form-urlencoded" }
            };
            var req = new HttpRequestMessage(HttpMethod.Post, requestUrl) { Content = new FormUrlEncodedContent(dict) };
            var response = await httpClient.SendAsync(req);
            var token = JsonConvert.DeserializeObject<GmailToken>(await response.Content.ReadAsStringAsync());
         //   Session["user"] = token.AccessToken;
            var obj = await GetuserProfile(token.AccessToken);
            string type = state.Split('_')[1];
            //IdToken property stores user data in Base64Encoded form
            //var data = Convert.FromBase64String(token.IdToken.Split('.')[1]);
            //var base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data);


            AspNetUsersManager usr = new AspNetUsersManager();

            SecurityManager securityManager = new SecurityManager();
            var usrfound = usr.GetByEmail(obj.Email);
            if (usrfound != null)
            {
                usrfound.EmailConfirmed = true;
                usrfound.ConfirmedEmail = true;
                usr.Update(usrfound);
                securityManager.SignInauto(HttpContext.GetOwinContext().Get<ApplicationSignInManager>(), usrfound.Id, false);
                return RedirectToAction("index", "Home");
            }
            string result = securityManager.CreateUserExternal(obj.GivenName + " " + obj.FamilyName, obj.Email, "", "", type);

            if (result.Length > 0 && !result.Contains(','))
            {
                //  securityManager.SignInauto(HttpContext.GetOwinContext().Get<ApplicationSignInManager>(), result, false);
                return RedirectToAction("ConfirmLogin", "confirm", new { userid = result });
            }
            else
            { return RedirectToAction("Register", "Account", new { id = type }); }
            // return View("UserProfile", obj);
        }

        /// <summary>
        /// To fetch User Profile by access token
        /// </summary>
        /// <param name="accesstoken">access token</param>
        /// <returns>User Profile page</returns>
        public async Task<UserProfile_Google> GetuserProfile(string accesstoken)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://www.googleapis.com")
            };
            string url = $"https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token={accesstoken}";
            var response = await httpClient.GetAsync(url);
            return JsonConvert.DeserializeObject<UserProfile_Google>(await response.Content.ReadAsStringAsync());
        }

    }
    public class Tokenclass
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
    }
    public class Userclassgoogle
    {
        public string id { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string link { get; set; }
        public string picture { get; set; }
        public string gender { get; set; }
        public string locale { get; set; }
    }
}