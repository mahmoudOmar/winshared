﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.security;
using ps.haweya.win.obs.viewModels;
using ps.haweya.win.obs.viewModels.CustomModel;
using ps.haweya.win.obs.web.App_Start;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;

namespace ps.haweya.win.obs.web.Controllers
{
    public class mapitem
    {
        public string lat { get; set; }

        public string lng { get; set; }
    }
    public class MyAction
    {
        public string Name { get; set; }

        public bool IsHttpPost { get; set; }
    }

    public class MyController
    {
        public string Name { get; set; }

        public string Namespace { get; set; }

        public IEnumerable<MyAction> MyActions { get; set; }
    }

    public class MyArea
    {
        public string Name { get; set; }

        public IEnumerable<string> Namespace { get; set; }

        public IEnumerable<MyController> MyControllers { get; set; }
    }
    public class dtsearch
    {
        public long ProductId { get; set; }
        public string ProductImage { get; set; }
        public string ProductTitle { get; set; }

    }
    public class HomeController : BaseController
    {

        private static List<Type> GetSubClasses<T>()
        {
            return Assembly.GetCallingAssembly().GetTypes().Where(type => type.IsSubclassOf(typeof(T))).ToList();
        }

        private IEnumerable<MyAction> GetListOfAction(Type controller)
        {
            var navItems = new List<MyAction>();

            // Get a descriptor of this controller
            ReflectedControllerDescriptor controllerDesc = new ReflectedControllerDescriptor(controller);

            // Look at each action in the controller
            foreach (ActionDescriptor action in controllerDesc.GetCanonicalActions())
            {
                bool validAction = true;
                bool isHttpPost = false;

                // Get any attributes (filters) on the action
                object[] attributes = action.GetCustomAttributes(false);

                // Look at each attribute
                foreach (object filter in attributes)
                {
                    // Can we navigate to the action?
                    if (filter is ChildActionOnlyAttribute)
                    {
                        validAction = false;
                        break;
                    }
                    if (filter is HttpPostAttribute)
                    {
                        isHttpPost = true;
                    }

                }

                // Add the action to the list if it's "valid"
                if (validAction)
                    navItems.Add(new MyAction()
                    {
                        Name = action.ActionName,
                        IsHttpPost = isHttpPost
                    });
            }

            return navItems;
        }



        [AllowAnonymous]
        public ActionResult match()
        {
            string input = "RE: TICKET[#00010] << الوضع صعب جدا جدا من اكمل >>‎";

            Match match = Regex.Match(input, @"TICKET[#/d{5}]");

            // Here we check the Match instance.
            if (match.Success)
            {
                // Finally, we get the Group value and display it.
                string key = match.Groups[1].Value;
                Console.WriteLine(key);
            }

            return Content("aa");
        }



        [AllowAnonymous]
        public ActionResult email()
        {
            new EmailManager().FetchTickets();
            return Content("aa");
        }
        [AllowAnonymous]
        public ActionResult sendemail()
        {
            new MailManager().sendmessagenoreply();
            return RedirectToAction("index");
        }

        //[AllowAnonymous]
        //public ActionResult test111()
        //{
        //    var xxx = StordProcedurManager.testOutput();
        //    return Content("aa");
        //}

        [AllowAnonymous]
        public ActionResult GenerateLinks()
        {
            var list = GetSubClasses<Controller>();

            var getAllcontrollers = (
                                     from item in list
                                     let name = item.Name
                                     select new MyController()
                                     {
                                         Name = name.Replace("Controller", ""),
                                         Namespace = item.Namespace,
                                         MyActions = GetListOfAction(item)
                                     }).ToList();

            foreach (var item in getAllcontrollers)
            {
                foreach (var action in item.MyActions)
                {
                    string area = "";

                    if (item.Namespace.Contains("Areas"))
                    {
                        var x = item.Namespace.Split('.');
                        var index = Array.IndexOf(x, "Areas");
                        area = x[++index];
                    }

                    new LinkManager().AddOrIgnore(new LinkViewModel() { Area = area, Controller = item.Name, Action = action.Name });

                }
            }

            return Content("DONE");
        }

        [OBSAuthorize]
        public ActionResult UnAuthorized(string url)
        {
            ViewBag.URL = url ?? Url.Action("Index");
            return View();
        }

        public ActionResult Error404()
        {
            ViewBag.URL = Url.Action("Index");
            return View();
        }

        [OBSAuthorize(Roles = "Buyer")]
        public ActionResult Test()
        {
            var x = Thread.CurrentPrincipal.Identity;
            var x1 = User.IsInRole("Buyer");

            var roles = ((ClaimsIdentity)User.Identity).Claims
                        .Where(c => c.Type == ClaimTypes.Role)
                        .Select(c => c.Value.ToLower());

            return Content("DONE");
        }

        //public ActionResult CreateRole()
        //{

        //    var RoleManager = new SecurityManager().RoleManager;

        //    string roleName = "Technical";
        //    IdentityResult roleResult;

        //    // Check to see if Role Exists, if not create it
        //    if (!RoleManager.RoleExists(roleName))
        //    {
        //        roleResult = RoleManager.Create(new IdentityRole(roleName));
        //    }
        //    return Content("DONE");
        //}



        // GET: Home
        public ActionResult Index(int? id, string q)
        {
            //DateTime d = DateTime.Now;
            //int ms1 = d.Millisecond;
            var newslist = CacheManager.HomeNews;
            ViewBag.NewsList = newslist;
            ViewBag.Banars = CacheManager.Banars;
            ViewBag.Categories = CacheManager.HomeCategory;//new CategoryManager().Get();
            ViewBag.Sliders = CacheManager.SlidersHome;
           //// ViewBag.query = q;
            ProductDetailsManager PM = new ProductDetailsManager();
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
                ViewBag.UserFavorite = new FavoriteManager().GetFavoriteIdsByUserID(User.Identity.GetUserId());

            ////var products = PM.Get(id: id, q: q,active:true);
            ////ViewBag.ProductList = products;
            //   var productmap = PM.GetToMaphome().Select(x => new { lat = x.lat, lng = x.lon }).ToList();     
            //int productcount = StordProcedurManager.ExecuteStoredProcedureCounters("GetProductcount", new { id = id, q = q,AuctionCheck=0, DirectCheck=0, active =1 });
           ViewBag.productcount=PM.GetCount(id: id, q: q,null,null,brandsID:null,null,null,null,null,null,null,active:true);
           
            var Auctions = PM.GetMainEndingAuctionsTop(3);
           
            // ViewBag.Manufacturers = new ManufacturerManager().GetActive(true);

            // ViewBag.ManufacturersList = CacheManager.OBSManufacturers;
            ViewBag.MinPrice = PM.GetMinPrice();
            ViewBag.MaxPrice = PM.GetMaxPrice();
           
            ViewBag.Auctions = Auctions;
       
            //DateTime d1 = DateTime.Now;
            //int ms2 = d1.Millisecond;
            return View();
        }
        public JsonResult GetProductMapData()
        {
            var productMaps = new ProductDetailsManager().GetHomeProductForMap().Where(x => x.lat != 0 && x.lon != 0).ToList();
            return Json(productMaps, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddtoFavorite(long id)
        {
            if (User.IsInRole("Buyer"))
            {
                string userid = User.Identity.GetUserId();

                var fmanager = new FavoriteManager();
                var favitem= fmanager.GetByProductId(userid, id);
                if (favitem != null)
                {
                    fmanager.Delete(favitem.ID);
                   
                        return Json(new { status = 0, msg = "s: item  removed from favorite  successfully" }, JsonRequestBehavior.AllowGet);
                    

                }
                int result = fmanager.AddByUserID(userid, id);

             
                return Json(new { status = 1, msg = "s: item added to favorite Success " }, JsonRequestBehavior.AllowGet);
            }


            return Json(new { status = 0, msg = "e: To add a favorite, Please Login as a Buyer" }, JsonRequestBehavior.AllowGet);

        }

        //NotifyCode
        public JsonResult notifyUser(int id)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
            {
                var auction = new ProductDetailsManager().GetByid(id).ProductInformation;
                if (auction != null)
                {
                    string userid = new AspNetUsersManager().GetByEmail(User.Identity.Name).Id;
                    NotifyUserManager N = new NotifyUserManager();
                    if (N.GetCount(id, userid) > 0)
                    {
                        return Json(new { status = 0, msg = "e: You Request Notifyes befor" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        NotifyUserViewModel n = new NotifyUserViewModel();
                        n.ProductID = id;
                        n.IsNotifyed = false;
                        n.insertdate = DateTime.Now;
                        n.NotifyDate = Convert.ToDateTime(Convert.ToDateTime(auction.DeadLine).AddDays(-1));
                        n.AspNetUserID = userid;
                        n.EmailAddress = User.Identity.Name;
                        int result = N.AddNotify(n);
                       new ReportDeliveryManager().SendEmailNotifyMe(result);
                        return Json(new { status = 1, msg = "s: We will notify you before it starts." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { status = 0, msg = "e: Ther is no bid with this id" }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (User.Identity.IsAuthenticated && User.IsInRole("Seller"))
            {
                return Json(new { status = 0, msg = "e: Please Login as Buyer" }, JsonRequestBehavior.AllowGet);
            }
            else if (!User.Identity.IsAuthenticated)
            {
                return Json(new { status = 0, msg = "e: Please Login To Notify" }, JsonRequestBehavior.AllowGet);
            }
            else { return Json(new { status = 0, msg = "e: Please Login To Notify" }, JsonRequestBehavior.AllowGet); }

        }

        public ActionResult GetProductResultmap(int? id, string q, bool? AuctionCheck, bool? DirectCheck, string allVal, string amount, int? sorting, int? viewcount, string condetion = "")
        {
            var mapitems = new ProductDetailsManager().GetProductResultmap(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount, condetion);
            
            return Json(mapitems, JsonRequestBehavior.AllowGet);
        }
            public ActionResult ProductListAjax(int? id, string q, bool? AuctionCheck, bool? DirectCheck, string allVal, string amount, int? sorting, int? viewcount,string condetion="",int page=1,string currencysign = "SR" ,double currencyrate=1)
        {
            ViewBag.query = q;
            viewcount= viewcount ?? 9;
            ProductDetailsManager manager = new ProductDetailsManager();
          // var items = manager.GetProductajaxhome(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount, condetion,page-1, currencysign, currencyrate);
            var items = manager.GetProductajaxhomeproc(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount, condetion, page - 1, currencysign, currencyrate);
            var UserFavorite = new List<long>();
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
                UserFavorite = new FavoriteManager().GetFavoriteIdsByUserID(User.Identity.GetUserId());
            var allcount = manager.GetProductajaxhomeprocCount(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount, condetion, page -1, currencysign, currencyrate);//manager.GetProductajaxhomecount(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount, condetion, page, currencysign);
                                                                                                                                                                                          //  var mapitems =manager.GetProductResultmap(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount, condetion);


            var pagemodel = new PagingViewModel();
            pagemodel.PageIndex = page;
            pagemodel.PageSize = viewcount??9;
            pagemodel.RecordsCount = allcount;
            var data = new { items, UserFavorite , pagemodel };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetManufacturersListHomeajax(string manufactuertxt)
        {
            var m = CacheManager.OBSManufacturers.Where(x => x.ManufacturersName.ToLower().Contains(manufactuertxt.ToLower())).Select(x => new
            {
                x.ManufacturersName,
                x.ManufacturersId,
                ProductCount = x.ProductCount
            });

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSearchProducts(string SearshTest)
        {
            //var data = new ProductDetailsManager().Get(null, SearshTest, null, null, null, null, null, null, null, null, active: true).Select(x => new { ProductId = x.ProductId, ProductImage = x.ProductInformation.ProductImg[0], ProductTitle = x.ProductInformation.ProductTitle });
            //if (data.Count() == 0)
            //{


            //    dtsearch  dataitemtest =new dtsearch{ ProductId = -1, ProductImage = "", ProductTitle = "NO Data Found" };
            //   List<dtsearch> dtlist=new List<dtsearch>();
            //    dtlist.Add(dataitemtest);
            //    data = dtlist.Select(x=>new { x.ProductId, x.ProductImage, x.ProductTitle });
            //}

            var data =  StordProcedurManager.GetSearchhomePRODUCTS(SearshTest).Select(x => new { x.ProductId, x.ProductImage, x.ProductTitle });
            if (data.Count() == 0)
            {


                dtsearch dataitemtest = new dtsearch { ProductId = -1, ProductImage = "", ProductTitle = "NO Data Found" };
                List<dtsearch> dtlist = new List<dtsearch>();
                dtlist.Add(dataitemtest);
                data = dtlist.Select(x => new { x.ProductId, x.ProductImage, x.ProductTitle });
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchResult(int? page, string q, int? sorting, int? Viewcount, bool? DirectCheck, bool? AuctionCheck)
        {
            Viewcount = Viewcount ?? 9;
            int pagesize = 9;
            if (Viewcount != null)
                pagesize = Convert.ToInt32(Viewcount);
            
            ViewBag.q = q;
            ViewBag.sorting = sorting;
            ViewBag.Viewcount = Viewcount;
            ViewBag.DirectCheck = DirectCheck;
            ViewBag.AuctionCheck = AuctionCheck;

            ViewBag.Categories = new CategoryManager().Get();
            ViewBag.query = q;
            page = page ?? 1;
            ProductDetailsManager manager = new ProductDetailsManager();
            var products = manager.GetProductajaxhomeproc(null,q, AuctionCheck, DirectCheck, "","", sorting,Viewcount,"", Convert.ToInt32(page - 1),"",1);
            int TotalItemsCount = manager.GetProductajaxhomeprocCount(null, q, AuctionCheck, DirectCheck, "", "", sorting, Viewcount, "", 0, "", 1);
            ViewBag.CurrentPage = page != null ? page : 1;
         //   ViewBag.TotlaPages = (TotalItemsCount % pagesize == 0 ? TotalItemsCount / pagesize : (TotalItemsCount / pagesize));

            ViewBag.TotalItemsCount = TotalItemsCount;
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
                ViewBag.UserFavorite = new FavoriteManager().GetFavoriteIdsByUserID(User.Identity.GetUserId());


            return View(products);

        }

        public ActionResult About()
        {

           TempData["Clients"] = new ManufacturerManager().GetActive(true);
            var setting = new SettingsManager().GetByid(1);
            return View(setting);
        }

        public ActionResult CoreValues()
        {
            return View(new NewsFeedManager().GetTop(3));
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Manufactuer()
        {
            CacheManager.OBSManufacturers = StordProcedurManager.ManufacturersHomeData();
            return View(CacheManager.OBSManufacturers);
        }

        public ActionResult NewsDetails(int id)
        {
            var news = new NewsManager().GetByid(id);
            return View(news);
        }

        public ActionResult News(int? page)
        {

            int pagesize = 12;
            var news = new NewsManager().Get(page??1);
            int TotalItemsCount = new NewsManager().GetCount();
            ViewBag.CurrentPage = page != null ? page : 1;
            ViewBag.TotlaPages = (TotalItemsCount % pagesize == 0 ? TotalItemsCount / pagesize : (TotalItemsCount / pagesize) + 1);
            return View(news);
        }
        public ActionResult Pages(int id)
        {
            var page = new PageManager().GetByid(id);
            return View(page);
        }
        public ActionResult PowerStationAuction()
        {
            // var page = new PageManager().GetByid(id);

            var products = new ProductDetailsManager().Get(null, q: "POWER PLANT - STATION 4 IN RIYADH",true,false, null, null, null,null, null, 8, active: true).OrderBy(x=>x.ProductInformation.ProductTitle);
           
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
                ViewBag.UserFavorite = new FavoriteManager().GetFavoriteIdsByUserID(User.Identity.GetUserId());


            return View(products);
          
        }
        public ActionResult Thanks()
        {
            return View();
        }

        public ActionResult ThanksSeller()
        {
            return View();
        }



        public JsonResult GetplaceData(long ProductID)
        {
            string aspnetuser = User.Identity.GetUserId();
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
            {
                var details = new OBSBidDetailsManager().GetLastByProductID(ProductID);
                    var productdetails = new ProductInformationManager().GetByid(ProductID);
                    var biddef = productdetails.BidDifferenceType == 0 ? 0 : productdetails.BidDifferenceType == 1 ? productdetails.BidDifference : productdetails.BidDifferenceType == 2 ? ((productdetails.BidDifference * (productdetails.OBSBidDetails.Count()>0? productdetails.OBSBidDetails.Max(x=>x.BidPrice):0))/100) : 0;
                    return Json(new { status = 1, Price = (details!=null?details.BidPrice: productdetails.ProductDetails.FirstOrDefault().MinimumSellingPrice), Title = productdetails.ProductTitle, Qty = productdetails.Quantity, BidDifference = biddef, IsInspect = productdetails.IsInspect , ShowBidAmmount=productdetails.ShowBidAmmount==false?0:1 }, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json(new { status = 0, msg = "e:pleas Login as Buyer" }, JsonRequestBehavior.AllowGet); }

        }

        [Authorize(Roles = "Buyer")]
        [HttpPost]
        public JsonResult Addplace()
        {
            //int ProductID, double Amount,int Inspection,HttpPostedFileBase BidOffer
           

            int ProductID = Convert.ToInt32(Request.Form["ProductID"]);
            double Amount = Convert.ToDouble(Request.Form["Amount"]);
            int Inspection = Convert.ToInt32(Request.Form["Inspection"]);

            if (Request.Files.Count == 0 && ProductID == 0)
            {
                return Json(new { status = 0, msg = "please select bid offer !!" });
            }
            string bidoffername = "";
            string otherfiles = "";
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string guid = Guid.NewGuid().ToString();
                    bidoffername = guid + Path.GetExtension(file.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\BidsOffers\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }

                    bidoffername = file.FileName.Replace(Path.GetExtension(file.FileName), "_") + bidoffername;
                    var filePath = Path.Combine(path, bidoffername);
                    file.SaveAs(filePath);
                }
                if (Request.Files.Count > 1)
                {
                    for (int i = 1; i < Request.Files.Count; i++)
                    {
                        string otherfilename = "";
                        HttpPostedFileBase otherfile = Request.Files[i];
                        if (otherfile != null && otherfile.ContentLength > 0)
                        {
                            string guid = Guid.NewGuid().ToString();
                            
                            otherfilename = guid + Path.GetExtension(otherfile.FileName);
                             otherfilename = otherfile.FileName.Replace(Path.GetExtension(otherfile.FileName), "_") + otherfilename;
                            var path = Server.MapPath("~\\Content\\UploadData\\BidsOffers\\");
                            if (!System.IO.Directory.Exists(path))
                            {
                                System.IO.Directory.CreateDirectory(path);
                            }
                            var filePath = Path.Combine(path, otherfilename);
                            otherfile.SaveAs(filePath);
                            otherfiles += otherfilename + "&";
                        }
                    }
                }



            }
            string aspnetuser = User.Identity.GetUserId();
            OBSBidDetailsManager oBSBidDetailsManager = new OBSBidDetailsManager();
            var List = new CurrencyManager().Get().ToList();
            var maincurrency = List.Where(x => x.IsMain == true).FirstOrDefault();
            var cookie = Request.Cookies.Get("MyCurrency");
            if (cookie != null)
            {
                int id = Convert.ToInt32(cookie.Value);
                maincurrency = List.Where(x => x.Id == id).FirstOrDefault();
            }
          
            oBSBidDetailsManager.AddToBuyer(ProductID, Amount, aspnetuser, Inspection==1?true:false,maincurrency.Id,maincurrency.RateToRiyal??1,Amount* (maincurrency.RateToRiyal ?? 1),bidoffername,otherfiles);
            ProductInformationManager pmanager = new ProductInformationManager();
            ProductInformationViewModel pmodel = pmanager.GetByid(ProductID);
            pmodel.BidCount = pmodel.OBSBidDetails.Sum(x=>x.BidCount);
            pmanager.UpdateFull(pmodel);
            //send bid to seller
            new ReportDeliveryManager().SendBidsSummaryForSellerbyproduct(ProductID);

            return Json(new { status = 1, msg = "s:your bid was Added",bidscount= pmodel.BidCount,lastbidammout= Amount ,Sign=maincurrency.Sign}, JsonRequestBehavior.AllowGet);

        }



        [AllowAnonymous]
        public ActionResult Login()
        {

            if (User != null&& User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        public ActionResult LoginAdmin()
        {
            return View();
        }

        public ActionResult CheckoutLogin(string returnurl)
        {
            return View(new LoginViewModel() { returnurl = returnurl });
        }

        ///  post actions

        [HttpPost]
        public ActionResult Contact(ContactViewModel c)
        {
            //ViewBag.News = new NewsFeedManager().GetTop(3);
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "Please Fill All Data");
                //TempData["msg"] = "e:Please Fill All Data";
                return View(c);
            }
            //
            new MailManager().SendMessage("wingait19@gmail.com", "contact us message", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "mailto:" + c.Email, "this message sent to you from portal site contact us message details:<br/>name:" + c.Name + "<br/>Email:" + c.Email + "<br/>phone:" + c.phone + " <br/>messgae:" + c.Message + "<br/>", "Warehouse Integration Company", "Site Admin", "Reply");
            TempData["msg"] = "s:The message was sent success";
            c = new ContactViewModel() { Email = "", Name = "", Message = "", phone = "" };
            return View(c);
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var securityManager = new SecurityManager();

                ApplicationUser user = securityManager.GetUser(model.Email);

             
                var isValid = securityManager.SignIn(HttpContext.GetOwinContext().Get<ApplicationSignInManager>(),
                    model.Email, model.Password, model.RememberMe);

                if (isValid == SignInStatus.Failure)
                {
                    ModelState.AddModelError("Error", "Invalid User or Password");
                    return View();
                }

                if (isValid == SignInStatus.RequiresVerification)
                {
                    TempData["UserId"] = user.Id;
                    return RedirectToAction("Verify", "Account");
                }
                if (user.EmailConfirmed == false)
                {
                    TempData["UserId"] = user.Id;
                    return RedirectToAction("Verify", "Account");

                }
                if (user.LockoutEnabled == false)
                {
                    ModelState.AddModelError("Error", "This User is InActive yet");
                    //TempData["msg"] = "e: This User is InActive yet";
                    return View();
                }
                // user signed in succ.3

                if (securityManager.UserManager.IsInRole(user.Id, "Buyer"))
                {

                    // user signed in succ.

                    var productsValue = Request.Cookies.Get("products");

                    var products = new List<CartProductModel>();

                    if (productsValue != null)
                    {
                        products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
                    }

                    var cartManager = new UserCartManager();

                    cartManager.UpdateUserCart(user.Id, products);

                    var userCart = cartManager.GetByUserId(User.Identity.GetUserId());

                    ResetCartValues();
                    if (model.returnurl != null)
                    {
                        return Redirect(model.returnurl);
                    }
                    return RedirectToAction("Index");
                }
                // this user is seller 
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("Error", "Invalid User or Password");            
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult LoginAdmin(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var securityManager = new SecurityManager();
                ApplicationUser user = securityManager.GetUser(model.Email);

                if(user.LockoutEnabled==false)
                {
                    ModelState.AddModelError("Error", "This User is InActive yet");

                    TempData["msg"] = "e: This User is InActive yet";
                    return View();
                }
                var isValid = securityManager.SignIn(HttpContext.GetOwinContext().Get<ApplicationSignInManager>(),
          model.Email, model.Password, model.RememberMe);

                if (isValid == SignInStatus.Failure)
                {
                    ModelState.AddModelError("Error", "Invalid User or Password");
                    TempData["msg"] = "e: Invalid User or Password";
                    return View();
                }

                var defaultRole = user.Claims.SingleOrDefault(t => t.ClaimType.Equals("DEFAULT_ROLE")).ClaimValue;

                return RedirectToAction("Index", "Home", new { area = defaultRole });
            }

            ModelState.AddModelError("Error", "Invalid User or Password");
            TempData["msg"] = "e: Invalid User or Password";
            return View();
        }

        [HttpPost]
        public ActionResult CheckoutLogin(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var securityManager = new SecurityManager();

                ApplicationUser user = securityManager.GetUser(model.Email);
                if (user.LockoutEnabled == false)
                {
                    ModelState.AddModelError("Error", "This User is InActive yet");
                    //TempData["msg"] = "e: This User is InActive yet";
                    return View();
                }
                var isValid = securityManager.SignIn(HttpContext.GetOwinContext().Get<ApplicationSignInManager>(),
                    model.Email, model.Password, model.RememberMe);

                if (isValid == SignInStatus.Failure)
                {
                    ModelState.AddModelError("Error", "Invalid User or Password");
                    return View();
                }

                if (isValid == SignInStatus.RequiresVerification)
                {
                    TempData["UserId"] = user.Id;
                    return RedirectToAction("Verify", "Account");
                }

                // user signed in succ.3

                if (securityManager.UserManager.IsInRole(user.Id, "Buyer"))
                {

                    // user signed in succ.

                    var productsValue = Request.Cookies.Get("products");

                    var products = new List<CartProductModel>();

                    if (productsValue != null)
                    {
                        products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
                    }

                    var cartManager = new UserCartManager();

                    cartManager.UpdateUserCart(user.Id, products);

                    var userCart = cartManager.GetByUserId(User.Identity.GetUserId());

                    // UpdateCookieCartValues(Response.Cookies, userCart.Count(), userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice));

                    ResetCartValues();

                    if (model.returnurl != null)
                    {
                        return Redirect(model.returnurl);
                    }

                    return RedirectToAction("OrderSummaryinspection", "ProductDetails");
                }


                // this user is seller 
                return RedirectToAction("Index");

            }

            ModelState.AddModelError("Error", "Invalid User or Password");
            //TempData["error"] = "Invalid User or Password";

            return View();

            
        }
        [HttpPost]
        public ActionResult LogOut()
        {

            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Index", "Home");


        }
        [OutputCache(Duration = 3600, VaryByParam = "filename")]
        public ActionResult Thumbnail(string filename,int width=363,int height=268)
        {
            try
            {

                    WebImage file = new WebImage(Server.MapPath("~/" + filename));
                    var img = file
                        .Resize(width, height, false, true);
                  return   new ImageResult(new MemoryStream(img.GetBytes()), "binary/octet-stream");
                   
            }
            catch {

             
                  WebImage file = new WebImage(Server.MapPath("~/" + "Content/FrontEnd/images/giphy.gif"));
             var img = file
                 .Resize(width, height, false, true);
            return new ImageResult(new MemoryStream(img.GetBytes()), "binary/octet-stream");
           
            }
            
            
        }


        public ActionResult Terms()
        {
            var setting = new SettingsManager().GetByid(1);
            return View(setting);
        }

        public ActionResult Privacy()
        {
            var setting = new SettingsManager().GetByid(1);
            return View(setting);
        }



        public ActionResult GenerateIndex()
        {

          
            return new Rotativa.ActionAsPdf("IndexGenerat");
          
        }
        [AllowAnonymous]
        public ActionResult IndexGenerat()
        {
            ViewBag.OrdersCount = new BuyerOrderManager().GetCountstoAdmin();
            ViewBag.ProcusctsCount = new ProductInformationManager().GetCountstoAdmin();
            ViewBag.BuyersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Buyer");
            ViewBag.SellersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Seller");

            List<bidtableadminhome> list = new List<bidtableadminhome>();

            ViewBag.GetBidsTable = new OBSBidDetailsManager().GetWithGroupingProductsmodel("ProductInformation", 1, 10, "bidid", "desc", "").ToList();
           
            ViewBag.GetPRoductCountByCountry = JsonConvert.SerializeObject(new ProductDetailsManager().GetWithGroupingProductsByCountry().ToList());
            ViewBag.GetPRoductCountByCategory = JsonConvert.SerializeObject(new ProductInformationManager().GetWithGroupingProductsByCategory().ToList());
            ViewBag.GetBidsCountByProduct = JsonConvert.SerializeObject(new ProductInformationManager().GetWithbidcounttocahrt().ToList());
            ViewBag.GetBuyerssCountByCountry = JsonConvert.SerializeObject(new AspNetUsersManager().GetBuyersGroupingByCountry().ToList());
            return View();
        }


        [HttpPost]
        public JsonResult SaveImage(string base64,string imgname)
        {
            string filePath = "";

            string FileName = imgname;
            string path = "~/content/DocFiles/";
            try
            {
                var imageParts = base64.Split(',').ToList<string>();

                byte[] imageBytes = Convert.FromBase64String(imageParts[1]);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                Image image = Image.FromStream(ms, true);
                using (var newImage = new Bitmap(800, 800))
                using (var graphics = Graphics.FromImage(newImage))
                using (var stream = new MemoryStream())
                {
                    graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    graphics.DrawImage(image, new Rectangle(0, 0, 800, 800));
                    newImage.Save(stream, ImageFormat.Png);
                    //return File(stream.ToArray(), "image/png");
                    byte[] bytes = stream.ToArray();
                    filePath = Server.MapPath(path + FileName + ".png");
                    System.IO.File.WriteAllBytes(filePath, bytes);
                    filePath = path  + FileName + ".png";
                }
            }
            catch (Exception ex)
            {
                filePath = "";
            }
            return Json(filePath, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetBidsTabel(string q = "")
        {

            var list = new OBSBidDetailsManager().GetWithGroupingProducts("ProductInformation", 1, 10, "bidid", "desc", q).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
       
        public JsonResult GetPRoductCountByCountry()
        {

            var list = new ProductDetailsManager().GetWithGroupingProductsByCountry().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    
        public JsonResult GetPRoductCountByCategory()
        {

            var list = new ProductInformationManager().GetWithGroupingProductsByCategory().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBidsCountByProduct()
        {

            var list = new ProductInformationManager().GetWithbidcounttocahrt().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
  
        public JsonResult GetBuyerssCountByCountry()
        {

            var list = new AspNetUsersManager().GetBuyersGroupingByCountry().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        [AllowAnonymous]
        public ActionResult TicketsDetails(int id)
        {
            return View(new TicketManager().Get(id));
        }



        public ActionResult GetHomeMap()
        {
            var productmap = new ProductDetailsManager().GetHomeProductForMap();
            var path = Server.MapPath("~/Content/DocFiles/Mapitemsjson.json");
            string json = JsonConvert.SerializeObject(productmap.ToArray());
            System.IO.File.WriteAllText(path, json);

            return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUpcomingSlider()
        {
            var AUCTION = new ProductDetailsManager().GetMainAuctionsTop(5);
            //DateTime d = DateTime.Now.AddMonths(1);
            // ViewBag.AUCTION = AUCTION;

            //ViewBag.Year = d.Year;
            //ViewBag.Month = d.Month;
            //ViewBag.Day = d.Day;
            //ViewBag.Hour = d.Hour;
            //ViewBag.Minute = d.Minute;
            //ViewBag.Second = d.Second;
            //if (AUCTION != null)
            //{
            //    d = Convert.ToDateTime(AUCTION.ProductInformation.BidStartDate);
            //    if (d > DateTime.Now)
            //    {
            //        ViewBag.Year = d.Year;
            //        ViewBag.Month = d.Month;
            //        ViewBag.Day = d.Day;
            //        ViewBag.Hour = d.Hour;
            //        ViewBag.Minute = d.Minute;
            //        ViewBag.Second = d.Second;
            //    }
            //}
            var items = AUCTION.Select(x => new
            {
                x.ProductInformation.ProductTitle,
                x.Description,
                x.ProductInformation.BidStartDate,
                x.ProductId,
                datetext = Convert.ToDateTime(x.ProductInformation.BidStartDate).ToString("dd MMM yyyy"),
                timetext = Convert.ToDateTime(x.ProductInformation.BidStartDate).ToString("hh:mm tt"),
                Year = Convert.ToDateTime(x.ProductInformation.BidStartDate).Year,
                Month = Convert.ToDateTime(x.ProductInformation.BidStartDate).Month-1,
                Day = Convert.ToDateTime(x.ProductInformation.BidStartDate).Day,
                Hour = Convert.ToDateTime(x.ProductInformation.BidStartDate).Hour,
                Minute = Convert.ToDateTime(x.ProductInformation.BidStartDate).Minute,
                Second = Convert.ToDateTime(x.ProductInformation.BidStartDate).Second
            }) ;

            return Json(new { items,status=1}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetManufacturers()
        {

             var items=new ManufacturerManager().GetActive(true);
            return Json(items, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult updatecurrencyCookie(string currecnyId)
        {
            HttpCookie cookie = new HttpCookie("MyCurrency")
            {
                Value = currecnyId,
                Expires = DateTime.Now.AddYears(1)
            };
            Response.Cookies.Add(cookie);
            // var cookiecurrency = new CurrencyManager().GetByid(Convert.ToInt32(currecnyId));
            return Json(new { status = 1 });//, cookiecurrency });
        }
        [AllowAnonymous]
        public ActionResult Getgt()
        {


            var httpWebRequest1 = (HttpWebRequest)WebRequest.Create("https://twitter.com/i/flow/signup");
            httpWebRequest1.KeepAlive = false;
            httpWebRequest1.CookieContainer = new CookieContainer();
            httpWebRequest1.Method = "Get";
            httpWebRequest1.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36";
            try
            {
                string cookeis = "";
                string gt = "";
                var httpResponse = (HttpWebResponse)httpWebRequest1.GetResponse();
                CookieCollection cok = httpResponse.Cookies;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    gt = result.Substring(result.IndexOf("gt="), 19 + 3).Replace("gt=", "");

                    foreach (Cookie cook in httpResponse.Cookies)
                    {
                        //string name = cook.Name;
                        cookeis += cook.Name + "=" + cook.Value + ";";
                    }


                }
                return Json(new { gt = gt, cookeis = cookeis }, JsonRequestBehavior.AllowGet);
            }
            catch { return Json(new { gt = "", cookeis = "" },JsonRequestBehavior.AllowGet); }
          
        }
        [HttpPost]
        public JsonResult subscripeNewsletter(string email)
        {

            News_letter_EmailsViewModel model = new News_letter_EmailsViewModel() { Email = email, InserdDate = DateTime.Now };
            var manager= new News_Letter_Manager();
            var dbmodel = manager.GetByEmail(email);
            if (dbmodel != null)
                return Json(new { status = 0,msg="e:this email was subscripe befor" });
            manager.Add(model);
            return Json(new { status = 1,msg= "s:You Subscribed successfully" });
        }

        [HttpPost]
        public JsonResult SendSuggestionProduct(string productname, string productDetails)
        {

            if (productname != "" && productDetails != "")
            {
                string message = "";
                message = "<h3> Product Name: "+productname+"</h3>"+"<p>Product Details:<br/>"+productDetails+"</p>";
                MailManager mailing = new MailManager();
                mailing.SendMessage(new SettingsManager().GetByid(1).Email, "Suggestion Product from customer", mailing.messageboodyProductSigesstion(message)) ;

                return Json(new { status = 1, msg = "s:Thank You, Your Suggestion Sent" });

            }



            return Json(new { status = 0, msg = "e:Please fill all data" });
          
            
        }
    }
}