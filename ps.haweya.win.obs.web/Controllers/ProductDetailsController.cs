﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.security;
using ps.haweya.win.obs.viewModels;
using ps.haweya.win.obs.viewModels.CustomModel;
using ps.haweya.win.obs.web.App_Start;
using ps.haweya.win.obs.web.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
//using System.Web.Http;
using System.Web.Mvc;
using System.Configuration;

namespace ps.haweya.win.obs.web.Controllers
{

    //[EnableCors("*")]
  //  [AllowCrossSiteJson]
    public class ProductDetailsController : BaseController
    {
        double vatvalue = CacheManager.Setting.VatValue ?? 0;
        string vattext = CacheManager.Setting.VatText;

        public ActionResult auctions()
        {
            return RedirectToAction("Index", "ProductDetails", new { auction = 1 });
        }

            // GET: Product
            public ActionResult Index(int? id, string q,string manufacturid)
        {
           // ViewBag.Categories = new CategoryManager().Get();
            ViewBag.query = q;
            ViewBag.Categories = StordProcedurManager.CategoryHomeData();
            ProductDetailsManager PM = new ProductDetailsManager();

            //if (!string.IsNullOrEmpty(auction))
            //{
            //    ViewBag.ProductList = PM.Get(id, q, true, null, null, null, null, null, null, null, active: true);
            //}
            //else
            //{

            int brandid = Convert.ToInt32(string.IsNullOrEmpty(manufacturid)?"-1":manufacturid);
            if (brandid > -1)
            {
                List<int> brands = new List<int>();
                brands.Add(brandid);
                ViewBag.ProductList = PM.Get(id, q, null, null, brands, null, null, null, null, null, active: true);

                ViewBag.productcount = PM.GetCount(id: id, q: q, null, null, brandsID: brands, null, null, null, null, null, null, true);
            }
            else {
                ViewBag.ProductList = PM.Get(id, q, null, null, null, null, null, null, null, null, active: true);
                //}
                ViewBag.productcount = PM.GetCount(id: id, q: q, null, null, brandsID: null, null, null, null, null, null, null, true);
            } 
            ViewBag.ManufacturersList =  StordProcedurManager.ManufacturersHomeData();
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
                ViewBag.UserFavorite = new FavoriteManager().GetFavoriteIdsByUserID(User.Identity.GetUserId());
            ViewBag.MinPrice = PM.GetMinPrice();
            ViewBag.MaxPrice = PM.GetMaxPrice();
            var condestionlist = StordProcedurManager.Conditiontohome();
            ViewBag.Condetions = condestionlist;
            return View();
        }

        public ActionResult auctionList(int? id, string q)
        {
            // ViewBag.Categories = new CategoryManager().Get();
            ViewBag.query = q;
            ViewBag.Categories = StordProcedurManager.CategoryHomeDataAuctions();
            ProductDetailsManager PM = new ProductDetailsManager();


            ViewBag.ProductList = PM.Get(id, q, true, null, null, null, null, 3, null, active: true);
            
            ViewBag.productcount = PM.GetCount(id: id, q: q,true, null, brandsID: null, null, null, null, null, null, null, active: true);
            ViewBag.ManufacturersList = StordProcedurManager.ManufacturersHomeDataAuctions();
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
                ViewBag.UserFavorite = new FavoriteManager().GetFavoriteIdsByUserID(User.Identity.GetUserId());
            ViewBag.MinPrice = PM.GetMinPrice();
            ViewBag.MaxPrice = PM.GetMaxPrice();
            var condestionlist = StordProcedurManager.Conditiontohome();
            ViewBag.Condetions = condestionlist;
            return View();
        }
        //public ActionResult ProductListAjax(int? id, string q, bool? AuctionCheck, bool? DirectCheck, string allVal, string amount, int? sorting, int? viewcount)
        //{
        //    ViewBag.query = q;

        //    var items = new ProductDetailsManager().GetProductajaxhome(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount);
        //    return Json(items, JsonRequestBehavior.AllowGet);
        //}
        //public JsonResult GetManufacturersListHomeajax(string manufactuertxt)
        //{
        //    var m = new ManufacturerDetailsManager().GetManufacturersListHome(manufactuertxt);
        //    return Json(m, JsonRequestBehavior.AllowGet);
        //}
        //public JsonResult GetSearchProducts(string SearshTest)
        //{
        //    return Json(new ProductDetailsManager().Get(null, SearshTest, null, null, null, null, null, null, null, null).Select(x => new { ProductId = x.ProductId, ProductImage = x.ProductInformation.ProductImage != null ? x.ProductInformation.ProductImage.Split(',')[0] : "img_not_available.png", ProductTitle = x.ProductInformation.ProductTitle }), JsonRequestBehavior.AllowGet);
        //}

        //   [EnableCors("*")]
        public ActionResult Details(int id,string title="")
        {

            //Response.AddHeader("Access-Control-Allow-Origin", "*");
            ProductDetailViewModel productDetail = new ProductDetailsManager().GetByid(Convert.ToInt32(id));
            if (productDetail == null)
            {
                return HttpNotFound();
               //throw new System.Web.Http.HttpResponseException(HttpStatusCode.NotFound);
            }
            ViewBag.ProductList = new ProductDetailsManager().Getrelated(productDetail.ProductInformation.Category, id);
            if (productDetail.ProductInformation.OBSBidDetails.Where(s => s.IsSold == true).Count() > 0)
            {
                try
                {
                    var Country = CacheManager.Countries.Where(x => x.id == Convert.ToInt32(productDetail.ProductInformation.OBSBidDetails.Where(s => s.IsSold == true).FirstOrDefault().AspNetUser.CountryId)).FirstOrDefault().name;
                    var state = CacheManager.States.Where(x => x.id == Convert.ToInt32(productDetail.ProductInformation.OBSBidDetails.Where(s => s.IsSold == true).FirstOrDefault().AspNetUser.StateId)).FirstOrDefault().name;
                    var city = CacheManager.Cities.Where(x => x.id == Convert.ToInt32(productDetail.ProductInformation.OBSBidDetails.Where(s => s.IsSold == true).FirstOrDefault().AspNetUser.CityId)).FirstOrDefault().text;
                    ViewBag.Location = Country + " , " + state + " , " + city;
                }
                catch { }
            }
            List<BidersLocations> locations = new List<BidersLocations>();
            foreach ( var bid in productDetail.ProductInformation.OBSBidDetails)
            {
                BidersLocations bl = new BidersLocations();
                bl.Coutry = CacheManager.Countries.FirstOrDefault(x => x.id == Convert.ToInt32(bid.AspNetUser.CountryId)) != null ? CacheManager.Countries.FirstOrDefault(x => x.id == Convert.ToInt32(bid.AspNetUser.CountryId)).name : "";
                bl.State = CacheManager.States.FirstOrDefault(x => x.id == Convert.ToInt32(bid.AspNetUser.StateId)) != null ? CacheManager.States.FirstOrDefault(x => x.id == Convert.ToInt32(bid.AspNetUser.StateId)).name : "";
                bl.City = CacheManager.Cities.FirstOrDefault(x => x.id == Convert.ToInt32(bid.AspNetUser.CityId)) != null ? CacheManager.Cities.FirstOrDefault(x => x.id == Convert.ToInt32(bid.AspNetUser.CityId)).text : "";
                bl.price = bid.BidPrice;
                bl.time = bid.AddedOn;
                locations.Add(bl);
            }

            ViewBag.Locations = locations.OrderByDescending(x=>x.price);

            ViewBag.Shipping_Returns= new SettingsManager().GetByid(1)!=null? new SettingsManager().GetByid(1).shipping_Return:"";
           
            
                var Addresscookevalue = Request.Cookies.Get("Address");
            var Addressvalue = new AddressModel();
            ViewBag.BuyerCountryName = "-1";
            if (Addresscookevalue != null)
            {
                Addressvalue = JsonConvert.DeserializeObject<AddressModel>(StringCipher.Decrypt(Addresscookevalue.Value, "HAWEYA.PS"));
                ViewBag.BuyerCountryName = Addressvalue.CountryName;
            }
            else {

                if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
                {
                    var usermanager = new AspNetUsersManager();
                    var buyer = usermanager.GetByEmail(User.Identity.Name);
                    var shippingaddress = new ShippingDataManager().GetbyBuyerIDPrimary(buyer.Id).FirstOrDefault();


                    // var shippingaddress = buyer.ShippingDatas.Find(x => x.IsPrimary==true);

                    if (shippingaddress != null)
                    {
                        var buyercountryid = shippingaddress.CountryID ?? Convert.ToInt32(buyer.CountryId);

                        var buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
                        ViewBag.BuyerCountryName = buyercountry.name;
                    }
                }
            }
            return View(productDetail);
        }
        [Authorize(Roles="Buyer")]
        public ActionResult BuyerAddresses(string OrderID="")
        {
            //if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
            //{
                string buyerid = User.Identity.GetUserId();
                var itmes = new ShippingDataManager().GetbyBuyerID(buyerid).ToList();

            ViewBag.OrderID = new BuyerOrderManager().GetLastPinddingByBuyerid(buyerid).ID;
                return View(itmes);
            //}
            //else
            //{
            //    return RedirectToAction("CheckoutLogin", "Account", new { returnUrl = Url.Action("BuyerAddresses", "ProductDetails") });
            //}

        }
        [Authorize(Roles = "Buyer")]
        public ActionResult EndOrdershippingDataEdit(int id)
        {
         
                var itmdb = new ShippingDataManager().GetByid(id);

                return View(itmdb);
           

        }
        public JsonResult SelectedAddress(int id)
        {
            ShippingDataManager shippingDataManager = new ShippingDataManager();
            var ship = shippingDataManager.GetByid(id);
            if (ship != null)
            {
                var shiplist = shippingDataManager.GetbyBuyerID(ship.UserID).ToList();
                foreach (var s in shiplist)
                {
                    s.IsPrimary = false;
                    shippingDataManager.UpdateShipData(s);
                }
                ship.IsPrimary = true;
                shippingDataManager.UpdateShipData(ship);
                return Json(new { satatus = 1 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { satatus = 0 }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult EndOrderVisaData(int OrderID)
        {

            
            
            var shipping = new ShippingDataManager().GetbyBuyerIDPrimary(User.Identity.GetUserId()).ToList();
           
                var usermanager = new AspNetUsersManager();
                var buyer = usermanager.GetByEmail(User.Identity.Name);
                var shippingaddress = shipping.FirstOrDefault();
            ViewBag.BuyerCountryName = "-1";

            if (shippingaddress != null)
                {
                    var buyercountryid = shippingaddress.CountryID ?? Convert.ToInt32(buyer.CountryId);

                    var buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
                    ViewBag.BuyerCountryName = buyercountry.name;
                }
            
            ViewBag.ShippingItems = shipping;
            ViewBag.banklist = new BankAccountsManager().GetActive();
            var order = new BuyerOrderManager().GetByid(OrderID);
            if (order.CartItems.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0).Count() > 0)
            {




                double shippingpricetotal = 0;
                //  string shippingcurrency;
                var manager = new CartItemsManager();
                List<CartItemViewModel> newitems = new List<CartItemViewModel>();
                foreach (var itm in order.CartItems.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0))
                {
                    dynamic jsonData = DHLShippingtest(Convert.ToInt32(itm.ProductID), itm.Qty ?? 0).Data;

                    if (jsonData.status == 1 && jsonData.result != null)

                    {
                        dynamic result = JsonConvert.DeserializeObject<dynamic>(jsonData.result);
                        //  var result = jsonData.result;
                        try
                        {
                            // dynamic r = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(result.RateResponse));
                            //  dynamic p = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(r.Provider));
                            var s = result.RateResponse.Provider[0].Service;


                            if (s.Count > 0)
                            {
                                double shippingprice = Convert.ToDouble(s[0].TotalNet.Amount);
                                //  shippingcurrency = result.RateResponse.Provider[0].Service[0].TotalNet.Currency;
                                shippingpricetotal += shippingprice;
                                itm.ShippingValue = shippingprice * order.Currency.RateToRiyal;

                                newitems.Add(itm);


                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }

                foreach (var itm in newitems)
                {
                    manager.Update(itm);
                }

                ViewBag.shippingvalue = shippingpricetotal;
                ViewBag.shipcurrency = order.Currency.ShortName;
                order = new BuyerOrderManager().GetByid(OrderID);
            }
            ViewBag.Order = order;
            return View();
        }
        [Authorize(Roles ="Buyer")]
        public ActionResult OrderSummaryinspection()
        
        {

            var Mycurrency = TempData["maincurrency"] as CurrencyViewModel;

            if (TempData["cookiecurrency"] != null)
            {
                Mycurrency = TempData["cookiecurrency"] as CurrencyViewModel;
            }
            var productsValue = Request.Cookies.Get("products");
            var products = new List<CartProductModel>();

            if (productsValue != null)
            {
                products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
            }
            string buyerid = User.Identity.GetUserId();

           // var shipping = new ShippingDataManager().GetbyBuyerIDPrimary(buyerid).ToList();

            BuyerOrderManager buyerOrderManager = new BuyerOrderManager();

            
            BuyerOrderViewModel O = buyerOrderManager.GetLastPinddingByBuyerid(buyerid);
            if (O == null)
            {
                O = new BuyerOrderViewModel();
                O.StatusID = 1;
                // O.ShippingDataID = shipping.FirstOrDefault().ID;
                O.InsertDate = DateTime.Now;
                O.IsNeedInspection = false;
                O.IsNeedDelivery = false;
                O.IsPaid = false;
                O.IsSentToAdmin = false;
                O.UserID = buyerid;
                O.StageID = 4;
                //O.ClosedDate = DateTime.Now;
                O.CloseStatus = false;
                O.StatusID = 1;
                O.VatText = CacheManager.Setting.VatText;
                O.VatValue = CacheManager.Setting.VatValue;
                buyerOrderManager.AddOrder(O);

                
            }

            CartItemsManager cartItemsManager = new CartItemsManager();
            //List<CartItemViewModel> currentcart = cartItemsManager.GetByBuyerId(buyerid, false).ToList();
          
            UserCartManager userCartManager = new UserCartManager();
            var usercart = userCartManager.GetByUserId(buyerid);
            List<int> ids = new List<int>();
            foreach (var itm in O.CartItems)
            {
                ids.Add(itm.ID);
                cartItemsManager.Delete(itm.ID);
            }
            foreach (var itm in ids)
            {

                O.CartItems.Remove(O.CartItems.Where(x=>x.ID==itm).FirstOrDefault());
            }
            O.TotalPrice = O.CartItems.Sum(x => x.ProductInformation.UnitPrice * x.Qty);
            buyerOrderManager.UpdateOrder(O);
                //}
                // 
                if (usercart.Count() > 0)
            {
                foreach (UserCartViewModel itm in usercart)
                {

                   if (O.CartItems.Where(x => x.ProductID == itm.ItemId).Count() == 0)
                    {
                        itm.IsPending = false;
                        cartItemsManager.Add(new CartItemViewModel() { AspNetUserId = buyerid, IsBought = false, InsertDate = DateTime.Now, OrderID = O.ID, Qty = itm.Quantity, ProductID = itm.ItemId, ProductInformation = itm.ProductInformation, NeedInspection = false, NeesShipment = false });
                    }
                }
                
                var cart = cartItemsManager.GetByBuyerId(buyerid, false, O.ID);
                //if (O.Currency == null)
                //    O.Currency = Mycurrency;
                O.TotalPrice = O.CartItems.Sum(x => x.Qty * x.ProductInformation.UnitPrice);
                
                buyerOrderManager.UpdateOrder(O);

            }

      
            //Emptycart();
            //  return RedirectToAction("FinishedOrder");



            O = buyerOrderManager.GetLastPinddingByBuyerid(buyerid);



            //var Addresscookevalue = Request.Cookies.Get("Address");
            //var Addressvalue = new AddressModel();
            //ViewBag.BuyerCountryName = "-1";
            //if (Addresscookevalue != null)
            //{
            //    Addressvalue = JsonConvert.DeserializeObject<AddressModel>(StringCipher.Decrypt(Addresscookevalue.Value, "HAWEYA.PS"));
            //    ViewBag.BuyerCountryName = Addressvalue.CountryName;
            //}
            //else
            //{

            //    if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
            //    {
            //        var usermanager = new AspNetUsersManager();
            //        var buyer = usermanager.GetByEmail(User.Identity.Name);
            //        var shippingaddress = new ShippingDataManager().GetbyBuyerIDPrimary(buyer.Id).FirstOrDefault();


            //        if (shippingaddress != null)
            //        {
            //            var buyercountryid = shippingaddress.CountryID ?? Convert.ToInt32(buyer.CountryId);

            //            var buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
            //            ViewBag.BuyerCountryName = buyercountry.name;
            //        }
            //    }
            //}
            if (O.CartItems.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0).Count() > 0)
            {
                double shippingpricetotal = 0;
                //  string shippingcurrency;
                var manager = new CartItemsManager();
                List<CartItemViewModel> newitems = new List<CartItemViewModel>();
                foreach (var itm in O.CartItems.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0))
                {
                    dynamic jsonData = DHLShippingtest(Convert.ToInt32(itm.ProductID), itm.Qty ?? 0).Data;

                    if (jsonData.status == 1 && jsonData.result != null)

                    {
                        dynamic result = JsonConvert.DeserializeObject<dynamic>(jsonData.result);
                        //  var result = jsonData.result;
                        try
                        {
                            // dynamic r = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(result.RateResponse));
                            //  dynamic p = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(r.Provider));
                            var s = result.RateResponse.Provider[0].Service;


                            if (s.Count > 0)
                            {
                                double shippingprice = Convert.ToDouble(s[0].TotalNet.Amount);
                                //  shippingcurrency = result.RateResponse.Provider[0].Service[0].TotalNet.Currency;
                                shippingpricetotal += shippingprice;
                                itm.ShippingValue = shippingprice;
                                //* O.Currency.RateToRiyal
                                newitems.Add(itm);


                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }

                foreach (var itm in newitems)
                {
                    manager.Update(itm);
                }

                ViewBag.shippingvalue = shippingpricetotal;
                ViewBag.shipcurrency = O.Currency != null ? O.Currency.ShortName : "SR";
                O = new BuyerOrderManager().GetByid(O.ID);
            }
            ViewBag.Order = O;

            return View(O);
        }
        public ActionResult FinishedOrder(int OrderID)
        {


            string buyerid = User.Identity.GetUserId();
            var shipping = new ShippingDataManager().GetbyBuyerIDPrimary(buyerid).ToList();

            ViewBag.ShippingItems = shipping;

            var order = new BuyerOrderManager().GetByid(OrderID);
            ViewBag.TotalPrice = order.TotalPrice;



            return View(order);
        }
        public ActionResult PrintOrder(int OrderID)
        {
            string buyerid = User.Identity.GetUserId();
            var shipping = new ShippingDataManager().GetbyBuyerIDPrimary(buyerid).ToList();

            ViewBag.ShippingItems = shipping;

            var order = new BuyerOrderManager().GetByid(OrderID);
            ViewBag.TotalPrice = order.TotalPrice;



            return View(order);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EndOrdershippingDataEdit(ShippingDataViewModel model)
        {
        
                if (ModelState.IsValid)
                {

                    ShippingDataManager SM = new ShippingDataManager();



                    string buyerid = User.Identity.GetUserId();
                    var itmes = new ShippingDataManager().GetbyBuyerID(buyerid).ToList();
                    var shiplist = SM.GetbyBuyerID(buyerid);
                model.UserID = buyerid;
                    if (model.IsPrimary == true && shiplist.Where(x => x.IsPrimary == true && x.ID != model.ID).Count() > 0)
                    {

                        foreach (ShippingDataViewModel s in shiplist)
                        {
                            s.IsPrimary = false;
                            SM.UpdateShipData(s);
                        }
                    SM.UpdateShipData(model);
                }
                    else
                    {
                        SM.UpdateShipData(model);
                    }


                    //db.Entry(videoFromDB).State = EntityState.Detached;
                    //db.Entry(itemdb).State = EntityState.Modified;
                    //db.SaveChanges();
                    BuyerOrderManager OM = new BuyerOrderManager();
                    BuyerOrderViewModel O = OM.GetLastPinddingByBuyerid(buyerid);
                    if (O != null)
                    {
                        O.ShippingDataID = model.ID;
                        O.StageID = 2;
                        OM.UpdateOrder(O);
                    }
                    else
                    {
                        OM.AddOrder(new BuyerOrderViewModel() { ShippingDataID = model.ID, StageID = 2 });
                    }
                    TempData["msg"] = "s: Data Saved Successfully ";
                    return RedirectToAction("BuyerAddresses");
                }

                return View(model);
            

        }

        public ActionResult EndOrdershippingData()
        {
            //if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
            //{
            //    List<CartList> CL = new List<CartList>();
            //    if (Session["CurrentCart"] != null)
            //    {
            //        CL = Session["CurrentCart"] as List<CartList>;
            //    }
            //    ViewBag.CartList = CL;

            //    ViewBag.TotalPrice = CL.Sum(x => x.TotalPrice);
            //}
            //else
            //{
            //    TempData["msg"] = "e: please log in as buyer to continue";
            //}
            return View();
        }

        [Authorize(Roles = "Buyer")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EndOrdershippingData(ShippingDataViewModel model)
        {

            // if (SecurityManager.IsUserInRole(User.Identity.GetUserId(), "Buyer"))
            // {
            if (ModelState.IsValid)
            {
                ShippingDataManager SM = new ShippingDataManager();

                string buyerid = User.Identity.GetUserId();
                var itmes = new ShippingDataManager().GetbyBuyerID(buyerid).ToList();
                var shiplist = SM.GetbyBuyerID(buyerid);
                model.UserID = buyerid;
                if (model.IsPrimary == true && shiplist.Where(x => x.IsPrimary == true && x.ID != model.ID).Count() > 0)
                {

                    foreach (ShippingDataViewModel s in shiplist)
                    {
                        s.IsPrimary = false;
                        SM.UpdateShipData(s);
                    }
                    SM.AddShipingData(model);
                }
                else
                {
                   // model.UserID = buyerid;
                    SM.AddShipingData(model);
                }

                BuyerOrderManager OM = new BuyerOrderManager();
                BuyerOrderViewModel O = OM.GetLastPinddingByBuyerid(buyerid);
                if (O != null)
                {
                    O.ShippingDataID = model.ID;
                    O.StageID = 2;
                    OM.UpdateOrder(O);
                }
                else
                {
                    OM.AddOrder(new BuyerOrderViewModel() { ShippingDataID = model.ID, StageID = 2 });
                }
                TempData["msg"] = "s: Data Saved Successfully ";
                return RedirectToAction("BuyerAddresses");
            }

            return View(model);

        }

        [Authorize(Roles = "Buyer")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EndOrderVisaData(VisaDataViewModel model, string payment_method, int OrderID)
        {

            //HttpPostedFileBase reciptcopy

            if (payment_method == "Visa")
            {
                if (model.Expiryday == null ||
                    model.CCV == null ||
                    string.IsNullOrEmpty(model.CardHolderName) ||
                    model.VisaNO == null)
                {
                    TempData["msg"] = "e:Please Fill All Data";
                    return View();
                }
            }

            string buyerid = User.Identity.GetUserId();
            ViewBag.banklist = new BankAccountsManager().GetActive();


            BuyerOrderManager buyerOrderManager = new BuyerOrderManager();

            BuyerOrderViewModel O = buyerOrderManager.GetByid(OrderID);




            string filename = "";








            try
            {
                O.payment_method = payment_method == "card" ? "Visa:*******" + model.VisaNO.Substring(model.VisaNO.Length - 4, 3) : "Bank Transfer";
            }
            catch { O.payment_method = ""; }


            O.IsSentToAdmin = true;
            O.StatusID = 1;
            O.InsertDate = DateTime.Now;

            new OrderHistoryManager().Add(new OrderHistoryViewModel() { AddedBy = buyerid, OrderID = O.ID, NewStatusID = 1, OldStatusID = 1, InsertDate = DateTime.Now, Notes = "New Order From Customer" });
            buyerOrderManager.UpdateOrder(O);

            ProductInformationManager pmanager = new ProductInformationManager();
            foreach (var itm in O.CartItems)
            {
                itm.ProductInformation.AvailableQuantity = itm.ProductInformation.AvailableQuantity - itm.Qty ?? 0;
                pmanager.Updateqty(itm.ProductInformation);
            }


            Emptycart();

            if (O.CartItems.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0).Count() > 0)
            { 
                var ship = Pickupshipping(O.ID, buyerid);
            O = buyerOrderManager.GetByid(OrderID);
            }


                CartItemsManager cartItemsManager = new CartItemsManager();
            var items = O.CartItems;
                string tableShipping = "";

                tableShipping = "<table><tr><td style='border: 1px solid; padding:0 3px;' >Product Title</td>" +
                @"<td style='border: 1px solid; padding:0 3px;'>Quantity</td>" +
                @"<td style='border: 1px solid; padding:0 3px;'>Unit Price</td>" +
                 @"<td style='border: 1px solid; padding:0 3px;'>NetTotal</td>" +
                 @"<td style='border: 1px solid; padding:0 3px;'>Total&VAT" + vattext + "</td></tr>";
                //@"<td style='border: 1px solid; padding:0 3px;'>Inspection Value</td>" +
                //    @"<td style='border: 1px solid; padding:0 3px;'>shipping Value</td>"+
                //    @"<td style='border: 1px solid; padding:0 3px;'>Total</td>"+
                //    @"<td style='border: 1px solid; padding:0 3px;'>Total&VAT5%</td></tr>";
                foreach (var itm in items.ToList())
                {
                    double total = Convert.ToDouble(itm.ProductInformation.UnitPrice * itm.Qty);
                    double totalwithvat = Convert.ToDouble(total + (total * vatvalue));

                    tableShipping += "<tr><td style='border: 1px solid; padding:0 3px;'>" + itm.ProductInformation.ProductTitle + "</td>" +
                        @"<td style='border: 1px solid; padding:0 3px;'>" + itm.Qty + "</td>" +
                        @"<td style='border: 1px solid; padding:0 3px;'>" + itm.ProductInformation.UnitPrice + "</td>" +
                        @"<td style='border: 1px solid; padding:0 3px;'>" + total + "</td>" +
                        @"<td style='border: 1px solid; padding:0 3px;'>" + totalwithvat + " </td></tr>";
                    cartItemsManager.Update(itm);
                }

                tableShipping += "</table>";
                string bankdata = new BankAccountsManager().BankDatatoEmail();
                double totalbtn = Convert.ToDouble(O.CartItems.Sum(x => x.ProductInformation.UnitPrice * x.Qty) + (O.CartItems.Sum(x => x.ProductInformation.UnitPrice * x.Qty) * vatvalue));
                new MailManager().SendMessage(O.AspNetUser.Email, "Your Request  Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", " We would like to inform you that your order has arrived in our system   <br/> and waiting for the Request  value of Request to complete the process..  <br/> Please check with the company to complete the procedures .<br/> Our Banks Data:" + bankdata + " <br/>Notes:this data maybe change after admin proccess .. we will notify you if that happend.<br/>  Your Request Value is.<br/> " + tableShipping, "Warehouse Integration Company", O.AspNetUser.FullName, totalbtn + "");

           
            return RedirectToAction("FinishedOrder",new { OrderID =O.ID});

        }


        [HttpPost]
        public ActionResult OrderSummaryinspection(FormCollection form)
        {
            string buyerid = User.Identity.GetUserId();
            BuyerOrderManager buyerOrderManager = new BuyerOrderManager();
            BuyerOrderViewModel O = buyerOrderManager.GetLastPinddingByBuyerid(buyerid);
            CartItemsManager c = new CartItemsManager();
            int counterinspection = 0;
            int countershipment = 0;
            foreach (string k in form.AllKeys)
            {
                if (k.Contains("cbxInspect_"))
                {
                    string productid = k.Substring(k.IndexOf("cbxInspect_") + 11, k.Length - 11);
                    var ischecked =form.Get(k);

                    foreach (var itm in O.CartItems)
                    {
                        if (itm.ProductID == int.Parse(productid))
                        {
                            counterinspection++;
                            itm.NeedInspection = true;
                        }
                    }
                }

                if (k.Contains("cbxShipment_"))
                {
                    string productid = k.Substring(k.IndexOf("cbxShipment_") + 12, k.Length - 12);
                    var ischecked = form.Get(k);

                    foreach (var itm in O.CartItems)
                    {
                        if (itm.ProductID == int.Parse(productid))
                        {
                            countershipment++;
                            itm.NeesShipment = true;
                        }
                    }
                }

               

            }
            if (countershipment > 0 || counterinspection > 0)
            {
                foreach (var itm in O.CartItems.ToList())
                {
                    c.Update(itm);
                }
            }
            if (form.AllKeys.FirstOrDefault(x=>x.Contains("DeliveryFrom"))!=null)
            {
                var DeliveryFrom = form.Get(form.AllKeys.FirstOrDefault(x => x.Contains("DeliveryFrom")));
           
                    O.DeliveryType = 2;
                    if (DeliveryFrom == "1")
                    {
                        O.DeliveryType = 1;
                    }
               
            }

            if (countershipment > 0)
            {
                O.IsNeedDelivery = true;
                var shipping = new ShippingDataManager().GetbyBuyerIDPrimary(buyerid).FirstOrDefault();
                if (shipping != null)
                    O.ShippingDataID = shipping.ID;

            }

            if(counterinspection>0)
                O.IsNeedInspection = true;

            if (countershipment > 0 || counterinspection > 0)
                buyerOrderManager.UpdateOrder(O);


           
                var cookie = Request.Cookies.Get("MyCurrency");

                
            

            var List = new CurrencyManager().Get().ToList();
            var maincurrency = List.Where(x => x.IsMain == true).FirstOrDefault();

            if (cookie != null)
            {
                int id = Convert.ToInt32(cookie.Value);
                maincurrency = List.Where(x => x.Id == id).FirstOrDefault();
            }
            O.CurrencyId = maincurrency.Id;
            O.CurrencyRate = maincurrency.RateToRiyal;
            buyerOrderManager.UpdateOrder(O);

          //  if (countershipment > 0)
            


                return RedirectToAction("BuyerAddresses", new { OrderID = O.ID });
            

          //  return RedirectToAction("EndOrderVisaData", new { OrderID = O.ID });

        }
        public void Emptycart()
        {
            int cartCount = 0;
            double totalPrice = 0;



            // user has no valid session, so we need to save information in the cookie.

            var productsValue = Request.Cookies.Get("products");
            var products = new List<CartProductModel>();

            if (productsValue != null)
            {
                products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
            }
          

            products.RemoveRange(0, products.Count());

            UserCartManager userCart = new UserCartManager();
            userCart.DeleteByUserID(User.Identity.GetUserId());

            cartCount = products.Count();


            //  productsValue.Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS");
            var cookie = new System.Web.HttpCookie("products")
            {
                Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS"),
                Expires = DateTime.Now.AddDays(15)
            };


            totalPrice = products.Sum(x => x.Quantity * x.BasePrice);

            Response.Cookies.Add(cookie);

            UpdateCookieCartValues(Response.Cookies, cartCount, totalPrice);
        }

        [HttpPost]
        public JsonResult DeleteItem(int ID)
        {

            //try
            //{
                var item = new CartItemsManager().GetByid(ID);


            if (item != null)
            {
                int ProductID = Convert.ToInt32(item.ProductID);


                int cartCount = 0;
                double totalPrice = 0;
                // {
                var productsValue = Request.Cookies.Get("products");
                var products = new List<CartProductModel>();

                if (productsValue != null)
                {
                    products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));



                    var cookieProduct = products.SingleOrDefault(x => x.ProductId == ProductID);

                    if (cookieProduct != null)
                    {
                        products.Remove(cookieProduct);
                    }


                    productsValue.Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS");

                    totalPrice = products.Sum(x => x.Quantity * x.BasePrice);

                    cartCount = products.Count();
                    Response.Cookies.Add(productsValue);

                    UpdateCookieCartValues(Response.Cookies, cartCount, totalPrice);
                }
                // return Json(new { status = 1, message = "Items have been deleted successfully", count = cartCount, totalprice = totalPrice + (totalPrice * 0.05) }, JsonRequestBehavior.AllowGet);

                //}



                var cartManager = new UserCartManager();

                var userCart = cartManager.GetByUserId(User.Identity.GetUserId());

                var product = userCart.SingleOrDefault(x => x.ItemId == ProductID);

                if (product != null)
                {
                    cartManager.Delete(product.Id);
                    userCart = cartManager.GetByUserId(User.Identity.GetUserId());
                }

                userCart = cartManager.GetByUserId(User.Identity.GetUserId());
                cartCount = userCart.Count();
                totalPrice = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice);



                UpdateCookieCartValues(Response.Cookies, cartCount, totalPrice);

                string buyerid = User.Identity.GetUserId();
                if (new CartItemsManager().GetByid(Convert.ToInt32(ProductID)) != null)
                    new CartItemsManager().Delete(Convert.ToInt32(ProductID));
                BuyerOrderViewModel O = new BuyerOrderManager().GetLastPinddingByBuyerid(buyerid);
                //CartItemsManager c = new CartItemsManager();

                if (O != null)
                {
                    O.TotalPrice = O.CartItems.Sum(x => x.ProductInformation.UnitPrice * x.Qty);
                    new BuyerOrderManager().UpdateOrder(O);
                }


                //  string buyerid = User.Identity.GetUserId();
                //    var item= new CartItemsManager().GetByid(ID);
                new CartItemsManager().Delete(ID);
                // BuyerOrderViewModel O = new BuyerOrderManager().GetLastPinddingByBuyerid(buyerid);

                O.TotalPrice = O.CartItems.Sum(x => x.ProductInformation.UnitPrice * x.Qty);
                new BuyerOrderManager().UpdateOrder(O);

            }
            //}
            //catch { }
            return Json(new { status = 1, message = "Items have been deleted successfully" });
        }




        public JsonResult DHLShipping(int id)
        {

            //var jsonRequest = Json(new { }).ToString();
            //HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("https://wsbexpress.dhl.com/rest/sndpt/RateRequest");
            //client.DefaultRequestHeaders
            //      .Accept
            //      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

            //HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "relativeAddress");

            //request.Content = new StringContent(jsonRequest,
            //                                    Encoding.UTF8,
            //                                    "application/json");//CONTENT-TYPE header

            //client.SendAsync(request)
            //      .ContinueWith(responseTask =>
            //      {
            //          //here your response 
            //          //Debug.WriteLine("Response: {0}", responseTask.Result);
            //          return Json(responseTask.Result, JsonRequestBehavior.AllowGet);
            //      });



            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://wsbexpress.dhl.com/rest/sndpt/RateRequest");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {




                httpWebRequest.Headers.Add("Authorization", "Basic d2luU0E6RSQwcFokOGxHITZ3");
                httpWebRequest.Headers.Add("Cookie", "BIGipServer~WSB~pl_wsb-express-cbj.dhl.com_443=310126791.64288.0000");

                //var client = new RestClient("https://wsbexpress.dhl.com/rest/sndpt/RateRequest");
                //client.Timeout = -1;
                //var request = new RestRequest(Method.POST);
                //request.AddHeader("Content-Type", "application/json");
                //request.AddHeader("Authorization", "Basic d2luU0E6RSQwcFokOGxHITZ3");
                //request.AddHeader("Cookie", "BIGipServer~WSB~pl_wsb-express-cbj.dhl.com_443=310126791.64288.0000");
                //request.AddParameter("application/json", "{\r\n    \"RateRequest\": {\r\n    \"ClientDetails\": null,\r\n    \"RequestedShipment\": {\r\n      \"DropOffType\": \"REGULAR_PICKUP\",\r\n      \"NextBusinessDay\": \"N\",\r\n      \"Ship\": {\r\n        \"Shipper\": {\r\n          \"StreetLines\": \"River House\",\r\n          \"City\": \"Riyadh\",\r\n          \"PostalCode\": \"\",\r\n          \"CountryCode\": \"SA\"\r\n        },\r\n        \"Recipient\": {\r\n          \"StreetLines\": \"Main Street\",\r\n          \"City\": \"Dubai\",\r\n          \"PostalCode\": \"\",\r\n          \"CountryCode\": \"AE\"\r\n        }\r\n      },\r\n      \"Packages\": {\r\n        \"RequestedPackages\": {\r\n          \"@number\": \"1\",\r\n          \"Weight\": {\r\n            \"Value\": 0.5\r\n          },\r\n          \"Dimensions\": {\r\n            \"Length\": 1,\r\n            \"Width\": 1,\r\n            \"Height\": 1\r\n          }\r\n        }\r\n      },\r\n      \"ShipTimestamp\": \"2020-07-16T13:00:00GMT+00:00\",\r\n      \"UnitOfMeasurement\": \"SI\",\r\n      \"Content\": \"NON_DOCUMENTS\",\r\n      \"PaymentInfo\": \"DAP\",\r\n      \"Account\": \"961230867\"\r\n    }\r\n  }\r\n}", ParameterType.RequestBody);
                //IRestResponse response = client.Execute(request);
                //Console.WriteLine(response.Content);



                string json = "{\"RateRequest\": {\"ClientDetails\": null,\"RequestedShipment\":{\"DropOffType\":\"REGULAR_PICKUP\",\"NextBusinessDay\":\"N\",\"Ship\":{\"Shipper\":{\"StreetLines\":\"RiverHouse\",\"City\":\"Riyadh\",\"PostalCode\":\"\",\"CountryCode\":\"SA\"},\"Recipient\":{\"StreetLines\":\"MainStreet\",\"City\":\"Dubai\",\"PostalCode\":\"\",\"CountryCode\":\"AE\"}},\"Packages\":{\"RequestedPackages\":{\"@number\":\"1\",\"Weight\":{\"Value\":0.5},\"Dimensions\":{\"Length\":1,\"Width\":1,\"Height\":1}}},\"ShipTimestamp\":\"2020-07-16T13:00:00GMT+00:00\",\"UnitOfMeasurement\":\"SI\",\"Content\":\"NON_DOCUMENTS\",\"PaymentInfo\":\"DAP\",\"Account\":\"961230867\"}}}";

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                return Json(result, JsonRequestBehavior.AllowGet); ;
            }
            return Json(new {status=1}, JsonRequestBehavior.AllowGet);


        }


        public JsonResult DHLShippingtest(int id,int qty)
        {   int buyercountryid = 0;
            int buyercityid = 0;

            var buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
            var buyercity = CacheManager.Cities.Where(x => x.id == buyercityid).FirstOrDefault();
            string buyerstreet = "";
            string buyerpostal ="";
            string buyercountyname = "";
            string buyercountryshort = "";
            string buyercityname = "";


            var usermanager = new AspNetUsersManager();
            if (!User.Identity.IsAuthenticated && !User.IsInRole("Buyer"))
            {


                var Addresscookevalue = Request.Cookies.Get("Address");
                var Addressvalue = new AddressModel();

                if (Addresscookevalue != null)
                {
                    Addressvalue = JsonConvert.DeserializeObject<AddressModel>(StringCipher.Decrypt(Addresscookevalue.Value, "HAWEYA.PS"));

                    buyercountyname = Addressvalue.CountryName;
                    buyercountryshort = Addressvalue.CountryCode;
                    buyercityname = Addressvalue.City;
                    buyerstreet = Addressvalue.StreetLines;
                    buyerpostal = "";
                    // return Json(new { status = 1, address = Addressvalue }, JsonRequestBehavior.AllowGet);
                }
                else { return Json(new { status = 0, messge = "please login to view shipping price" }, JsonRequestBehavior.AllowGet); }

            }
            else
            {
             
                var buyer = usermanager.GetByEmail(User.Identity.Name);

                var shippingaddress = new ShippingDataManager().GetbyBuyerIDPrimary(buyer.Id).FirstOrDefault();


                // var shippingaddress = buyer.ShippingDatas.Find(x => x.IsPrimary==true);

                if (shippingaddress != null)
                {
                    buyercountryid = shippingaddress.CountryID ?? Convert.ToInt32(buyer.CountryId);
                    buyercityid = shippingaddress.CityID ?? Convert.ToInt32(buyer.CityId);

                    buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
                    buyercountyname = buyercountry.name;
                    buyercountryshort = buyercountry.sortname;
                    buyercity = CacheManager.Cities.Where(x => x.id == buyercityid).FirstOrDefault();
                    buyercityname = buyercity.text;
                    buyerstreet = shippingaddress.FirstAddress ?? shippingaddress.SecondAddress ?? buyercity.text;
                    buyerpostal = shippingaddress.ZipCode;

                }


                else
                {
                    return Json(new { status = 0, messge = "update your shipping data befor view shipping price" }, JsonRequestBehavior.AllowGet);
                }

            }

                var model = new ProductInformationManager().GetByid(id);
            if (model.Width > 0 && model.Height > 0 && model.Length > 0 && model.Weight > 0)
            {
                string userid = model.Username ?? "seller2208@gmail.com";
                var seller = usermanager.GetByEmail(userid);

                var materialaddress = new MaterialPickupLocationManager().GetBysellerid(seller.Id);
                int countryid = materialaddress == null ? Convert.ToInt32(seller.CountryId) : Convert.ToInt32(materialaddress.Country);
                int cityid = materialaddress == null ? Convert.ToInt32(seller.CityId) : Convert.ToInt32(materialaddress.City);

                var country = CacheManager.Countries.Where(x => x.id == countryid).FirstOrDefault();
                var city = CacheManager.Cities.Where(x => x.id == cityid).FirstOrDefault();
                string street = materialaddress == null ? city.text : (materialaddress.Street == null ? city.text : materialaddress.Street);
                string sellerpostal = seller.POBox ?? "";



                //var reagion = CacheManager.r.Where(x => x.id == materialaddress.Country).FirstOrDefault();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://wsbexpress.dhl.com/rest/sndpt/RateRequest");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string username = "winSA";
                    string password = "E$0pZ$8lG!6w";

                    string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                    httpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
                    // httpWebRequest.Headers.Add("Authorization", "Basic d2luU0E6RSQwcFokOGxHITZ3");
                    //httpWebRequest.Headers.Add("Cookie", "BIGipServer~WSB~pl_wsb-express-cbj.dhl.com_443=310126791.64288.0000");


                    string time = DateTime.Now.ToString("yyyy-MM-ddT13:00:00GMT+00:00");
                    string json = "{\"RateRequest\": {\"ClientDetails\": null,\"RequestedShipment\":{\"DropOffType\":\"REGULAR_PICKUP\",\"NextBusinessDay\":\"N\",\"Ship\":{\"Shipper\":{\"StreetLines\":\"" + street + "\",\"City\":\"" + city.text + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + country.sortname + "\"},\"Recipient\":{\"StreetLines\":\"" + buyerstreet + "\",\"City\":\"" + buyercityname + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + buyercountryshort + "\"}},\"Packages\":{\"RequestedPackages\":{\"@number\":\"" + qty + "\",\"Weight\":{\"Value\":" + model.Weight + "},\"Dimensions\":{\"Length\":" + model.Length + ",\"Width\":" + model.Width + ",\"Height\":" + model.Height + "}}},\"ShipTimestamp\":\"" + time + "\",\"UnitOfMeasurement\":\"SI\",\"Content\":\"NON_DOCUMENTS\",\"PaymentInfo\":\"DAP\",\"Account\":\"961230867\"}}}";

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd().Replace("@type", "type");

                    return Json(new { status = 1, result = result }, JsonRequestBehavior.AllowGet); ;
                }

                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
            else { return Json(new { status = 0 }, JsonRequestBehavior.AllowGet); }


        }


        public JsonResult DHLShippingtestCart(int id, int qty)
        {
          //  List<UserCartViewModel> cart = new List<UserCartViewModel>();

           
           
            int buyercountryid = 0;
            int buyercityid = 0;

            var buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
            var buyercity = CacheManager.Cities.Where(x => x.id == buyercityid).FirstOrDefault();
            string buyerstreet = "";
            string buyerpostal = "";
            string buyercountyname = "";
            string buyercountryshort = "";
            string buyercityname = "";


            var usermanager = new AspNetUsersManager();
            if (!User.Identity.IsAuthenticated && !User.IsInRole("Buyer"))
            {
                //var productsValue = Request.Cookies.Get("products");
                //var products = new List<CartProductModel>();

                //if (productsValue != null)
                //{
                //    products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
                //}
                //foreach (var product in products)
                //{
                //    cart.Add(new UserCartViewModel { IsPending = true, ItemId = product.ProductId, Quantity = product.Quantity, ProductInformation = productmanager.GetByid(Convert.ToInt32(product.ProductId)) });
                //}

                var Addresscookevalue = Request.Cookies.Get("Address");
                var Addressvalue = new AddressModel();

                if (Addresscookevalue != null)
                {
                    Addressvalue = JsonConvert.DeserializeObject<AddressModel>(StringCipher.Decrypt(Addresscookevalue.Value, "HAWEYA.PS"));

                    buyercountyname = Addressvalue.CountryName;
                    buyercountryshort = Addressvalue.CountryCode;
                    buyercityname = Addressvalue.City;
                    buyerstreet = Addressvalue.StreetLines;
                    buyerpostal = "";
                    // return Json(new { status = 1, address = Addressvalue }, JsonRequestBehavior.AllowGet);
                }
                else { return Json(new { status = 0, messge = "please login to view shipping price" }, JsonRequestBehavior.AllowGet); }

            }
            else
            {
                //var cartManager = new UserCartManager();
                //var userCart = cartManager.GetByUserId(User.Identity.GetUserId());
                //cart = userCart.ToList();
                var buyer = usermanager.GetByEmail(User.Identity.Name);

                var shippingaddress = new ShippingDataManager().GetbyBuyerIDPrimary(buyer.Id).FirstOrDefault();


                // var shippingaddress = buyer.ShippingDatas.Find(x => x.IsPrimary==true);

                if (shippingaddress != null)
                {
                    buyercountryid = shippingaddress.CountryID ?? Convert.ToInt32(buyer.CountryId);
                    buyercityid = shippingaddress.CityID ?? Convert.ToInt32(buyer.CityId);

                    buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
                    buyercountyname = buyercountry.name;
                    buyercountryshort = buyercountry.sortname;
                    buyercity = CacheManager.Cities.Where(x => x.id == buyercityid).FirstOrDefault();
                    buyercityname = buyercity.text;
                    buyerstreet = shippingaddress.FirstAddress ?? shippingaddress.SecondAddress ?? buyercity.text;
                    buyerpostal = shippingaddress.ZipCode;

                }


                else
                {
                    return Json(new { status = 0, messge = "update your shipping data befor view shipping price" }, JsonRequestBehavior.AllowGet);
                }

            }

          var model = new ProductInformationManager().GetByid(id);
            string userid = model.Username ?? "seller2208@gmail.com";
            var seller = usermanager.GetByEmail(userid);

            var materialaddress = new MaterialPickupLocationManager().GetBysellerid(seller.Id);
            int countryid = materialaddress == null ? Convert.ToInt32(seller.CountryId) : Convert.ToInt32(materialaddress.Country);
            int cityid = materialaddress == null ? Convert.ToInt32(seller.CityId) : Convert.ToInt32(materialaddress.City);

            var country = CacheManager.Countries.Where(x => x.id == countryid).FirstOrDefault();
            var city = CacheManager.Cities.Where(x => x.id == cityid).FirstOrDefault();
            string street = materialaddress == null ? city.text : (materialaddress.Street == null ? city.text : materialaddress.Street);
            string sellerpostal = seller.POBox ?? "";



            //var reagion = CacheManager.r.Where(x => x.id == materialaddress.Country).FirstOrDefault();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://wsbexpress.dhl.com/rest/sndpt/RateRequest");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string username = "winSA";
                string password = "E$0pZ$8lG!6w";

                string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                httpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
                // httpWebRequest.Headers.Add("Authorization", "Basic d2luU0E6RSQwcFokOGxHITZ3");
                //httpWebRequest.Headers.Add("Cookie", "BIGipServer~WSB~pl_wsb-express-cbj.dhl.com_443=310126791.64288.0000");
                string json = "{\"RateRequest\": {\"ClientDetails\": null,\"RequestedShipment\":{\"DropOffType\":\"REGULAR_PICKUP\",\"NextBusinessDay\":\"N\",\"Ship\":{\"Shipper\":{\"StreetLines\":\"" + street + "\",\"City\":\"" + city.text + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + country.sortname + "\"},\"Recipient\":{\"StreetLines\":\"" + buyerstreet + "\",\"City\":\"" + buyercityname + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + buyercountryshort + "\"}},\"Packages\":{\"RequestedPackages\":{\"@number\":\"" + 1 + "\",\"Weight\":{\"Value\":" + model.Weight + "},\"Dimensions\":{\"Length\":" + model.Length + ",\"Width\":" + model.Width + ",\"Height\":" + model.Height + "}}},\"ShipTimestamp\":\"2020-07-16T13:00:00GMT+00:00\",\"UnitOfMeasurement\":\"SI\",\"Content\":\"NON_DOCUMENTS\",\"PaymentInfo\":\"DAP\",\"Account\":\"961230867\"}}}";


                //string json = "{\"RateRequest\": {\"ClientDetails\": null,\"RequestedShipment\":{\"DropOffType\":\"REGULAR_PICKUP\",\"NextBusinessDay\":\"N\",\"Ship\":{\"Shipper\":{\"StreetLines\":\"" + street + "\",\"City\":\"" + city.text + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + country.sortname + "\"},\"Recipient\":{\"StreetLines\":\"" + buyerstreet + "\",\"City\":\"" + buyercityname + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + buyercountryshort + "\"}},\"Packages\":{\"RequestedPackages\":[";
                //int count = 1;
                //foreach(var itm in cart)
                //{

                //    json += "{\"@number\":\"" +count + "\",\"Weight\":{\"Value\":" + itm.ProductInformation.Weight + "},\"Dimensions\":{\"Length\":" + itm.ProductInformation.Length + ",\"Width\":" + itm.ProductInformation.Width + ",\"Height\":" + itm.ProductInformation.Height + "}}";
                //    count++;
                //                }

                //        json+="]},\"ShipTimestamp\":\"2020-07-16T13:00:00GMT+00:00\",\"UnitOfMeasurement\":\"SI\",\"Content\":\"NON_DOCUMENTS\",\"PaymentInfo\":\"DAP\",\"Account\":\"961230867\"}}}";

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                return Json(new { status = 1, result = result }, JsonRequestBehavior.AllowGet); ;
            }

            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);


        }


        //private void UpdateCookieCartValues(HttpCookieCollection cookie, int count, double price)
        //{
        //    cookie.Add(new HttpCookie("cartCount")
        //        Value
        //    { = count.ToString(),
        //        Expires = DateTime.Now.AddDays(15)
        //    });

        //    cookie.Add(new HttpCookie("cartPrice")
        //    {
        //        Value = price.ToString(),
        //        Expires = DateTime.Now.AddDays(15)
        //    });
        //}

        public JsonResult GetManufacturersListHomeajaxauctions(string manufactuertxt)
        {
            var m = StordProcedurManager.ManufacturersHomeDataAuctions().Where(x => x.ManufacturersName.ToLower().Contains(manufactuertxt.ToLower())).Select(x => new
            {
                x.ManufacturersName,
                x.ManufacturersId,
                ProductCount = x.ProductCount
            });

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult checkaddresscooke(AddressModel model)
        {

            var Addresscookevalue = Request.Cookies.Get("Address");
            var Addressvalue= new AddressModel();

            if (Addresscookevalue != null)
            {
                Addressvalue = JsonConvert.DeserializeObject<AddressModel>(StringCipher.Decrypt(Addresscookevalue.Value, "HAWEYA.PS"));
                return Json(new { status = 1, address = Addressvalue }, JsonRequestBehavior.AllowGet);
            }
            else {

                if (model != null)
                {
                    Addressvalue = model;

                     Addresscookevalue = new System.Web.HttpCookie("Address")
                    {
                        Value = StringCipher.Encrypt(JsonConvert.SerializeObject(Addressvalue), "HAWEYA.PS"),
                        Expires = DateTime.Now.AddDays(15)
                    };
                   // Addresscookevalue.Value = StringCipher.Encrypt(JsonConvert.SerializeObject(Addressvalue), "HAWEYA.PS");
                    Response.Cookies.Add(Addresscookevalue);
                    return Json(new { status = 1, address = Addressvalue }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCartjson(int Orderid)
        {

            var order= new BuyerOrderManager().GetByid(Orderid);
          

       
            return Json(order.CartItems.Select(x => new { ItemId=x.ProductID, Quantity=x.Qty }), JsonRequestBehavior.AllowGet);
        }

        public object Pickupshipping(int orderId, string userid="")
        {
            var usermanager = new AspNetUsersManager();
            userid = userid ==""? User.Identity.GetUserId():userid;
            var user = usermanager.GetByUserId(userid);
            var order = new BuyerOrderManager().GetByid(orderId);

            
            var Address = new ShippingDataManager().GetbyBuyerIDPrimary(userid).FirstOrDefault();
            if (Address == null)
                Address = new ShippingDataManager().GetbyBuyerID(userid).FirstOrDefault();
            if (order.CartItems.Count() > 0)
            {

                if (Address != null)
                {

                    int buyercountryid = 0;
                    int buyercityid = 0;

                    var buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
                    var buyercity = CacheManager.Cities.Where(x => x.id == buyercityid).FirstOrDefault();
                    string buyerstreet = "";
                    string buyerpostal = "";
                    string buyercountyname = "";
                    string buyercountryshort = "";
                    string buyercityname = "";

                      
                            buyercountryid = Address.CountryID ?? Convert.ToInt32(user.CountryId);
                            buyercityid = Address.CityID ?? Convert.ToInt32(user.CityId);
                     var buyerstatid = Address.StateID ?? Convert.ToInt32(user.StateId);

                            buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
                            buyercountyname = buyercountry.name;
                            buyercountryshort = buyercountry.sortname;
                    var buyerstat= CacheManager.States.Where(x => x.id == buyerstatid).FirstOrDefault();
                    buyercity = CacheManager.Cities.Where(x => x.id == buyercityid).FirstOrDefault();
                            buyercityname = buyercity.text;
                            buyerstreet = Address.FirstAddress ?? Address.SecondAddress ?? buyercity.text;
                            buyerpostal = Address.ZipCode;

                        



                    

                

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://wsbexpress.dhl.com/rest/sndpt/ShipmentRequest");
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string username = "winSA";
                        string password = "E$0pZ$8lG!6w";

                        string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                        httpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
                        var model = new ProductInformationManager().GetByid(Convert.ToInt32(order.CartItems.FirstOrDefault().ProductID));
                        string usersellerid = model.Username ?? "seller2208@gmail.com";
                        var seller = usermanager.GetByEmail(usersellerid);

                        var materialaddress = new MaterialPickupLocationManager().GetBysellerid(seller.Id);
                        int countryid = materialaddress == null ? Convert.ToInt32(seller.CountryId) : Convert.ToInt32(materialaddress.Country);
                        int cityid = materialaddress == null ? Convert.ToInt32(seller.CityId) : Convert.ToInt32(materialaddress.City);

                        var country = CacheManager.Countries.Where(x => x.id == countryid).FirstOrDefault();
                        var city = CacheManager.Cities.Where(x => x.id == cityid).FirstOrDefault();
                        string street = materialaddress == null ? city.text : (materialaddress.Street == null ? city.text : materialaddress.Street);
                        string sellerpostal = seller.POBox ?? "";
                        string time = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd")+ "T12:30:47GMT+02:00";

                        string requststring = "{\"ShipmentRequest\":{\"RequestedShipment\":{\"ShipmentInfo\":{\"DropOffType\":\"REGULAR_PICKUP\",\"ServiceType\":\"P\",\"Account\":961230867,\"Currency\":\"SAR\",\"UnitOfMeasurement\":\"SI\"},\"ShipTimestamp\":\"" + time + "\",\"PaymentInfo\":\"DAP\",\"InternationalDetail\":{\"Commodities\":";
                        requststring+= "{\"NumberOfPieces\":" + order.CartItems.Count() + ",\"Description\":\"WIN Shipping By DHL\",\"CountryOfManufacture\":\"" + order.CartItems.FirstOrDefault().ProductInformation.OBSManufacturer.ManufacturersName + "\",\"Quantity\":" + order.CartItems.Sum(x=>x.Qty) + ",\"UnitPrice\":" + order.CartItems.Sum(x=>x.ProductInformation.UnitPrice) + ",\"CustomsValue\":10},\"Content\":\"NON_DOCUMENTS\"},\"Ship\":{\"Shipper\":{\"Contact\":{\"PersonName\":\"" + seller.FullName + "\",\"CompanyName\":\"" + "WIN" + "\",\"PhoneNumber\":" + seller.PhoneNumber + ",\"EmailAddress\":\"" + seller.Email + "\"},\"Address\":{\"StreetLines\":\"" + street + "\",\"City\":\"" + city.text + " \",\"PostalCode\":" + sellerpostal + ",\"CountryCode\":\"" + country.sortname + "\"}},\"Recipient\":{\"Contact\":{\"PersonName\":\"" + user.FullName + "\",\"CompanyName\":\"WIN\",\"PhoneNumber\":" + user.PhoneNumber + ",\"EmailAddress\":\"" + user.Email + "\"},\"Address\":{\"StreetLines\":\"" + buyerstreet + "\",\"City\":\"" + buyercityname + "\",\"StateOrProvinceCode\":\"\",\"PostalCode\":" + buyerpostal + ",\"CountryCode\":\"" + buyercountryshort + "\"}}},\"Packages\":{\"RequestedPackages\":[";
                        int counter = 1;
                        string items = "";
                        foreach (var item in order.CartItems.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0))
                    {



                             items+= "{\"@number\":\"" + counter + "\",\"Weight\":" + item.ProductInformation.Weight + ",\"Dimensions\":{\"Length\":" + item.ProductInformation.Length + ",\"Width\":" + item.ProductInformation.Width + ",\"Height\":" + item.ProductInformation.Height + "},\"CustomerReferences\":\"Piece 1\"}";
                            counter++;
                    }
                        requststring += items;
                        requststring+= "]}}}}";
                           // string json = "{\"RateRequest\": {\"ClientDetails\": null,\"RequestedShipment\":{\"DropOffType\":\"REGULAR_PICKUP\",\"NextBusinessDay\":\"N\",\"Ship\":{\"Shipper\":{\"StreetLines\":\"" + street + "\",\"City\":\"" + city.text + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + country.sortname + "\"},\"Recipient\":{\"StreetLines\":\"" + buyerstreet + "\",\"City\":\"" + buyercityname + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + buyercountryshort + "\"}},\"Packages\":{\"RequestedPackages\":{\"@number\":\"" + qty + "\",\"Weight\":{\"Value\":" + model.Weight + "},\"Dimensions\":{\"Length\":" + model.Length + ",\"Width\":" + model.Width + ",\"Height\":" + model.Height + "}}},\"ShipTimestamp\":\"" + time + "\",\"UnitOfMeasurement\":\"SI\",\"Content\":\"NON_DOCUMENTS\",\"PaymentInfo\":\"DAP\",\"Account\":\"961230867\"}}}";

                        streamWriter.Write(requststring);
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd().Replace("@code", "code");
                        dynamic jsonData = JsonConvert.DeserializeObject<dynamic>(result).ShipmentResponse;

                        if (jsonData.Notification[0].code== 0&& jsonData.Notification[0].Message == null)
                        {
                            var trakingNO = jsonData.PackagesResult.PackageResult;
                            var LabelImage = jsonData.LabelImage[0];
                            var ShipmentIdentificationNumber = jsonData.ShipmentIdentificationNumber;
                            string TrakingNOs = "";
                            foreach (var itm in trakingNO)
                            {
                                TrakingNOs += itm.TrackingNumber + ",";
                            }
                            TrakingNOs= TrakingNOs.Length>0?TrakingNOs.Remove(TrakingNOs.LastIndexOf(','), 1):"";


                            var GraphicImage = Convert.ToString(LabelImage.GraphicImage);
                            var LabelImageFormat = LabelImage.LabelImageFormat;

                            byte[] bytes = Convert.FromBase64String(GraphicImage);

                            var path = Server.MapPath("~\\Content\\UploadData\\OrdersShipment");
                            if (!string.IsNullOrEmpty(order.AspNetUser.Id))
                            {

                                path = Server.MapPath("~\\Content\\UploadData\\OrdersShipment\\" + order.AspNetUser.Id);
                                if (!System.IO.Directory.Exists(path))
                                {
                                    System.IO.Directory.CreateDirectory(path);
                                }

                            }
                            string fname = order.ID + "_" + ShipmentIdentificationNumber + ".pdf";
                            //// Get the complete folder path and store the file inside it.  
                            var filePath = Path.Combine(path, fname);
                            
                            

                           System.IO.File.WriteAllBytes(filePath, bytes);
                           
                           
                            order.LabelImage = fname;
                            order.ShipmentIdentificationNumber = ShipmentIdentificationNumber;
                            order.TrakingNOs = TrakingNOs;
                            new BuyerOrderManager().UpdateOrder(order);
                        }

                        return Json(new { status = 1, result = result }, JsonRequestBehavior.AllowGet); 
                    }

                    return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);


                }
            }

            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }

    }
}