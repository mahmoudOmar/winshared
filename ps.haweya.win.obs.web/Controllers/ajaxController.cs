﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Controllers
{
    public class ajaxController : Controller
    {
        // GET: ajax
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductListAjax(int? id, string q, bool? AuctionCheck, bool? DirectCheck, string allVal, string amount, int? sorting, int? viewcount, string condetion = "", int page = 1, string currencysign = "SR", double currencyrate = 1)
        {
            ViewBag.query = q;
            viewcount = viewcount ?? 9;
            ProductDetailsManager manager = new ProductDetailsManager();
            var items = manager.GetProductajaxhome(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount, condetion, page - 1, currencysign, currencyrate);
            var UserFavorite = new List<long>();
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
                UserFavorite = new FavoriteManager().GetFavoriteIdsByUserID(User.Identity.GetUserId());
            var allcount = manager.GetProductajaxhomecount(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount, condetion, page, currencysign);
            //  var mapitems =manager.GetProductResultmap(id, q, AuctionCheck, DirectCheck, allVal, amount, sorting, viewcount, condetion);


            var pagemodel = new PagingViewModel();
            pagemodel.PageIndex = page;
            pagemodel.PageSize = viewcount ?? 9;
            pagemodel.RecordsCount = allcount;
            var data = new { items, UserFavorite, pagemodel };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}