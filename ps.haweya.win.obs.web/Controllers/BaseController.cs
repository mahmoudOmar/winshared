﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System.Configuration;
namespace ps.haweya.win.obs.web.Controllers
{
    public class BaseController : Controller
    {
        double vatvalue = CacheManager.Setting.VatValue ?? 0;
        string vattext = CacheManager.Setting.VatText;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            base.OnActionExecuting(filterContext);
            TempData["URL"] = "#";
            if (User.Identity.IsAuthenticated)
            {
                var aspid = User.Identity.GetUserId();
                var aspnetuser = new AspNetUsersManager().GetByUserId(aspid);
                if (aspnetuser.LockoutEnabled == true)
                    TempData["URL"] = "/" + ((ClaimsIdentity)User.Identity).Claims.SingleOrDefault(c => c.Type == "DEFAULT_ROLE").Value + "/Home/Index";
                else if (aspnetuser.EmailConfirmed == false)
                {
                    TempData["UserId"] = aspnetuser.Id;
                    TempData["URL"] = "/" + "Account/" + "Verify";
                }
                else if (aspnetuser.AspNetRoles.Where(x => x.Name.ToLower().Equals("seller")).Count() > 0 && aspnetuser.EmailConfirmed == true)
                {
                    TempData["URL"] = "/" + "SellerSite/" + "RegistrationOverView";
                }
              

            }
            // that's all
         

            var List= new CurrencyManager().Get().ToList();
            var maincurrency = List.Where(x => x.IsMain == true).FirstOrDefault();
            var cookiecurrencyvalue = GetCookieValueByKey("MyCurrency");
            var cookiecurency = maincurrency;
            TempData["CurrencyList"] = List;
            TempData["maincurrency"] = maincurrency;
            if (cookiecurrencyvalue != null&&cookiecurrencyvalue!=0)
            {
                int id = Convert.ToInt32(cookiecurrencyvalue);
                TempData["cookiecurrency"] = cookiecurency = List.Where(x=>x.Id==id).FirstOrDefault();
            }
            TempData["cartCount"] = GetCookieValueByKey("cartCount");
            TempData["cartPrice"] = string.Format("{0:n}{1}",(GetCookieValueByKey("cartPrice") == 0 ? 0 : (GetCookieValueByKey("cartPrice")/ cookiecurency.RateToRiyal)), cookiecurency.Sign);

            //List<NewsLayout> NL = new List<NewsLayout>();
            // foreach (var itm in newslist)
            //{ NewsLayout nl1 = new NewsLayout();
            //    nl1.Id = itm.Id;
            //    nl1.Title = itm.Title;
            //    nl1.AddOn = itm.AddOn;
            //    nl1.Brief = itm.Brief;

            //    NL.Add(nl1);

            //}

            TempData["menuLinks"]=CacheManager.MenuItems;
            TempData["Setting"] = CacheManager.Setting;
            TempData["Version"] = "sdsdsssasasasas";
            var currentAction = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();
            var currentcontroler = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
         var link=new LinkManager().GetByActiontoseo(currentcontroler,currentAction);
            TempData["Seo_Likes"] = link.FirstOrDefault();
            //Regex.Replace(itm.Details, "<.*?>", String.Empty)
        }

        private double GetCookieValueByKey (string Key)
        {
            var cookie = Request.Cookies.Get(Key);

            if (cookie == null) return 0;

            return double.Parse(cookie.Value);
        }

        public void UpdateCookieCartValues(HttpCookieCollection cookie, int count, double price)
        {
            price =price + (price * vatvalue);
            cookie.Add(new HttpCookie("cartCount")
            {
                Value = count.ToString(),
                Expires = DateTime.Now.AddDays(15)
            });

            cookie.Add(new HttpCookie("cartPrice")
            {
               
            Value =Math.Ceiling(price).ToString(),
                Expires = DateTime.Now.AddDays(15)
            });
        }

        public void ResetCartValues()
        {
      
            Request.Cookies.Remove("cartCount");
            Request.Cookies.Remove("cartPrice");
            Request.Cookies.Remove("products");
           
        }

    }
   
}