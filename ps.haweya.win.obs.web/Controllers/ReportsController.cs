﻿using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ps.haweya.win.obs.business.Managers.OBSJobSchedulerManager;

namespace ps.haweya.win.obs.web.Controllers
{
    public class ReportsController : Controller
    {
        //public ActionResult SendStatusReport(int id)
        //{
        //    new ReportDeliveryManager().SendStatusReport(id);
        //    return View();
        //}
        public ActionResult BuyerReport(int id)
        {
            new ReportDeliveryManager().SendBuyerPerfrencesNewsletter(id);
            return View();
        }
        public ActionResult ClosingBids()
        {
            new ReportDeliveryManager().SendClosingBids();
            return View();
        }
        public ActionResult BidClosedReport(int id)
        {

            return View();
        }
    }
}