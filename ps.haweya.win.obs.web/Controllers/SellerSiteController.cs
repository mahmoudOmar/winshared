﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.security;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Controllers
{
    public class SellerSiteController : BaseController
    {
        // GET: Seller


        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SellerDocumentID()
        {

            return View();

        }

        public ActionResult MaterialPickupLocation()
        {


            string aspuserid = User.Identity.GetUserId();
            var seller = new  SecurityManager().GetByUserId(aspuserid);
            ViewBag.Country = seller.CountryId;
            ViewBag.City = seller.CityId;
            ViewBag.State = seller.StateId;
            MaterialPickupLocationSellerViewModel ml = new MaterialPickupLocationManager().GetBysellerid(seller.Id);

            if (ml == null)
            {
                 ml = new MaterialPickupLocationSellerViewModel();
                ml.State = Convert.ToInt32(seller.StateId);
                ml.Country = Convert.ToInt32(seller.CountryId);
                ml.City = Convert.ToInt32(seller.CityId);


            }

            return View(ml);
        }

        public ActionResult SellerVendorName()
        {



            string aspuserid = User.Identity.GetUserId();
            var seller = new SellerManager().GetByUserId(aspuserid);
            //string aspuserid = User.Identity.GetUserId();
            //var seller = new SecurityManager().GetByUserId(aspuserid);
            VendorNameSellerViewModel s = new VendorNameSellerViewModel();
            OBSVendorDetailViewModel ov = new OBSVendorDetailManager().GetBysellerid(aspuserid);
            if (ov != null)
            {
                s.Name = ov.VendorName;
            }
            return View(s);


        }


        public ActionResult RegistrationOverView()
        {

            string aspuserid = User.Identity.GetUserId();
            var seller = new SellerManager().GetByUserId(aspuserid);
            var material = new MaterialPickupLocationManager().GetBysellerid(aspuserid);
            var vendorname = new OBSVendorDetailManager().GetBysellerid(aspuserid);
            ViewBag.Contact = "COMPLETED";
            ViewBag.Verfication = seller.DocumentID != null  ? "COMPLETED" : "START";
            ViewBag.Material = material != null ? "COMPLETED" : "START";
            ViewBag.SellrDetails = vendorname != null ? "COMPLETED" : "START";
            ViewBag.Link = material == null || vendorname == null ? "IncompleteMSG" : "WellcomeSeller";
            return View();


        }
        public ActionResult IncompleteMSG()
        {
            return View();
        }
        public ActionResult WellcomeSeller()
        {
            return View();
        }

        public ActionResult MyAccount()
        {
            string aspnetid = User.Identity.GetUserId();

            ApplicationUser u = new SecurityManager().UserManager.FindById(aspnetid);
          RegisterViewModel   s = new RegisterViewModel() { Email = u.Email, Name = u.FullName, Counrty = Convert.ToInt32(u.CountryId), State = Convert.ToInt32(u.StateId), City = Convert.ToInt32(u.CityId), POBox = u.POBox,  Mobile = u.PhoneNumber };

            return View(s);

            //string aspnetuser = User.Identity.GetUserId();
            //var seller =new SellerManager().GetByUserId(aspnetuser);

            //if (seller == null)
            //{
            //    return HttpNotFound();
            //}
            //if (string.IsNullOrEmpty(seller.Email))
            //    seller.Email = User.Identity.GetUserName();
            //return View(seller);

        }

        // POST: Sellers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        public ActionResult SellerDocumentID(SellerAccountVerification reg, HttpPostedFileBase Doc)
        {
            if (Doc == null)
            {
                TempData["msg"] = "e:Please Uploud your Document id";
                return View();
            }
            if (!Doc.ContentType.Contains("image") && !Doc.ContentType.Contains("pdf"))
            {
                TempData["msg"] = "e: Please Select Image or Pdf file";
                return View(reg);

            }
            //اسم عشوائي جديد لصورة مع الامتداد تاعها
            reg.DocumentID = Guid.NewGuid() + System.IO.Path.GetExtension(Doc.FileName);
            //تم حفظ الصورة بالاسم الجديد بمجلد الاوريجنال
            Doc.SaveAs(Server.MapPath("/Content/DocFiles/" + reg.DocumentID));


            if (reg.Typeid == 2 && reg.BusinessTypeid == null)
            {
                //AddErrors()
                ModelState.AddModelError("BusinessTypeid", "Please select Business  Account Type");
            }

            if (reg.Typeid == 1 && reg.BusinessTypeid == null)
            {
                //AddErrors()
                ModelState.AddModelError("BusinessTypeid", "Please select Individual  Account Type");
            }
            else
            {
                ModelState.Remove("BusinessTypeid");
            }
            if (ModelState.IsValid)
            {
                string aspnetuser = User.Identity.GetUserId();
                new SellerManager().updateVerfication(reg, aspnetuser);
                TempData["msg"] = "s:Data Saved Successfully wait admin approved";
                return RedirectToAction("MaterialPickupLocation");
            }
            return View(reg);

        }
        [HttpPost]
        public ActionResult MaterialPickupLocation(MaterialPickupLocationSellerViewModel model)
        {
            //string aspuserid = User.Identity.GetUserId();
            //var seller = new SellerManager().GetByUserId(aspuserid);
            string aspuserid = User.Identity.GetUserId();
            var seller = new SecurityManager().GetByUserId(aspuserid);
            ViewBag.Country = seller.CountryId;
            ViewBag.City = seller.CityId;
            ViewBag.State = seller.StateId;
            MaterialPickupLocationManager materialPickupLocationManager = new MaterialPickupLocationManager();
            MaterialPickupLocationSellerViewModel ml = materialPickupLocationManager.GetBysellerid(aspuserid);
            model.UserId = aspuserid;
            if (ml != null)
            {
                ml.Area = model.Area;
                ml.BuildingNo = model.BuildingNo;
                ml.City = model.City;
                ml.Country = model.Country;
                ml.LandLine = model.LandLine;
                ml.lat = model.lat;
                ml.lon = model.lon;
                ml.Mobile = model.Mobile;
                ml.State = model.State;
                ml.Street = model.Street;
                materialPickupLocationManager.Update(ml);
            }
            materialPickupLocationManager.Add(model);
            return RedirectToAction("SellerVendorName");
        }

        [HttpPost]
        public ActionResult SellerVendorName(VendorNameSellerViewModel model)
        {
            //string aspuserid = User.Identity.GetUserId();
            //var seller = new SellerManager().GetByUserId(aspuserid);
            string aspuserid = User.Identity.GetUserId();
            var seller = new SecurityManager().GetByUserId(aspuserid);
            var Vendormanager = new OBSVendorDetailManager();
           // VendorNameSellerViewModel s = new VendorNameSellerViewModel();
            OBSVendorDetailViewModel ov = Vendormanager.GetBysellerid(aspuserid);
            if (ov != null)
            {
                ov.VendorName = model.Name;
                Vendormanager.Update(ov);
                return RedirectToAction("RegistrationOverView");
            }
            ov = new OBSVendorDetailViewModel();
            ov.VendorName = model.Name;
            ov.AddedOn = DateTime.Now;
            ov.UserId = aspuserid;
            Vendormanager.Add(ov);
            return RedirectToAction("RegistrationOverView");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MyAccount(RegisterViewModel seller, HttpPostedFileBase img)
        {
          
            seller.Agreepolicies = true;
            seller.Password = "pass123456";
            seller.ConfirmPassword= "pass123456";
            if (ModelState.IsValid)
            {
                if (img != null)
                {
                    if (img.ContentType.Contains("image"))
                    {
                        //اسم عشوائي جديد لصورة مع الامتداد تاعها
                     //   seller.ProfilePhoto = Guid.NewGuid() + System.IO.Path.GetExtension(img.FileName);
                        //تم حفظ الصورة بالاسم الجديد بمجلد الاوريجنال
                       // img.SaveAs(Server.MapPath("/Content/DocFiles/" + seller.ProfilePhoto));
                    }

                }

                SecurityManager securityManager = new SecurityManager();
                string result = securityManager.UpdateSeller(seller,User.Identity.GetUserId());


                Guid guid = Guid.Empty;
                Guid.TryParse(result, out guid);


                if (guid == Guid.Empty)
                {
                    ModelState.AddModelError("type", "Please Fill All Data");
                    return View(seller);
                }
                //BuyerManager buyerManager= new BuyerManager();
                //BuyerViewModel itmdb = buyerManager.GetByid(buyer.ASPNetUserID);
                //itmdb.Name = buyer.Name;
                //itmdb.POBox = buyer.POBox;
                //itmdb.Country = buyer.Country;
                //itmdb.City = buyer.City;
                //itmdb.State = buyer.State;
                //itmdb.Mobile = buyer.Mobile;
                //itmdb.Email = buyer.Email;

                TempData["msg"] = "s: Data Updated Successfully";
            }

            return View(seller);


        }




    }
}