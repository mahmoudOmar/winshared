﻿using Newtonsoft.Json;
using ps.haweya.win.obs.web.Models;
using ps.haweya.win.obs.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.viewModels;
using ps.haweya.win.obs.security;
using ps.haweya.win.obs.viewModels.CustomModel;
using ps.haweya.win.obs.business;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;

namespace ps.haweya.win.obs.web.Controllers
{
    public class CartController : BaseController
    {
        double vatvalue = CacheManager.Setting.VatValue ?? 0;
        string vattext = CacheManager.Setting.VatText;
        public ActionResult Index()
        {
            int cartCount = 0;
            double totalPrice = 0;

            List<UserCartViewModel> cart = new List<UserCartViewModel>();
     ViewBag.BuyerCountryName = "-1";
            if (User.Identity.IsAuthenticated)
            {
                // fetch from database
                var cartManager = new UserCartManager();
                var userCart = cartManager.GetByUserId(User.Identity.GetUserId());
                cartCount = userCart.Count();
                totalPrice = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice);

                
                cart = userCart.ToList();

                TempData["cartCount"] = cartCount;
                TempData["totalPrice"] = totalPrice;


                if (User.IsInRole("Buyer"))
                {
                    var usermanager = new AspNetUsersManager();
                    var buyer = usermanager.GetByEmail(User.Identity.Name);
                    var shippingaddress = new ShippingDataManager().GetbyBuyerIDPrimary(buyer.Id).FirstOrDefault();


                    // var shippingaddress = buyer.ShippingDatas.Find(x => x.IsPrimary==true);

                    if (shippingaddress != null)
                    {
                        var buyercountryid = shippingaddress.CountryID ?? Convert.ToInt32(buyer.CountryId);

                        var buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
                        ViewBag.BuyerCountryName = buyercountry.name;
                    }
                }
                if (cart.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0).Count() > 0)
                {
                    double shippingpricetotal = 0;
                    var manager = new CartItemsManager();
                    List<CartItemViewModel> newitems = new List<CartItemViewModel>();
                    foreach (var itm in cart.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0))
                    {
                        dynamic jsonData = DHLShippingtest(Convert.ToInt32(itm.ItemId), itm.Quantity).Data;

                        if (jsonData.status == 1 && jsonData.result != null)

                        {
                            dynamic result = JsonConvert.DeserializeObject<dynamic>(jsonData.result);
                          
                            try
                            {
                            
                                var s = result.RateResponse.Provider[0].Service;


                                if (s.Count > 0)
                                {
                                    double shippingprice = Convert.ToDouble(s[0].TotalNet.Amount);
                                    
                                    shippingpricetotal += shippingprice;
                                   

                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }

                    ViewBag.shippingvalue = shippingpricetotal;
                }
                return View(cart);
            }
            var Addresscookevalue = Request.Cookies.Get("Address");
            var Addressvalue = new AddressModel();
       
            if (Addresscookevalue != null)
            {
                Addressvalue = JsonConvert.DeserializeObject<AddressModel>(StringCipher.Decrypt(Addresscookevalue.Value, "HAWEYA.PS"));
                ViewBag.BuyerCountryName = Addressvalue.CountryName;
            }

            var productsValue = Request.Cookies.Get("products");
            var products = new List<CartProductModel>();

            if (productsValue != null)
            {
                products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
            }

            cartCount = products.Count();
            totalPrice = products.Sum(x => x.Quantity * x.BasePrice);
            
            TempData["cartCount"] = cartCount;
            TempData["totalPrice"] = totalPrice;
            ProductInformationManager productmanager = new ProductInformationManager();

            foreach (var product in products)
            {
                cart.Add(new UserCartViewModel { IsPending = true, ItemId = product.ProductId, Quantity = product.Quantity, ProductInformation = productmanager.GetByid(Convert.ToInt32(product.ProductId)) });
            }

            // get from cookie.

            UpdateCookieCartValues(Response.Cookies, cartCount, totalPrice);

            if (cart.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0).Count() > 0)
            {
                double shippingpricetotal = 0;
                var manager = new CartItemsManager();
                List<CartItemViewModel> newitems = new List<CartItemViewModel>();
                foreach (var itm in cart.Where(x => x.ProductInformation.Width > 0 && x.ProductInformation.Height > 0 && x.ProductInformation.Length > 0 && x.ProductInformation.Weight > 0))
                {
                    dynamic jsonData = DHLShippingtest(Convert.ToInt32(itm.ItemId), itm.Quantity).Data;

                    if (jsonData.status == 1 && jsonData.result != null)

                    {
                        dynamic result = JsonConvert.DeserializeObject<dynamic>(jsonData.result);

                        try
                        {

                            var s = result.RateResponse.Provider[0].Service;


                            if (s.Count > 0)
                            {
                                double shippingprice = Convert.ToDouble(s[0].TotalNet.Amount);

                                shippingpricetotal += shippingprice;


                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }

                ViewBag.shippingvalue = shippingpricetotal;
            }





            return View(cart.Where(x => x.ProductInformation != null));
        }
       // [HttpPost]
        public JsonResult DeleteCartItem(long ProductID)
        {
            int cartCount = 0;
            double totalPrice = 0;
            var pm = new ProductInformationManager();
             var orginalproduct = pm.GetByid(Convert.ToInt32(ProductID));
            if (!User.Identity.IsAuthenticated)
            {
                var productsValue = Request.Cookies.Get("products");
                var products = new List<CartProductModel>();

                if (productsValue != null)
                {
                    products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
                }


                var cookieProduct = products.SingleOrDefault(x => x.ProductId == ProductID);

                orginalproduct.AvailableQuantity = orginalproduct.AvailableQuantity + cookieProduct.Quantity;
                pm.UpdateFull(orginalproduct);
                if (cookieProduct != null)
                {
                    products.Remove(cookieProduct);
                }
                

                productsValue.Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS");

                totalPrice = products.Sum(x => x.Quantity * x.BasePrice);
                
                cartCount = products.Count();
                Response.Cookies.Add(productsValue);

                UpdateCookieCartValues(Response.Cookies, cartCount, totalPrice);

                return Json(new { status = 1, message = "Items have been deleted successfully", count = cartCount, totalprice = totalPrice+(totalPrice*vatvalue) },JsonRequestBehavior.AllowGet);

            }

            var cartManager = new UserCartManager();

            var userCart = cartManager.GetByUserId(User.Identity.GetUserId());

            var product = userCart.SingleOrDefault(x => x.ItemId == ProductID);

            if (product != null)
            {
                orginalproduct.AvailableQuantity = orginalproduct.AvailableQuantity + product.Quantity;
                pm.UpdateFull(orginalproduct);
                cartManager.Delete(product.Id);
                userCart = cartManager.GetByUserId(User.Identity.GetUserId());
            }

            userCart = cartManager.GetByUserId(User.Identity.GetUserId());
            cartCount = userCart.Count();
            totalPrice = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice) ;
            


            UpdateCookieCartValues(Response.Cookies, cartCount, totalPrice);

            string buyerid = User.Identity.GetUserId();
            if(new CartItemsManager().GetByid(Convert.ToInt32(ProductID)) !=null)
            new CartItemsManager().Delete(Convert.ToInt32(ProductID));
            BuyerOrderViewModel O = new BuyerOrderManager().GetLastPinddingByBuyerid(buyerid);
            //CartItemsManager c = new CartItemsManager();

            if (O != null)
            {
                O.TotalPrice = O.CartItems.Sum(x => x.ProductInformation.UnitPrice * x.Qty);
                new BuyerOrderManager().UpdateOrder(O);
            }

            return Json(new { status = 1, message = "Items have been added successfully", count = cartCount, totalprice = totalPrice },JsonRequestBehavior.AllowGet);
        }

       
        public JsonResult Add(long ItemId, int Quantity, double UnitPrice)
        {

            var List = new CurrencyManager().Get().ToList();
            var maincurrency = List.Where(x => x.IsMain == true).FirstOrDefault();
            var cookiecurrencyvalue = GetCookieValueByKey("MyCurrency");
            var cookiecurency = maincurrency;
            TempData["CurrencyList"] = List;
            TempData["maincurrency"] = maincurrency;
            if (cookiecurrencyvalue != null&& cookiecurrencyvalue!=0)
            {
                int id = Convert.ToInt32(cookiecurrencyvalue);
                TempData["cookiecurrency"] = cookiecurency = List.Where(x => x.Id == id).FirstOrDefault();
            }

            int cartCount = 0;
            double totalPrice = 0;

            if (Quantity <= 0) throw new BusinessException("Quantity must be greater than 0");
           var  PManager = new ProductInformationManager();
            var productq = PManager.GetByid(ItemId);
            if (productq.AvailableQuantity < Quantity)
            {
                return Json(new { status = 0, message = "e:there is no Available Quantity", count = cartCount, totalprice = string.Format("{0:0.00}", totalPrice + (totalPrice *vatvalue)) }, JsonRequestBehavior.AllowGet);
               // throw new BusinessException("there is no Available Quantity");
            }
            else
            {
                productq.AvailableQuantity = productq.AvailableQuantity - Quantity;
                PManager.UpdateFull(productq);
            }
            // user has no valid session, so we need to save information in the cookie.
            if (!User.Identity.IsAuthenticated)
            {
                var productsValue = Request.Cookies.Get("products");

                var products = new List<CartProductModel>();

                if (productsValue != null)
                {
                    products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
                }
                //foreach (var itm in products)
                //{
                //    if (itm.ProductId == 0)
                //    {
                //        products.Remove(itm);

                //    }
                //}



                cartCount = products.Count();

                var cookieProduct = products.SingleOrDefault(x => x.ProductId == ItemId);

                if (cookieProduct == null)
                {
                    products.Add(new CartProductModel() { ProductId = ItemId, Quantity = Quantity, BasePrice = UnitPrice });
                }
                else
                {
                    // increment quantity by 1
                    cookieProduct.Quantity = Quantity;
                    products[products.FindIndex(x => x.ProductId == ItemId)] = cookieProduct;
                }

                //  productsValue.Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS");
                var cookie = new HttpCookie("products")
                {
                    Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS"),
                    Expires = DateTime.Now.AddDays(15)
                };
                cartCount = products.Count();

                totalPrice = products.Sum(x => x.Quantity * x.BasePrice) ;

                Response.Cookies.Add(cookie);

                UpdateCookieCartValues(Response.Cookies, cartCount, totalPrice);

                return Json(new { status = 1, message = "Items have been added successfully", count = cartCount, totalprice = string.Format("{0:0.00}", totalPrice + (totalPrice * vatvalue)) },JsonRequestBehavior.AllowGet);

            }

            // user has valid session
            var cartManager = new UserCartManager();

            var userCart = cartManager.GetByUserId(User.Identity.GetUserId());

            var product = userCart.SingleOrDefault(x => x.ItemId == ItemId);

            if (product == null)
            {

                cartManager.Add(new UserCartViewModel()
                {
                    ItemId = ItemId,
                    Quantity = Quantity,
                    IsPending = true,
                    LastModifiedAt = DateTime.Now,
                    UserId = User.Identity.GetUserId()
                });

            }
            else
            {
                // increment quantity by 1
                product.Quantity = Quantity;
                product.LastModifiedAt = DateTime.Now;

                cartManager.Update(product);
            }

            userCart = cartManager.GetByUserId(User.Identity.GetUserId());
            cartCount = userCart.Count();
            // totalPrice = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice);

            foreach (var itm in userCart)
            {
                if (itm.ProductInformation != null)
                    totalPrice += itm.Quantity * itm.ProductInformation.UnitPrice;
            }

            
            UpdateCookieCartValues(Response.Cookies, cartCount, totalPrice);

            return Json(new { status = 1, message = "Items have been added successfully", count = cartCount, totalprice = string.Format("{0:0.00}", (totalPrice + (totalPrice * vatvalue))/ cookiecurency.RateToRiyal),sign= cookiecurency.Sign }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult UpdateQty()
        {

            string qtytext = Request["Quantities"];

            string[] items = qtytext.Split(',');
            List<updatecartmodel> productsq = new List<updatecartmodel>();

            foreach (string itm in items)
            {
                if (itm.Length > 0)
                {
                    string[] p = itm.Split('_');
                    if (p.Count() > 0)
                    {
                        updatecartmodel m = new updatecartmodel();
                        m.ProductID = Convert.ToInt32(p[0].Remove(0, 3));
                        m.Qty = Convert.ToInt32(p[1]);

                        productsq.Add(m);
                    }
                }
            }
            if (productsq.Count() > 0)
            {

                if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
                {

                    var cartManager = new UserCartManager();
                    var userCart = cartManager.GetByUserId(User.Identity.GetUserId());
                    foreach (var a in productsq)
                    {
                        if (userCart.SingleOrDefault(x => x.ItemId == a.ProductID) != null)
                        {
                           var  cartitm = userCart.SingleOrDefault(x => x.ItemId == a.ProductID);
                            if (cartitm.ProductInformation.AvailableQuantity >= a.Qty)
                            {
                                cartitm.Quantity = a.Qty;
                                cartManager.Update(cartitm);
                            }
                        }
                    }

                }

                else
                {
                    var productsValue = Request.Cookies.Get("products");

                    var products = new List<CartProductModel>();

                    if (productsValue != null)
                    {
                        products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
                    }

                    foreach (var a in productsq)
                    {

                        if (products.SingleOrDefault(x => x.ProductId == a.ProductID) != null)
                        {
                            var cookieProduct = products.SingleOrDefault(x => x.ProductId == a.ProductID);
                            ProductInformationViewModel pinfo = new ProductInformationManager().GetByid(a.ProductID);
                            // increment quantity by 1
                            if (pinfo.AvailableQuantity >= a.Qty)
                            {
                                cookieProduct.Quantity = a.Qty;
                                products[products.FindIndex(x => x.ProductId == a.ProductID)] = cookieProduct;
                            }
                        }

                    }

                    //  productsValue.Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS");
                    var cookie = new HttpCookie("products")
                    {
                        Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS"),
                        Expires = DateTime.Now.AddDays(15)
                    };

                    Response.Cookies.Add(cookie);
                }
            }
             return Json(new {status=1});
        }
        [HttpPost]
        public JsonResult Updateqtyitem(string itemid, int qty)
        {
            var pm = new ProductInformationManager();
         
            int itemno = 0;
            int.TryParse(itemid.ToLower().Replace("qty", ""), out itemno);
            var orginalproduct = pm.GetByid(Convert.ToInt32(itemno));
            if (User.Identity.IsAuthenticated && User.IsInRole("Buyer"))
            {

                var cartManager = new UserCartManager();
                var userCart = cartManager.GetByUserId(User.Identity.GetUserId());
                  var cartitm = userCart.SingleOrDefault(x => x.ItemId == itemno);
                    if (cartitm != null)
                    {

                    if (cartitm.ProductInformation.AvailableQuantity >= (qty - cartitm.Quantity) && (qty - cartitm.Quantity) > 0)
                    {
                        cartitm.Quantity = qty;
                        cartManager.Update(cartitm);
                        orginalproduct.AvailableQuantity = orginalproduct.AvailableQuantity - (qty - cartitm.Quantity);
                        pm.UpdateFull(orginalproduct);

                        return Json(new { status = 1, id = itemid, itmqty = cartitm.Quantity, tabletotal = cartitm.ProductInformation.UnitPrice * cartitm.Quantity, subtotal = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice), totalwthvat = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice) + (userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice) * vatvalue) });
                    }
                    else {
                        return Json(new { status = 0, message = "e:there is no Available Quantity", id = itemid, itmqty = cartitm.Quantity, tabletotal = cartitm.ProductInformation.UnitPrice * cartitm.Quantity, subtotal = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice), totalwthvat = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice) + (userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice) * vatvalue) });
                       //
                    }

                    return Json(new { status = 1, id = itemid, itmqty = cartitm.Quantity, tabletotal = cartitm.ProductInformation.UnitPrice * cartitm.Quantity, subtotal = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice), totalwthvat = userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice)+( userCart.Sum(x => x.Quantity * x.ProductInformation.UnitPrice) * vatvalue) });
                }

                return Json(new { status = 0, id = itemid, itmqty =0, tabletotal = 0, subtotal = 0, totalwthvat = 0 });
            }

            else
            {
                var productsValue = Request.Cookies.Get("products");

                var products = new List<CartProductModel>();

                if (productsValue != null)
                {
                    products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
                }

                var cookieProduct = products.SingleOrDefault(x => x.ProductId == itemno);
                if (cookieProduct!=null)
                    {

                    //  ProductInformationViewModel pinfo = new ProductInformationManager().GetByid(itemno);
                    // increment quantity by 1
                    if (orginalproduct.AvailableQuantity >= (qty - cookieProduct.Quantity) && (qty - cookieProduct.Quantity) > 0)
                    {
                        cookieProduct.Quantity = qty;
                        products[products.FindIndex(x => x.ProductId == itemno)] = cookieProduct;
                        orginalproduct.AvailableQuantity = orginalproduct.AvailableQuantity - (qty - cookieProduct.Quantity);
                        pm.UpdateFull(orginalproduct);
                        var cookie = new HttpCookie("products")
                        {
                            Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS"),
                            Expires = DateTime.Now.AddDays(15)
                        };

                        Response.Cookies.Add(cookie);

                        return Json(new { status = 1, id = itemid, itmqty = cookieProduct.Quantity, tabletotal = cookieProduct.BasePrice * cookieProduct.Quantity, subtotal = products.Sum(x => x.Quantity * x.BasePrice), totalwthvat = products.Sum(x => x.Quantity * x.BasePrice) + (products.Sum(x => x.Quantity * x.BasePrice) * vatvalue) });

                    }
                    else {
                        return Json(new { status = 0, message = "e:there is no Available Quantity", id = itemid, itmqty = cookieProduct.Quantity, tabletotal = cookieProduct.BasePrice * cookieProduct.Quantity, subtotal = products.Sum(x => x.Quantity * x.BasePrice), totalwthvat = products.Sum(x => x.Quantity * x.BasePrice) + (products.Sum(x => x.Quantity * x.BasePrice) * vatvalue) });
                        
                    }
                    return Json(new { status = 1, id = itemid, itmqty = cookieProduct.Quantity, tabletotal = cookieProduct.BasePrice * cookieProduct.Quantity, subtotal = products.Sum(x => x.Quantity * x.BasePrice), totalwthvat = products.Sum(x => x.Quantity * x.BasePrice)+( products.Sum(x => x.Quantity * x.BasePrice) * vatvalue )});
                }

                return Json(new { status = 0, id = itemid, itmqty = 0, tabletotal = 0, subtotal = 0, totalwthvat = 0});

                //  productsValue.Value = StringCipher.Encrypt(JsonConvert.SerializeObject(products), "HAWEYA.PS");

            }
            
        }

        public string AddUnit(CartItemViewModel c)
        {



            if (c.Qty <= 0)
                return ("Quantity must be greater than 0");



            else
            {

                //var cartManager = new UserCartManager();

                //var userCart = cartManager.GetByUserId(User.Identity.GetUserId());

                //var product = userCart.SingleOrDefault(x => x.ItemId == ItemId);

                //if (product == null)
                //{

                //    cartManager.Add(new UserCartViewModel()
                //    {
                //        ItemId = ItemId,
                //        Quantity = Quantity,
                //        IsPending = true,
                //        LastModifiedAt = DateTime.Now,
                //        UserId = User.Identity.GetUserId()
                //    });

                //}
                //else
                //{
                //    // increment quantity by 1
                //    product.Quantity = Quantity;
                //    product.LastModifiedAt = DateTime.Now;

                //    cartManager.Update(product);
                //}
                return ("Item Added Successfully to cart");
            }





        }
        private double GetCookieValueByKey(string Key)
        {
            var cookie = Request.Cookies.Get(Key);

            if (cookie == null) return 0;

            return double.Parse(cookie.Value);
        }

        public JsonResult GetCartjson()
        {
            List<UserCartViewModel> cart = new List<UserCartViewModel>();
            ViewBag.BuyerCountryName = "-1";
            if (User.Identity.IsAuthenticated)
            {
                // fetch from database
                var cartManager = new UserCartManager();
                var userCart = cartManager.GetByUserId(User.Identity.GetUserId());
                cart = userCart.ToList();
                return Json(cart.Select(x => new { x.ItemId, x.Quantity }), JsonRequestBehavior.AllowGet);
            }
         
            var productsValue = Request.Cookies.Get("products");
            var products = new List<CartProductModel>();

            if (productsValue != null)
            {
                products = JsonConvert.DeserializeObject<List<CartProductModel>>(StringCipher.Decrypt(productsValue.Value, "HAWEYA.PS"));
            }

            
            ProductInformationManager productmanager = new ProductInformationManager();

            foreach (var product in products)
            {
                cart.Add(new UserCartViewModel { IsPending = true, ItemId = product.ProductId, Quantity = product.Quantity, ProductInformation = productmanager.GetByid(Convert.ToInt32(product.ProductId)) });
            }

            // get from cookie.

            return Json(cart.Select(x => new { x.ItemId, x.Quantity }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DHLShippingtest(int id, int qty)
        {
            int buyercountryid = 0;
            int buyercityid = 0;

            var buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
            var buyercity = CacheManager.Cities.Where(x => x.id == buyercityid).FirstOrDefault();
            string buyerstreet = "";
            string buyerpostal = "";
            string buyercountyname = "";
            string buyercountryshort = "";
            string buyercityname = "";


            var usermanager = new AspNetUsersManager();
            if (!User.Identity.IsAuthenticated && !User.IsInRole("Buyer"))
            {


                var Addresscookevalue = Request.Cookies.Get("Address");
                var Addressvalue = new AddressModel();

                if (Addresscookevalue != null)
                {
                    Addressvalue = JsonConvert.DeserializeObject<AddressModel>(StringCipher.Decrypt(Addresscookevalue.Value, "HAWEYA.PS"));

                    buyercountyname = Addressvalue.CountryName;
                    buyercountryshort = Addressvalue.CountryCode;
                    buyercityname = Addressvalue.City;
                    buyerstreet = Addressvalue.StreetLines;
                    buyerpostal = "";
                    // return Json(new { status = 1, address = Addressvalue }, JsonRequestBehavior.AllowGet);
                }
                else { return Json(new { status = 0, messge = "please login to view shipping price" }, JsonRequestBehavior.AllowGet); }

            }
            else
            {

                var buyer = usermanager.GetByEmail(User.Identity.Name);

                var shippingaddress = new ShippingDataManager().GetbyBuyerIDPrimary(buyer.Id).FirstOrDefault();


                // var shippingaddress = buyer.ShippingDatas.Find(x => x.IsPrimary==true);

                if (shippingaddress != null)
                {
                    buyercountryid = shippingaddress.CountryID ?? Convert.ToInt32(buyer.CountryId);
                    buyercityid = shippingaddress.CityID ?? Convert.ToInt32(buyer.CityId);

                    buyercountry = CacheManager.Countries.Where(x => x.id == buyercountryid).FirstOrDefault();
                    buyercountyname = buyercountry.name;
                    buyercountryshort = buyercountry.sortname;
                    buyercity = CacheManager.Cities.Where(x => x.id == buyercityid).FirstOrDefault();
                    buyercityname = buyercity.text;
                    buyerstreet = shippingaddress.FirstAddress ?? shippingaddress.SecondAddress ?? buyercity.text;
                    buyerpostal = shippingaddress.ZipCode;

                }


                else
                {
                    return Json(new { status = 0, messge = "update your shipping data befor view shipping price" }, JsonRequestBehavior.AllowGet);
                }

            }

            var model = new ProductInformationManager().GetByid(id);
            if (model.Width > 0 && model.Height > 0 && model.Length > 0 && model.Weight > 0)
            {
                string userid = model.Username ?? "seller2208@gmail.com";
                var seller = usermanager.GetByEmail(userid);

                var materialaddress = new MaterialPickupLocationManager().GetBysellerid(seller.Id);
                int countryid = materialaddress == null ? Convert.ToInt32(seller.CountryId) : Convert.ToInt32(materialaddress.Country);
                int cityid = materialaddress == null ? Convert.ToInt32(seller.CityId) : Convert.ToInt32(materialaddress.City);

                var country = CacheManager.Countries.Where(x => x.id == countryid).FirstOrDefault();
                var city = CacheManager.Cities.Where(x => x.id == cityid).FirstOrDefault();
                string street = materialaddress == null ? city.text : (materialaddress.Street == null ? city.text : materialaddress.Street);
                string sellerpostal = seller.POBox ?? "";



                //var reagion = CacheManager.r.Where(x => x.id == materialaddress.Country).FirstOrDefault();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://wsbexpress.dhl.com/rest/sndpt/RateRequest");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string username = "winSA";
                    string password = "E$0pZ$8lG!6w";

                    string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                    httpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
                    // httpWebRequest.Headers.Add("Authorization", "Basic d2luU0E6RSQwcFokOGxHITZ3");
                    //httpWebRequest.Headers.Add("Cookie", "BIGipServer~WSB~pl_wsb-express-cbj.dhl.com_443=310126791.64288.0000");


                    string time = DateTime.Now.ToString("yyyy-MM-ddT13:00:00GMT+00:00");
                    string json = "{\"RateRequest\": {\"ClientDetails\": null,\"RequestedShipment\":{\"DropOffType\":\"REGULAR_PICKUP\",\"NextBusinessDay\":\"N\",\"Ship\":{\"Shipper\":{\"StreetLines\":\"" + street + "\",\"City\":\"" + city.text + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + country.sortname + "\"},\"Recipient\":{\"StreetLines\":\"" + buyerstreet + "\",\"City\":\"" + buyercityname + "\",\"PostalCode\":\"\",\"CountryCode\":\"" + buyercountryshort + "\"}},\"Packages\":{\"RequestedPackages\":{\"@number\":\"" + qty + "\",\"Weight\":{\"Value\":" + model.Weight + "},\"Dimensions\":{\"Length\":" + model.Length + ",\"Width\":" + model.Width + ",\"Height\":" + model.Height + "}}},\"ShipTimestamp\":\"" + time + "\",\"UnitOfMeasurement\":\"SI\",\"Content\":\"NON_DOCUMENTS\",\"PaymentInfo\":\"DAP\",\"Account\":\"961230867\"}}}";

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd().Replace("@type", "type");

                    return Json(new { status = 1, result = result }, JsonRequestBehavior.AllowGet); ;
                }

                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
            else { return Json(new { status = 0 }, JsonRequestBehavior.AllowGet); }


        }
    }
    class updatecartmodel
    {
        public int ProductID { get; set; }
        public int Qty { get; set; }

    }

   
}