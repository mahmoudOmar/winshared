﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;

namespace ps.haweya.win.obs.web.Controllers
{
    public static class  HelperTools
    {
        
            public static string GenerateSlug(this string phrase)
            {
                string str = phrase.ToLower();
                // invalid chars           
                str = Regex.Replace(str, @"[^a-zأ-ي0-9\s-]", "");
                // convert multiple spaces into one space   
                str = Regex.Replace(str, @"\s+", " ").Trim();
                // cut and trim 
                str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
                str = Regex.Replace(str, @"\s", "-"); // hyphens   
                return str;
            }
        public static string Thumbnail(string filename, int width = 363, int height = 268)
        {
            try
            {

                string extintion = Path.GetExtension("~/" + filename);
                string fname = Path.GetFileName("~/" + filename).Replace(extintion, "");
                string resultname = "/Content/UploadData/UploadImages/Resize/" + fname + "_" + width + "_" + height + extintion;

                if (!System.IO.File.Exists("~/" + resultname))
                {
                    WebImage file = new WebImage("~/" + filename);
                    var img = file
                        .Resize(width, height, false, true);
                    //var imgresult= new ImageResult(new MemoryStream(img.GetBytes()), "binary/octet-stream");
                    // var myimage = new Image().Save(;


                    img.Save("~/Content/UploadData/UploadImages/Resize/" + fname + "_" + width + "_" + height + extintion);
                }

                return resultname;
            }
            catch
            {
                return "/Content/FrontEnd/images/giphy.gif";
            }


        }


    }
}