﻿using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Controllers
{

    public class UploadFileViewModel
    {
        public string OriginalName { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public long Size { get; set; }
        public string Path { get; set; }
        public string uuid { get; set; }

        public List<KeyValuePair<string, string>> Attributes { get; set; }
    }

    public class UploaderController : Controller
    {
        List<UploadFileViewModel> uploadedFiles = new List<UploadFileViewModel>();

        [HttpPost]
        public JsonResult Up()
        {

            var files = Request.Files.Count > 0 ? Request.Files : null;

            var filesAttributes = Request.Form;

            if (files == null) return Json(new { Status = 200 });

            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFileBase file = files.Get(i);

                var fileAttr = "";

                try
                {
                    fileAttr = filesAttributes.Get(i);
                }
                catch (Exception e) { }

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = $@"{Guid.NewGuid()}" + Path.GetExtension(file.FileName);
                    var path = GeneratePath(fileName);

                    // add it to the list 
                    uploadedFiles.Add(new UploadFileViewModel()
                    {
                        OriginalName = file.FileName,
                        Name = fileName,
                        Path = path,
                        Size = file.ContentLength,
                        Type = file.ContentType,
                        uuid = fileAttr
                    });

                    file.SaveAs(path);

                }
            }

            return Json(new { Status = 200, Files = uploadedFiles });
        }
        [HttpPost]
        public JsonResult Updocs()
        {

            var files = Request.Files.Count > 0 ? Request.Files : null;

            var filesAttributes = Request.Form;

            if (files == null) return Json(new { Status = 200 });

            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFileBase file = files.Get(i);

                var fileAttr = "";

                try
                {
                    fileAttr = filesAttributes.Get(i);
                }
                catch (Exception e) { }

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = $@"{Guid.NewGuid()}" + Path.GetExtension(file.FileName);
                    var path = GeneratePathdocs(fileName);

                    // add it to the list 
                    uploadedFiles.Add(new UploadFileViewModel()
                    {
                        OriginalName = file.FileName,
                        Name = fileName,
                        Path = path,
                        Size = file.ContentLength,
                        Type = file.ContentType,
                        uuid = fileAttr
                    });

                    file.SaveAs(path);

                }
            }

            return Json(new { Status = 200, Files = uploadedFiles });
        }

        private string GeneratePath(string FileName)
        {
            try
            {
                string path = string.Format("{0}", System.Web.Hosting.HostingEnvironment.MapPath("~/Content/UploadData/UploadImages"));



                //***

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                return path = string.Format("{0}//{1}", System.Web.Hosting.HostingEnvironment.MapPath("~/Content/UploadData/UploadImages"), FileName);

            }
            catch (Exception e)
            {
                throw new Exception("Permission Needed", e);
            }

        }
        private string GeneratePathdocs(string FileName)
        {
            try
            {
                string path = string.Format("{0}", System.Web.Hosting.HostingEnvironment.MapPath("~/Content/UploadData/UploadDocuments"));



                //***

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                return path = string.Format("{0}//{1}", System.Web.Hosting.HostingEnvironment.MapPath("~/Content/UploadData/UploadDocuments"), FileName);

            }
            catch (Exception e)
            {
                throw new Exception("Permission Needed", e);
            }

        }
    }
}