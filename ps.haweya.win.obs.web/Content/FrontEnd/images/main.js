(function ($) {

    $('.payment-form input[type=radio][name=payment_method]').change(function() {
        if (this.value == 'bank') {
            $('.payment-form .card-inputs').hide();
        }
        else if (this.value == 'card') {
            $('.payment-form .card-inputs').show();
        }
    });

    $(document).on('click', '.filterBtn', function (e) {
        e.preventDefault();
        $(".filter-sidebar").css('left','0');

    });$(document).on('click', '.filterClose', function (e) {
        e.preventDefault();
        $(".filter-sidebar").css('left','-340px');

    });


    var swiper = new Swiper('.search-swiper', {
        effect: 'fade',
        centeredSlides: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },

    });

    var swiper1 = new Swiper('.client-swiper', {
        slidesPerView: 5,
        spaceBetween: 30,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
            }
        }

    });var swiper1 = new Swiper('.client2-swiper', {
        slidesPerView: 4,
        spaceBetween: 30,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
            }
        }

    });

    var swiper1 = new Swiper('.cat-swiper', {
        slidesPerView: 5,
        spaceBetween: 10,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 5,
            },
            992: {
                slidesPerView: 4,
            },
            640: {
                slidesPerView: 2,
            }
        }

    });
    var swiper2 = new Swiper('.offers-swiper', {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 5,
        loop: true,
        freeMode: true,
        loopedSlides: 5, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        loop:true,
        loopedSlides: 5, //looped slides should be the same
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs,
        },
    });

    $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').addClass('buttons_added').append('<input type="button" value="+" id="add1"     class="plus" />').prepend('<input type="button" value="-" id="minus1" class="minus"     />');

    $('#product-slider').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        directionNav:false
    });
})(jQuery);

jQuery(function($) {
    var $testProp = $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').find('qty');
    if ($testProp && $testProp.prop('type') != 'date') {
        $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').addClass('buttons_added').append('<input type="button" value="+" class="plus" />').prepend('<input type="button" value="-" class="minus" />');
        $('input.qty:not(.product-quantity input.qty)').each(function() {
            var min = parseFloat($(this).attr('min'));
            if (min && min > 0 && parseFloat($(this).val()) < min) {
                $(this).val(min);
            }
        });
        $(document).on('click', '.plus, .minus', function() {
            var $qty = $(this).closest('.quantity').find('.qty'),
                currentVal = parseFloat($qty.val()),
                max = parseFloat($qty.attr('max')),
                min = parseFloat($qty.attr('min')),
                step = $qty.attr('step');
            if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
            if (max === '' || max === 'NaN') max = '';
            if (min === '' || min === 'NaN') min = 0;
            if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;
            if ($(this).is('.plus')) {
                if (max && (max == currentVal || currentVal > max)) {
                    $qty.val(max);
                } else {
                    $qty.val(currentVal + parseFloat(step));
                }
            } else {
                if (min && (min == currentVal || currentVal < min)) {
                    $qty.val(min);
                } else if (currentVal > 0) {
                    $qty.val(currentVal - parseFloat(step));
                }
            }
            $qty.trigger('change');
        });
    }

});