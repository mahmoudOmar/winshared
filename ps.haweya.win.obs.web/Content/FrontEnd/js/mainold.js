//var additionalURL = '/Home/AddtoFavorite/")';
//var placeURL = '/Home/GetplaceData/';
//var AddPlace = '/Home/Addplace';
//var AddCartURL = '/cart/Add/';
//var SearchResult = '/Home/SearchResult/';
//var GetSearchProducts = '/Home/GetSearchProducts/';
//var ProductDetailsURL = '/ProductDetails/Details/';

var ShowMessage = function ShowMessage(msg) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",

        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    var msgTitle = 'Attention!!';
    var cls = "info";
    if (msg.indexOf("s:") == 0) { cls = "success"; msg = msg.substring(2); }
    if (msg.indexOf("w:") == 0) { cls = "warning"; msg = msg.substring(2); }
    if (msg.indexOf("e:") == 0) { cls = "error"; msg = msg.substring(2); }
    if (msg.indexOf("i:") == 0) { cls = "info"; msg = msg.substring(2); }
    toastr[cls](msg, msgTitle);

}

var floatNumber = function () {
    $(document).on('keypress', '.allow-float', function (e) {

        if ($(this).val().toString().indexOf(".") !== -1 && e.which === 46) return false;

        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
            return false;
        }

    });
};

var updatecartcount = function updatecartcount(cart, totalprice) {
    $("#itemcount").html(cart);
    $("#itemcount1").html(cart);


    if (cart == 0) {
        $(".itemcount").css("display", "none");
        $("#totalprice").css("display", "none");

    }
    else {
        $(".itemcount").css("display", "block");
        $("#totalprice").css("display", "inline-block");
    }
    $("#totalprice").html(totalprice + "SAR");
    $("#totalprice1").html(totalprice + "SAR");
}

var numbersOnly = function () {
    $(document).on('keypress', '.allow-number', function (e) {
        console.log(e.which);
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
};



$(document).ready(function () {

   

    $(document).on('click', '.favitm', function () {
        var id = $(this).attr("myid");

        var selector = $(this);

        $.get('/Home/AddtoFavorite/' + "?id=" + id, function (result) {

            console.log(result);
            if (result.status == 1) {

                selector.empty();
                selector.html("<i class='fas fa-heart'></i>");

            }
            else {
                selector.html("  <i class='far fa-heart'></i>");
              
            }
            ShowMessage(result.msg);

        }); return false;
    });

    $(document).on("click", ".placebid", function () {
        $("#errorlbl").html("");

        var productid = $(this).attr("myid");
        $("#bidproductid").val(productid);

        //alert(placeURL);
       // var place = (PlaceURL + '?ProductID=' + productid);
        //alert(place);
        $.get('/Home/GetplaceData/?ProductID=' + productid, function (result) {
            if (result.status != 0) {

                $("#bidmaterialname").val(result.Title);
                $("#bidDifference").val(result.BidDifference);

                $("#bidqty").val(result.Qty);
                $("#bidlastammount").val(result.Price);
                if (result.IsInspect == 1) {
                    $("#inspectionvaluehdn").val(1);
                    $("#isinspection").show();
                    //$("#bidammount").hide();
                }
                else {
                    $("#inspectionvaluehdn").val(0);
                    //$("#bidammount").show();
                    $("#isinspection").hide();
                }


                $("#mdlPlacebid").modal();
            }
            else { ShowMessage(result.msg) }
        })



        return false;
    });
    $(document).on("click", "#submitbid", function () {



        var amount = $("#Curentbidamount").val();
        var productid = $("#bidproductid").val();
        var isinspect = $("#inspectionvaluehdn").val();
        var needinspection = 0;

        console.log(isinspect, needinspection);
        if (isinspect == 1) {

            needinspection = $("#inspectionvalue").val();

            if (needinspection == -1) {
                $("#errorlbl").html("please select if you need inspection or not");
                return false;
            }
        }
        //console.log(isinspect, needinspection);



        if ($("#Curentbidamount").val() != "") {

            if (isNaN(parseFloat(amount))) {
                $("#errorlbl").html("please add your bid ammount");
                return false;
            }
            else {
                var pastamount = $("#bidlastammount").val();
                var bidDifference = $("#bidDifference").val();
                console.log(pastamount, amount);
                if (parseFloat(amount) < (parseFloat(pastamount) + parseFloat(bidDifference))) {
                    ShowMessage("e:amount must be more than (last amount plus Bid Difference  )");
                    $("#errorlbl").html("amount must be more than (last amount plus Bid Difference  )");
                    return false;
                }

                $("#errorlbl").html("");
                $('div#biddialog').block({ message: '<h5> Processing ... <img src="/Content/FrontEnd/images/loader.gif" /></h5>', css: { backgroundColor: '#ebebeb', color: '#007bff', border: 'none', "padding-top": '10px', width: '60%', left: '20%' } });
                $.get('/Home/Addplace/?ProductID=' + productid + "&Amount=" + amount + "&&Inspection=" + needinspection, function (result) {
                    if (result.status != 0) {
                        $("#closebtn").click();
                    }

                    $('div#biddialog').unblock();
                    ShowMessage(result.msg);
                })
            }
        }
        else {
            $("#errorlbl").html("please add your bid ammount");
            ShowMessage("e:please add your bid ammount");
            $("#Curentbidamount").focus();
        }
        return false;

    });

    $(document).on("click", ".btnclose", function () {

        $("#mdladdtocart").modal("hide");
    });

    $(document).on("click", ".submitcart", function (e) {

        e.preventDefault();
        var qty = 1;
        //var id = $(this).attr("myid");
        var id = this.dataset.id;
        var baseprice = this.dataset.baseprice;

        $.post('/cart/Add/', { ItemId: id, Quantity: 1, UnitPrice: baseprice }, function (result) {
            //console.log(result.count, result.totalprice);
            //updatecartcount(result.count, result.totalprice);

            if (result.message.indexOf("e:") != -1) {
                $("#mdladdtocartbefor").modal();

                //return false;
            }

            else {
                $("#mdladdtocart").modal();
                // return false;
            }
        });
        //return false;
    });



    var scrollTop = $('header').height();
    $(window).scroll(function () {

        if ($(window).scrollTop() >= scrollTop) {
            setTimeout(function () {
                $('header').addClass('sticky-menu');
            }, 1);
        }
        if ($(window).scrollTop() < (scrollTop)) {
            $('header').removeClass('sticky-menu');
        }

    });


    $('.close-menu').on('click', function () {
        $(".mobile-wrap").css('left', '-335px');
        $('.navbar-toggler').removeClass('active');
    });
    $('.navbar-toggler').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(".mobile-wrap").css('left', '-335px');
        } else {
            $(this).addClass('active');
            $(".mobile-wrap").css('left', '0');
        }

    });

    $(document).ready(function () {

        $('.payment-form .card-inputs').hide();
        $('.payment-form .Bank-inputs').show();
    })
    $('.payment-form input[type=radio][name=payment_method]').change(function () {
        if (this.value == 'bank') {
            $('.payment-form .card-inputs').hide();
            $('.payment-form .Bank-inputs').show();
        }
        else if (this.value == 'card') {

            $('.payment-form .card-inputs').show();
            $('.payment-form .Bank-inputs').hide();
        }
    });

    $(document).on('click', '.filterBtn', function (e) {
        e.preventDefault();
        $(".filter-sidebar").css('left', '0');

    }); $(document).on('click', '.filterClose', function (e) {
        e.preventDefault();
        $(".filter-sidebar").css('left', '-340px');

    });


    var swiper = new Swiper('.search-swiper', {
        effect: 'fade',
        centeredSlides: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },

    });

    var swiper1 = new Swiper('.client-swiper', {
        slidesPerView: 5,
        spaceBetween: 30,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
            }
        }

    }); var swiper1 = new Swiper('.client2-swiper', {
        slidesPerView: 4,
        spaceBetween: 30,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
            }
        }

    });

    var swiper1 = new Swiper('.cat-swiper', {
        slidesPerView: 5,
        spaceBetween: 10,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 5,
            },
            992: {
                slidesPerView: 4,
            },
            640: {
                slidesPerView: 2,
            }
        }

    });
    var swiper2 = new Swiper('.offers-swiper', {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 5,
        loop: true,
        freeMode: true,
        loopedSlides: 5, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        loop: true,
        loopedSlides: 5, //looped slides should be the same
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs,
        },
    });

    $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').addClass('buttons_added').append('<input type="button" value="+" id="add1"     class="plus" />').prepend('<input type="button" value="-" id="minus1" class="minus"     />');

    $('#product-slider').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        directionNav: false
    });

    var screen = $(window).width();
    if (screen > 768) {
        $(".product-img").imagezoomsl({
            zoomrange: [5, 5],
            magnifierspeedanimate: 10,
            scrollspeedanimate: 1,
            zoomspeedanimate: 1
        });
    }

    var $testProp = $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').find('qty');
    if ($testProp && $testProp.prop('type') != 'date') {
        $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').addClass('buttons_added').append('<input type="button" value="+" class="plus" />').prepend('<input type="button" value="-" class="minus" />');
        $('input.qty:not(.product-quantity input.qty)').each(function () {
            var min = parseFloat($(this).attr('min'));
            if (min && min > 0 && parseFloat($(this).val()) < min) {
                $(this).val(min);
            }
        });
        $(document).on('click', '.plus, .minus', function () {
            var $qty = $(this).closest('.quantity').find('.qty'),
                currentVal = parseFloat($qty.val()),
                max = parseFloat($qty.attr('max')),
                min = parseFloat($qty.attr('min')),
                step = $qty.attr('step');
            if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
            if (max === '' || max === 'NaN') max = '';
            if (min === '' || min === 'NaN') min = 0;
            if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;
            if ($(this).is('.plus')) {
                if (max && (max == currentVal || currentVal > max)) {
                    $qty.val(max);
                } else {
                    $qty.val(currentVal + parseFloat(step));
                }
            } else {
                if (min && (min == currentVal || currentVal < min)) {
                    $qty.val(min);
                } else if (currentVal > 0) {
                    $qty.val(currentVal - parseFloat(step));
                }
            }
            $qty.trigger('change');
        });
    }






    /**************************************************************************/




    $('img').each(function (i, item) {
        $(item).attr('alt', $(item).attr('src'));
    });

    $('#qmodel').autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: '/Home/GetSearchProducts/',
                method: 'post',
                data: { SearshTest: $("#qmodel").val() },
                dataType: 'json',
                success: function (data) {
                    response(data);
                },
                error: function (err) {
                    alert(err);
                }
            });
        },
        focus: updateTextBox,
        select: updateTextBox
    })
        .autocomplete('instance')._renderItem = function (ul, item) {
            return $("<li class='autocomplete_li'>")
                .append("<a href='/ProductDetails/Details/?id=" + item.ProductId + "'><img class='Autocompleteimg' src='" + item.ProductImage + "' alt=" + item.ProductTitle + " /><span>" + item.ProductTitle + "</span></a>")

                .appendTo(ul);
        };
    function updateTextBox(event, ui) {
        $(this).val(ui.item.Name);
        return false;
    }


    $(document).on('click', '.searchajx', function () {
        window.location.href = '/Home/SearchResult/' + "?q=" + $("#q").val();
        return false;
    });


    numbersOnly();
    floatNumber();

});