//var additionalURL = '/Home/AddtoFavorite/")';
//var placeURL = '/Home/GetplaceData/';
//var AddPlace = '/Home/Addplace';
//var AddCartURL = '/cart/Add/';
//var SearchResult = '/Home/SearchResult/';
//var GetSearchProducts = '/Home/GetSearchProducts/';
//var ProductDetailsURL = '/ProductDetails/Details/';

var ShowMessage = function ShowMessage(msg) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",

        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    var msgTitle = 'Attention!!';
    var cls = "info";
    if (msg.indexOf("s:") == 0) { cls = "success"; msg = msg.substring(2); }
    if (msg.indexOf("w:") == 0) { cls = "warning"; msg = msg.substring(2); }
    if (msg.indexOf("e:") == 0) { cls = "error"; msg = msg.substring(2); }
    if (msg.indexOf("i:") == 0) { cls = "info"; msg = msg.substring(2); }
    toastr[cls](msg, msgTitle);

}

var floatNumber = function () {
    $(document).on('keypress', '.allow-float', function (e) {

        if ($(this).val().toString().indexOf(".") !== -1 && e.which === 46) return false;

        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
            return false;
        }

    });
};

var updatecartcount = function updatecartcount(cart, totalprice,sign) {
    $("#itemcount").html(cart);
    $("#itemcount1").html(cart);


    if (cart == 0) {
        $(".itemcount").css("display", "none");
        $("#totalprice").css("display", "none");

    }
    else {
        $(".itemcount").css("display", "block");
        $("#totalprice").css("display", "inline-block");
    }
    $("#totalprice").html(totalprice + " "+sign);
    $("#totalprice1").html(totalprice + " "+sign );
}

var numbersOnly = function () {
    $(document).on('keypress', '.allow-number', function (e) {
        console.log(e.which);
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
};



$(document).ready(function () {

   
    if ($('.wow').length) {
        var wow = new WOW(
            {
                boxClass: 'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset: 0,          // distance to the element when triggering the animation (default is 0)
                mobile: false,       // trigger animations on mobile devices (default is true)
                live: true       // act on asynchronously loaded content (default is true)
            }
        );
        wow.init();
    }
    $(document).on('click', '.favitm', function () {

        var id = $(this).attr("myid");

        var selector = $(this);
        //  var l = Ladda.create(this);

        //console.log(l);
        $(this).block({ message: '<img src="/Content/FrontEnd/images/loading.gif" />', css: { backgroundColor: 'none', color: '#007bff', border: 'none', height: '100%', "padding-top": '0px', "margin-top": '-5px', width: '100%', left: '0', } });
        $.get('/Home/AddtoFavorite/' + "?id=" + id, function (result) {

            console.log(result);
            if (result.status == 1) {

                selector.empty();
                selector.html("<i class='fas fa-heart'></i>");

            }
            else {
                selector.html("  <i class='far fa-heart'></i>");

            }
            ShowMessage(result.msg);
            $(this).unblock();
        }); return false;
    });

    $(document).on("click", ".placebid", function () {
        $("#errorlbl").html("");
        $(this).block({ message: '<img src="/Content/FrontEnd/images/loading.gif" height="40" />', css: { backgroundColor: 'none', color: '#007bff', border: 'none', "padding-top": '0px', "margin-top": '-5px', width: '100%', left: '0', } });
        var productid = $(this).attr("myid");
        $("#bidproductid").val(productid);

        //alert(placeURL);
        // var place = (PlaceURL + '?ProductID=' + productid);
        //alert(place);
        $.get('/Home/GetplaceData/?ProductID=' + productid, function (result) {
            if (result.status != 0) {

                $("#bidmaterialname").val(result.Title);
                $("#bidDifference").val(result.BidDifference);

                $("#bidqty").val(result.Qty);
                $("#bidlastammount").val(result.Price);
                if (result.IsInspect == 1) {
                    $("#inspectionvaluehdn").val(1);
                    $("#isinspection").show();
                    //$("#bidammount").hide();
                }
                else {
                    $("#inspectionvaluehdn").val(0);
                    //$("#bidammount").show();
                    $("#isinspection").hide();
                }
                if (result.ShowBidAmmount == 0) { $(".bidammount").hide(); $("#showbidamountfiled").val(0); } else { $(".bidammount").show(); $("#showbidamountfiled").val(1);}

                $("#mdlPlacebid").modal();

            }
            else { ShowMessage(result.msg) }

            $(".blockUI").hide();
        })



        return false;
    });

    $(document).on('change', '#Bidoffer', function () {
        Object.values(this.files).forEach(function (file) {
            if (file.type != 'application/pdf') {
                $("#errorlblbidoffer").html("only pdf files allowed");
                $("#Bidoffer").val('');
                return false;
            }
            $("#errorlblbidoffer").html("");
        })
    });
    $(document).on('change', '#Bidothers', function () {
        Object.values(this.files).forEach(function (file) {
            if (file.type != 'application/pdf') {

                $("#errorlblbidotherfiles").html("only pdf files allowed");
                $("#Bidothers").val('');
                return false;
            }
            $("#errorlblbidotherfiles").html("");
        })
    });
    $(document).on("click", "#submitbid", function () {



        var amount = $("#Curentbidamount").val();
        var productid = $("#bidproductid").val();
        var isinspect = $("#inspectionvaluehdn").val();
        var needinspection = 0;
        var showbidamount = $("#showbidamountfiled").val();
        var BidOfferfile = document.getElementById('Bidoffer');
        if (BidOfferfile.files.length == 0) {
            $("#errorlblbidoffer").html("Please select pdf file");
            return false;
        }
        $("#errorlblbidoffer").html("");
       
        var bidoffer = BidOfferfile.files[0];

        if ($("#accepttermscbx").prop("checked") != true) {
            $("#errorlblterms").html("You should Accept Terms and Condition");
            return false;
        }
        else {
            $("#errorlblterms").html("");
        }


        console.log(isinspect, needinspection);
        if (isinspect == 1) {

            needinspection = $("#inspectionvalue").val();

            if (needinspection == -1) {
                $("#errorlbl").html("please select if you need inspection or not");
                return false;
            }
        }
        //console.log(isinspect, needinspection);

        

        if ($("#Curentbidamount").val() != "") {

            if (isNaN(parseFloat(amount))) {
                $("#errorlbl").html("please add your bid ammount");
                return false;
            }
            else {

                if (showbidamount == 1) {
                    var pastamount = $("#bidlastammount").val();
                    var bidDifference = $("#bidDifference").val();
                    console.log(pastamount, amount);
                    if (parseFloat(amount) < (parseFloat(pastamount) + parseFloat(bidDifference))) {
                        ShowMessage("e:amount must be more than (last amount plus Bid Difference  )");
                        $("#errorlbl").html("amount must be more than (last amount plus Bid Difference  )");
                        return false;
                    }
                }

                $("#errorlbl").html("");
                $('div#biddialog').block({ message: '<h5> Processing ... <img src="/Content/FrontEnd/images/loader.gif" /></h5>', css: { backgroundColor: '#ebebeb', color: '#007bff', border: 'none', "padding-top": '10px', width: '60%', left: '20%' } });


                var formdata = new FormData();
                var BidOffer = BidOfferfile.files[0];
                formdata.append("ProductID", productid);
                formdata.append("Amount", amount);
                formdata.append("Inspection", needinspection);
                formdata.append(BidOfferfile.files[0].name, BidOfferfile.files[0]);
                var Bidothersfiles = document.getElementById('Bidothers');
                if (Bidothersfiles.files.length > 0) {
                    for (var i = 0; i < Bidothersfiles.files.length; i++) {
                        formdata.append(Bidothersfiles.files[i].name, Bidothersfiles.files[i]);

                    }
                }

                ////formdata.append("ProductID", productid);
                ////formdata.append("ProductID", productid);
                ////formdata.append("ProductID", productid);
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/Home/Addplace');
                xhr.send(formdata);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        //$.get('/Home/Addplace/?ProductID=' + productid + "&Amount=" + amount + "&&Inspection=" + needinspection, function (result) {
                        //  if (result.status != 0) {
                        //    $("#closebtn").click();
                        //}
                        var result = JSON.parse(xhr.responseText);
                        if (result.status != 0) {
                            $(".bidscount_" + productid).html(result.bidscount + " bids");
                            $(".lastbidprice_" + productid).html(result.lastbidammout + " " + result.Sign);
                            $('div#biddialog').unblock();
                            ShowMessage(result.msg);

                            //})
                        }
                    }
                }
            }
        }
                    else {
                        $("#errorlbl").html("please add your bid ammount");
                        ShowMessage("e:please add your bid ammount");
                        $("#Curentbidamount").focus();
                    }
                    return false;

                }
            );

    $(document).on("click", ".btnclose", function () {

        $("#mdladdtocart").modal("hide");
    });

    $(document).on("click", ".submitcart", function (e) {

        e.preventDefault();
        $(this).block({ message: '<img src="/Content/FrontEnd/images/loading.gif" height="40" />', css: { backgroundColor: 'none', color: '#007bff', border: 'none', "padding-top": '0px', "margin-top": '-5px', width: '100%', left: '0', } });
        var qty = 1;
        //var id = $(this).attr("myid");
        var id = this.dataset.id;
        var baseprice = this.dataset.baseprice;

        $.post('/cart/Add/', { ItemId: id, Quantity: 1, UnitPrice: baseprice }, function (result) {
            //console.log(result.count, result.totalprice);
            updatecartcount(result.count, result.totalprice,result.sign);

            if (result.message.indexOf("e:") != -1) {
                if (result.status == 0) { ShowMessage(result.message); }
               // $("#mdladdtocartbefor").modal();

                //return false;
            }

            else {

                $("#mdladdtocart").modal();
                // return false;
            }
            $(".blockUI").hide();
        });


        //return false;
    });

(function ($) {

    var scrollTop = $('header').height();
    $('.absolute-header').css('height', scrollTop);
    $(window).scroll(function() {

        if ($(window).scrollTop() >= scrollTop) {
            setTimeout(function() {
                $('absolute-header').show();
                $('header').addClass('sticky-menu');
            }, 1);
        }
        if ($(window).scrollTop() < (scrollTop)) {
            $('absolute-header').hide();
            $('.absolute-header').fadeIn();
            $('.absolute-header').delay(2000);
            $('header').removeClass('sticky-menu');
        }

    });


    $('.close-menu').on('click', function () {
        $(".mobile-wrap").css('left','-335px');
        $('.navbar-toggler').removeClass('active');
    });
    $('.navbar-toggler').on('click', function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(".mobile-wrap").css('left','-335px');
        }else{
            $(this).addClass('active');
            $(".mobile-wrap").css('left','0');
        }

    });

    $(document).ready(function () {

        $('.payment-form .card-inputs').hide();
        $('.payment-form .Bank-inputs').show();
    })
    $('.payment-form input[type=radio][name=payment_method]').change(function() {
        if (this.value == 'bank') {
            $('.payment-form .card-inputs').hide();
            $('.payment-form .Bank-inputs').show();
        }
        else if (this.value == 'card') {
          
            $('.payment-form .card-inputs').show();
            $('.payment-form .Bank-inputs').hide();
        }
    });

    $(document).on('click', '.filterBtn', function (e) {
        e.preventDefault();
        $(".filter-sidebar").css('left','0');

    });$(document).on('click', '.filterClose', function (e) {
        e.preventDefault();
        $(".filter-sidebar").css('left','-340px');

    });


    var swiper = new Swiper('.search-swiperslider', {
        effect: 'fade',
        centeredSlides: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },

    });
    var swiperauction = new Swiper('.upcoming-auction-swiper', {
        centeredSlides: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },

    });
    //var swiper12 = new Swiper('.client-swiper', {
    //    slidesPerView: 5,
    //    spaceBetween: 30,
    //    // init: false,
    //    pagination: {
    //        el: '.swiper-pagination',
    //        clickable: true,
    //    },
    //    breakpoints: {
    //        1024: {
    //            slidesPerView: 4,
    //        },
    //        768: {
    //            slidesPerView: 3,
    //        },
    //        640: {
    //            slidesPerView: 2,
    //            spaceBetween: 20,
    //        },
    //        320: {
    //            slidesPerView: 1,
    //        }
    //    }

    //});
    var swiper13 = new Swiper('.client2-swiper', {
        slidesPerView: 4,
        spaceBetween: 30,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
            }
        }

    });

    var swiper1 = new Swiper('.cat-swiper', {
        slidesPerView: 5,
        spaceBetween: 10,
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 5,
            },
            992: {
                slidesPerView: 4,
            },
            640: {
                slidesPerView: 3,
            },
            360: {
                slidesPerView: 2,
            }
        }

    });
    var swiper2 = new Swiper('.offers-swiper', {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
           992: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            480: {
                slidesPerView: 1,
            }
        }
    });

    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 5,
        loop: true,
        freeMode: true,
        loopedSlides: 5, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        loop:true,
        loopedSlides: 5, //looped slides should be the same
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs,
        },
    });

    $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').addClass('buttons_added').append('<input type="button" value="+" id="add1"     class="plus" />').prepend('<input type="button" value="-" id="minus1" class="minus"     />');

    $('#product-slider').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        directionNav:false
    });

    var screen = $( window ).width();
    if(screen > 768){
        $(".product-img").imagezoomsl({
            zoomrange: [1, 10],
            magnifierspeedanimate: 10,
            scrollspeedanimate: 1,
            zoomspeedanimate: 1
        });
    }
})(jQuery);

jQuery(function($) {
    var $testProp = $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').find('qty');
    if ($testProp && $testProp.prop('type') != 'date') {
        $('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').addClass('buttons_added').append('<input type="button" value="+" class="plus" />').prepend('<input type="button" value="-" class="minus" />');
        $('input.qty:not(.product-quantity input.qty)').each(function() {
            var min = parseFloat($(this).attr('min'));
            if (min && min > 0 && parseFloat($(this).val()) < min) {
                $(this).val(min);
            }
        });
        $(document).on('click', '.plus, .minus', function() {
            var $qty = $(this).closest('.quantity').find('.qty'),
                currentVal = parseFloat($qty.val()),
                max = parseFloat($qty.attr('max')),
                min = parseFloat($qty.attr('min')),
                step = $qty.attr('step'),
                itemid=$qty.attr(id);
            if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
            if (max === '' || max === 'NaN') max = '';
            if (min === '' || min === 'NaN') min = 0;
            if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;
            if ($(this).is('.plus')) {
                if (max && (max == currentVal || currentVal > max)) {
                    $qty.val(max);
                } else {
                    $qty.val(currentVal + parseFloat(step));
                }
            } else {
                if (min && (min == currentVal || currentVal < min)) {
                    $qty.val(min);
                } else if (currentVal > 0) {
                    $qty.val(currentVal - parseFloat(step));
                }
            }
            $qty.trigger('change');
            console.log(itemid+"-"+itemid.replace("qty"));
            
        });
    }

    
    $(document).on('click', '.currencyitem', function (e) {
        e.preventDefault();
        var Id = $(this).attr("currencyId");
        $.post("/home/updatecurrencyCookie?currecnyId=" + Id, function (data) {

            if (data.status == 1) {

                location.reload();
                //if (data.cookiecurrency != null) {                 

                  //  $(".btn-currency").html("<span class='flag-icon flag-icon-" + data.cookiecurrency.Flag.toLowerCase() + " flag-icon-squared'></span><span class='flag-text'> " + data.cookiecurrency.ShortName +"</span>");
                //}
            }

        })
    });

    /**************************************************************************/
    $('.widget-head').addClass('open');
    $(document).on('click', '.widget-head', function() {
        if($(this).hasClass('open')){
            $(this).parent().find('>.entry-widget').slideUp();
            $(this).removeClass('open');
        }else{
            $(this).parent().find('>.entry-widget').slideDown();
            $(this).addClass('open');
        }
        
    });


    $('img').each(function (i, item) {
        $(item).attr('alt', $(item).attr('src'));
    });

    $('#qmodel').autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: '/Home/GetSearchProducts/',
                method: 'post',
                data: { SearshTest: $("#qmodel").val() },
                dataType: 'json',
                success: function (data) {
                    response(data);
                },
                error: function (err) {
                    alert(err);
                }
            });
        },
        focus: updateTextBox,
        select: updateTextBox
    })
        .autocomplete('instance')._renderItem = function (ul, item) {

            if (item.ProductId != -1) {
            return $("<li class='autocomplete_li'>")
                .append("<a href='/ProductDetails/Details/" + item.ProductId + "/" +item.ProductTitle.replace(/[^a-zA-Z0-9-_]/g, '')+"'><img class='Autocompleteimg' src='" + item.ProductImage + "' alt=" + item.ProductTitle + " /><span>" + item.ProductTitle + "</span></a>")

                .appendTo(ul);
            }
            else {

                return $("<li class='autocomplete_li' style='text-align: center;'>")
                    .append("<span style='text-align:center;cursor:default;width:100%'> NO Record Found</span>")

                    .appendTo(ul);
            }
        };
    function updateTextBox(event, ui) {
        $(this).val(ui.item.Name);
        return false;
    }


    $(document).on('click', '.searchajx', function () {
        window.location.href = '/Home/SearchResult/' + "?q=" + $("#q").val();
        return false;
    });

   
    numbersOnly();
    floatNumber();

});
});
var formatNumber = function formatNumber(num) {
    return (
        num
            .toFixed(2) // always two decimal digits
            .replace(',', '.') // replace decimal point character with ,
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    ) // use . as a separator
}

