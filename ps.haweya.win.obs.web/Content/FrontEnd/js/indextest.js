﻿(function ($) {

    var expandChildTree = function () {
        $(document).on('click', '.cat-arrow', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).parent().next().slideToggle();
        });
    };

    jQuery(document).ready(function () {

        expandChildTree();

        FetchImages();

        //Listcontent
        $('#auctionTimer').syotimer({
            year:@ViewBag.Year,
        month: @ViewBag.Month,
        day: @ViewBag.Day,
        hour: @ViewBag.Hour,
        minute: @ViewBag.Minute,
    });
});
$("#slider-range").slider({
    range: true,
    min: @(ViewBag.MinPrice != null ? ViewBag.MinPrice : 500),
max: @(ViewBag.MaxPrice != null ? ViewBag.MaxPrice : 500),
values: [ @(ViewBag.MinPrice != null ? ViewBag.MinPrice : 0), @(ViewBag.MaxPrice != null ? ViewBag.MaxPrice / 2 : 500) ],
slide: function(event, ui) {
    $("#amount").val("SR " + ui.values[0] + " - SR " + ui.values[1]);
}
            });
$("#amount").val("SR " + $("#slider-range").slider("values", 0) +
    " - SR " + $("#slider-range").slider("values", 1));

$('#q').autocomplete({
    minLength: 1,
    source: function (request, response) {
        $.ajax({
            url: '@Url.Action("GetSearchProducts","Home")',
            method: 'post',
            data: { SearshTest: $("#q").val() },
            dataType: 'json',
            success: function (data) {
                response(data);
            },
            error: function (err) {
                alert(err);
            }
        });
    },
    focus: updateTextBox,
    select: updateTextBox
})
    .autocomplete('instance')._renderItem = function (ul, item) {
        return $("<li class='autocomplete_li'>")
            .append("<a href='@Url.Action("Details", "ProductDetails")?id=" + item.ProductId + "'><img class='Autocompleteimg' src='" + item.ProductImage + "' alt=" + item.ProductTitle + "/><span>" + item.ProductTitle + "</span></a>")

            .appendTo(ul);
    };
function updateTextBox(event, ui) {
    $(this).val(ui.item.Name);
    return false;
}
        }) (jQuery);




$(document).on('click', '.Viewcount', function () {

    binalldata();

})
$(document).on('click', '.sorting', function () {
    binalldata();

})


$(document).on('click', '.Notifybtn', function () {
    var id = $("#AUCTIONID").val();
    //$.blockUI({  message: '<h5> Processing ... <img src="/Content/FrontEnd/images/loader.gif" /></h5>', css:{color:'#5480c1',border:'none',"padding-top":'10px',width:'20%',left:'40%'} });
    $('div.upcoming-auction-wrap').block({ message: '<h5> Processing ... <img src="/Content/FrontEnd/images/loader.gif" /></h5>', css: { backgroundColor: 'transparent', color: '#cbd9f6', border: 'none', "padding-top": '10px', width: '20%', left: '40%' } });
    $.get("@Url.Action("notifyUser")?id=" + id, function (result) {
        //$.unblockUI();
        $('div.upcoming-auction-wrap').unblock();
        ShowMessage(result.msg);

    }); return false;
});

$(document).on('click', '.searchcategory', function (e) {

    e.preventDefault();
    e.stopPropagation();

    var id = $(this).attr("myid");
    $("#id").val(id);
    $("#q").val("");
    binalldata();

    $('.searchcategory').removeClass('active');

    $(this).toggleClass('active');
})

$(document).on('change', '.filterselect', function () {
    $("#q").val("");
    var id = $(this).val();
    $("#id").val(id);
    //location.href = "";
    binalldata();
    return false;
})

$(document).on('click', '.checkesfilter', function () {

    binalldata();

})

$(document).on('click', '#serchmanufactuerbtn', function () {
    bindbrandsmanufactuer();
    return false;
})
$(document).on('click', '#filterbtn', function () {
    binalldata();
    return false;
})

function bindbrandsmanufactuer() {
    $("#ManufacturerFilter").empty();
    $("#ManufacturerFilter").append("<div class='col-md-12' style='text-align:center'><img src='../../Content/FrontEnd/images/loader.gif' /></div>");
    $.get("@Url.Action("GetManufacturersListHomeajax")?manufactuertxt=" + $("#serchmanufactuertxt").val(), function (result) {

        var divcontent = "";

        $.each(result, function (i, itm) {
            divcontent += "<div class='custom-control custom-checkbox'><input type='checkbox' class='custom-control-input' id='check_" + itm.ManufacturersId + "' name='check_" + itm.ManufacturersId + "'><label class='custom-control-label' for='check_" + itm.ManufacturersId + "'>" + itm.ManufacturersName + " <small>(" + itm.ProductCount + ")</small></label></div>";

        });
        $("#ManufacturerFilter").empty(); $("#ManufacturerFilter").append(divcontent);
    });
}

function binalldata() {


    var allVal = '';
    $("#ManufacturerFilter  div> input").each(function () {

        var type = $(this).prop("type");
        if (type == "checkbox") {
            if ($(this).prop("checked")) {
                var cbxname = $(this).attr('name');
                allVal += cbxname + ',';
            }
        }
    });



    var allcondetionVal = '';
    $("#condtionlistdiv  div> input").each(function () {

        var type = $(this).prop("type");
        if (type == "checkbox") {
            if ($(this).prop("checked")) {
                var cbxname = $(this).attr('name');
                allcondetionVal += cbxname + ',';
            }
        }
    });

    var amount = $("#amount").val();
    $('html, body').animate({
        scrollTop: $("#Listcontent").offset().top
    }, 1000);
    $("#Productslist").empty();
    $("#Productslist").append("<div class='col-md-12' style='text-align:center'><img src='../../Content/FrontEnd/images/loader.gif' /></div>");
    var pageindex = $("#currentpage").val();
    $.get("@Url.Action("ProductListAjax")?id=" + $("#id").val() + "&q=" + $("#q").val() + "&DirectCheck=" + $("#DirectCheck").prop("checked") + "&AuctionCheck=" + $("#AuctionCheck").prop("checked") + "&allVal=" + allVal + "&amount=" + amount + "&sorting=" + $("#sorting").val() + "&Viewcount=" + $("#Viewcount").val() + "&condetion=" + allcondetionVal + "&page=" + pageindex, function (result) {
        console.log(result);
        var divcontent = "";
        var count = 0;
        $.each(result.items, function (i, item) {
            count++;
            var itmcontent = "";
            var itmcontent = "<div class='col-lg-4 col-md-6'><div class='auction-item'><div class='auction-img bg-image' style='background-image: url(" + item.ProductImage + ")'>" +
                "<a href='@Url.Action("Details", "ProductDetails")?id=" + item.ProductId + "'></a> <div class='overlay'> </div></div><div class='entry-auction'>" +
                    "<div class='auction-info'>" +
                    "<span class='auction-cat'><a href='#'>" + item.CategoryName + "</a> </span>" +
                    "<h3  ><a href='@Url.Action("Details", "ProductDetails")?id=" + item.ProductId + "'>" + item.ProductTitle + "</a> </h3>" +
                        "<div class='auction-meta'> <ul class='list-inline'> " +
                        "<li class='list-inline-item'" + (item.bidscount == "" ? "style='display:none'" : "") + "> " + item.PurchaseTimes + "<span> " + item.bidscount + " " + item.Plabel + "</span></li>" +
                        "<li class='list-inline-item'>Qty: <span>" + item.AvailableQuantity + " item</span></li>" +
                        " <li class='list-inline-item'>" + item.PriceLabel + ": <span>" + item.TotalPrice + " SR</span></li>" +
                        "</ul> </div> </div>" +
                        "<div class='auction-footer'>" +
                        "<span><i class='" + (item.ProductLocation != '' ? 'svg-location' : '') + " '></i>" + item.ProductLocation + "</span>" +
                        "<a href='#'  myid='" + item.ProductId + "' class='favitm'> " + (jQuery.inArray(item.ProductId, result.UserFavorite) !== -1 ? "<i class='fas fa-heart'></i>" : "<i class='far fa-heart'></i>") + " </a>";
            if (item.PricingType != "Auctionable") {
                if (item.AvailableQuantity <= 0) {
                    itmcontent += " <label class='lbl'>Sold Out</label> ";
                }
                else {
                    itmcontent += "<a class='" + (item.bidscount != '' ? 'placebid' : 'submitcart') + "'  href='@Url.Action("Details", "ProductDetails")?id=" + item.ProductId + "'  data-id='" + item.ProductId + "'   data-baseprice='" + item.TotalPrice + "' myid='" + item.ProductId + "'>" + item.Purchasebtn + "</a>";
                }
            }
            else {
                if (item.BidStartDate != null && moment(item.BidStartDate) <= moment(new Date()) && moment(item.DeadLine) >= moment(new Date())) {
                    itmcontent += "<a class='" + (item.bidscount != '' ? 'placebid' : 'submitcart') + "'  href='@Url.Action("Details", "ProductDetails")?id=" + item.ProductId + "' myid=" + item.ProductId + ">" + item.Purchasebtn + "</a>";
                }
                else {
                    if (item.BidStartDate != null && moment(item.BidStartDate) > new Date()) {
                        itmcontent += "<label class='lbl'>Comming Soon</label>";
                    }
                    else {
                        itmcontent += "<label class='lbl'>Closed</label>";
                    }


                }

            }
            itmcontent += "</div></div></div></div>";
            divcontent += itmcontent;



        });
        if (count == 0) {

            $("#Productslist").empty(); $("#Productslist").append("<div class='col-md-12'> <div class='alert alert-warning'>There is no result to view </div></div>");
        }
        else {
            $("#Productslist").empty(); $("#Productslist").append(divcontent);
        }



    });
}
function FetchImages() {

    $('.img-loading').each(function () {
        var url = $(this).attr('data-img');

        var current = $(this);

        $.get(url, function () {

            current.css('background-image', 'url(' + url + ')');
            current.removeClass('img-loading');
        });

    });
}