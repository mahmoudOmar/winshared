﻿$(document).on('click', '.favitm', function () {

    var id = $(this).attr("myid");

    var selector = $(this);
    //  var l = Ladda.create(this);

    //console.log(l);
    $(this).block({ message: '<img src="/Content/FrontEnd/images/loading.gif" />', css: { backgroundColor: 'none', color: '#007bff', border: 'none', height: '100%', "padding-top": '0px', "margin-top": '-5px', width: '100%', left: '0', } });
    $.get('/Home/AddtoFavorite/' + "?id=" + id, function (result) {

        console.log(result);
        if (result.status == 1) {

            selector.empty();
            selector.html("<i class='fas fa-heart'></i>");

        }
        else {
            selector.html("  <i class='far fa-heart'></i>");

        }
        ShowMessage(result.msg);
        $(this).unblock();
    }); return false;
});

$(document).on("click", ".placebid", function () {
    $("#errorlbl").html("");
    $(this).block({ message: '<img src="/Content/FrontEnd/images/loading.gif" height="40" />', css: { backgroundColor: 'none', color: '#007bff', border: 'none', "padding-top": '0px', "margin-top": '-5px', width: '100%', left: '0', } });
    var productid = $(this).attr("myid");
    $("#bidproductid").val(productid);

    //alert(placeURL);
    // var place = (PlaceURL + '?ProductID=' + productid);
    //alert(place);
    $.get('/Home/GetplaceData/?ProductID=' + productid, function (result) {
        if (result.status != 0) {

            $("#bidmaterialname").val(result.Title);
            $("#bidDifference").val(result.BidDifference);

            $("#bidqty").val(result.Qty);
            $("#bidlastammount").val(result.Price);
            if (result.IsInspect == 1) {
                $("#inspectionvaluehdn").val(1);
                $("#isinspection").show();
                //$("#bidammount").hide();
            }
            else {
                $("#inspectionvaluehdn").val(0);
                //$("#bidammount").show();
                $("#isinspection").hide();
            }


            $("#mdlPlacebid").modal();
          
        }
        else { ShowMessage(result.msg) }

        $(".blockUI").hide(); 
    })


  
    return false;
});
$(document).on("click", "#submitbid", function () {



    var amount = $("#Curentbidamount").val();
    var productid = $("#bidproductid").val();
    var isinspect = $("#inspectionvaluehdn").val();
    var needinspection = 0;

    console.log(isinspect, needinspection);
    if (isinspect == 1) {

        needinspection = $("#inspectionvalue").val();

        if (needinspection == -1) {
            $("#errorlbl").html("please select if you need inspection or not");
            return false;
        }
    }
    //console.log(isinspect, needinspection);



    if ($("#Curentbidamount").val() != "") {

        if (isNaN(parseFloat(amount))) {
            $("#errorlbl").html("please add your bid ammount");
            return false;
        }
        else {
            var pastamount = $("#bidlastammount").val();
            var bidDifference = $("#bidDifference").val();
            console.log(pastamount, amount);
            if (parseFloat(amount) < (parseFloat(pastamount) + parseFloat(bidDifference))) {
                ShowMessage("e:amount must be more than (last amount plus Bid Difference  )");
                $("#errorlbl").html("amount must be more than (last amount plus Bid Difference  )");
                return false;
            }

            $("#errorlbl").html("");
            $('div#biddialog').block({ message: '<h5> Processing ... <img src="/Content/FrontEnd/images/loader.gif" /></h5>', css: { backgroundColor: '#ebebeb', color: '#007bff', border: 'none', "padding-top": '10px', width: '60%', left: '20%' } });
            $.get('~/Home/Addplace/?ProductID=' + productid + "&Amount=" + amount + "&&Inspection=" + needinspection, function (result) {
                if (result.status != 0) {
                    $("#closebtn").click();
                }

                $('div#biddialog').unblock();
                ShowMessage(result.msg);
            })
        }
    }
    else {
        $("#errorlbl").html("please add your bid ammount");
        ShowMessage("e:please add your bid ammount");
        $("#Curentbidamount").focus();
    }
    return false;

});

$(document).on("click", ".btnclose", function () {

    $("#mdladdtocart").modal("hide");
});

$(document).on("click", ".submitcart", function (e) {

    e.preventDefault();
    $(this).block({ message: '<img src="/Content/FrontEnd/images/loading.gif" height="40" />', css: { backgroundColor: 'none', color: '#007bff', border: 'none', "padding-top": '0px', "margin-top": '-5px', width: '100%', left: '0', } });
    var qty = 1;
    //var id = $(this).attr("myid");
    var id = this.dataset.id;
    var baseprice = this.dataset.baseprice;

    $.post('/cart/Add/', { ItemId: id, Quantity: 1, UnitPrice: baseprice }, function (result) {
        //console.log(result.count, result.totalprice);
        //updatecartcount(result.count, result.totalprice);
    
        if (result.message.indexOf("e:") != -1) {
           
            $("#mdladdtocartbefor").modal();

            //return false;
        }

        else {
          
            $("#mdladdtocart").modal();
            // return false;
        }
        $(".blockUI").hide();
    });
  
 
    //return false;
});