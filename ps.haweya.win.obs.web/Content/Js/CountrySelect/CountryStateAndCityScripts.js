﻿var CityList = [];
var StateList = [];
var CountryList = [];
var groupbyState = [];
var groupbyCountry = [];
var url = window.location.pathname.split("/");
var Action = url[1];
//var CityName = (Action == "Selling") ? "city" : "City";
//var StateName = (Action == "Selling") ? "state" : "State";
//var CountryName = (Action == "Selling") ? "country" : "Country";
var CityName = "City";
var StateName ="State";
var CountryName ="Country";
var isSelectState = false;
var isSelectCity = false;
$(document).ready(function () {
    function GetCountryData() {
        $.ajax({
            url: '/Content/JS/JsonData/countries.json',
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                CountryList = data;
                var Country = [];
                $.each(data, function (i, item) {
                    
                    Country.push({ "id": item.id, "text": item.name });
                });
                $("#" + CountryName).select2({
                    data: Country,
                    placeholder: "Select Country",
                    width: "100%",
                });
            },
            failure: function (response) {
              
                $('#result').html(response);
            }
        });
    }
    GetCountryData();
    GetCountryDatanames();
    function GetStateData() {
        $.ajax({
            url: '/Content/JS/JsonData/states.json',
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                StateList = data;
                var State = [];
                $.each(data, function (i, item) {
                    if (!State[item.country_id]) State[item.country_id] = [];
                    State.push({ "id": item.id, "text": item.name });
                });
                if (url[1] == "UserDetails") {
                    $("#" + StateName).select2({
                        data: State,
                        placeholder: "Select State",
                        width: "100%",
                    });
                    isSelectState = true;
                }
                
            },
            failure: function (response) {
                $('#result').html(response);
            }
        });
       
    }
    GetStateData();
    function ErrorPageTrigger() {
        if (url[1] === "Account") {
            if ($("#Country").val() !== '') {
                FilterByCountry(parseInt($("#Country").val()));

                if ($("#State").val() != "") {
                    $("#State").val(parseInt($("#State").val())).trigger("change.select2");
                    FilterByState();
                }
                if ($("#City").val() != "") {
                    $("#City").val(parseInt($("#City").val())).trigger("change.select2");
                }
            }
        }
    }
    function GetCityData() {
        $.ajax({
            url: '/Content/JS/JsonData/cities.json',
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                CityList = data;
                var City = []
                $.each(data, function (i, item) {
                    if (!City[item.Stateid]) City[item.Stateid] = [];
                    City.push({ "id": item.id, "text": item.text });
                });
                if (url[1] == "UserDetails") {
                    $("#" + CityName).select2({
                        data: City,
                        placeholder: "Select City",
                        width: "100%",
                    });
                    isSelectCity = true;
                }
                ErrorPageTrigger();
            },
            failure: function (response) {
                $('#result').html(response);
            }
        });
    }
    GetCityData();
    
    $("#" + CityName).focus(function () {
        if ($("#" + CountryName).val() === "" && $("#" + StateName).val() === "") {

            Showmessage("e:Please Select Country And State")
            //$.notify("Please Select Country And State", "error");
        }
        if ($("#" + CountryName).val() !== "" && $("#" + StateName).val() === ""){
            //$.notify("Please Select State", "error");
            Showmessage("e:Please Select State And State")
        }
    });
    $("#" + StateName).focus(function () {
        if ($("#" + CountryName).val() === "" && $("#" + StateName).val() == "") {
            $.notify("Please Select Country", "error");
        }
    });
    $("#" + CountryName).change(function () {

        var countryId = parseInt($("#" + CountryName).val());
        var eVal = (isNaN(countryId)) ? 0 : countryId;
        if (eVal !== 0) {
          
            FilterByCountry(countryId);
        }
    });   
    //$("#" + CityName).on('change', function () {
    //    FilterByCity();
    //})
    //function FilterByCity() {
    //    var cityId = $("#" + CityName).val();
    //    var stateId = 0;
    //    $.each(CityList, function (row, col) {
    //        if (col.id == cityId) {
    //            stateId = col.Stateid;
    //            $("#" + StateName).val(col.StateId).trigger('change.select2');
    //        }
    //    })
    //    $.each(StateList, function (row, col) {
    //        if (col.id == stateId) {
    //            $("#" + CountryName).val(col.CountryId).trigger('change.select2');
    //        }
    //    })
    //}
    if(url[1] !== "UserDetails") {
        $("#" + CountryName).change(function () {
            var countryId = parseInt($("#" + CountryName).val());
            var eVal = (isNaN(countryId)) ? 0 : countryId;
            if (eVal !== 0) {
                FilterByCountry(countryId);
            }

        });   
    }
   
   
    function FilterByCountry(countryId) {
        var States = [];
 
        $.each(StateList, function (i, item) {
            if (item.country_id == countryId) {
                States.push({ "id": item.id, "text": item.name });
            }

        });
        //comented by mahmoud omar

        $("#" + StateName).select2({
            data: States,
            placeholder: "Select State",
            width: "100%",
        });
        isSelectState = true;


        ////add by mahmoudomar to bind cities from country direct without state
        //var Cities = [];
        //$.each(States, function (i, itm) {
        //    var stateId = itm.id;
            
        //    $.each(CityList, function (i, item) {
        //        if (item.Stateid == stateId) {
        //            Cities.push({ "id": item.id, "text": item.text });
        //        }
        //    });
           
        //    if (Cities.length == 0) {
        //        Cities.push({ "id": "0", "text": "Other" });
        //    }

        //});
        //$("#" + CityName).select2({
        //        data: Cities,
        //        placeholder: "Select City",
        //        width: "100%",
        //    }); console.log(Cities);
        //isSelectCity = true;
     
        //$("#" + CityName).trigger('change');
        ////
    }
    if (url[1] != "UserDetails") {
        $("#" + StateName).change(function () {
            FilterByState();
        });
    }
    $("#" + StateName).change(function () {
        FilterByState();
    });
    function FilterByState() {
        var stateId = $("#" + StateName).val();
        var Cities = [];
        $.each(CityList, function (i, item) {
            if (item.Stateid == stateId) {
                Cities.push({ "id": item.id, "text": item.text });
            }
        });
        if (Cities.length == 0) {
            Cities.push({ "id": "0", "text": "Other" });
        }
        $("#" + CityName).select2({
            data: Cities,
            placeholder: "Select City",
            width: "100%",
        });
        isSelectCity = true;
        $("#" + CityName).trigger('change');
        //$("#select2-City-container").html($("#" + CityName).select2('data')[0].text);
    }
    if (url[1] === "Account") {
        var validationSummary = $("#errorData").html();
        if (validationSummary !== '') {
            FilterByCountry(parseInt($("#Country").val()));
        }
        if ($("#State").val() != "") {
            var States = [];
            $.each(StateList, function (i, item) {
                if (item.country_id == countryId) {
                    States.push({ "id": item.id, "text": item.name });
                }

            });
            $("#" + StateName).select2({
                data: States,
                placeholder: "Select State",
                width: "100%",
            });
            $("#State").val(parseInt($("#State").val())).trigger("change.select2");
        }
           
        if ($("#City").val() != "") {
            var stateId = $("#" + StateName).val();
            var Cities = [];
            $.each(CityList, function (i, item) {
                if (item.Stateid == stateId) {
                    Cities.push({ "id": item.id, "text": item.text });
                }
            });
            if (Cities.length == 0) {
                Cities.push({ "id": "0", "text": "Other" });
            }
            $("#" + CityName).select2({
                data: Cities,
                placeholder: "Select City",
                width: "100%",
            });
        }
        $("#City").val(parseInt($("#City").val())).trigger("change.select2");
    }


    function GetCountryDatanames() {
        $.ajax({
            url: '/Content/JS/JsonData/countries.json',
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                CountryList = data;
                var Country = [];
                $.each(data, function (i, item) {
                    Country.push({ "id": item.id, "text": item.name });
                });
              
            },
            failure: function (response) {
                $('#result').html(response);
            }
        });
    }
})
   