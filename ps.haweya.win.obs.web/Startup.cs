﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

using Hangfire;
using Hangfire.SqlServer;

using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.security;
using ps.haweya.win.obs.web.App_Start;

[assembly: OwinStartup(typeof(ps.haweya.win.obs.web.Startup))]

namespace ps.haweya.win.obs.web
{
    public class Startup
    {
        //private IEnumerable<IDisposable> GetHangfireServers()
        //{
        //    GlobalConfiguration.Configuration
        //        .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
        //        .UseSimpleAssemblyNameTypeSerializer()
        //        .UseRecommendedSerializerSettings()
        //        .UseSqlServerStorage("Server=.; Database=HangfireTest; Integrated Security=True;", new SqlServerStorageOptions
        //        {
        //            CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
        //            SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
        //            QueuePollInterval = TimeSpan.Zero,
        //            UseRecommendedIsolationLevel = true,
        //            UsePageLocksOnDequeue = true,
        //            DisableGlobalLocks = true
        //        });

        //    yield return new BackgroundJobServer();
        //}

        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                CookieName = "cookie",
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Home/Login"),
                LogoutPath = new PathString("/Home/Logout"),
                ExpireTimeSpan = TimeSpan.FromMinutes(90)
            });

            GlobalConfiguration.Configuration.UseSqlServerStorage("obsDBNative");
            app.UseHangfireDashboard("/Hangfire", new DashboardOptions()
            {
                Authorization = new[] { new HangfireAthorizationFilter() }
            });


            app.UseHangfireServer();
            new ReportDeliveryManager().StartCronJobs();


          

        }

        
    }
}
