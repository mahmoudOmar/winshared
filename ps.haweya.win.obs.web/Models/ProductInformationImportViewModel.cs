﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;

namespace ps.haweya.win.obs.web.Models
{
    public partial class ProductInformationImportViewModel
    {



        public object _Category
        {
            set
            {
                value = value ?? "";
                var categoryobj = CacheManager.Categories.FirstOrDefault(x => x.CategoryName.ToLower().Equals(value.ToString().ToLower()));
                this.Category = categoryobj == null ? 0 : categoryobj.CategoryId;


            }
        }
        public object _ProductBrands
        {

            set
            {
                value = value ?? "";
                var brandsobj = CacheManager.OBSManufacturers.FirstOrDefault(x => x.ManufacturersName.ToLower().Equals(value.ToString().ToLower()));
                this.ProductBrands = brandsobj == null ? 0 : brandsobj.ManufacturersId;


            }
        }

        //public List<ProductDetailImportViewModel> ProductDetails{ get; set; }
        public long ProductId { get; set; }
        public string ProductTitle { get; set; }
        public string ProductLocation { get; set; }
        public string Quantity { get; set; }
        public System.DateTime DeadLine { get; set; }
        public string ProductImage { get; set; }
        public int BidCount { get; set; }
        public double Price { get; set; }
        public int Category { get; set; }
        public int ProductBrands { get; set; }
        public System.DateTime CreateDate { get; set; }
        public double BidDifference { get; set; }
        public string PricingType { get; set; }
        public string Username { get; set; }
        public bool isActive { get; set; }
        public bool IsWinActive { get; set; }
        public string SuggestionByWin { get; set; }
        public int AvailableQuantity { get; set; }
        public int DeadLineInDays { get; set; }
        public bool isPrivate { get; set; }
        public bool IsSold { get; set; }
        public double ShipmentCharges { get; set; }
        public bool IsShipment { get; set; }
        public bool IsInspect { get; set; }
        public string OtherCategory { get; set; }
        public string OtherManufacturer { get; set; }
        public Nullable<System.DateTime> ExtendDeadline { get; set; }
        public bool isExtendDeadline { get; set; }
        public bool IsDelete { get; set; }
        public bool IsBulkUpload { get; set; }
        public int DisciplineId { get; set; }
        public int CommodityId { get; set; }
        public int SubCommodityId { get; set; }
        public Nullable<System.DateTime> ManufacturingDate { get; set; }
        public int UOMId { get; set; }
        public string EndUser { get; set; }
        public string SupplierName { get; set; }
        public string CertificatesOrDataSheet { get; set; }
        public string WINAssement { get; set; }
        public string WINAssementby { get; set; }
        public string Repairable { get; set; }
        public string UsedEquipmentOrApplication { get; set; }
        public double Age { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public string SellerName { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public double MarketPrice { get; set; }
        public string SellerMaterialId { get; set; }
        public Nullable<System.DateTime> PreviousDeadline { get; set; }
        public bool IsExtendOverwrite { get; set; }
        public string BatchNumber { get; set; }
        public string Description { get; set; }
        public string Condition { get; set; }
        public string SpecialInstructions { get; set; }
        public string Contact { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Country { get; set; }
        public double WholesaleValue { get; set; }
        public double RetailValue { get; set; }
        public double ClaimPrice { get; set; }
        public double ReservePrice { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public double MinimumSellingPrice { get; set; }
        public string SellerState { get; set; }
        public string SellerCity { get; set; }
        public string SellerCountry { get; set; }
        public object _Country
        {
            set
            {
                value = value ?? "";
                var countryobj = CacheManager.Countries.FirstOrDefault(x => x.name.ToLower().Equals(value.ToString().ToLower()));
                this.Country = countryobj == null ? 0 : countryobj.id;
            }
        }

        public string Keywords { get; set; }
        //}

        //public ProductDetailViewModel ProductDetail { get; set; }
        //public List<OBSBidDetailViewModel> OBSBidDetails { get; set; }
        //public  OBSManufacturerViewModel OBSManufacturer { get; set; }
        //public OBSCommodityViewModel OBSCommodity { get; set; }
        //public OBSDisciplineViewModel OBSDiscipline { get; set; }
        //public OBSSubCommodityViewModel OBSSubCommodity { get; set; }
        //public OBSUOMViewModel OBSUOM { get; set; }
    }
}
