﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ps.haweya.win.obs.web.Models
{
    public class PDFPagesData
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string PurchaseOn { get; set; }
        public string ProductTitle { get; set; }
        public string MinimumSellingPrice { get; set; }
        public string BuyQuantity { get; set; }
        public string SubTotals { get; set; }
        public string Totals { get; set; }
        public string InvoiceNo { get; set; }
        public string ImagePath { get; set; }
        public string ShipmentCost { get; set; }

    }
}