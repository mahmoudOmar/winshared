﻿using Canonicalize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace ps.haweya.win.obs.web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
           

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
               name: "Home",
               url: "",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, title = UrlParameter.Optional },
                 namespaces: new[] { "ps.haweya.win.obs.web.Controllers" }
           );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}/{title}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, title= UrlParameter.Optional },
                  namespaces: new[] { "ps.haweya.win.obs.web.Controllers" }
            );

            routes.Canonicalize().Lowercase();
        }
       
    }
}
