﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace ps.haweya.win.obs.web.App_Start
{
    public  class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new Bundle("~/Content/scriptsjs")
                .Include("~/Content/FrontEnd/js/popper.min.js")
                .Include("~/Content/FrontEnd/js/bootstrap.min.js")
            .Include("~/Content/FrontEnd/js/jquery.flexslider-min.js")
           .Include("~/Content/FrontEnd/js/jquery.syotimer.js")
           .Include("~/Content/FrontEnd/js/swiper.min.js")
           .Include("~/Content/FrontEnd/js/zoomsl.min.js")
            .Include("~/Content/FrontEnd/js/wow.min.js")
            .Include("~/Content/FrontEnd/js/bootstrap-select.min.js")
            .Include("~/Content/FrontEnd/js/jquery-ui.js")
            .Include("~/Content/plugins/toastr/build/toastr.min.js")
            .Include("~/Content/plugins/select2/select2.full.min.js")
            
            .Include("~/Content/FrontEnd/js/jquery.blockUI.min.js")
            .Include("~/Content/plugins/general/moment/min/moment.min.js")
            .Include("~/Content/FrontEnd/js/main_min.js")
            );







            bundles.Add(new StyleBundle("~/Content/stylescss")
                .Include("~/Content/FrontEnd/css/font-awesome/css/all.min.css", new  CssRewriteUrlTransform()).Include(
                      "~/Content/FrontEnd/css/animate.css",
                      "~/Content/FrontEnd/css/flexslider.css",
                      "~/Content/FrontEnd/css/swiper.min.css",
                      "~/Content/FrontEnd/css/bootstrap.min.css",
                      "~/Content/FrontEnd/css/magiczoomplus.css",
                      "~/Content/FrontEnd/css/jquery-ui.css",
                      "~/Content/FrontEnd/css/bootstrap-select.min.css",
                        "~/Content/FrontEnd/css/toastr.min.css",
                         "~/Content/FrontEnd/css/select2.min.css"
                     
                      ));
            
            BundleTable.EnableOptimizations = true;
        }
    }
}