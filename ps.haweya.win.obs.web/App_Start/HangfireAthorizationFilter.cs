﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire.Annotations;
using Hangfire.Dashboard;

namespace ps.haweya.win.obs.web.App_Start
{
    public class HangfireAthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            if (HttpContext.Current.User != null)
            {
                if (HttpContext.Current.User.IsInRole("Admin"))
                {
                    return HttpContext.Current.User.Identity.IsAuthenticated;
                }
            }
            return false;
        }
    }
}