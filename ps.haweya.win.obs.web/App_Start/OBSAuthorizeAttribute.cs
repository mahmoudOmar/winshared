﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.security;

namespace ps.haweya.win.obs.web.App_Start
{
    public class OBSAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            var allowedRoles = (Roles ?? "").Split(',').Select(t => t.Trim().ToLower()).Where(t => t != "");

            var UserIdentity = httpContext.GetOwinContext().Authentication.User.Identity;

            if (!UserIdentity.IsAuthenticated)
            {
                return false;
            }

            // authenticated users regardless the granted roles
            if (allowedRoles.Count() == 0)
            {
                return true;
            }

            var defaultUserRole = ((ClaimsIdentity)UserIdentity).Claims.SingleOrDefault(c => c.Type == "DEFAULT_ROLE").Value.ToLower();

            var secondaryRoles = ((ClaimsIdentity)UserIdentity).Claims
                .Where(c => c.Type == ClaimTypes.Role && c.Value.ToLower() != defaultUserRole)
                .Select(c => c.Value.ToLower());

            if (allowedRoles.Contains(defaultUserRole))
            {
                return true;
            }

            // // authenticated users and role must matched.
            if (allowedRoles.Intersect(secondaryRoles).Count() != 0)
            {
                string area = Convert.ToString(httpContext.Request.RequestContext.RouteData.DataTokens["area"]).ToLower();
                string controller = Convert.ToString(httpContext.Request.RequestContext.RouteData.Values["controller"]).ToLower();
                string action = Convert.ToString(httpContext.Request.RequestContext.RouteData.Values["action"]).ToLower();

                var link = CacheManager.Links.SingleOrDefault(t => t.Area.ToLower().Equals(area) &&
                                                                 t.Controller.ToLower().Equals(controller) &&
                                                                 t.Action.ToLower().Equals(action));
                if (link != null)
                {
                    var userLinkManager = new UserLinkManager();
                    var hasAccess = userLinkManager.GetByUserId(UserIdentity.GetUserId(), link.Id) != null;
                    return hasAccess;
                }

            }

            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var UserIdentity = filterContext.HttpContext.User.Identity;

            if (!UserIdentity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                var url = filterContext.HttpContext.Request.UrlReferrer;

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "UnAuthorized",
                    area="",
                    url = url == null ? null : url.AbsoluteUri 
                }));
            }
        }

    }
}