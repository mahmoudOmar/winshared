﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.web.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
namespace ps.haweya.win.obs.web.Areas.Technical.Controllers
{
    [OBSAuthorizeAttribute(Roles = "Technical")]
    public class BuyerOrdersController : BaseTechnicalController
    {
        double vatvalue = CacheManager.Setting.VatValue ?? 0;
        string vattext = CacheManager.Setting.VatText;
        public ActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var CountryList = new CountryManager().Get();

            var items = new BuyerOrderManager().GetWithIncloudsTechnical("Status", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new BuyerOrderManager().GetWithIncloudsTechnicalCount(query);

            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.ID,
                       FullName = x.AspNetUser.FullName,
                       ProfilePhoto = x.AspNetUser.ProfilePhoto,
                       Email = x.AspNetUser.Email,
                       CountryId = x.AspNetUser.CountryId,
                       ItemsCount = x.CartItems.Count(),
                       //  TotalPrice = ((x.TotalPrice ?? 0) + ((x.TotalPrice ?? 0) * 0.05)).ToString("N0"),
                       TotalPrice = Convert.ToDouble((x.CartItems.Sum(c => (c.ProductInformation.UnitPrice * c.Qty) + (c.ShippingValue ?? 0) + (c.InspectionValue ?? 0))) + (x.CartItems.Sum(c => (c.ProductInformation.UnitPrice * c.Qty) + (c.ShippingValue ?? 0) + (c.InspectionValue ?? 0))) * vatvalue).ToString("N0"),
                       payment_method = x.payment_method ?? "",
                       StatusID = x.StatusID,
                       StatusText = x.Status.Name,
                       x.InsertDate,
                       DeliveryType=x.DeliveryType??0,
                       Country = CountryList.Where(t => t.id == int.Parse(x.AspNetUser.CountryId ?? "0")).FirstOrDefault()
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OrderByStatus()
        {

            return Json(new BuyerOrderManager().GetOrdersByStatus(), JsonRequestBehavior.AllowGet);
        }

        // GET: Admin/BuyerOrders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            BuyerOrderViewModel buyerOrder = new BuyerOrderManager().GetByid(Convert.ToInt32(id));

            if (buyerOrder == null)
            {
                return HttpNotFound();
            }

            return View(buyerOrder);
        }

        public ActionResult Invoice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BuyerOrderViewModel buyerOrder = new BuyerOrderManager().GetByid(Convert.ToInt32(id));
            if (buyerOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.Bank = new BankAccountsManager().Get().FirstOrDefault();
            return View(buyerOrder);
        }

        public JsonResult GetInspectionItemsByOrderID(int id)
        {
            CartItemsManager cartItemsManager = new CartItemsManager();
            var items=cartItemsManager.GetInspectionItemsByOrderId(id).Select(x => new
            {
                productimg = x.ProductInformation.ProductImg[0],
                ProductTitle = x.ProductInformation.ProductTitle,
                Quantity = x.Qty,
                UnitPrice = x.ProductInformation.UnitPrice,
                ID = x.ID,
                ProductID = x.ProductID

            }).ToList(); 
            
             
            return Json(items, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetshippmentsItemsByOrderID(int id)
        {
            CartItemsManager cartItemsManager = new CartItemsManager();
            var items = cartItemsManager.GetShipmentItemsByOrderId(id).Select(x => new
            {
                productimg = x.ProductInformation.ProductImg[0],
                ProductTitle = x.ProductInformation.ProductTitle,
                Quantity = x.Qty,
                UnitPrice = x.ProductInformation.UnitPrice,
                ID = x.ID,
                ProductID = x.ProductID

            }).ToList();


            return Json(items, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult UpdateOrderStatus(int orderId, int newStatus, string note, int oldStatusid)
        {

            int ID = new OrderHistoryManager().Add(orderId, newStatus, note, User.Identity.GetUserId(), oldStatusid);
            if (ID > 0)
            {
                new BuyerOrderManager().UpdateOrderStatus(orderId, newStatus);


            }
            return Json(new { status = ID });
        }
        [HttpPost]
        public JsonResult SendOrderInspection(int orderId, string InspectionValue, string note)
        {
            var order = new BuyerOrderManager().GetByid(orderId);
            var Orderstatus = order.StatusID ?? 1;
            CartItemsManager cartItemsManager = new CartItemsManager();
            var items = cartItemsManager.GetInspectionItemsByOrderId(orderId);
            double AllInspection = 0;
            foreach (string itm in InspectionValue.Split(','))
            {
                string[] valueswithid = itm.Split('_');
                if (valueswithid.Length > 1)
                {
                    double itemvalue =  double.Parse(valueswithid[1]);
                    long itemid = int.Parse(valueswithid[0]);
                    var cartitem = items.FirstOrDefault(x => x.ID == itemid);
                    cartitem.InspectionValue = itemvalue;
                    AllInspection += itemvalue;
                        }
            }
            string tableinspection = "<table><tr><td>Product Title</td><td>Quantity</td><td>Unit Price</td><td>Inspection Value</td></tr>";
            if (AllInspection > 0)

            {
                foreach (var itm in items.ToList())
                {
                    tableinspection += "<tr><td>" + itm.ProductInformation.ProductTitle + "</td><td>" + itm.Qty + "</td><td>" + itm.ProductInformation.UnitPrice + "</td><td>" + itm.InspectionValue + "</td></tr>";
                    cartItemsManager.Update(itm);
                }
            }

            tableinspection += "</table>";

            int ID = new OrderActionManager().Add(orderId, AllInspection, note, User.Identity.GetUserId(), Orderstatus);

            new MailManager().SendMessage(order.AspNetUser.Email, "Your Inspection Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We would like to inform you that your application has been registered in our system <br/> and that it is in the process of preparing and waiting for the value of the inspection to complete the process ... <br/> Please check with the company to complete the procedures.<br/> Notes:" + note + ".<br/> Your Inspection Value is.<br/> "+ tableinspection, "Warehouse Integration Company", order.AspNetUser.FullName, AllInspection + "");
            return Json(new { status = ID });
        }

        public JsonResult SendOrderDelivery(int orderId, string DeliveryValue, string note)
        {
            var order = new BuyerOrderManager().GetByid(orderId);
            var Orderstatus = order.StatusID ?? 1;

            CartItemsManager cartItemsManager = new CartItemsManager();
            var items = cartItemsManager.GetShipmentItemsByOrderId(orderId);
            double AllDeliveryValue = 0;
            foreach (string itm in DeliveryValue.Split(','))
            {
                string[] valueswithid = itm.Split('_');
                if (valueswithid.Length > 1)
                {
                    double itemvalue = double.Parse(valueswithid[1]);
                    long itemid = int.Parse(valueswithid[0]);
                    var cartitem = items.FirstOrDefault(x => x.ID == itemid);
                    cartitem.ShippingValue = itemvalue;
                    AllDeliveryValue += itemvalue;
                }
            }
            string tableShipping = "<table><tr><td>Product Title</td><td>Quantity</td><td>Unit Price</td><td>shipping Value</td></tr>";
            if (AllDeliveryValue > 0)

            {
                foreach (var itm in items.ToList())
                {
                    tableShipping += "<tr><td>" + itm.ProductInformation.ProductTitle + "</td><td>" + itm.Qty + "</td><td>" + itm.ProductInformation.UnitPrice + "</td><td>" + itm.ShippingValue + "</td></tr>";
                    cartItemsManager.Update(itm);
                }
            }

            tableShipping += "</table>";




            int ID = new OrderActionManager().Add(orderId, AllDeliveryValue, note, User.Identity.GetUserId(), Orderstatus, Convert.ToInt32(order.DeliveryType));
            if (order.DeliveryType == 1)
                new MailManager().SendMessage(order.AspNetUser.Email, "Your Delivery Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", " We would like to inform you that your order has arrived in our system   <br/> and waiting for the value of delivery to complete the process..  <br/> Please check with the company to complete the procedures . <br/>Delivery from:<b>" + (order.DeliveryType == 1 ? "Win Delivery" : "Customer Delivery") + "</b>.<br/> Notes:" + note + ".<br/>  Your Delivery Value is.<br/> ", "Warehouse Integration Company", order.AspNetUser.FullName, AllDeliveryValue + "");

            return Json(new { status = ID });
        }



        public JsonResult OrderHistory(int id)
        {
            var history = new OrderHistoryManager().GetByOrderId(id).ToList();
            if (history.Count() == 0)
            {
                var order = new BuyerOrderManager().GetByid(id);
                history.Add(new OrderHistoryViewModel() { OrderID = id, InsertDate = order.InsertDate, NewStatusID = order.StatusID, Status1 = order.Status, Notes = "New Order" });
            }


            return Json(history.Select(x => new { FullName = x.AspNetUser.FullName, Status1 = x.Status1, x.InsertDate, x.Notes }), JsonRequestBehavior.AllowGet);
        }

    }
}