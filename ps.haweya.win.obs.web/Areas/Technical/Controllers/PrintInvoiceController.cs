﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Technical.Controllers
{
    public class PrintInvoiceController : Controller
    {
        // GET: Admin/PrintInvoice
        public ActionResult DownloadOrderInvoice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BuyerOrderViewModel buyerOrder = new BuyerOrderManager().GetByid(Convert.ToInt32(id));
            if (buyerOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.Bank = new BankAccountsManager().Get().FirstOrDefault();
            return View(buyerOrder);
            // return new PdfResult(buyerOrder, "DownloadOrderInvoice");
        }

        public ActionResult GenerateInvoicePDF(int id)
        {
            return new Rotativa.ActionAsPdf("DownloadOrderInvoice", new {  id = id});
            //return new PdfResult( "Index");
        }
    }
}