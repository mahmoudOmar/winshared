﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Technical.Controllers
{
    public class BidDetailsController : BaseTechnicalController
    {
        // GET: Admin/BidDetails
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            OBSBidDetailViewModel bid = new OBSBidDetailsManager().Get(Convert.ToInt64(id));

            if (bid == null)
            {
                return HttpNotFound();
            }

            return View(bid);
        }

        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var CountryList = new CountryManager().Get();

            var items = new OBSBidDetailsManager().GetWithIncloudsTechnical("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new OBSBidDetailsManager().GetWithIncloudsTechnicalCount(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.BidId,
                       x.BidCount,
                       x.BidPrice,
                       x.AspNetUser.FullName,
                       x.AspNetUser.Email,
                       x.AspNetUser.ProfilePhoto,
                       x.ProductInformation.ProductId,
                       x.IsShipment,
                       x.IsSold,
                       x.AddedOn,
                       x.UserDetailsId,
                       ProductImg=x.ProductInformation.ProductImg[0],
                       x.ProductInformation.ProductTitle,
                       StatusText= x.Status.Name,
                       x.StatusID,
                       x.NeedInspection
                       
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BidByStatus()
        {

            return Json(new OBSBidDetailsManager().GetBidByStatus(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInspectionItemsByBidID(int id)
        {

            var items = new OBSBidDetailsManager().Get(id);
            if(items.ProductInformation.IsInspect==true)
            return Json(new {status=1,productimage=items.ProductInformation.ProductImg[0],items.ProductInformation.ProductId,items.ProductInformation.ProductTitle ,items.ProductInformation.Quantity,items.ProductInformation.UnitPrice}, JsonRequestBehavior.AllowGet);

            return Json(new { status = 0 },JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public JsonResult UpdateOrderStatus(int bid, int newStatus, string note, int oldStatusid)
        {

            int ID = new BidHistoryManager().Add(bid, newStatus, note, User.Identity.GetUserId(), oldStatusid);
            if (ID > 0)
            {
                new OBSBidDetailsManager().UpdateBidStatus(bid, newStatus);
            }
            return Json(new { status = ID });
        }

        [HttpPost]
        public JsonResult SendBidInspection(long bidid, double InspectionValue, string note)
        {
            var bid = new OBSBidDetailsManager().Get(bidid);
            var bidstatus = bid.StatusID ?? 1;
            bid.InspectionValue = InspectionValue;
            string tableinspection = "<table ><tr><td style='border: 1px solid; padding:0 3px;'>Product Title</td><td style='border: 1px solid; padding:0 3px;'>bidcount</td><td style='border: 1px solid; padding:0 3px;'> Price</td><td style='border: 1px solid; padding:0 3px;'>Inspection Value</td></tr>";
            if (InspectionValue > 0)
            {
                    tableinspection += "<tr><td style='border: 1px solid; padding:0 3px;'>" + bid.ProductInformation.ProductTitle + "</td><td style='border: 1px solid; padding:0 3px;'>" + bid.BidCount + "</td><td style='border: 1px solid; padding:0 3px;'>" + bid.BidPrice + "</td><td style='border: 1px solid; padding:0 3px;'>" + bid.InspectionValue + "</td></tr>";
            }

            tableinspection += "</table>";

          //  int ID = new OrderActionManager().Add(orderId, AllInspection, note, User.Identity.GetUserId(), Orderstatus);

            new MailManager().SendMessage(bid.AspNetUser.Email, "Your Inspection Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We would like to inform you that your application has been registered in our system <br/> and that it is in the process of preparing and waiting for the value of the inspection to complete the process ... <br/> Please check with the company to complete the procedures.<br/> Notes:" + note + ".<br/> Your Inspection Value is.<br/> " + tableinspection, "Warehouse Integration Company", bid.AspNetUser.FullName, InspectionValue + "");
            return Json(new { status = bid.BidId });
        }

        public JsonResult OrderHistory(int id)
        {
            var history = new BidHistoryManager().GetByBidId(id).ToList();
            return Json(history.Select(x => new { FullName = x.AspNetUser.FullName, Status1 = x.Status1, x.InsertDate, x.Notes }), JsonRequestBehavior.AllowGet);
        }
  
        [HttpPost]
        public JsonResult Aproved_Inspection_Recipt(int id, bool approved)
        {
            OBSBidDetailsManager bidManager = new OBSBidDetailsManager();
            var bid = bidManager.Get(id);
            bid.ApprovedInspictionReciptCopy = approved;
            bid.ApprovedInspictionReciptCopyTime = DateTime.Now;
            bidManager.UpdateBid(bid);
            return Json(new { status = 1 });
        }

        [HttpPost]
        public JsonResult Aproved_Payment_Recipt(int id, bool approved)
        {
            OBSBidDetailsManager bidManager = new OBSBidDetailsManager();
            var bid = bidManager.Get(id);
            bid.ApprovedPaymentReciptCopy = approved;
            bid.ApprovedPaymentRceiptCopyTime = DateTime.Now;
            bidManager.UpdateBid(bid);
            return Json(new { status = 1 });
        }
    }
}