﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Finance.Controllers
{
    public class BidDetailsController : FinanceBaseController
    {
        // GET: Admin/BidDetails
        double vatvalue = CacheManager.Setting.VatValue ?? 0;
        string vattext = CacheManager.Setting.VatText;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            OBSBidDetailViewModel bid = new OBSBidDetailsManager().Get(Convert.ToInt64(id));

            if (bid == null)
            {
                return HttpNotFound();
            }

            return View(bid);
        }
        public ActionResult BidInvoic(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            OBSBidDetailViewModel bid = new OBSBidDetailsManager().Get(Convert.ToInt64(id));

            if (bid == null)
            {
                return HttpNotFound();
            }

            return View(bid);
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var CountryList = new CountryManager().Get();

            var items = new OBSBidDetailsManager().GetWithIncloudsFinance("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new OBSBidDetailsManager().GetWithIncloudsFinanceCount(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.BidId,
                       x.BidCount,
                       x.BidPrice,
                       x.AspNetUser.FullName,
                       x.AspNetUser.Email,
                       x.AspNetUser.ProfilePhoto,
                       x.ProductInformation.ProductId,
                       x.IsShipment,
                       x.IsSold,
                       x.AddedOn,
                       x.UserDetailsId,
                       ProductImg=x.ProductInformation.ProductImg[0],
                       x.ProductInformation.ProductTitle,
                       StatusText= x.Status.Name,
                       x.StatusID,
                       x.NeedInspection,
                       timercount = x.BidsHistories.LastOrDefault() != null ? x.BidsHistories.LastOrDefault().InsertDate.Value.AddHours(48) : DateTime.Now

                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BidByStatus()
        {

            return Json(new OBSBidDetailsManager().GetBidByStatus(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInspectionItemsByBidID(int id)
        {

            var items = new OBSBidDetailsManager().Get(id);
            if(items.ProductInformation.IsInspect==true)
            return Json(new {status=1,productimage=items.ProductInformation.ProductImg[0],items.ProductInformation.ProductId,items.ProductInformation.ProductTitle ,items.ProductInformation.Quantity,items.ProductInformation.Price}, JsonRequestBehavior.AllowGet);

            return Json(new { status = 0 },JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public JsonResult UpdateOrderStatus(int bid, int newStatus, string note, int oldStatusid)
        {
            string bankdata = new BankAccountsManager().BankDatatoEmail();
            int ID = new BidHistoryManager().Add(bid, newStatus, note, User.Identity.GetUserId(), oldStatusid);
            if (ID > 0)
            {
                new OBSBidDetailsManager().UpdateBidStatus(bid, newStatus);
            }
            if(newStatus==4)
            {

                var biddetails =new OBSBidDetailsManager().Get(bid);

                double percrnt = 0;
                if (biddetails.ProductInformation.SecurityDeposit == 1)
                {
                    percrnt = ((biddetails.ProductInformation.SecurityDepositPercent ?? 0) / (100));
                    percrnt = biddetails.BidPrice * percrnt;
                }
                else if (biddetails.ProductInformation.SecurityDeposit == 2)
                {
                    percrnt = biddetails.ProductInformation.SecurityDepositPercent ?? 0;
                }

                double netbid = biddetails.BidPrice - percrnt;
                new MailManager().SendMessage(biddetails.AspNetUser.Email, "Your Bid Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", " We would like to inform you that your bid has arrived in our system   <br/> and waiting for the value of bid to complete the process..  <br/> Please check with the company to complete the Process .<br/> Our Banks Data:" + bankdata + "<br/> Notes:" + note ," Warehouse Integration Company", biddetails.AspNetUser.FullName, netbid + " "+ @System.Configuration.ConfigurationManager.AppSettings["System_Currency"]);
            }

            if (newStatus == 21)
            {
                var biddetails = new OBSBidDetailsManager().Get(bid);

                double percrnt = 0;
                if (biddetails.ProductInformation.SecurityDeposit == 1)
                {
                    percrnt = ((biddetails.ProductInformation.SecurityDepositPercent ?? 0) / (100));
                    percrnt = biddetails.BidPrice * percrnt;
                }
                else if (biddetails.ProductInformation.SecurityDeposit == 2)
                {
                    percrnt = biddetails.ProductInformation.SecurityDepositPercent ?? 0;
                }
                new MailManager().SendMessage(biddetails.AspNetUser.Email, "Your Security deposit Bid Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We would like to inform you that your application has been registered in our system <br/> and that it is in the process of preparing and waiting for the 15% from BidPrice to complete the process ... <br/> Please check with the company to complete the procedures.<br/> Our Banks Data:" + bankdata + "<br/> Notes:" + note + ".<br/> Your Deposit Value <br/> ", "Warehouse Integration Company", biddetails.AspNetUser.FullName, percrnt + " "+ @System.Configuration.ConfigurationManager.AppSettings["System_Currency"]);

                try
                {
                    new ReportDeliveryManager().SendWinnerBidClosedReportafterrecipt(biddetails.ProductInformation);
                }
                catch { };


            }
            return Json(new { status = ID });
        }

        [HttpPost]
        public JsonResult SendBidInspection(long bidid, double InspectionValue, string note)
        {
            string bankdata = new BankAccountsManager().BankDatatoEmail();
            var bid = new OBSBidDetailsManager().Get(bidid);
            var bidstatus = bid.StatusID ?? 1;
            bid.InspectionValue = InspectionValue;
            string tableinspection = "<table ><tr><td style='border: 1px solid; padding:0 3px;'>Product Title</td><td style='border: 1px solid; padding:0 3px;'>bidcount</td><td style='border: 1px solid; padding:0 3px;'> Price</td><td style='border: 1px solid; padding:0 3px;'>Inspection Value</td></tr>";
            if (InspectionValue > 0)
            {
                    tableinspection += "<tr><td style='border: 1px solid; padding:0 3px;'>" + bid.ProductInformation.ProductTitle + "</td><td style='border: 1px solid; padding:0 3px;'>" + bid.BidCount + "</td><td style='border: 1px solid; padding:0 3px;'>" + bid.BidPrice + "</td><td style='border: 1px solid; padding:0 3px;'>" + bid.InspectionValue + "</td></tr>";
            }

            tableinspection += "</table>";

          //  int ID = new OrderActionManager().Add(orderId, AllInspection, note, User.Identity.GetUserId(), Orderstatus);

            new MailManager().SendMessage(bid.AspNetUser.Email, "Your Inspection Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We would like to inform you that your application has been registered in our system <br/> and that it is in the process of preparing and waiting for the value of the inspection to complete the process ... <br/> Please check with the company to complete the procedures.<br/> Our Banks Data:" + bankdata + "<br/> Notes:" + note + ".<br/> Your Inspection Value is.<br/> " + tableinspection, "Warehouse Integration Company", bid.AspNetUser.FullName, InspectionValue + "");
            return Json(new { status = bid.BidId });
        }

        public JsonResult OrderHistory(int id)
        {
            var history = new BidHistoryManager().GetByBidId(id).ToList();
            return Json(history.Select(x => new { FullName = x.AspNetUser.FullName, Status1 = x.Status1, x.InsertDate, x.Notes }), JsonRequestBehavior.AllowGet);
        }
  
        [HttpPost]
        public JsonResult Aproved_Inspection_Recipt(int id, bool approved)
        {
            OBSBidDetailsManager bidManager = new OBSBidDetailsManager();
            var bid = bidManager.Get(id);
            bid.ApprovedInspictionReciptCopy = approved;
            bid.ApprovedInspictionReciptCopyTime = DateTime.Now;
            bidManager.UpdateBid(bid);
            return Json(new { status = 1 });
        }
        [HttpPost]
        public JsonResult Approved_Percent_Recipt(int id, bool approved)
        {
            OBSBidDetailsManager bidManager = new OBSBidDetailsManager();
            var bid = bidManager.Get(id);
            bid.ApprovedPercentReciptCopy = approved;
            bid.ApprovedPercentRceiptCopyTime = DateTime.Now;
            bidManager.UpdateBid(bid);
            return Json(new { status = 1 });
        }

        [HttpPost]
        public JsonResult Aproved_Payment_Recipt(int id, bool approved)
        {
            OBSBidDetailsManager bidManager = new OBSBidDetailsManager();
            var bid = bidManager.Get(id);
            bid.ApprovedPaymentReciptCopy = approved;
            bid.ApprovedPaymentRceiptCopyTime = DateTime.Now;
            bidManager.UpdateBid(bid);
            return Json(new { status = 1 });
        }
    }
}