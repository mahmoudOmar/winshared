﻿using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Finance.Controllers
{
    public class HomeController : FinanceBaseController
    {
        // GET: Finance/Home
        public ActionResult Index()
        {
            ViewBag.OrdersCount = new BuyerOrderManager().GetCountstoAdmin();
            ViewBag.ProcusctsCount = new ProductInformationManager().GetCountstoAdmin();
            ViewBag.BuyersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Buyer");
            ViewBag.SellersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Seller");
            return View();
        }

        public JsonResult GetBidsTabel(string q = "")
        {

            var list = new OBSBidDetailsManager().GetWithGroupingProducts("ProductInformation", 1, 10, "bidid", "desc", q).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPRoductCountByCountry()
        {

            var list = new ProductDetailsManager().GetWithGroupingProductsByCountry().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPRoductCountByCategory()
        {

            var list = new ProductInformationManager().GetWithGroupingProductsByCategory().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBidsCountByProduct()
        {

            var list = new ProductInformationManager().GetWithbidcounttocahrt().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBuyerssCountByCountry()
        {

            var list = new AspNetUsersManager().GetBuyersGroupingByCountry().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}