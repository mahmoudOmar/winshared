﻿using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Technical_Support.Controllers
{
    public class SupportController : BaseTechnical_SupportController
    {

        public ActionResult Tickets()
        {
            return View();
        }

        #region GET

        public JsonResult Get()
        {
            var ticketManager = new TicketManager();

            var query = Request.QueryString["query[query]"] ?? "";

            var SortBy = Request.QueryString["sort[field]"] ?? "Date";
            var SortMethod = Request.QueryString["sort[sort]"] ?? "desc";

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perPage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var items = ticketManager.Get("", page, perPage, SortBy, SortMethod, query,true);
            var totalItemsCount = ticketManager.Count(query,true);

            return Json(new
            {
                meta = new
                {
                    page,
                    pages = Math.Ceiling(totalItemsCount / double.Parse(perPage + "")),
                    perpage = perPage,
                    total = totalItemsCount
                },
                data = items.Select(x =>
                {
                    x.StatusLabel = ticketManager.GetStatusName(x.Status);
                    return x;
                }).ToList()
            }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult TicketsByStatus()
        {
            return Json(new TicketManager().GetByStatus(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult TicketHistory(int TicketId)
        {
            return Json(new TicketManager().GetTicketHistory(TicketId), JsonRequestBehavior.AllowGet);
        }


        public JsonResult Send_to_Thecnical(int TicketId)
        {

            
            new MailManager().SendMessage("mahmoudomry@gmail.com", "Technical Support WIN", "https://www.win.com.sa/Content/FrontEnd/images/logo.png", "https://www.win.com.sa/",
                "https://www.win.com.sa/" + Url.Action("TicketsDetails","Home",new {area="",id=TicketId }), "This Ticket is forward to you from WIN Admin  it's need special support from you .<br/> to see the ticket details click the button", "Warehouse Integration Company", "WIN Technical Support","more details");
            return Json(new
            {
                Status = 200,
                msg = "The Ticket Send to Technical Support"
            },JsonRequestBehavior.AllowGet); ;

        }


    
        #endregion

        #region POST
        [HttpPost]
        public JsonResult AddTicketRow(TicketViewModel ticket)
        {
            return Json(new
            {
                Status = 200,
                Data = new TicketRowManager().Add(ticket.TicketRows[0], User.Identity.Name)
            });
        }

        [HttpPost]
        public JsonResult UpdateTicket(TicketViewModel ticket)
        {
            return Json(new
            {
                Status = 200,
                Data = new TicketManager().Update(ticket)
            });
        }

        #endregion
    }
}