﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.web.App_Start;

namespace ps.haweya.win.obs.web.Areas.Technical_Support.Controllers
{
 
    public class HomeController : BaseTechnical_SupportController
    {   
        [OBSAuthorizeAttribute(Roles = "Technical_Support")]
        // GET: Admin/Home
        public ActionResult Index()
        {
            //ViewBag.OrdersCount = new BuyerOrderManager().GetCountstoAdmin();
            //ViewBag.ProcusctsCount = new ProductInformationManager().GetCountstoAdmin();
            //ViewBag.BuyersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Buyer");
            //ViewBag.SellersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Seller");
            return View();
        }

   
      
    }
}