﻿using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Technical_Support
{
    public class Technical_SupportAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Technical_Support";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Technical_Support_default",
                "Technical_Support/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}