﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.viewModels;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class CategoryController : AdminBaseController
    {

        CategoryManager categoryManager = new CategoryManager();

        public ActionResult Index()
        {
            var categories = GetData(sortBy: "DateCreated", sortMethod: "desc");

            return View(categories);
        }

        [HttpGet]
        public JsonResult Get()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"] ?? "DateCreated";
            var sortMethod = Request.QueryString["sort[sort]"] ?? "desc";

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);
            var totalItemsCount = categoryManager.Count(query);

            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = GetData("", page, perpage, sortBy, sortMethod, query)
               };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private object GetData(string columns = "", int page = 1, int perpage = 50, string sortBy = "", string sortMethod = "", string query = "")
        {
            var items = categoryManager.Get("", page, perpage, sortBy, sortMethod, query);
            return items;
        }



        public JsonResult RoleData(int roleid = 0)
        {
            TypicalImageRoleViewModel role = new TypicalImageRoleViewModel();
            if (roleid != 0)
                role = new TypicalImageRoleManager().GetByid(roleid);


            return Json(role, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteRole(int id)
        {
            new TypicalImageRoleManager().Delete(id);
            return Json(new { status = 1, msg = "Role Deleted Successfully" });

        }



        #region POST
        public ActionResult Add(OBSProductCategoryViewModel category)
        {
            category.DateCreated = DateTime.Now;
            category.CreatedBy = User.Identity.GetUserId();

            var files = Request.Files;
            if (files.Count > 0)
            {
                var file = files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = $@"{Guid.NewGuid()}" + Path.GetExtension(file.FileName);

                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\");
                    var filePath = Path.Combine(path, fileName);
                    file.SaveAs(filePath);

                    category.Image = fileName;
                }
            }

            if (category.CategoryId == 0)
            {
                categoryManager.Add(category);
                return Json(new { Status = 200, Message = "Success" });
            }

            category.ModifiedBy = User.Identity.GetUserId();
            categoryManager.Update(category);


            CacheManager.HomeCategory = StordProcedurManager.CategoryHomeData();
            return Json(new { Status = 200, Message = "Success" });
        }

     

        public ActionResult Delete(int Id)
        {
            var data = categoryManager.Delete(Id);
            CacheManager.HomeCategory = StordProcedurManager.CategoryHomeData();
            return Json(new { Data =data, Status = 200, Message = "Success" },
                 JsonRequestBehavior.AllowGet);
        }

        #endregion
        [HttpPost]
        public JsonResult SaveRole()
        {
            int roleid = Convert.ToInt32(Request.Form["roleid"]);
            int CategoryID = Convert.ToInt32(Request.Form["CategoryID"]);
            int DisciplineID = Convert.ToInt32(Request.Form["DisciplineID"]);
            int CommodityID = Convert.ToInt32(Request.Form["CommodityID"]);
            int SubCommodityID = Convert.ToInt32(Request.Form["SubCommodityID"]);

            if (Request.Files.Count == 0 && roleid == 0)
            {
                return Json(new { status = 0, msg = "please select image to role !!" });
            }

            TypicalImageRoleManager typicalImageRoleManager = new TypicalImageRoleManager();

            string filename = "";
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string guid = Guid.NewGuid().ToString();
                    filename = guid + Path.GetExtension(file.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    file.SaveAs(filePath);
                }

            }
            int result = typicalImageRoleManager.AddOrUpdate(roleid, CategoryID, DisciplineID, CommodityID, SubCommodityID, filename, User.Identity.GetUserId());
            CacheManager.HomeCategory = StordProcedurManager.CategoryHomeData();
            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Role Saved Success " });

        }


        [HttpPost]
        public JsonResult RunRoleStatus(int id)
        {
            new TypicalImageRoleManager().UpdateRun(id, User.Identity.GetUserId());
            return Json(new { status = 1, msg = "Role Updated Successfully" });

        }

    }
}