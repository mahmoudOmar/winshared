﻿using ClosedXML.Excel;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.business.PageModel;
using ps.haweya.win.obs.security;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class ProductController : AdminBaseController
    {

        ProductUnit pUnit = new ProductUnit();
        public ActionResult Preview(int ProductId)
        {
            var model = new ProductInformationManager().GetByid(ProductId);

            //model.Country = CacheManager.Countries.SingleOrDefault(t => t.id == model.ProductDetails.First().Country);
            //model.State = CacheManager.States.SingleOrDefault(t => t.id == int.Parse(model.ProductDetails.First().State ?? "0"));
            //model.City = CacheManager.Cities.SingleOrDefault(t => t.id == int.Parse(model.ProductDetails.First().City ?? "0"));

            int country = 0;
            int state = 0;
            int city = 0;

            int.TryParse(model.ProductDetails.First() != null ? model.ProductDetails.First().Country+"" : "0", out country);
            model.Country = CacheManager.Countries.SingleOrDefault(t => t.id == country);

            int.TryParse(!string.IsNullOrEmpty(model.ProductDetails.First().State) ? model.ProductDetails.First().State : "0", out state);
            model.State = CacheManager.States.SingleOrDefault(t => t.id == state);

            int.TryParse(!string.IsNullOrEmpty(model.ProductDetails.First().City) ? model.ProductDetails.First().City : "0", out city);
            model.City = CacheManager.Cities.SingleOrDefault(t => t.id == city);
            ViewBag.Sellers = new AspNetUsersManager().GetByEmail(model.Username)!=null? new AspNetUsersManager().GetByEmail(model.Username).FullName:model.Username;
           // ViewBag.Company = new CompanyManager().GetByName(model.ProductDetails[0].Company);
            return View(model);
        }
        //public ActionResult NewListing(int ProductId)
        //{
        //    var model = new ProductInformationManager().GetByid(ProductId);

        //    model.Country = CacheManager.Countries.SingleOrDefault(t => t.id == model.ProductDetails.First().Country);
        //    model.State = CacheManager.States.SingleOrDefault(t => t.id == int.Parse(!string.IsNullOrEmpty(model.ProductDetails.First().State)? model.ProductDetails.First().State: "0"));
        //    model.City = CacheManager.Cities.SingleOrDefault(t => t.id == int.Parse(!string.IsNullOrEmpty(model.ProductDetails.First().City)? model.ProductDetails.First().City:"0"));

        //    return View(model);
        //}
        public ActionResult NewListing()
        {

            var listingItemPageModel = new NewListingItemPageModel();

            listingItemPageModel.Categories = new CategoryManager().Get();
            // listingItemPageModel.Manufacturers = CacheManager.OBSManufacturers;
            listingItemPageModel.Countries = CacheManager.Countries;
            listingItemPageModel.Companies = new CompanyManager().Get();
            listingItemPageModel.SubCommodities = new SubCommodityManager().Get();
            listingItemPageModel.Manufacturers = new ManufacturerDetailsManager().Get();
            ViewBag.Sellers = new SelectList(new AspNetUsersManager().GetByRole("seller"), "UserName", "FullName");
            ViewBag.Companies = new CompanyManager().Get();
            
            return View(listingItemPageModel);
        }
        public ActionResult EditProduct(long id)
        {
            var model = new ProductInformationManager().GetByid(id);

            int country = 0;
            int state = 0;
            int city = 0;

            int.TryParse(model.ProductDetails.First() != null ? model.ProductDetails.First().Country + "" : "0", out country);
            model.Country = CacheManager.Countries.SingleOrDefault(t => t.id == country);

            int.TryParse(!string.IsNullOrEmpty(model.ProductDetails.First().State) ? model.ProductDetails.First().State : "0", out state);
            model.State = CacheManager.States.SingleOrDefault(t => t.id == state);

            int.TryParse(!string.IsNullOrEmpty(model.ProductDetails.First().City) ? model.ProductDetails.First().City : "0", out city);
            model.City = CacheManager.Cities.SingleOrDefault(t => t.id == city);


            var listingItemPageModel = new NewListingItemPageModel();

            listingItemPageModel.Categories = new CategoryManager().Get();
            // listingItemPageModel.Manufacturers = CacheManager.OBSManufacturers;
            listingItemPageModel.Countries = CacheManager.Countries;
            listingItemPageModel.Companies = new CompanyManager().Get();
            listingItemPageModel.SubCommodities = model.Category!=null?new SubCommodityManager().GetByCategory(model.Category): new SubCommodityManager().Get();
            listingItemPageModel.Manufacturers = new ManufacturerDetailsManager().Get();
            listingItemPageModel.ProductInformationViewModel = model;

            ViewBag.States = CacheManager.States.Where(x => x.country_id == country).Select(x => new { x.id, x.name }).ToList();
            ViewBag.Cities = CacheManager.Cities.Where(x => x.Stateid == state);
            ViewBag.Sellers = new SelectList(new AspNetUsersManager().GetByRole("seller"), "UserName", "FullName", model.Username);
            ViewBag.Companies = new CompanyManager().Get();
            List<YESORNO> choices = new List<YESORNO>();
            choices.Add(new YESORNO() { Value = 1, Text = "Yes" });
            choices.Add(new YESORNO() { Value = 0, Text = "No" });
            ViewBag.Showbidchoices = new SelectList(choices, "Value", "Text", model.ShowBidAmmount == true ? 1 : 0);
            return View(listingItemPageModel);
        }

        public JsonResult ListOfStates(int CountryId)
        {
            var states = CacheManager.States.Where(t => t.country_id.Equals(CountryId));
            return Json(states, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListOfCities(int StateId)
        {
            var states = CacheManager.Cities.Where(t => t.Stateid.Equals(StateId));
            return Json(states, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ArchiveActive()
        {
            return View("ArchiveActive");
        }

        public ActionResult DirectMaterials()
        {
            return View("DirectMaterials");
        }

        public ActionResult Archive()
        {
            return View("Archive");
        }

        public ActionResult ArchiveBid()
        {
            return View("ArchiveBid");
        }

        public ActionResult MyMaterial()
        {
            return View("Index");
        }
        public ActionResult BidMaterial()
        {
            return View();
        }
        public JsonResult CartByStatus()
        {
            return Json(new
            {
                Data = pUnit.GetCartItemsByStatus()
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BidByStatus()
        {
            return Json(new
            {
                Data = pUnit.GetCartItemsByStatus()
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActiveStatus()
        {
            return Json(new
            {
                Data = pUnit.GetByActiveStatus()
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActiveStatusBids()
        {
            return Json(new
            {
                Data = pUnit.GetBidPRoductsActiveStatus()
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get(string Type)
        {

            var query = Request.QueryString["query[query]"] ?? "";

            var SortBy = Request.QueryString["sort[field]"] ?? "Id";
            var SortMethod = Request.QueryString["sort[sort]"] ?? "desc";

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perPage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            object items = null;
            var totalItemsCount = 0;

            switch (Type)
            {
                case "1":
                    items = pUnit.GetPurchaseItems(null, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        t.ProductInformation.ProductId,
                        ProductImg = t.ProductInformation.ProductImg[0],
                        t.ProductInformation.Username,
                        t.ProductInformation.ProductTitle,
                        t.ProductInformation.Quantity,
                        BuyerName = t.BuyerOrder.AspNetUser.FullName,
                        t.BuyerOrder.AspNetUser.Email,
                        t.ProductInformation.PricingType,
                        t.BuyerOrder.AspNetUser.PhoneNumber,
                        t.ProductInformation.CreateDate,
                        Status = t.IsBought ? "Purchased" : "UnPurchased",
                        
                    }).ToList();

                    totalItemsCount = pUnit.GetPurchaseItemsCount(null, query);

                    break;
                case "2":
                    items = pUnit.GetBidItems(null, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        t.ProductInformation.ProductId,
                        t.ProductInformation.Username,
                        ProductImg = t.ProductInformation.ProductImg[0],
                        t.ProductInformation.ProductTitle,
                        t.ProductInformation.Quantity,
                        Bidder = t.AspNetUser.FullName,
                        t.ProductInformation.PricingType,
                        t.BidPrice,
                        t.IsShipment,
                        t.AspNetUser.Email,
                        t.AspNetUser.PhoneNumber,
                        t.ProductInformation.CreateDate,
                        Status = t.IsSold ? "Sold" : "UnSold",
                        CountryName = CacheManager.Countries.Where(l => l.id == Convert.ToInt32(t.AspNetUser.CountryId)).FirstOrDefault() != null ? CacheManager.Countries.Where(l => l.id == Convert.ToInt32(t.AspNetUser.CountryId)).FirstOrDefault().name : "",
                        StateName = CacheManager.States.Where(l => l.id == Convert.ToInt32(t.AspNetUser.StateId)).FirstOrDefault() != null ? CacheManager.States.Where(l => l.id == Convert.ToInt32(t.AspNetUser.StateId)).FirstOrDefault().name : "",
                        CityName = CacheManager.Cities.Where(l => l.id == Convert.ToInt32(t.AspNetUser.CityId)).FirstOrDefault() != null ? CacheManager.Cities.Where(l => l.id == Convert.ToInt32(t.AspNetUser.CityId)).FirstOrDefault().text : "",
                        t.AddedOn,
                        BidsCount = t.BidCount,
                    }).ToList();

                    totalItemsCount = pUnit.GetBidItemsCount(null, query);
                    break;

                case "3":
                    items = pUnit.GetByActiveStatus(null, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        t.ProductId,
                        t.Username,
                        ProductImg =t.ProductImg[0],
                        t.ProductTitle,
                        t.Quantity,
                        t.IsWinActive,
                        t.PricingType,
                        t.CreateDate,
                        t.SellerName,
                        Status = t.IsWinActive ? "Active" : "In Active",
                        BidsCount = t.OBSBidDetails.Sum(s=>s.BidCount),
                        t.IsSold
                    }).ToList();

                    totalItemsCount = pUnit.GetByActiveStatusCount(null, query);
                    break;


                case "4":
                    items = pUnit.GetBidsProduct(null, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        t.ProductId,
                        t.Username,
                        ProductImg = t.ProductImg[0],
                        t.ProductTitle,
                        t.Quantity,
                        t.IsWinActive,
                        t.PricingType,
                        t.CreateDate,
                        t.SellerName,
                        Status = t.IsWinActive ? "Active" : "In Active",
                        SoldStatus = t.OBSBidDetails.Where(x=>x.IsSold==true).Count() != 0 ? "Sold" : "UnSold",
                        Soldcount = t.OBSBidDetails.Where(x => x.IsSold == true).Count(),
                        BidsCount = t.OBSBidDetails.Sum(s => s.BidCount),
                    }).ToList(); ;

                    totalItemsCount = pUnit.GetBidsProductCount(null, query);
                    break;

                case "5":
                    items = pUnit.GetDirectByActiveStatus(null, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        t.ProductId,
                        t.Username,
                        ProductImg = t.ProductImg[0],
                        t.ProductTitle,
                        t.Quantity,
                        t.IsWinActive,
                        t.PricingType,
                        t.CreateDate,
                        t.SellerName,
                        Status = t.IsWinActive ? "Active" : "In Active",

                    });

                    totalItemsCount = pUnit.GetDirectByActiveStatusCount(null, query);
                    
                    break;

            }

            return Json(new
            {
                meta = new
                {
                    page,
                    pages = Math.Ceiling(totalItemsCount / double.Parse(perPage + "")),
                    perpage = perPage,
                    total = totalItemsCount
                },
                data = items
            }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetAttachment(int id)
        {
            var model = new ProductInformationManager().GetByid(id);
            List<AttachmentsModel> myimages = new List<AttachmentsModel>();
            string[] stringSeparators = new string[] { "(&)" };
            string[] imagesstring = model.ProductImage.Split(stringSeparators, StringSplitOptions.None);
            int counter = 0;
            foreach (var itm in imagesstring)
            {
                if (System.IO.File.Exists("/Content/UploadData/UploadImages/" + itm))
                {
                    FileInfo myfile = new FileInfo("/Content/UploadData/UploadImages/" + itm);

                    var newimg = new AttachmentsModel() { AttachmentID = counter + 1, FileName = "/Content/UploadData/UploadImages/" + itm, Path = "/Content/UploadData/UploadImages/" + itm, size = myfile.Length };

                    myimages.Add(newimg);

                }
                else
                {

                    FileInfo myfile = new FileInfo("/Content/UploadData/UploadImages/" + itm);

                    var newimg = new AttachmentsModel() { AttachmentID = counter + 1, FileName = "/Content/UploadData/UploadImages/" + itm, Path = "/Content/UploadData/UploadImages/" + itm, size = 12345 };

                    myimages.Add(newimg);
                }
            }
            return Json(new { Data = myimages }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAttachmentdocs(int id)
        {
            var model = new ProductInformationManager().GetByid(id);
            List<AttachmentsModel> mydocs = new List<AttachmentsModel>();
            string[] stringSeparators = new string[] { "," };
            string[] docsstring = model.ProductDocument.Split(stringSeparators, StringSplitOptions.None);
            int counter = 0;
            foreach (var itm in docsstring)
            {
                if (System.IO.File.Exists("/Content/UploadData/UploadImages/" + itm))
                {
                    FileInfo myfile = new FileInfo("/Content/UploadData/UploadImages/" + itm);

                    var newimg = new AttachmentsModel() { AttachmentID = counter + 1, FileName = "/Content/UploadData/UploadImages/" + itm, Path = "/Content/UploadData/UploadImages/" + itm, size = myfile.Length };

                    mydocs.Add(newimg);

                }
                else
                {

                    FileInfo myfile = new FileInfo("/Content/UploadData/UploadImages/" + itm);

                    var newimg = new AttachmentsModel() { AttachmentID = counter + 1, FileName = "/Content/UploadData/UploadImages/" + itm, Path = "/Content/UploadData/UploadImages/" + itm, size = 12345 };

                    mydocs.Add(newimg);
                }
            }
            return Json(new { Data = mydocs }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSubCommodity(int categoryid)
        {
            return Json(new SubCommodityManager().GetByCategory(categoryid).Select(x => new { x.SubCommodity, x.SubCommodityId }), JsonRequestBehavior.AllowGet);
        }

        #region POST

        public JsonResult UpdateStatus(ProductInformationViewModel product)
        {
            var result = pUnit.UpdateProduct(product);

            return Json(new
            {
                Status = 200,
                Data = result.ProductId
            });
        }


        

        [HttpPost, ValidateInput(false)]

        public JsonResult AddProduct(ProductInformationViewModel model,int? ShowBidAmmount,int? hasMinimumSellingPrice,int? ShowInMap,int?AllowShowBiddersLocations)
        {

            //            var images = (model.Images ?? "").Split(',');
            //            var documents = (model.Documents ?? "").Split(',');
            
            var seller = new SecurityManager().GetUser(model.Username??User.Identity.Name);
            model.ShowBidAmmount = ShowBidAmmount == 1 ? true : false;
            model.hasMinimumSellingPrice = hasMinimumSellingPrice == 1 ? true : false;
            model.AllowShowBiddersLocations = AllowShowBiddersLocations == 1 ? true : false;
            model.ShowInMap = ShowInMap==1?true:false;
            model.CreateDate = DateTime.Now;
            if (model.DeadLineInDays > 0)
            {
                model.DeadLine = DateTime.Now.AddDays(model.DeadLineInDays);
            }
            else{ model.DeadLine = null; }
            //model.DeadLine = DateTime.Now.AddDays(model.DeadLineInDays == 0 ? 7 : model.DeadLineInDays);
            model.Username = seller.Email;
            model.SellerName = seller.FullName;
            model.ProductImage = model.ProductImage ?? string.Empty;
            model.AvailableQuantity = int.Parse(model.Quantity ?? "0");
            if (model.PricingType == "Auctionable")
            {
                model.BidStartDate = model.BidStartDate == null ? DateTime.Now : model.BidStartDate;
            }

            // these values should be reviewed carefully
            #region default values
            model.BidCount = model.BidCount == null ? 0 : model.BidCount;
            model.ProductBrands = model.ProductBrands == null ? 0 : model.ProductBrands;
            model.ShipmentCharges = model.ShipmentCharges == null ? 0 : model.ShipmentCharges;
            model.ProductLocation = model.ProductLocation ?? string.Empty;
            model.SuggestionByWin = model.SuggestionByWin ?? string.Empty;
            model.OtherCategory = model.OtherCategory ?? string.Empty;
            model.OtherManufacturer = model.OtherManufacturer ?? string.Empty;
            model.EndUser = model.EndUser ?? string.Empty;
            model.SupplierName = model.SupplierName ?? string.Empty;
            model.CertificatesOrDataSheet = model.CertificatesOrDataSheet ?? string.Empty;
            model.WINAssement = model.WINAssement ?? string.Empty;
            model.WINAssementby = model.WINAssementby ?? string.Empty;
            model.Repairable = model.Repairable ?? string.Empty;
            model.UsedEquipmentOrApplication = model.UsedEquipmentOrApplication ?? string.Empty;
            model.SellerMaterialId = model.SellerMaterialId ?? string.Empty;

            //model.UnitPrice = model.Price;
            model.ProductDetails[0].Description = model.ProductDetails[0].Description ?? string.Empty;
            model.ProductDetails[0].MinimumSellingPrice = model.ProductDetails[0].MinimumSellingPrice;
            model.ProductDetails[0].Condition = model.ProductDetails[0].Condition ?? string.Empty;
            model.ProductDetails[0].SellerState = model.ProductDetails[0].SellerState ?? string.Empty;
            model.ProductDetails[0].SellerCity = model.ProductDetails[0].SellerCity ?? string.Empty;
            model.ProductDetails[0].Fax = model.ProductDetails[0].Fax ?? string.Empty;
            model.ProductDetails[0].Company = model.ProductDetails[0].Company ?? string.Empty;
            model.ProductDetails[0].Contact = model.ProductDetails[0].Contact ?? string.Empty;
            model.ProductDetails[0].Phone = model.ProductDetails[0].Phone ?? string.Empty;
            model.ProductDetails[0].SpecialInstructions = model.ProductDetails[0].SpecialInstructions ?? string.Empty;

            model.ProductDetails[0].City = model.ProductDetails[0].City ?? string.Empty;
            model.ProductDetails[0].State = model.ProductDetails[0].State ?? string.Empty;
            model.isActive = model.isActive==null?false:model.isActive;
            model.IsWinActive = model.IsWinActive == null ? false : model.IsWinActive;
            model.isPrivate = false;
            model.IsSold = false;

            model.SecurityDepositDays = model.SecurityDepositDays ?? 0;
            model.SecurityDepositPercent = model.SecurityDepositPercent ?? 0;
            

            var response = new ProductInformationViewModel();
            #endregion
            if (model.ProductId != null && model.ProductId != 0)
            {

                 
               
              new ProductDetailsManager().Update(model.ProductDetails[0]);

                response = new ProductUnit().UpdateFull(model);


            }
            else
            {
                response = new ProductUnit().AddProduct(model);
                //new ReportDeliveryManager().SetSendBidClosedReport(response);

            }

            if (response.PricingType == "Auctionable"&&model.DeadLineInDays>0&&model.DeadLine!=null)
            {
                //send hangfile  for the winner;
                try
                {
                    new ReportDeliveryManager().SetSendWinnerBidClosedReport(response);
                }
                catch { };
            }

            return Json(new
            {
                Status = 200,
                Code = response.ProductId > 0 ? 1 : 2,
                Message = response.ProductId > 0 ? "New Product has been added successfully" : "Error Please try again"
            });
        }
        [HttpPost]
        public JsonResult deleteproduct(long id)
        {

            ProductInformationManager manager = new ProductInformationManager();
            var produt = manager.GetByid(id);
            produt.IsDelete = true;
            produt.IsWinActive = false;
            manager.Delete(produt);
            return Json(new { status = 1 });


        }
        [HttpPost]
        public ActionResult GetExportFile()
        {
        var items = pUnit.GetByActiveStatusExport_Admin(null).Select(t => new
            {
                t.ProductId,
                t.Username,
            ProductImg = t.ProductImage.Length>0?Path.GetFileName(t.ProductImg[0]):"",
                t.ProductTitle,
                t.Quantity,
                t.IsWinActive,
                t.PricingType,
                t.CreateDate,
                t.SellerName,
                Status = t.IsWinActive ? "Active" : "In Active",
                BidsCount = t.OBSBidDetails.Sum(s => s.BidCount),
                t.IsSold,
            UnitPrice=t.UnitPrice,
            Market_Price= t.UnitPrice,
            SellerMaterialId=t.SellerMaterialId,
            Manufacturers = t.OBSManufacturer!=null? t.OBSManufacturer.ManufacturersName:"",
            Condition=t.ProductDetails.FirstOrDefault()!=null?t.ProductDetails.FirstOrDefault().Condition:""

        }).ToList();
            DataTable dt =ps.haweya.win.obs.common.Helper.ToDataTable(items); ;

            string filenametikes = "AllProducts_export_admin";
            using (XLWorkbook workBook = new XLWorkbook())
            {
                //Read the first Sheet from Excel file.
                workBook.Worksheets.Add(dt, "ExportedAllProducts");

                workBook.SaveAs(Server.MapPath("~/content/uploaddata/" + filenametikes + ".xlsx"));
            }

            try
            {
                var fileInfo = new FileInfo(Server.MapPath("~/content/uploaddata/" + filenametikes + ".xlsx"));
                return Json(new { filename = filenametikes + ".xlsx" });
            }
            catch { }

            return View();
        }


        [HttpPost]
        public JsonResult ActiveSelected(string[] ids,bool active)
        {

            ProductInformationManager manager = new ProductInformationManager();
            foreach (string s in ids)
            {
                var Product = manager.GetByid(Convert.ToInt32(s));
                Product.IsWinActive = active;
                manager.Update(Product);
            }
            return Json(new { status = 1 });
        }

        #endregion

    }
    public class YESORNO { 
    public int Value { get; set; }
        public  string Text { get; set; }

    }
}