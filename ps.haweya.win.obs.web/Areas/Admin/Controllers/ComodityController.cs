﻿using ClosedXML.Excel;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.business.PageModel;
using ps.haweya.win.obs.security;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class ComodityController : AdminBaseController
    {

        // GET: Admin/Commodity
        public ActionResult Index()
        {
            ViewBag.CategoryList = new SelectList(new CategoryManager().Get(), "CategoryId", "CategoryName");

            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);



            var items = new OBSCommodityManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new OBSCommodityManager().Count(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.CommodityId,
                       x.Commodity,
                       x.CategoryID,
                       CategoryName = x.OBSProductCategory != null ? x.OBSProductCategory.CategoryName : "",

                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CommodityData(int Id = 0)
        {
            OBSCommodityViewModel item = new OBSCommodityViewModel();
            if (Id != 0)
                item = new OBSCommodityManager().GetByid(Id);


            return Json(new { item.Commodity, item.CommodityId, item.CategoryID }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveCommodity()
        {
            int Id = Convert.ToInt32(Request.Form["ID"]);
            string Name = Request.Form["Name"];
            int categoryid = Convert.ToInt32(Request.Form["categoryid"]);
           


            OBSCommodityManager commidityManager = new OBSCommodityManager();


            int result = commidityManager.AddOrUpdate(Id, Name, categoryid);

            if (result <= 0)
            {
                return Json(new { ID = 0, status = 0, msg = "Please Try Again" });
            }



            return Json(new { ID = result, status = 1, msg = "Commodity Saved Success " });

        }

        public JsonResult DeleteCommodity(int id)
        {
            new OBSCommodityManager().Delete(id);
            return Json(new { status = 1, msg = "Commodity Deleted Successfully" });

        }

        
    }
}