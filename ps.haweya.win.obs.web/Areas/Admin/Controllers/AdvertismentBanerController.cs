﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class AdvertismentBanerController : AdminBaseController
    {
        // GET: Admin/AdvertismentBaner
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            

            var items = new AdvertismentBanerManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new AdvertismentBanerManager().Count(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                       x.Title,
                       x.Photo,
                       x.IsActive,
                       x.Link,
                       x.InsertDate,
                       x.OrderNo,
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BanarData(int Id = 0)
        {
            AdvertismentBanerViewModel item = new AdvertismentBanerViewModel();
            if (Id != 0)
                item = new AdvertismentBanerManager().GetByid(Id);


            return Json(item, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveBanar()
        {
            int Id = Convert.ToInt32(Request.Form["Id"]);
            string Title = Request.Form["Title"];
            string Link = Request.Form["Link"];
            bool IsActive = Convert.ToBoolean(Request.Form["IsActive"]);
            int orderno = Convert.ToInt32(Request.Form["Orderno"]);
            if (Request.Files.Count == 0 && Id == 0)
            {
                return Json(new { status = 0, msg = "please select image to banar !!" });
            }

            AdvertismentBanerManager banerManager = new AdvertismentBanerManager();

            string filename = "";
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string guid = Guid.NewGuid().ToString();
                    filename = guid + Path.GetExtension(file.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\banars\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    file.SaveAs(filePath);
                }

            }
            int result = banerManager.AddOrUpdate(Id, Title,  Link, IsActive, filename,orderno);
            CacheManager.Banars = new AdvertismentBanerManager().GetActive(true).OrderBy(x => x.OrderNo).ToList();
            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "banar Saved Success " });

        }

        public JsonResult Deletebanar(int id)
        {
            new AdvertismentBanerManager().Delete(id);
            CacheManager.Banars = new AdvertismentBanerManager().GetActive(true).OrderBy(x => x.OrderNo).ToList();
            return Json(new { status = 1, msg = "banar Deleted Successfully" });

        }
        [HttpPost]
        public JsonResult IsActiveManufacturerStatus(int id)
        {
            new AdvertismentBanerManager().UpdateIsActive(id);
            CacheManager.Banars = new AdvertismentBanerManager().GetActive(true).OrderBy(x => x.OrderNo).ToList();
            return Json(new { status = 1, msg = "banar Updated Successfully" });

        }
    }
}