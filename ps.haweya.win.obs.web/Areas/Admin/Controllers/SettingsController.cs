﻿using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class SettingsController : AdminBaseController
    {
        // GET: Admin/Settings
        public ActionResult Index()
        {
            var settings = new SettingsManager().GetByid(1);
            return View(settings);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult Index(SettingViewModel model, HttpPostedFileBase Website_Logo, HttpPostedFileBase Company_Profile)
        {
            var settings = new SettingsManager().GetByid(1);
         
            string filename = "";
            if (Request.Files.Count > 0)
            {
                if (Website_Logo != null && Website_Logo.ContentLength > 0)
                {
                    string guid = Guid.NewGuid().ToString();
                    filename = guid + Path.GetExtension(Website_Logo.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    Website_Logo.SaveAs(filePath);
                    model.WebLogoName = filename;
                }
                else { model.WebLogoName = settings.WebLogoName; }
                if (Company_Profile != null && Company_Profile.ContentLength > 0)
                {
                    string filenameprofile = "";
                    string guidprofile = Guid.NewGuid().ToString();
                    filenameprofile = guidprofile + Path.GetExtension(Company_Profile.FileName);
                    var pathprofile = Server.MapPath("~\\Content\\UploadData\\");
                    if (!System.IO.Directory.Exists(pathprofile))
                    {
                        System.IO.Directory.CreateDirectory(pathprofile);
                    }
                    var filePathprofile = Path.Combine(pathprofile, filenameprofile);
                    Company_Profile.SaveAs(filePathprofile);
                    model.Company_Profile = filenameprofile;

                }
                else { settings.Company_Profile = settings.Company_Profile; }
            }
            else
            {
                model.WebLogoName = settings.WebLogoName;
                settings.Company_Profile = settings.Company_Profile;
            }
            int result = 0;
            if (settings == null)
                result= new SettingsManager().Add(model);
            else
            result = new SettingsManager().Update(model);
           

            VatHistoryManager vm = new VatHistoryManager();
            var vmodel = vm.GetLast();
            if (vmodel is null||(vmodel.VATText != model.VatText || vmodel.VATValue != model.VatValue))
            {
                vm.Add(new VATHistoryViewModel() { VATText = model.VatText, VATValue = model.VatValue, InsertDate = DateTime.Now });
            }

            CacheManager.Setting = new SettingsManager().GetByid(1);
            TempData["msg"] = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong> Settings Saved Successfully! </strong> Settings Saved<button type ='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            return View(model);
        }
        public JsonResult AjaxDTHistory(string Type)
        {




            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perPage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);


            

            IEnumerable<VATHistoryViewModel> items = new List<VATHistoryViewModel>();
            int totalItemsCount = 0;
            items = new VatHistoryManager().Get();
            totalItemsCount = items.Count();

            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perPage + "")),
                       perpage = perPage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                      x.VATText,
                      x.VATValue,
                      x.InsertDate
                   }).ToList()
               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}