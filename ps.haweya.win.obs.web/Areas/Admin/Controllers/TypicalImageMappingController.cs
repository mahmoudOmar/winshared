﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.viewModels;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class TypicalImageMappingController : AdminBaseController
    {
        // GET: Admin/TypicalImageMapping
        public ActionResult Index()
        {
            ViewBag.Category = new SelectList(new CategoryManager().Get(), "CategoryId", "CategoryName");
            ViewBag.Discipline = new SelectList(new OBSDisciplinesManager().Get(), "DisciplineId", "Discipline");
            ViewBag.Commodity = new SelectList(new OBSCommodityManager().Get(), "CommodityId", "Commodity");
            ViewBag.SubCommodity = new SelectList(new OBSSubCommodityManager().Get(), "SubCommodityId", "SubCommodity");
            return View();
        }

        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var CountryList = new CountryManager().Get();

            var items = new TypicalImageRoleManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new TypicalImageRoleManager().Count(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.ID,
                       x.CategoryID,
                       x.CommodityID,
                       x.DisciplineID,
                       x.SubCommodityID,
                       Category = x.OBSProductCategory != null ? x.OBSProductCategory.CategoryName : "",
                       Commodity = x.OBSCommodity != null ? x.OBSCommodity.Commodity : "",
                       Discipline = x.OBSDiscipline != null ? x.OBSDiscipline.Discipline : "",
                       SubCommodity = x.OBSSubCommodity != null ? x.OBSSubCommodity.SubCommodity : "",
                       x.ImageFile,
                       x.IsRun,
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RoleData(int roleid = 0)
        {
            TypicalImageRoleViewModel role = new TypicalImageRoleViewModel();
            if (roleid != 0)
                role = new TypicalImageRoleManager().GetByid(roleid);


            return Json(role, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveRole()
        {
            int roleid = Convert.ToInt32(Request.Form["roleid"]);
            int CategoryID = Convert.ToInt32(Request.Form["CategoryID"]);
            int DisciplineID = Convert.ToInt32(Request.Form["DisciplineID"]);
            int CommodityID = Convert.ToInt32(Request.Form["CommodityID"]);
            int SubCommodityID = Convert.ToInt32(Request.Form["SubCommodityID"]);

            if (Request.Files.Count == 0 && roleid == 0)
            {
                return Json(new { status = 0, msg = "please select image to role !!" });
            }

            TypicalImageRoleManager typicalImageRoleManager = new TypicalImageRoleManager();

            string filename = "";
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string guid = Guid.NewGuid().ToString();
                    filename = guid + Path.GetExtension(file.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    file.SaveAs(filePath);
                }

            }
            int result = typicalImageRoleManager.AddOrUpdate(roleid, CategoryID, DisciplineID, CommodityID, SubCommodityID, filename, User.Identity.GetUserId());

            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Role Saved Success " });

        }
      
        public JsonResult DeleteRole(int id)
        {
             new TypicalImageRoleManager().Delete(id);
            return Json(new { status=1,msg="Role Deleted Successfully"});

        }
        [HttpPost]
        public JsonResult RunRoleStatus(int id)
        {
            new TypicalImageRoleManager().UpdateRun(id,User.Identity.GetUserId());
            return Json(new { status = 1, msg = "Role Updated Successfully" });

        }
        
    }
}