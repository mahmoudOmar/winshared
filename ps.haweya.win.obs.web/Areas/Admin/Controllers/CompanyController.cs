﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class CompanyController : AdminBaseController
    {
        // GET: Admin/Company
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);



            var items = new CompanyManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new CompanyManager().Count(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.ID,
                       x.Name,
                       
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CompanyData(int Id = 0)
        {
            CompanyViewModel item = new CompanyViewModel();
            if (Id != 0)
                item = new CompanyManager().GetByid(Id);


            return Json(item, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveCompny()
        {
            int Id = Convert.ToInt32(Request.Form["ID"]);
            string Name = Request.Form["Name"];
          
          

            CompanyManager companyManager = new CompanyManager();


            int result = companyManager.AddOrUpdate(Id, Name);

            if (result <= 0)
            {
                return Json(new {ID=0, status = 0, msg = "Please Try Again" });
            }



            return Json(new { ID = result, status = 1, msg = "company Saved Success " });

        }

        public JsonResult Deletecompany(int id)
        {
            new CompanyManager().Delete(id);
            return Json(new { status = 1, msg = "company Deleted Successfully" });

        }
    }
}