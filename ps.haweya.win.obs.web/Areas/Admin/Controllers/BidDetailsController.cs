﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Windows;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class BidDetailsController : AdminBaseController
    {
        // GET: Admin/BidDetails

        double vatvalue = CacheManager.Setting.VatValue ?? 0;
        string vattext = CacheManager.Setting.VatText;
        public ActionResult Index(int Id)
        {
            
            ViewBag.ProductId = Id;
            return View();
        }
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            OBSBidDetailViewModel bid = new OBSBidDetailsManager().Get(Convert.ToInt64(id));

            if (bid == null)
            {
                return HttpNotFound();
            }

            return View(bid);
        }

        public ActionResult BidInvoic(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            OBSBidDetailViewModel bid = new OBSBidDetailsManager().Get(Convert.ToInt64(id));

            if (bid == null)
            {
                return HttpNotFound();
            }
            ViewBag.Bank = new BankAccountsManager().Get().FirstOrDefault();
            return View(bid);
        }

        public JsonResult AjaxDT(int Id)
        {
            var query = Request.QueryString["query[query]"] ?? "";
            //query += "&ProductInformation.ProductId="+ Id;

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var CountryList = new CountryManager().Get();

            var items = new OBSBidDetailsManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query, Id);
            var totalItemsCount = new OBSBidDetailsManager().Count(query, Id);

            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.BidId,
                       x.BidCount,
                       x.BidPrice,
                       x.VatText,
                       x.ProductInformation.AvailableQuantity,
                       Total= (x.BidPrice + (x.BidPrice * x.VatValue ?? 0)),
                       x.MainCurrencyValue,
                       MainCurrencyValueVAt= (x.MainCurrencyValue + (x.MainCurrencyValue * x.VatValue ?? 0)),
                       x.Currency,
                       x.AspNetUser.FullName,
                       x.AspNetUser.Email,
                       x.AspNetUser.ProfilePhoto,
                       x.ProductInformation.ProductId,
                       x.IsShipment,
                       x.IsSold,
                       x.AddedOn,
                       x.UserDetailsId,
                       ProductImg = x.ProductInformation.ProductImg[0],
                       x.ProductInformation.ProductTitle,
                       StatusText = x.Status.Name,
                       x.StatusID,
                       x.NeedInspection,
                       x.BidOffer,
                       timercount = x.BidsHistories.LastOrDefault() != null ? x.BidsHistories.LastOrDefault().InsertDate.Value.AddHours(x.ProductInformation.SecurityDepositDays??0*24) : DateTime.Now,
                        acceptbid = x.IsSold != true,

                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BidByStatus()
        {

            return Json(new OBSBidDetailsManager().GetBidByStatus(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInspectionItemsByBidID(int id)
        {

            var items = new OBSBidDetailsManager().Get(id);
            if(items.ProductInformation.IsInspect==true)
            return Json(new {status=1,productimage=items.ProductInformation.ProductImg[0],items.ProductInformation.ProductId,items.ProductInformation.ProductTitle ,items.ProductInformation.Quantity, items.ProductInformation.Price}, JsonRequestBehavior.AllowGet);

            return Json(new { status = 0 },JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public JsonResult UpdateOrderStatus(int bid, int newStatus, string note, int oldStatusid)
        {


            
            string bccemails = "Saboor@win.com.sa,Omer.arif@win.com.sa";
            int ID = new BidHistoryManager().Add(bid, newStatus, note, User.Identity.GetUserId(), oldStatusid);
            if (ID > 0)
            {
                

                string bankdata = new BankAccountsManager().BankDatatoEmail();
                OBSBidDetailsManager bidmanager = new OBSBidDetailsManager();
                var biddetails = bidmanager.Get(bid);
                string vattext = biddetails.VatText;
                double vatvalue = biddetails.VatValue??0;

                bidmanager.UpdateBidStatus(bid, newStatus);
                if (newStatus == 12)
                {
                   // var biddetails = bidmanager.Get(bid);
                    biddetails.IsSold = true;
                    bidmanager.UpdateBid(biddetails);
                    var prodduct = biddetails.ProductInformation;
                    prodduct.IsSold = true;
                    new ProductInformationManager().UpdateFull(prodduct);

                }
                if (newStatus == 18)
                {
                  
                   // var biddetails = bidmanager.Get(bid);
                    biddetails.IsSold = true;
                   
                    bidmanager.UpdateBid(biddetails);

                }
                if (newStatus == 19)
                {
                   // var biddetails = bidmanager.Get(bid);
                    biddetails.IsSold = false;
                    bidmanager.UpdateBid(biddetails);

                }

                if (newStatus == 21)
                {

                    
                  //  var biddetails = bidmanager.Get(bid);
                    double percrnt = 0;
                    if (biddetails.ProductInformation.SecurityDeposit == 1)
                    {
                        percrnt = ((biddetails.ProductInformation.SecurityDepositPercent ?? 0) / (100));
                        percrnt = biddetails.BidPrice * percrnt;
                    }
                    else if (biddetails.ProductInformation.SecurityDeposit == 2)
                    {
                        percrnt = biddetails.ProductInformation.SecurityDepositPercent??0;
                    }


                  
                    string body = @"**For confirmation from your end, kindly pay a security deposit of" + percrnt + @"saudi riyals immediately or within " + biddetails.ProductInformation.SecurityDepositDays + @"days to the below account details:";
                //Note: WIN Bank info:<br/>";
                    BankAccountsManager m = new BankAccountsManager();
                    foreach (var b in m.GetActive())
                    {

                        string bankinfoline = "<div style='width:100%;margin-bottom:20px;'>";
                        bankinfoline += "<div style='width:45%;margin-right:5%'><img style='max-height:50px' src='https://www.win.com.sa/Content/uploaddata/UploadImages/Bank/" + b.Logo + @"'/> </div>";
                        bankinfoline += "<div style='width:50%;'>Bank Name: " + b.Name+" <br /> ";
                        bankinfoline += "IBAN:" + b.IBAN + "<br/></div>";
                        bankinfoline += "</div>";
                        body += bankinfoline;

                    }
                    body += @"**This money will be part of the final full payment.<br/>
* *This money has to be paid by cash, Certified Check or bank transfer to WIN’s official bank account.<br/> **WIN will provide Auction participants with signed and stamped receipt of the money declaring all above mentioned refunding terms..<br/> Our Banks Data:" + bankdata + "";
                    new MailManager().SendMessage(biddetails.AspNetUser.Email, "Your Security Deposit Bid Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", body, "Warehouse Integration Company", biddetails.AspNetUser.FullName, percrnt + (biddetails.Currency!=null?biddetails.Currency.Sign:"SAR"), bccemails );

                    try
                    {

                        var prodduct = biddetails.ProductInformation;
                        prodduct.ProductDetails = null;
                        new ReportDeliveryManager().SendWinnerBidClosedReportafterrecipt(prodduct);
                    }
                    catch(Exception ex) { string a = ex.Message; };

                }
                if (newStatus == 4)
                {
                  //  var biddetails = bidmanager.Get(bid);

                    double percrnt = 0;
                    if (biddetails.ProductInformation.SecurityDeposit == 1)
                    {
                        percrnt = ((biddetails.ProductInformation.SecurityDepositPercent ?? 0) / (100));
                        percrnt = biddetails.BidPrice * percrnt;
                    }
                    else if (biddetails.ProductInformation.SecurityDeposit == 2)
                    {
                        percrnt = biddetails.ProductInformation.SecurityDepositPercent ?? 0;
                    }

                    double netbid = biddetails.BidPrice - percrnt;



                    string body = @"Your Payment has been successfully received! Kindly pay the remaining amount of the winning bid value  of " +netbid +@" Riyals  within " + biddetails.ProductInformation.FullPaymetDays + @"days.<br/> Please be aware that materials should be picked up within 7 days from the bid closing date.
The payment can be done via Certified Check, or Bank Transfer or LC at sight on loading the material. The LC has to be from international known bank.
Auction Winner is responsible to handle loading of materials from Seller’s yard, shipping of materials and all logistics activities(Ex Warehouse).
Upon Auction Winner’s request and expense, WIN can arrange for such shipping and logistics activities.
 All payments has to include "+vattext+"VAT as per policy inside Saudi Arabia..<br/> Our Banks Data:" + bankdata + "";
                    //BankAccountsManager m = new BankAccountsManager();
                    //foreach (var b in m.GetActive())
                    //{

                    //    string bankinfoline = "<div style='width:100%;margin-bottom:20px;'>";
                    //    bankinfoline += "<div style='width:45%;margin-right:5%'><img style='max-height:50px' src='https://www.win.com.sa/Content/uploaddata/UploadImages/Bank/" + b.Logo + @"'/> </div>";
                    //    bankinfoline += "<div style='width:50%;'>Bank Name: " + b.Name + " <br /> ";
                    //    bankinfoline += "IBAN:" + b.IBAN + "<br/></div>";
                    //    bankinfoline += "</div>";
                    //    body += bankinfoline;

                    //}

                    new MailManager().SendMessage(biddetails.AspNetUser.Email, "Your  Bid Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", body, "Warehouse Integration Company", biddetails.AspNetUser.FullName, netbid + " "+(biddetails.Currency!=null?biddetails.Currency.Sign:"SAR"),bccemails);

                    try
                    {

                        var prodduct = biddetails.ProductInformation;
                        prodduct.ProductDetails = null;
                        new ReportDeliveryManager().SendWinnerBidwaitingvalue(prodduct);
                    }
                    catch (Exception ex) { string a = ex.Message; };

                }
            }

           
            return Json(new { status = ID });
        }

        [HttpPost]
        public JsonResult SendBidInspection(long bidid, double InspectionValue, string note)
        {
            OBSBidDetailsManager bidManager = new OBSBidDetailsManager();
            var bid = bidManager.Get(bidid);
            var bidstatus = bid.StatusID ?? 1;
            bid.InspectionValue = InspectionValue;
            bidManager.UpdateBid(bid);
            string tableinspection = "<table ><tr><td style='border: 1px solid; padding:0 3px;'>Product Title</td><td style='border: 1px solid; padding:0 3px;'>bidcount</td><td style='border: 1px solid; padding:0 3px;'> Price</td><td style='border: 1px solid; padding:0 3px;'>Inspection Value</td></tr>";
            if (InspectionValue > 0)
            {
                    tableinspection += "<tr><td style='border: 1px solid; padding:0 3px;'>" + bid.ProductInformation.ProductTitle + "</td><td style='border: 1px solid; padding:0 3px;'>" + bid.BidCount + "</td><td style='border: 1px solid; padding:0 3px;'>" + bid.BidPrice + "</td><td style='border: 1px solid; padding:0 3px;'>" + bid.InspectionValue + "</td></tr>";
            }

            tableinspection += "</table>";
            string bankdata = new BankAccountsManager().BankDatatoEmail();
            //  int ID = new OrderActionManager().Add(orderId, AllInspection, note, User.Identity.GetUserId(), Orderstatus);

            new MailManager().SendMessage(bid.AspNetUser.Email, "Your Inspection Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We would like to inform you that your application has been registered in our system <br/> and that it is in the process of preparing and waiting for the value of the inspection to complete the process ... <br/> Please check with the company to complete the procedures.<br/> Our Banks Data:" + bankdata + "<br/> Notes:" + note + ".<br/> Your Inspection Value is.<br/> " + tableinspection, "Warehouse Integration Company", bid.AspNetUser.FullName, InspectionValue + "");
            return Json(new { status = bid.BidId });
        }

        public JsonResult OrderHistory(int id)
        {
            var history = new BidHistoryManager().GetByBidId(id).ToList();
            return Json(history.Select(x => new { FullName = x.AspNetUser.FullName, Status1 = x.Status1, x.InsertDate, x.Notes }), JsonRequestBehavior.AllowGet);
        }
  
        [HttpPost]
        public JsonResult Aproved_Inspection_Recipt(int id, bool approved)
        {
            OBSBidDetailsManager bidManager = new OBSBidDetailsManager();
            var bid = bidManager.Get(id);
            bid.ApprovedInspictionReciptCopy = approved;
            bid.ApprovedInspictionReciptCopyTime = DateTime.Now;

            if (approved == true)
                bid.StatusID = 3;
            bidManager.UpdateBid(bid);
            var lastbidhistory = new BidHistoryManager().GetByBidId(Convert.ToInt32(bid.BidId)).OrderByDescending(x => x.InsertDate).FirstOrDefault();
            int ID = new BidHistoryManager().Add(Convert.ToInt32(bid.BidId), 3, "Bid Inspection Value Was paid", User.Identity.GetUserId(), Convert.ToInt16(lastbidhistory != null ? lastbidhistory.NewStatusID : 1));
            return Json(new { status = 1 });
        }
        [HttpPost]
        public JsonResult Approved_Percent_Recipt(int id, bool approved)
        {
            OBSBidDetailsManager bidManager = new OBSBidDetailsManager();
            var bid = bidManager.Get(id);
            bid.ApprovedPercentReciptCopy = approved;
            bid.ApprovedPercentRceiptCopyTime = DateTime.Now;
            if (approved == true)
                bid.StatusID = 22;
            bidManager.UpdateBid(bid);
            var lastbidhistory = new BidHistoryManager().GetByBidId(Convert.ToInt32(bid.BidId)).OrderByDescending(x => x.InsertDate).FirstOrDefault();
            int ID = new BidHistoryManager().Add(Convert.ToInt32(bid.BidId), 22, "Bid Security Deposit Was paid", User.Identity.GetUserId(), Convert.ToInt16(lastbidhistory != null ? lastbidhistory.NewStatusID : 1));
            return Json(new { status = 1 });
        }
        
        [HttpPost]
        public JsonResult Aproved_Payment_Recipt(int id, bool approved)
        {
            OBSBidDetailsManager bidManager = new OBSBidDetailsManager();
            var bid = bidManager.Get(id);
            bid.ApprovedPaymentReciptCopy = approved;
            bid.ApprovedPaymentRceiptCopyTime = DateTime.Now;
            if (approved == true)
                bid.StatusID = 5;
            bidManager.UpdateBid(bid);
            var lastbidhistory = new BidHistoryManager().GetByBidId(Convert.ToInt32(bid.BidId)).OrderByDescending(x => x.InsertDate).FirstOrDefault();
            int ID = new BidHistoryManager().Add(Convert.ToInt32(bid.BidId), 5, "Bid Value Was paid", User.Identity.GetUserId(), Convert.ToInt16(lastbidhistory != null ? lastbidhistory.NewStatusID : 1));
            return Json(new { status = 1 });
        }

        [HttpPost]
        public JsonResult AcceptBid(int bid)
        {
            //int ID = 0;
            var lastbidhistory = new BidHistoryManager().GetByBidId(bid).OrderByDescending(x => x.InsertDate).FirstOrDefault();
            // if(lastbidhistory!=null)
            int ID = new BidHistoryManager().Add(bid, 18, "Admin Confirmed Sold", User.Identity.GetUserId(), Convert.ToInt16(lastbidhistory != null ? lastbidhistory.NewStatusID : 1));
            if (ID > 0)
            {
                OBSBidDetailsManager bidmanager = new OBSBidDetailsManager();
                bidmanager.UpdateBidStatus(bid, 18);

                var biddetails = bidmanager.Get(bid);
                biddetails.IsSold = true;
                bidmanager.UpdateBid(biddetails);
                var prodduct = biddetails.ProductInformation;
                prodduct.IsSold = true;
                new ProductInformationManager().UpdateFull(prodduct);
                string bccemails = "Saboor@win.com.sa,Omer.arif@win.com.sa";
                new MailManager().SendBidwinnerClosedReport(prodduct, biddetails.AspNetUser, biddetails.BidPrice,bccemails);
            }


            return Json(new { status = ID });
        }

    }
}