﻿using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class BankAccountsController : AdminBaseController
    {
        // GET: Admin/BankAccounts
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);


            var items = new BankAccountsManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = items.Count();

            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                       x.Logo,
                       x.Name,
                       x.No,
                       x.IBAN,
                       x.IsActive
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BankAccountData(int id = 0)
        {
            BankAccountViewModel item = new BankAccountViewModel();
            if (id != 0)
                item = new BankAccountsManager().GetByid(id);


            return Json(item, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveBankAccount()
        {
            int Id = Convert.ToInt32(Request.Form["Id"]);
            string Name = Request.Form["Name"].ToString();
            string No = Request.Form["No"].ToString();
            string IBAN = Request.Form["IBAN"].ToString();
            string CompanyName = Request.Form["CompanyName"].ToString();
            string Beneficiary_Account_Name = Request.Form["Beneficiary_Account_Name"].ToString();
            string C_Company = Request.Form["C_Company"].ToString();
            string VAT_Number = Request.Form["VAT_Number"].ToString();
            string Telephone_number = Request.Form["Telephone_number"].ToString();
            string Vendor_Location = Request.Form["Vendor_Location"].ToString();
            string Currency = Request.Form["Currency"].ToString();
            string SWIFT_CODE = Request.Form["SWIFT_CODE"].ToString();
            string Routing_number = Request.Form["Routing_number"].ToString();
            string Bank_Address_Country = Request.Form["Bank_Address_Country"].ToString();
            bool IsActive = Convert.ToBoolean(Request.Form["IsActive"]);

            if (Request.Files.Count == 0 && Id == 0)
            {
                return Json(new { status = 0, msg = "please select image to bank account !!" });
            }

            BankAccountsManager bankAccountsManager = new BankAccountsManager();

            string filename = "";
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string guid = Guid.NewGuid().ToString();
                    filename = guid + Path.GetExtension(file.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\Bank\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    file.SaveAs(filePath);
                }

            }
            int result = bankAccountsManager.AddOrUpdate(Id, filename, Name, No, IBAN,CompanyName,
                Beneficiary_Account_Name,C_Company,VAT_Number,Telephone_number,Vendor_Location,
                Currency,SWIFT_CODE,Routing_number,Bank_Address_Country, IsActive);

            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Bank Account Saved Success " });
        }
        public JsonResult DeleteBankAccount(int id)
        {
            new BankAccountsManager().Delete(id);
            return Json(new { status = 1, msg = "Bank Account Deleted Successfully" });

        }
        [HttpPost]
        public JsonResult BankAccountIsActive(int id)
        {
            new BankAccountsManager().UpdateIsActive(id);
            return Json(new { status = 1, msg = "Bank Account Updated Successfully" });

        }
    }
}