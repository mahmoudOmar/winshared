﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class NewsController : AdminBaseController
    {
        // GET: Admin/News
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var CountryList = new NewsManager().Get();

            var items = new NewsManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new NewsManager().Count(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                       x.Title,
                       x.ImageFile,
                       x.IsActive,
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult NewsData(int Id = 0)
        {
            NewsViewModel item = new NewsViewModel();
            if (Id != 0)
                item = new NewsManager().GetByid(Id);


            var result= Json(item, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult SaveNews(int Id, string Title,string Brief, string Details,string Keywords, bool IsActive)
        {
            //int Id = Convert.ToInt32(Request.Form["Id"]);
            //string Title = Request.Form["Title"];
            ////string Details = Request.Form["Details"];
            //bool IsActive = Convert.ToBoolean(Request.Form["IsActive"]);

            if (Request.Files.Count == 0 && Id == 0)
            {
                return Json(new { status = 0, msg = "please select image to page !!" });
            }

            NewsManager NewsManager = new NewsManager();

            string filename = "";
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string guid = Guid.NewGuid().ToString();
                    filename = guid + Path.GetExtension(file.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\News\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    file.SaveAs(filePath);
                }

            }
            int result = NewsManager.AddOrUpdate(Id, Title, Brief, Details, Keywords, IsActive, filename, User.Identity.GetUserId());
            CacheManager.HomeNews = new NewsManager().GetTopLayout(3).ToList();
            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "News Saved Success " });

        }

        public JsonResult DeleteNews(int id)
        {
            new NewsManager().Delete(id);
            CacheManager.HomeNews = new NewsManager().GetTopLayout(3).ToList();
            return Json(new { status = 1, msg = "News Deleted Successfully" });

        }
        [HttpPost]
        public JsonResult IsActiveNewsStatus(int id)
        {
            new NewsManager().UpdateIsActive(id, User.Identity.GetUserId());
            CacheManager.HomeNews = new NewsManager().GetTopLayout(3).ToList();
            return Json(new { status = 1, msg = "News Updated Successfully" });

        }

    }
}