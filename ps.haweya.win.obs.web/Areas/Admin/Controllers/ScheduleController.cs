﻿using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class ScheduleController : AdminBaseController
    {
        // GET: Admin/Schedule
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);


            var items = new OBSJobSchedulerManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = items.Count();

            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.JobSchedulerId,
                       SchedulerTime = x.SchedulerTime.ToString(),
                       AsOfUpdated = ((DateTime)x.AsOfUpdated).ToString("yyyy-MM-dd"),
                       x.UpdatedBy,
                       x.EmailIds,
                       x.IsActive,
                       x.ReportId,
                       x.Frequency
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SchedulerData(int JobSchedulerId = 0)
        {
            OBSJobSchedulerViewModel jobScheduler = new OBSJobSchedulerViewModel();
            if (JobSchedulerId != 0)
                jobScheduler = new OBSJobSchedulerManager().GetByid(JobSchedulerId);
            return Json(jobScheduler, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveScheduler()
        {
            long JobSchedulerId = Convert.ToInt64(Request.Form["JobSchedulerId"]);
            DateTime AsOfUpdated = Convert.ToDateTime(Request.Form["AsOfUpdated"]);
            string t = Request.Form["SchedulerTime"];
            TimeSpan SchedulerTime;

            string UpdatedBy = Convert.ToString(Request.Form["UpdatedBy"]);
            if (JobSchedulerId == 0)
            {
                UpdatedBy = User.Identity.Name;
            }
            DateTime d = new DateTime();
            string  customedate = AsOfUpdated.Date.ToShortDateString()+" "+t;
          //  var time = DateTime.ParseExact(t, "HH:mm", null).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US"));
       //     DateTime dateTime = DateTime.ParseExact(time,
         //                           "hh:mm tt", CultureInfo.InvariantCulture);
           // TimeSpan span = dateTime.TimeOfDay;
            //TimeSpan.TryParse(, out SchedulerTime);
            SchedulerTime = Convert.ToDateTime(customedate).TimeOfDay;
            string EmailIds = Convert.ToString(Request.Form["EmailIds"]);
            bool IsActive = Convert.ToBoolean(Request.Form["IsActive"]);
            int ReportId = Convert.ToInt32(Request.Form["ReportId"]);
            int Frequency = Convert.ToInt32(Request.Form["Frequency"]);

            OBSJobSchedulerManager jobSchedulerManager = new OBSJobSchedulerManager();

            long result = jobSchedulerManager.AddOrUpdate(JobSchedulerId, SchedulerTime, AsOfUpdated, UpdatedBy
                , EmailIds, IsActive, ReportId, Frequency, User.Identity.GetUserId());

            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Schedule Saved Successfully " });

        }

        public ActionResult SchedulesAdmin()
        {
            return View();
        }
        public JsonResult DeleteScheduler(int id)
        {
            new OBSJobSchedulerManager().Delete(id);
            return Json(new { status = 1, msg = "Scheduler Deleted Successfully" });

        }
    }
}