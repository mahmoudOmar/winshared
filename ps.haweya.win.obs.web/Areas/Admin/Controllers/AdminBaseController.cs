﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.web.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    [OBSAuthorize(Roles = "Admin")]
    public class AdminBaseController : Controller
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            UserLinks();
            base.OnActionExecuting(filterContext);

            if (User.Identity.IsAuthenticated)
            {
                var aspid = User.Identity.GetUserId();
                var aspnetuser = new AspNetUsersManager().GetByUserId(aspid);
                if (aspnetuser.LockoutEnabled == false)
                    filterContext.Result = new RedirectResult(Url.Action("LoginAdmin", "Home", new { area = "" }));
            }

            var List = new CurrencyManager().Get().ToList();
            var maincurrency = List.Where(x => x.IsMain == true).FirstOrDefault();
            TempData["CurrencyList"] = List;
            TempData["maincurrency"] = maincurrency;
        }

        private void UserLinks()
        {

            if (Request.IsAjaxRequest()) return;

            var cookie = Request.Cookies.Get("menu_loaded");
            var isMenuLoad = Convert.ToBoolean(cookie == null ? "False" : cookie.Value);

            if (!isMenuLoad)
            {

                
             
               var defaultUserRole = ((ClaimsIdentity)User.Identity).Claims.SingleOrDefault(c => c.Type == "DEFAULT_ROLE").Value;
                var arealinks = new LinkManager().GetByArea(defaultUserRole);
                var additionalLinks = new UserLinkManager().GetByUserId(User.Identity.GetUserId()).Select(t => t.Link);

                var userLinks = arealinks.Union(additionalLinks).ToList();

                Request.Cookies.Add(new HttpCookie("menu")
                {
                    Value = JsonConvert.SerializeObject(userLinks),
                    Expires = DateTime.Now.AddDays(15)
                });

                Request.Cookies.Add(new HttpCookie("menu_loaded")
                {
                    Value = Convert.ToString(true),
                    Expires = DateTime.Now.AddDays(15)
                });

                TempData["Links"] = userLinks;

                return;
            }

            // read links from cookies
            TempData["Links"] = JsonConvert.DeserializeObject<List<LinkViewModel>>(cookie.Value);

        }
    }
}