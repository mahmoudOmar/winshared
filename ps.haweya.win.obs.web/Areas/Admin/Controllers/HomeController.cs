﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.web.App_Start;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
 
    public class HomeController : AdminBaseController
    {   
        [OBSAuthorizeAttribute(Roles = "Admin")]
        // GET: Admin/Home
        public ActionResult Index()
        {
            ViewBag.OrdersCount = new BuyerOrderManager().GetCountstoAdmin();
            ViewBag.ProcusctsCount = new ProductInformationManager().GetCountstoAdmin();
            ViewBag.BuyersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Buyer");
            ViewBag.SellersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Seller");
            return View();
        }

   
      
        [OBSAuthorizeAttribute(Roles = "Admin")]
        public JsonResult GetBidsTabel(string q="")
        {

          var list=  new OBSBidDetailsManager().GetWithGroupingProducts("ProductInformation", 1, 10, "bidid", "desc", q).ToList();
            return Json(list,JsonRequestBehavior.AllowGet);
        }
        [OBSAuthorizeAttribute(Roles = "Admin")]
        public JsonResult GetPRoductCountByCountry()
        {

            var list = new ProductDetailsManager().GetWithGroupingProductsByCountry().ToList() ;
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [OBSAuthorizeAttribute(Roles = "Admin")]
        public JsonResult GetPRoductCountByCategory()
        {

            var list = new ProductInformationManager().GetWithGroupingProductsByCategory().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [OBSAuthorizeAttribute(Roles = "Admin")]
        public JsonResult GetBidsCountByProduct()
        {

            var list = new ProductInformationManager().GetWithbidcounttocahrt().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [OBSAuthorizeAttribute(Roles = "Admin")]
        public JsonResult GetBuyerssCountByCountry()
        {

            var list = new AspNetUsersManager().GetBuyersGroupingByCountry().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenerateIndex()
        {
            return new Rotativa.ActionAsPdf("IndexGenerat");
            //return new PdfResult( "Index");
        }
        public ActionResult IndexGenerat()
        {
            ViewBag.OrdersCount = new BuyerOrderManager().GetCountstoAdmin();
            ViewBag.ProcusctsCount = new ProductInformationManager().GetCountstoAdmin();
            ViewBag.BuyersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Buyer");
            ViewBag.SellersCount = new AspNetUsersManager().GetBuyersCountstoAdmin("Seller");
            return View();
        }
    }
}