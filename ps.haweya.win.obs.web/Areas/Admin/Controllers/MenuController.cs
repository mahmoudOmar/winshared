﻿using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class MenuController : AdminBaseController
    {
        public ActionResult Index()
        {
            ViewBag.Pages = new PageManager().Get().ToList();
            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);


            var items = new MenuItemManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = items.Count();

            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                       x.MenuId,
                       x.Title,
                       x.Link,
                       x.IsActive
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult MenuItemData(int id = 0)
        {
            MenuItemViewModel item = new MenuItemViewModel();
            if (id != 0)
                item = new MenuItemManager().GetByid(id);


            return Json(item, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveMenuItem()
        {
            int Id = Convert.ToInt32(Request.Form["Id"]);
            int MenuId = Convert.ToInt32(Request.Form["MenuId"]);
            string Title = Request.Form["Title"].ToString();
            string Link = Request.Form["Link"].ToString();
            bool IsActive = Convert.ToBoolean(Request.Form["IsActive"]);

            MenuItemManager MenuItemManager = new MenuItemManager();

            int result = MenuItemManager.AddOrUpdate(Id,MenuId, Title, Link, IsActive);
            CacheManager.MenuItems = new MenuItemManager().Gettosite().ToList();
            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Menu Item Saved Success " });
        }
        public JsonResult DeleteMenuItem(int id)
        {
            new MenuItemManager().Delete(id);
            CacheManager.MenuItems = new MenuItemManager().Gettosite().ToList();
            return Json(new { status = 1, msg = "Menu Item Deleted Successfully" });

        }
        [HttpPost]
        public JsonResult MenuItemIsActive(int id)
        {
            new MenuItemManager().UpdateIsActive(id);
            CacheManager.MenuItems = new MenuItemManager().Gettosite().ToList();
            return Json(new { status = 1, msg = "Menu Item Updated Successfully" });

        }
    }
}