﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class SliderController : AdminBaseController
    {
        // GET: Admin/Slider
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var CountryList = new SliderManager().Get();

            var items = new SliderManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new SliderManager().Count(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                       x.Title,
                       x.Link,
                       x.ImageFile,
                       x.IsActive,
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SliderData(int Id = 0)
        {
            SliderViewModel item = new SliderViewModel();
            if (Id != 0)
                item = new SliderManager().GetByid(Id);


            return Json(item, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveSlider()
        {
            int Id = Convert.ToInt32(Request.Form["Id"]);
            string Title = Request.Form["Title"];
            string Link = Request.Form["Link"];
            bool IsActive = Convert.ToBoolean(Request.Form["IsActive"]);

            if (Request.Files.Count == 0 && Id == 0)
            {
                return Json(new { status = 0, msg = "please select image to slider !!" });
            }

            SliderManager SliderManager = new SliderManager();

            string filename = "";
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string guid = Guid.NewGuid().ToString();
                    filename = guid + Path.GetExtension(file.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\Slider\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    file.SaveAs(filePath);
                }

            }
            int result = SliderManager.AddOrUpdate(Id, Title,Link, IsActive ,filename, User.Identity.GetUserId());
            CacheManager.SlidersHome = new SliderManager().GetActive().ToList();
            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Slider Saved Success " });

        }

        public JsonResult DeleteSlider(int id)
        {
            new SliderManager().Delete(id);
            CacheManager.SlidersHome = new SliderManager().GetActive().ToList();
            return Json(new { status = 1, msg = "Slider Deleted Successfully" });

        }
        [HttpPost]
        public JsonResult IsActiveSliderStatus(int id)
        {
            new SliderManager().UpdateIsActive(id, User.Identity.GetUserId());
            CacheManager.SlidersHome = new SliderManager().GetActive().ToList();
            return Json(new { status = 1, msg = "Slider Updated Successfully" });

        }

    }
}