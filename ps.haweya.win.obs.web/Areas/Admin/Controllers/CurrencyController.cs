﻿using System;
using System.Collections.Generic;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class CurrencyController : AdminBaseController
    {
        // GET: Admin/Currency
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);



            var items = new CurrencyManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new CurrencyManager().Count(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                       x.Name,
                       x.ShortName,
                       x.Sign,
                       x.RateToRiyal,
                       x.Flag,
                       x.IsMain,
                       x.LastUpdate,
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult BanarData(int Id = 0)
        {
            CurrencyViewModel item = new CurrencyViewModel();
            if (Id != 0)
                item = new CurrencyManager().GetByid(Id);


            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCurrency()
        {
            int Id = Convert.ToInt32(Request.Form["Id"]);
            string Name = Request.Form["Name"];
            string ShortName = Request.Form["ShortName"];
            string Sign = Request.Form["Sign"];
            double RateToRiyal =  Convert.ToDouble(Request.Form["RateToRiyal"]);
            bool IsMain = Convert.ToBoolean(Request.Form["IsMain"]);
            string flag = Request.Form["Flag"];
            //if (Request.Files.Count == 0 && Id == 0)
            //{
            //    return Json(new { status = 0, msg = "please select image to Flag !!" });
            //}

            CurrencyManager Cmanager = new CurrencyManager();

           
          
            int result = Cmanager.AddOrUpdate(Id, Name, ShortName,Sign,RateToRiyal,DateTime.Now,IsMain, flag, User.Identity.Name);

            new CurrencyChangesManager().AddRate(result, RateToRiyal);
         //   CacheManager.Banars = new AdvertismentBanerManager().GetActive(true).OrderBy(x => x.OrderNo).ToList();
            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Data Saved Success " });

        }
    }
}