﻿using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TransactionsLogController : AdminBaseController
    {
        // GET: Admin/TransactionsLog
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);


            var items = new TransactionsLogManager().Get(page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new TransactionsLogManager().Count(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.ID,
                       x.Model,
                       x.Action,
                       x.DateTime,
                       x.RecordID,
                       UserName = x.AspNetUser != null ? x.AspNetUser.FullName : "",
                       UserEmail = x.AspNetUser != null ? x.AspNetUser.Email : "",

                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteLog(int id)
        {
            new TransactionsLogManager().Delete(id);
            return Json(new { status = 1, msg = "Log Deleted Successfully" });

        }
    }
}