﻿using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.viewModels;
using ps.haweya.win.obs.security;
using ps.haweya.win.obs.web.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{

    [OBSAuthorize(Roles = "Admin")]
    public class UserController : AdminBaseController
    {
        // GET: Admin/User
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Buyers()
        {
            return View();
        }
        public ActionResult Sellers()
        {
            return View();
        }
        public JsonResult AjaxDT(string role)
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var CountryList = new CountryManager().Get();

            var items = new AspNetUsersManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query,role);
            var totalItemsCount = new AspNetUsersManager().Count(query,role);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                       Email = x.Email,
                       UserName = x.UserName,
                       EmailConfirmed = x.EmailConfirmed,
                       FullName = x.FullName,
                       PhoneNumber = x.PhoneNumber,
                       ProfilePhoto = x.ProfilePhoto,
                       CountryId = x.CountryId,
                       x.LockoutEnabled,
                       x.InsertDate,
                       Default_Role = x.AspNetUserClaims.SingleOrDefault(c => c.ClaimType == "DEFAULT_ROLE") != null ? x.AspNetUserClaims.SingleOrDefault(c => c.ClaimType == "DEFAULT_ROLE").ClaimValue.ToLower() : "",
                       TotalOrders = x.BuyerOrders.Count() > 0 ? x.BuyerOrders.Select(a => a.ID).Count() + "" : "",
                       Country = CountryList.Where(t => t.id == int.Parse(x.CountryId ?? "0")).FirstOrDefault()
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");
            var usr = new AspNetUsersManager().GetByUserId(id);
            if(usr==null)
                return RedirectToAction("Index");
            return View(usr);
        }
        public ActionResult NestedRols(string UserID)
        {
            var model = new AspNetUsersManager().GetByUserId(UserID);

            ViewBag.Countryimg = CacheManager.Countries.Where(x => x.id == Convert.ToInt32(model.CountryId ?? "0")).FirstOrDefault();

            var links = new UserLinkManager().GetByUserId(UserID).Select(x => x.LinkId);
            var UserLinks = CacheManager.Links.Where(x => links.Contains(x.Id));

            ViewBag.UserLinks = UserLinks.ToList();
            ViewBag.Links = CacheManager.Links.Except(UserLinks).ToList();

            return View(model);
            // return View("testroles");
        }

        public PartialViewResult UserLinks(string UserId)
        {
            var UserLinks = new UserLinkManager().GetByUserId(UserId).Select(x => x.LinkId);
            var defaultRole = (User.Identity as ClaimsIdentity).Claims.Single(t => t.Type.Equals("DEFAULT_ROLE"));
            var links = CacheManager.Links
                                    .Where(t => t.Area.Equals(defaultRole.Value))
                                    .Select(t=> {   t.IsSelected = UserLinks.Contains(t.Id);
                                                    return t;
                                    });

            return PartialView("_userlinks", links);
        }

        public ActionResult AddNewUser()
        {
            ViewBag.Roles = new SelectList(new AspNetRolesManager().Get().Select(x => new { x.Id, x.Name }), "Name", "Name");
            return View();
        }


        #region POST

        public JsonResult UpdateStatus(AspNetUserViewModel user)
        {
            var identityResult = new SecurityManager().UserManager.SetLockoutEnabled(user.Id, user.LockoutEnabled);
            if (user.LockoutEnabled)
            {
                AspNetUserViewModel aspnetuser = new AspNetUsersManager().GetById(user.Id);
                AspNetRoleViewModel role = new AspNetRolesManager().Get().Where(x => x.Name.ToLower().Contains("seller")).FirstOrDefault();

                if (aspnetuser.AspNetRoles.Where(x => x.Name.ToLower().Equals("seller")).Count() > 0)
                {
                    var vdetails = aspnetuser.OBSVendorDetails.FirstOrDefault();

                    if (vdetails != null)
                    {
                        if (string.IsNullOrEmpty(vdetails.VendorCode))
                        {

                            OBSVendorDetailManager vmanager = new OBSVendorDetailManager();
                            string Vcode = GetVCode();


                            vdetails.VendorCode = Vcode;
                            vmanager.Update(vdetails);
                            new MailManager().SendMessage(aspnetuser.Email, "Vendor ID ", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa/", "#", "we want notify you your VendorID is : " + Vcode + " . ", "Warehouse Integration Company", aspnetuser.FullName, Vcode);
                        }
                    }
                    else
                    {
                        OBSVendorDetailManager vmanager = new OBSVendorDetailManager();

                        string Vcode = GetVCode();
                        vdetails = new OBSVendorDetailViewModel();
                        vdetails.VendorCode = Vcode;
                        vdetails.UserId = aspnetuser.Id;
                        vdetails.AddedOn = DateTime.Now;

                        vmanager.Add(vdetails);
                        new MailManager().SendMessage(aspnetuser.Email, "Vendor ID", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa/", "#", "we want notify you your VendorID is : " + Vcode + " . ", "Warehouse Integration Company", aspnetuser.FullName, Vcode);
                    }


                }
            }

            return Json(new
            {
                Status = 200,
                Data = identityResult.Succeeded
            });
        }
        

        [HttpPost]
        public ActionResult AddNewUser(RegisterViewModel model, string Type)
        {
            ViewBag.Roles = new SelectList(new AspNetRolesManager().Get().Select(x => new { x.Id, x.Name }), "Name", "Name", Type);

            if (Type.Equals(""))
            {
                TempData["msg"] = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong> Please select user role</strong>   <button type ='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                return View(model);
            }

            model.Agreepolicies = true;
            if (ModelState.IsValid)
            {
                SecurityManager securityManager = new SecurityManager();
                string result = securityManager.CreateUser(model, Type);


                Guid guid = Guid.Empty;
                Guid.TryParse(result, out guid);


                if (guid != Guid.Empty)
                {
                    TempData["msg"] = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong> User Data Saved Successfully! </strong> All Data  Saved   <button type ='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                    securityManager.VerifyUser(guid.ToString());
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("ERROR", result);
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult AssignUserLinks(string userid, string ids)
        {

            string[] linkids = ids.Split(',');

            UserLinkManager umanager = new UserLinkManager();
            
                umanager.DeleteByUserID(userid);

                var list = new List<UserLinkViewModel>();

                foreach (string id in linkids)
                {
                    list.Add(new UserLinkViewModel()
                    {
                        LinkId = Convert.ToInt32(id),
                        UserId = userid,
                        CreatedBy = User.Identity.GetUserId(),
                        CreatedAt = DateTime.Now
                    });
                }

                umanager.AddLinkRangeToUser(list);

                // check if the user does not have secondary role
                var link = new LinkManager().GetById(int.Parse(linkids[0]));
                //var userHasSecondaryRole = SecurityManager.IsUserInRole(userid, link.AspNetRoles.FirstOrDefault().Name);
                var userHasSecondaryRole = SecurityManager.IsUserInRole(userid, link.Area);

                if (!userHasSecondaryRole)
                {
                    new SecurityManager().UserManager.AddToRole(userid, link.Area);
                }
            

            return Json(new { status = 1, msg = "msg" });
        }
        #endregion


        public string GetVCode()
        {
            Random r = new Random(100000);

            string Vcode = r.Next(100001, 999999).ToString();
            List<string> codelist = new OBSVendorDetailManager().GetCodes();
            while (codelist.Contains(Vcode))
            {
                Vcode = r.Next(100000, 999999).ToString();
            }
            return Vcode;
        }
    }
}