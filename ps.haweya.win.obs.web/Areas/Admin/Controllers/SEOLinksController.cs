﻿using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class SEOLinksController : AdminBaseController
    {
        // GET: Admin/SEOLinks
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);


            var items = new LinkManager().GetSeoLinks();
            var totalItemsCount = items.Count();

            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                       x.ActionDisplayName,
                       x.SEO_Titel,
                       x.SEO_Image,
                       x.AreaDisplayName,
                       x.ControllerDisplayName,
                      
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int Id)
        {

            var link = new LinkManager().GetById(Id);

            return View(link);        
        }
        [HttpPost]
        public ActionResult Edit(LinkViewModel model)
        {
            if (model.SEO_Titel.Length > 0 && model.SEO_Description.Length > 0 && model.SEO_Keyword.Length > 0)
            {
                
                var entity = new LinkManager().GetById(model.Id);
                entity.SEO_Titel = model.SEO_Titel;
                entity.SEO_Description = model.SEO_Description;
                entity.SEO_Keyword = model.SEO_Keyword;
                if (Request.Files.Count > 0)
                {

                    HttpPostedFileBase Image = Request.Files[0];


                    string guid = Guid.NewGuid().ToString();
                    string filename = Image.FileName;
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\SEO\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    if (!String.IsNullOrEmpty(filename))
                    { 
                    Image.SaveAs(filePath);
                    entity.SEO_Image = filename;
                }
                }

                new LinkManager().Update(entity);
                TempData["msg"]= "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong> Data Saved Successfully ! </strong> All Data  Saved <button type ='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("errors", "Please Fill all Data ");
            return View(model);
        }
    }
}