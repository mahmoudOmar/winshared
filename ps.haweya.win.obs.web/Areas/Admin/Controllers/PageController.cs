﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Admin.Controllers
{
    public class PageController : AdminBaseController
    {
        // GET: Admin/Page
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AjaxDT()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perpage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            var CountryList = new PageManager().Get();

            var items = new PageManager().GetWithInclouds("", page, perpage, sortBy, sortMethod, query);
            var totalItemsCount = new PageManager().Count(query);


            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perpage + "")),
                       perpage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.Id,
                       x.Title,
                       x.ImageFile,
                       x.IsActive,
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PageData(int Id = 0)
        {
            PageViewModel item = new PageViewModel();
            if (Id != 0)
                item = new PageManager().GetByid(Id);


            return Json(item, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult SavePage(int Id, string Title, string Details,string Keywords, bool IsActive)
        {
            //int Id = Convert.ToInt32(Request.Form["Id"]);
            //string Title = Request.Form["Title"];
            ////string Details = Request.Form["Details"];
            //bool IsActive = Convert.ToBoolean(Request.Form["IsActive"]);

            if (Request.Files.Count == 0 && Id == 0)
            {
                return Json(new { status = 0, msg = "please select image to page !!" });
            }

            PageManager PageManager = new PageManager();

            string filename = "";
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string guid = Guid.NewGuid().ToString();
                    filename = guid + Path.GetExtension(file.FileName);
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\pages\\");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var filePath = Path.Combine(path, filename);
                    file.SaveAs(filePath);
                }

            }
            int result = PageManager.AddOrUpdate(Id, Title, Details, Keywords, IsActive, filename, User.Identity.GetUserId());

            if (result <= 0)
            {
                return Json(new { status = 0, msg = "Please Try Again" });
            }



            return Json(new { status = 1, msg = "Page Saved Success " });

        }

        public JsonResult DeletePage(int id)
        {
            new PageManager().Delete(id);
            return Json(new { status = 1, msg = "Page Deleted Successfully" });

        }
        [HttpPost]
        public JsonResult IsActivePageStatus(int id)
        {
            new PageManager().UpdateIsActive(id, User.Identity.GetUserId());
            return Json(new { status = 1, msg = "Page Updated Successfully" });

        }

    }
}