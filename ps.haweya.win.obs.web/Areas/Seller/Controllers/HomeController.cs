﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.web.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Seller.Controllers
{

    public class HomeController : SellerBaseController
    {
        // GET: Seller/Home
        public ActionResult Index()
        {

            string name = new AspNetUsersManager().GetByUserId(User.Identity.GetUserId()).Email;
            ViewBag.OrdersCount = new BuyerOrderManager().GetCountstoSeller(name);
            ViewBag.ProcusctsCount = new ProductInformationManager().GetCountstoAdmin(name);
            ViewBag.ProductBids = new ProductInformationManager().GetBidsProductWithseller("Auctionable",name);
            ViewBag.DirectPurchased = new ProductInformationManager().GetBidsProductWithseller("Non-Auctionable", name);
            return View("View");
        }


        public ActionResult MyAccount()
        {
            var model = new AspNetUsersManager().GetByEmail(User.Identity.Name);

            ////model.Country = CacheManager.Countries.SingleOrDefault(t => t.id.ToString() == model.CountryId);
            ////model.State = CacheManager.States.SingleOrDefault(t => t.id.ToString() == model.StateId);
            ////model.City = CacheManager.Cities.SingleOrDefault(t => t.id.ToString() == model.CityId);

            return View(model);
        }

        public JsonResult GetBidsTabel(string q = "")
        {
            string name = new AspNetUsersManager().GetByUserId(User.Identity.GetUserId()).Email;

            var list = new OBSBidDetailsManager().GetWithGroupingProductsToSeller(name,"ProductInformation", 1, 10, "bidid", "desc", q).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPRoductCountByCountry()
        {
            string name = new AspNetUsersManager().GetByUserId(User.Identity.GetUserId()).Email;

            var list = new ProductDetailsManager().GetWithGroupingProductsByCountryToSeller(name).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPRoductCountByCategory()
        {
            string name = new AspNetUsersManager().GetByUserId(User.Identity.GetUserId()).Email;
            var list = new ProductInformationManager().GetWithGroupingProductsByCategory(name).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBidsCountByProduct()
        {
            string name = new AspNetUsersManager().GetByUserId(User.Identity.GetUserId()).Email;
            var list = new ProductInformationManager().GetWithbidcounttocahrt(name).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBuyerssCountByCountry()
        {
            string name = new AspNetUsersManager().GetByUserId(User.Identity.GetUserId()).Email;
            var list = new AspNetUsersManager().GetBuyersGroupingByCountry(name).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult MyAccount(AspNetUserViewModel user)
        {
            new AspNetUsersManager().Update(user);
            return RedirectToAction("MyAccount");
        }
    }
}