﻿using Hangfire;
using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.business.PageModel;
using ps.haweya.win.obs.security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using static ps.haweya.win.obs.business.Managers.OBSJobSchedulerManager;

namespace ps.haweya.win.obs.web.Areas.Seller.Controllers
{
    public class ProductController : SellerBaseController
    {
        ProductUnit pUnit = new ProductUnit();

        #region GET


        public ActionResult Preview(int ProductId)
        {
            var model = new ProductInformationManager().GetByid(ProductId);

            //model.Country = CacheManager.Countries.SingleOrDefault(t => t.id == model.ProductDetails.First().Country);
            //model.State = CacheManager.States.SingleOrDefault(t => t.id == int.Parse(model.ProductDetails.First().State ?? "0"));
            //model.City = CacheManager.Cities.SingleOrDefault(t => t.id == int.Parse(model.ProductDetails.First().City ?? "0"));
            int country = 0;
            int state = 0;
            int city = 0;

            int.TryParse(model.ProductDetails.First() != null ? model.ProductDetails.First().Country + "" : "0", out country);
            model.Country = CacheManager.Countries.SingleOrDefault(t => t.id == country);

            int.TryParse(!string.IsNullOrEmpty(model.ProductDetails.First().State) ? model.ProductDetails.First().State : "0", out state);
            model.State = CacheManager.States.SingleOrDefault(t => t.id == state);

            int.TryParse(!string.IsNullOrEmpty(model.ProductDetails.First().City) ? model.ProductDetails.First().City : "0", out city);
            model.City = CacheManager.Cities.SingleOrDefault(t => t.id == city);

            return View(model);
        }
        public ActionResult BidMaterial()
        {
            return View();
        }
        public ActionResult DirectMaterials()
        {
            return View("DirectMaterials");
        }
        public ActionResult ArchiveActive()
        {
            return View("ArchiveActive");
        }

        public ActionResult Archive()
        {
            return View("Archive");
        }

        public ActionResult ArchiveBid()
        {
            return View("ArchiveBid");
        }

        public ActionResult MyMaterial()
        {
            return View("Index");
        }

        public ActionResult NewListing()
        {

            var listingItemPageModel = new NewListingItemPageModel();

            listingItemPageModel.Categories = new CategoryManager().Get();
            // listingItemPageModel.Manufacturers = CacheManager.OBSManufacturers;
            listingItemPageModel.Countries = CacheManager.Countries;
            listingItemPageModel.Companies = new CompanyManager().Get();
            listingItemPageModel.SubCommodities = new SubCommodityManager().Get();
            listingItemPageModel.Manufacturers = new ManufacturerDetailsManager().Get();
            ViewBag.Companies = new CompanyManager().Get();
            return View(listingItemPageModel);
        }
        public ActionResult EditProduct(long id)
        {
            var model = new ProductInformationManager().GetByid(id);

            int country = 0;
            int state = 0;
            int city = 0;

            int.TryParse(model.ProductDetails.First() != null ? model.ProductDetails.First().Country + "" : "0", out country);
            model.Country = CacheManager.Countries.SingleOrDefault(t => t.id == country);

            int.TryParse(!string.IsNullOrEmpty(model.ProductDetails.First().State) ? model.ProductDetails.First().State : "0", out state);
            model.State = CacheManager.States.SingleOrDefault(t => t.id == state);

            int.TryParse(!string.IsNullOrEmpty(model.ProductDetails.First().City) ? model.ProductDetails.First().City : "0", out city);
            model.City = CacheManager.Cities.SingleOrDefault(t => t.id == city);


            var listingItemPageModel = new NewListingItemPageModel();

            listingItemPageModel.Categories = new CategoryManager().Get();
            // listingItemPageModel.Manufacturers = CacheManager.OBSManufacturers;
            listingItemPageModel.Countries = CacheManager.Countries;
            listingItemPageModel.Companies = new CompanyManager().Get();
            listingItemPageModel.SubCommodities = new SubCommodityManager().Get();
            listingItemPageModel.Manufacturers = new ManufacturerDetailsManager().Get();
            listingItemPageModel.ProductInformationViewModel = model;
            ViewBag.States = CacheManager.States.Where(x => x.country_id == country).Select(x => new { x.id, x.name }).ToList();
            ViewBag.Cities = CacheManager.Cities.Where(x => x.Stateid == state);
            ViewBag.Companies = new CompanyManager().Get();

            return View(listingItemPageModel);
        }
        public JsonResult ListOfStates(int CountryId)
        {
            var states = CacheManager.States.Where(t => t.country_id.Equals(CountryId));
            return Json(states, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListOfCities(int StateId)
        {
            var states = CacheManager.Cities.Where(t => t.Stateid.Equals(StateId));
            return Json(states, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CartByStatus()
        {
            return Json(new
            {
                Data = pUnit.GetCartItemsByStatus(User.Identity.Name)
            }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult BidByStatus()
        {
            return Json(new
            {
                Data = pUnit.GetCartItemsByStatus(User.Identity.Name)
            }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ActiveStatus()
        {
            return Json(new
            {
                Data = pUnit.GetByActiveStatus(User.Identity.Name)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Invoices()
        {
            ViewBag.SellerID = new AspNetUsersManager().GetByEmail(User.Identity.Name).Sellers[0].ID;

            return View();
        }

        public JsonResult Invoicesajaxdt()
        {
            var query = Request.QueryString["query[query]"] ?? "";

            var sortBy = Request.QueryString["sort[field]"];
            var sortMethod = Request.QueryString["sort[sort]"];

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perPage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);


            var CountryList = new CountryManager().Get();

            var items = new BuyerOrderManager().GetInvoicesSeller("Status", page, perPage, sortBy, sortMethod, query,User.Identity.Name);
            var totalItemsCount = new BuyerOrderManager().CountINvoicesSeller(query, User.Identity.Name);

            var result =
               new
               {
                   meta = new
                   {
                       page,
                       pages = Math.Ceiling(totalItemsCount / double.Parse(perPage + "")),
                       perpage = perPage,
                       total = totalItemsCount,
                   },
                   data = items.Select(x => new
                   {
                       x.ID,
                       FullName = x.AspNetUser.FullName,
                       ProfilePhoto = x.AspNetUser.ProfilePhoto,
                       Email = x.AspNetUser.Email,
                       CountryId = x.AspNetUser.CountryId,
                       ItemsCount = x.CartItems.Count(),
                       TotalPrice = ((x.TotalPrice ?? 0) + ((x.TotalPrice ?? 0) * 0.05)).ToString("N0"),
                       payment_method = x.payment_method ?? "",
                       StatusID = x.StatusID,
                       StatusText = x.Status.Name,
                       x.InsertDate,
                       DeliveryType = x.DeliveryType ?? 0,
                       x.AdminConfirmed,
                       Country = CountryList.Where(t => t.id == int.Parse(x.AspNetUser.CountryId ?? "0")).FirstOrDefault()
                   }).ToList()


               };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Invoice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BuyerOrderViewModel buyerOrder = new BuyerOrderManager().GetByid(Convert.ToInt32(id));
            ViewBag.SellerID = new AspNetUsersManager().GetByEmail(User.Identity.Name).Sellers[0].ID;
            if (buyerOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.Bank = new BankAccountsManager().Get().FirstOrDefault();
            return View(buyerOrder);
        }
        public JsonResult Get(string Type)
        {

            var query = Request.QueryString["query[query]"] ?? "";

            var SortBy = Request.QueryString["sort[field]"] ?? "Id";
            var SortMethod = Request.QueryString["sort[sort]"] ?? "desc";

            var page = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[page]"]) ? "1" : Request.QueryString["pagination[page]"]);
            var pages = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[pages]"]) ? "1" : Request.QueryString["pagination[pages]"]);
            var perPage = int.Parse(string.IsNullOrEmpty(Request.QueryString["pagination[perpage]"]) ? "10" : Request.QueryString["pagination[perpage]"]);

            object items = null;
            var totalItemsCount = 0;

            switch (Type)
            {
                case "1":
                    items = pUnit.GetPurchaseItems(new SecurityManager().GetUser(User.Identity.Name).Email, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        t.ProductInformation.ProductId,
                        ProductImg = t.ProductInformation.ProductImg[0],
                        t.ProductInformation.Username,
                        t.ProductInformation.ProductTitle,
                        t.ProductInformation.Quantity,
                        BuyerName = t.BuyerOrder.AspNetUser.FullName,
                        t.BuyerOrder.AspNetUser.Email,
                        t.BuyerOrder.AspNetUser.PhoneNumber,
                        t.ProductInformation.PricingType,
                        t.ProductInformation.CreateDate,
                        OrderNO = t.BuyerOrder.ID,
                        OrderDate = t.BuyerOrder.InsertDate,
                        Status = t.IsBought ? "Purchased" : "UnPurchased"
                    });

                    totalItemsCount = pUnit.GetPurchaseItemsCount(User.Identity.Name, query);

                    break;
                case "2":
                    items = pUnit.GetBidItems(new SecurityManager().GetUser(User.Identity.Name).Email, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        id=t.BidId,
                        t.ProductInformation.ProductId,
                        t.ProductInformation.Username,
                        ProductImg = t.ProductInformation.ProductImg[0],
                        t.ProductInformation.ProductTitle,
                        t.ProductInformation.Quantity,
                        Bidder = t.AspNetUser.FullName,
                        t.BidPrice,
                        t.ProductInformation.CreateDate,
                        t.IsShipment,
                        t.ProductInformation.PricingType,
                        t.AspNetUser.Email,
                        t.AspNetUser.PhoneNumber,
                        t.AddedOn,
                        CountryName=t.ProductInformation.AllowShowBiddersLocations==true?(CacheManager.Countries.Where(l=>l.id==Convert.ToInt32(t.AspNetUser.CountryId)).FirstOrDefault()!=null? CacheManager.Countries.Where(l => l.id == Convert.ToInt32(t.AspNetUser.CountryId)).FirstOrDefault().name:""):"",
                        StateName= t.ProductInformation.AllowShowBiddersLocations == true ? (CacheManager.States.Where(l => l.id == Convert.ToInt32(t.AspNetUser.StateId)).FirstOrDefault() != null ? CacheManager.States.Where(l => l.id == Convert.ToInt32(t.AspNetUser.StateId)).FirstOrDefault().name : ""):"",
                        CityName = t.ProductInformation.AllowShowBiddersLocations == true ? (CacheManager.Cities.Where(l => l.id == Convert.ToInt32(t.AspNetUser.CityId)).FirstOrDefault() != null ? CacheManager.Cities.Where(l => l.id == Convert.ToInt32(t.AspNetUser.CityId)).FirstOrDefault().text : ""):"",
                        Status = t.IsSold ? "Sold" : "UnSold",
                           BidsCount = t.BidCount,
                           acceptbid=t.IsSold!=true,
                    }) ;

                    totalItemsCount = pUnit.GetBidItemsCount(User.Identity.Name, query);
                    break;

                case "3":
                    items = pUnit.GetByActiveStatus(new SecurityManager().GetUser(User.Identity.Name).Email, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        t.ProductId,
                        t.Username,
                        ProductImg = t.ProductImg[0],
                        t.ProductTitle,
                        t.Quantity,
                        t.PricingType,
                        t.SellerName,
                        t.CreateDate,
                        Status = t.IsWinActive ? "Active" : "In Active",
                        t.IsSold,
                        BidsCount = t.OBSBidDetails.Sum(s => s.BidCount),
                    });

                    totalItemsCount = pUnit.GetByActiveStatusCount(User.Identity.Name, query);
                    break;
                case "4":
                    items = pUnit.GetBidsProduct(new SecurityManager().GetUser(User.Identity.Name).Email, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        t.ProductId,
                        t.Username,
                        ProductImg = t.ProductImg[0],
                        t.ProductTitle,
                        t.Quantity,
                        t.IsWinActive,
                        t.PricingType,
                        t.CreateDate,
                        t.SellerName,
                        Status = t.IsWinActive ? "Active" : "In Active",
                        SoldStatus = t.OBSBidDetails.Where(x => x.IsSold == true).Count() != 0 ? "Sold" : "UnSold",
                        Soldcount = t.OBSBidDetails.Where(x => x.IsSold == true).Count(),
                        BidsCount = t.OBSBidDetails.Sum(s => s.BidCount),
                    }); ;

                    totalItemsCount = pUnit.GetBidsProductCount(null, query);
                    break;

                case "5":
                    items = pUnit.GetDirectByActiveStatus(new SecurityManager().GetUser(User.Identity.Name).Email, query, SortBy, SortMethod, page, pages, perPage).Select(t => new
                    {
                        t.ProductId,
                        t.Username,
                        ProductImg = t.ProductImg[0],
                        t.ProductTitle,
                        t.Quantity,
                        t.IsWinActive,
                        t.PricingType,
                        t.CreateDate,
                        t.SellerName,
                        Status = t.IsWinActive ? "Active" : "In Active",

                    });

                    totalItemsCount = pUnit.GetDirectByActiveStatusCount(null, query);

                    break;
            }

            return Json(new
            {
                meta = new
                {
                    page,
                    pages = Math.Ceiling(totalItemsCount / double.Parse(perPage + "")),
                    perpage = perPage,
                    total = totalItemsCount
                },
                data = items
            }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetAttachment(int id)
        {
            var model = new ProductInformationManager().GetByid(id);
            List<AttachmentsModel> myimages = new List<AttachmentsModel>();
            string[] stringSeparators = new string[] { "(&)" };
            string[] imagesstring = model.ProductImage.Split(stringSeparators, StringSplitOptions.None);
            int counter = 0;
            foreach (var itm in imagesstring)
            {
                if (System.IO.File.Exists("/Content/UploadData/UploadImages/" + itm))
                {
                    FileInfo myfile = new FileInfo("/Content/UploadData/UploadImages/" + itm);

                    var newimg = new AttachmentsModel() { AttachmentID = counter + 1, FileName = "/Content/UploadData/UploadImages/" + itm, Path = "/Content/UploadData/UploadImages/" + itm, size = myfile.Length };
               
                myimages.Add(newimg);
                
                }
                else
                {

                    FileInfo myfile = new FileInfo("/Content/UploadData/UploadImages/" + itm);

                    var newimg = new AttachmentsModel() { AttachmentID = counter + 1, FileName = "/Content/UploadData/UploadImages/" + itm, Path = "/Content/UploadData/UploadImages/" + itm, size =12345 };

                    myimages.Add(newimg);
                }
            }
           return Json(new { Data = myimages }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAttachmentdocs(int id)
        {
            var model = new ProductInformationManager().GetByid(id);
            List<AttachmentsModel> myimages = new List<AttachmentsModel>();
            string[] imagesstring = model.ProductImage.Split(',');
            int counter = 0;
            foreach (var itm in imagesstring)
            {
                if (System.IO.File.Exists("/Content/UploadData/UploadDocuments/" + itm))
                {
                    FileInfo myfile = new FileInfo("/Content/UploadData/UploadDocuments/" + itm);

                    var newimg = new AttachmentsModel() { AttachmentID = counter + 1, FileName = "/Content/UploadData/UploadDocuments/" + itm, Path = "/Content/UploadData/UploadImages/" + itm, size = myfile.Length };

                    myimages.Add(newimg);

                }
                else
                {

                    FileInfo myfile = new FileInfo("/Content/UploadData/UploadDocuments/" + itm);

                    var newimg = new AttachmentsModel() { AttachmentID = counter + 1, FileName = "/Content/UploadData/UploadDocuments/" + itm, Path = "/Content/UploadData/UploadDocuments/" + itm, size = 12345 };

                    myimages.Add(newimg);
                }
            }
            return Json(new { Data = myimages }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region POST

        [HttpPost, ValidateInput(false)]
        
        public JsonResult AddProduct(ProductInformationViewModel model, int? ShowBidAmmount, int? hasMinimumSellingPrice, bool? ShowInMap)
        {
            
//            var images = (model.Images ?? "").Split(',');
//            var documents = (model.Documents ?? "").Split(',');

            var seller = new SecurityManager().GetUser(User.Identity.Name);


            model.ShowBidAmmount = ShowBidAmmount == 1 ? true : false;
            model.hasMinimumSellingPrice = hasMinimumSellingPrice == 1 ? true : false;
            model.ShowInMap = ShowInMap;

            model.CreateDate = DateTime.Now;
            if (model.DeadLineInDays > 0)
            {
                model.DeadLine =  DateTime.Now.AddDays(model.DeadLineInDays);
            }
            model.Username = seller.Email;
            model.SellerName = seller.FullName;
            model.ProductImage = model.ProductImage ?? string.Empty;
            model.AvailableQuantity = int.Parse(model.Quantity ?? "0");
            if (model.PricingType == "Auctionable")
            {
                model.BidStartDate = model.BidStartDate == null ? DateTime.Now : model.BidStartDate;
            }

            // these values should be reviewed carefully
            #region default values
            model.BidCount = model.BidCount==null?0:model.BidCount;
            model.ProductBrands = model.ProductBrands==null?0: model.ProductBrands;
            model.ShipmentCharges = model.ShipmentCharges==null? 0 : model.ShipmentCharges;
            model.ProductLocation = model.ProductLocation?? string.Empty;
            model.SuggestionByWin = model.SuggestionByWin??string.Empty;
            model.OtherCategory = model.OtherCategory??string.Empty;
            model.OtherManufacturer = model.OtherManufacturer?? string.Empty;
            model.EndUser = model.EndUser?? string.Empty;
            model.SupplierName = model.SupplierName?? string.Empty;
            model.CertificatesOrDataSheet = model.CertificatesOrDataSheet?? string.Empty;
            model.WINAssement = model.WINAssement??string.Empty;
            model.WINAssementby = model.WINAssementby?? string.Empty;
            model.Repairable = model.Repairable?? string.Empty;
            model.UsedEquipmentOrApplication = model.UsedEquipmentOrApplication?? string.Empty;
            model.SellerMaterialId = model.SellerMaterialId?? string.Empty;

            //model.UnitPrice = model.Price;
            model.ProductDetails[0].Description = model.ProductDetails[0].Description ?? string.Empty;
            model.ProductDetails[0].MinimumSellingPrice = model.ProductDetails[0].MinimumSellingPrice;
            model.ProductDetails[0].Condition = model.ProductDetails[0].Condition ?? string.Empty;
            model.ProductDetails[0].SellerState = model.ProductDetails[0].SellerState ?? string.Empty;
            model.ProductDetails[0].SellerCity = model.ProductDetails[0].SellerCity ?? string.Empty;
            model.ProductDetails[0].Fax = model.ProductDetails[0].Fax ?? string.Empty;
            model.ProductDetails[0].Company = model.ProductDetails[0].Company ??  string.Empty;
            model.ProductDetails[0].Contact = model.ProductDetails[0].Contact ?? string.Empty;
            model.ProductDetails[0].Phone = model.ProductDetails[0].Phone ?? string.Empty;
            model.ProductDetails[0].SpecialInstructions = model.ProductDetails[0].SpecialInstructions ?? string.Empty;
            model.ProductDetails[0].City = model.ProductDetails[0].City ?? string.Empty;
            model.ProductDetails[0].State = model.ProductDetails[0].State ?? string.Empty;
            
            model.isActive = false;
            model.IsWinActive = false;
            model.isPrivate = false;
            model.IsSold = false;
            
            model.SecurityDepositDays = model.SecurityDepositDays ?? 0;
            model.SecurityDepositPercent = model.SecurityDepositPercent ?? 0;
            var response = new ProductInformationViewModel();
            #endregion
            if (model.ProductId != null && model.ProductId != 0)
            {
                new ProductDetailsManager().Update(model.ProductDetails[0]);
                response = new ProductUnit().UpdateFull(model);

            }
            else
            {
                 response = new ProductUnit().AddProduct(model);
                //new ReportDeliveryManager().SetSendBidClosedReport(response);

            }

                if (response.PricingType == "Auctionable" && model.DeadLineInDays > 0 && model.DeadLine != null)
                {
                    //send hangfile  for the winner;
                    try
                    {
                        new ReportDeliveryManager().SetSendWinnerBidClosedReport(response);
                    }
                    catch { };
                }
            
            return Json(new
            {
                Status = 200,
                Code = response.ProductId > 0 ? 1 : 2,
                Message = response.ProductId > 0 ? "New Product has been added successfully" : "Error Please try again"
            });
        }
        [HttpPost]
        public JsonResult deleteproduct(long id)
        {

            ProductInformationManager manager = new ProductInformationManager();
            var produt = manager.GetByid(id);
            produt.IsDelete = true;
            produt.IsWinActive = false;
            manager.Delete(produt);
            return Json(new { status = 1 });


        }

        [HttpPost]
        public JsonResult AcceptBid(int bid)
        {
            //int ID = 0;
            var lastbidhistory = new BidHistoryManager().GetByBidId(bid).OrderByDescending(x => x.InsertDate).FirstOrDefault();
           // if(lastbidhistory!=null)
            int ID= new BidHistoryManager().Add(bid, 23, "Bid Accept By seller", User.Identity.GetUserId(),Convert.ToInt16(lastbidhistory!=null?lastbidhistory.NewStatusID:1));
            if (ID > 0)
            {
                OBSBidDetailsManager bidmanager = new OBSBidDetailsManager();
                bidmanager.UpdateBidStatus(bid, 23);
              
                    var biddetails = bidmanager.Get(bid);
                    biddetails.IsSold = true;
                    bidmanager.UpdateBid(biddetails);
                    var prodduct = biddetails.ProductInformation;
                    prodduct.IsSold = true;
                    new ProductInformationManager().UpdateFull(prodduct);

                new MailManager().SendBidwinnerClosedReport(prodduct, biddetails.AspNetUser,biddetails.BidPrice);
            }

           
            return Json(new { status = ID });
        }


        #endregion
    }
}