﻿using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ps.haweya.win.obs.common;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System.Data;
using ps.haweya.win.obs.web.Models;
using System.Data.SqlClient;
using MoreLinq;
using static ps.haweya.win.obs.web.Models.ProductInformationImportViewModel;
using System.Configuration;
using ps.haweya.win.obs.security;
using ClosedXML.Excel;

namespace ps.haweya.win.obs.web.Areas.Seller.Controllers
{
    public class UploadProductsController : SellerBaseController
    {
        // GET: Admin/UploadProducts
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UploadFiles()
        {

            ViewBag.Company = new SelectList(new CompanyManager().Get(), "ID", "Name");
            //ViewBag.Sellers = new SelectList(new AspNetUsersManager(), "UserName", "FullName");



            return View();
        }

        public ActionResult UploadImagesBulk(string batchno, string companyid)
        {
            ViewBag.Company = new SelectList(new CompanyManager().Get(), "ID", "Name", companyid);
            //ViewBag.Sellers = new SelectList(new AspNetUsersManager().GetByRole("seller"), "UserName", "FullName", sellerid);
            ViewBag.batchno = new SelectList(new ProductInformationManager().GetBatchNoBySellerID(""), batchno);
            //if (!String.IsNullOrEmpty(sellerid))
            //{
            //   // string Sellername = new SecurityManager().GetUser(sellerid).UserName;

                ViewBag.batchno = new SelectList(new ProductInformationManager().GetBatchNoBySellerID(User.Identity.Name), batchno);
            //}

            //ViewBag.Sellers = new SelectList(new ProductInformationManager().GetBatchNoBySellerID());

            return View();
        }

        public JsonResult GetSellerBathces()
        {
           // string Sellername = new SecurityManager().GetByUserId(sellerid).FullName;
            var batches = new ProductInformationManager().GetBatchNoBySellerID(User.Identity.Name);

            return Json(batches, JsonRequestBehavior.AllowGet);
        }


        public ActionResult UpdateBulkData()
        {
            ViewBag.Company = new SelectList(new CompanyManager().Get(), "ID", "Name");
            // ViewBag.Sellers = new SelectList(new AspNetUsersManager().GetByRole("seller"), "UserName", "FullName");
             //ViewBag.batchno = new SelectList(new ProductInformationManager().GetBatchNoBySellerID(User.Identity.Name), batchno);


            return View();
        }

        [HttpPost]
        public ActionResult UploadBulkUpdate()
        {
            string message = "Please Upload one File at least";
            string companyID = Request.Form["CompanyID"];
            string SellerID = User.Identity.Name;// Request.Form["SellerID"];
            int batchno = Convert.ToInt32(Request.Form["batchno"]);
          //  string Sellername = new SecurityManager().GetByUserId(SellerID).FullName;
            string companyName = new CompanyManager().GetByid(Convert.ToInt32(companyID)).Name;
            //  int batchno = new ProductInformationManager().GetLastBatchNo(Sellername);
            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];
                if (file != null && file.ContentLength > 0)
                {
                    var fname = file.FileName;
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadDocuments");
                    if (!string.IsNullOrEmpty(SellerID))
                    {

                        path = Server.MapPath("~\\Content\\UploadData\\UploadDocuments\\" + SellerID);
                        if (!System.IO.Directory.Exists(path))
                        {
                            System.IO.Directory.CreateDirectory(path);
                        }

                    }
                    //var path = Server.MapPath("~\\App_Data\\BulkUploadDoc\\");
                    //if (!System.IO.Directory.Exists(path))
                    //{
                    //    System.IO.Directory.CreateDirectory(path);
                    //}
                    //// Get the complete folder path and store the file inside it.  
                    var filePath = Path.Combine(path, fname);
                    file.SaveAs(filePath);

                    var data = filePath.ExcelToDataTable();

                    message = UpdateBulkAssetData(data, companyName, SellerID, batchno);

                    // TempData["msg"] = message;
                }
            }
            if (message.Contains("Successfully"))
            {
                return Json(new { status = 1, msg = message, batchno = batchno });
            }

            return Json(new { status = 0, msg = message, batchno = batchno });
        }
        [HttpPost]
        public ActionResult GetTempleteFile(int companyid, string batchno)
        {
            string sellerid = User.Identity.Name;
           // string Sellername = new SecurityManager().GetByUserId(sellerid).FullName;
            string companyname = new CompanyManager().GetByid(companyid).Name;
            //new ProductInformationManager 
            DataTable dt = new ProductDetailsManager().ExportProductsByBatch(sellerid, companyname, Convert.ToInt32(batchno));
            //string filepath = "";C:\Users\MahmoudPC\Desktop\Haweya\Obs_New\ps.haweya.win.obs.web\App_Data\BulkUploadDoc\ProductBulkUploadTemplate.xlsx
            using (XLWorkbook workBook = new XLWorkbook())
            {
                //Read the first Sheet from Excel file.
                workBook.Worksheets.Add(dt, "ProductTemplate");

                workBook.SaveAs(Server.MapPath("~/content/uploaddata/" + sellerid + batchno + ".xlsx"));


            }

            try
            {
                //string fullPath = Path.Combine(filepath.Trim(), "SampleProductUpload" + ".xlsm");
                var fileInfo = new FileInfo(Server.MapPath("~/content/uploaddata/" + sellerid + batchno + ".xlsx"));


                //return Redirect("~/content/uploaddata/" + Sellername + "_" + companyname + "_" + batchno + ".xlsx");
                //DownloadFile(fileInfo);

                return Json(new { filename = sellerid + batchno + ".xlsx" });
            }
            catch { }

            return View();
        }


        [HttpPost]
        public ActionResult GetTempleteInsertFile()
        {

            return Json(new { filename = "Product_template_insert.xlsx" });
        }

        [HttpPost]
        public ActionResult UploadImages()
        {
            string message = "Please Upload one File at least";
            string companyID = Request.Form["CompanyID"] != null ? Request.Form["CompanyID"].Split(',')[0] : "";
            string batchnostring = Request.Form["batchno"] != null ? Request.Form["batchno"].Split(',')[0] : "0";
            string SellerID =  User.Identity.Name;// Request.Form["SellerID"];
           // string Sellername = new SecurityManager().GetByUserId(SellerID).FullName;
           // string batchnostring = Request.Form["batchno"];
            int batchno = Convert.ToInt32(batchnostring);
            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];
                if (file != null && file.ContentLength > 0)
                {
                    var fname = file.FileName;
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadImages");
                    if (!string.IsNullOrEmpty(SellerID))
                    {

                        path = Server.MapPath("~\\Content\\UploadData\\UploadImages\\" + SellerID);
                        if (!System.IO.Directory.Exists(path))
                        {
                            System.IO.Directory.CreateDirectory(path);
                        }

                    }
                    // var path = Server.MapPath("~\\App_Data\\BulkUploadDoc\\");

                    // Get the complete folder path and store the file inside it.  
                    var filePath = Path.Combine(path, fname);

                    file.SaveAs(filePath);

                    // var data = filePath.ExcelToDataTable();


                }

            }
            message = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong> Bulk Upload Done Successfully! </strong> All Data  Saved <button type ='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            return Json(new { status = 1, msg = message });
        }
        [HttpPost]
        public ActionResult Upload()
        {
            string message = "Please Upload one File at least";
            string companyID = Request.Form["CompanyID"];
            string SellerID =  User.Identity.Name; //Request.Form["SellerID"];
            //string Sellername = new SecurityManager().GetByUserId(SellerID).FullName;
            string companyName = new CompanyManager().GetByid(Convert.ToInt32(companyID)).Name;
            int batchno = new ProductInformationManager().GetLastBatchNo(SellerID);
            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];
                if (file != null && file.ContentLength > 0)
                {
                    var fname = file.FileName;
                    var path = Server.MapPath("~\\Content\\UploadData\\UploadDocuments");
                    if (!string.IsNullOrEmpty(SellerID))
                    {

                        path = Server.MapPath("~\\Content\\UploadData\\UploadDocuments\\" + SellerID);
                        if (!System.IO.Directory.Exists(path))
                        {
                            System.IO.Directory.CreateDirectory(path);
                        }

                    }
                    //var path = Server.MapPath("~\\App_Data\\BulkUploadDoc\\");
                    //if (!System.IO.Directory.Exists(path))
                    //{
                    //    System.IO.Directory.CreateDirectory(path);
                    //}
                    //// Get the complete folder path and store the file inside it.  
                    var filePath = Path.Combine(path, fname);
                    file.SaveAs(filePath);

                    var data = filePath.ExcelToDataTable();

                    message = AddBulkAssetData(data, companyName, SellerID, batchno);

                    // TempData["msg"] = message;
                }
            }

            if (message.Contains("Successfully"))
            {
                return Json(new { status = 1, msg = message, batchno = batchno });
            }
            return Json(new { status = 0, msg = message, batchno = batchno });
        }


        public string AddBulkAssetData(DataTable dt, string companyname, string Sellername, int batchno)
        {
            // var ProductIds = new List<int>();
            //var productcategory =new CategoryManager().Get();
            //var manufacturerlist = new ManufacturerDetailsManager().Get();
            string message = "";
            try
            {
                var productData = new List<ProductInformationImportViewModel>();

                foreach (DataRow row in dt.Rows)
                {
                    var values = row.ItemArray;
                    var isEmpty = row.ItemArray.All(c => c.ToString() == "");

                    if (!isEmpty)
                    {
                        //var list = new List<ProductDetailImportViewModel>();

                        //list.Add(new ProductDetailImportViewModel()
                        ////{
                        //    Description = values[1].ToString(),
                        //    Condition = values[12].ToString(),
                        //    SpecialInstructions = values[13].ToString(),
                        //    Contact = values[14].ToString(),
                        //    Company = values[15].ToString(),
                        //    Phone = values[16].ToString(),
                        //    Fax = values[17].ToString(),
                        //    Email = values[18].ToString(),
                        //    Address = values[19].ToString(),
                        //    lat = (values[20].ToString() == "") ? 0 : values[20].ToString().ParseToDouble(),
                        //    lon = (values[21].ToString() == "") ? 0 : values[21].ToString().ParseToDouble(),
                        //    _Country = values[22],
                        //    State = values[23].ToString(),
                        //    City = values[24].ToString(),
                        //    WholesaleValue = (values[26].ToString() == "") ? 0 : values[26].ToString().ParseToDouble(),
                        //    RetailValue = (values[27].ToString() == "") ? 0 : values[27].ToString().ParseToDouble(),
                        //    ReservePrice = (values[28].ToString() == "") ? 0 : values[28].ToString().ParseToDouble(),
                        //    SellerCity = "",
                        //    SellerState=""

                        // }) ;


                        //var productDatum = new ProductInformationImportViewModel();

                        //productDatum.ProductTitle = values[0].ToString();

                        //    productDatum._Category = values[2];
                        //    productDatum.OtherCategory = values[3].ToString();

                        //    productDatum._ProductBrands = values[7].ToString() == "";

                        //   productDatum.OtherManufacturer = values[8].ToString();
                        //   productDatum.DeadLineInDays = (values[11].ToString() == "") ? 0 : values[11].ToString().ParseToInt();
                        //   productDatum.PricingType = values[25].ToString();
                        //   productDatum.BidDifference = (values[29].ToString() == "") ? 0 : values[29].ToString().ParseToDouble();
                        //   productDatum.Quantity = values[30].ToString();

                        //   productDatum.SellerMaterialId = (values[44].ToString() == "") ? "0" : values[44].ToString();
                        //   productDatum.EndUser = values[41].ToString();
                        //   productDatum.SellerName = Sellername;
                        //   productDatum.SupplierName = values[43].ToString();
                        //   productDatum.CertificatesOrDataSheet = values[34].ToString();
                        //   productDatum.WINAssement = values[35].ToString();
                        //   productDatum.WINAssementby = values[36].ToString();
                        //   productDatum.Repairable = values[37].ToString();
                        //   productDatum.UsedEquipmentOrApplication = values[38].ToString();
                        //   productDatum.Age = (values[39].ToString() == "") ? 0 : values[39].ToString().ParseToDouble();



                        //   productDatum.ProductLocation = "";
                        //   productDatum.DeadLine = DateTime.Now.AddYears(3);

                        //   productDatum.ProductImage = getproductimage(values[45].ToString(), Sellername);
                        //   productDatum.BidCount = 0;
                        //   productDatum.Price = 0;
                        //   productDatum.CreateDate = DateTime.Now;
                        //   productDatum.Username = User.Identity.Name;
                        //   productDatum.isActive = true;
                        //   productDatum.IsWinActive = true;
                        //   productDatum.SuggestionByWin = "";



                        //    productDatum.Description = values[1].ToString();
                        //   productDatum.Condition = values[12].ToString();
                        //   productDatum.SpecialInstructions = values[13].ToString();
                        //   productDatum.Contact = values[14].ToString();
                        //   productDatum.Company = companyname; //values[15].ToString();
                        //   productDatum.Phone = values[16].ToString();
                        //   productDatum.Fax = values[17].ToString();
                        //   productDatum.Email = values[18].ToString();
                        //  productDatum.Address = values[19].ToString();
                        //   productDatum.lat = (values[20].ToString() == "") ? 0 : values[20].ToString().ParseToDouble();
                        //   productDatum.lon = (values[21].ToString() == "") ? 0 : values[21].ToString().ParseToDouble();
                        //   productDatum._Country = values[22];
                        //   productDatum.State = values[23].ToString();
                        //   productDatum.City = values[24].ToString();
                        //   productDatum.WholesaleValue = (values[26].ToString() == "") ? 0 : values[26].ToString().ParseToDouble();
                        //    productDatum.RetailValue = (values[27].ToString() == "") ? 0 : values[27].ToString().ParseToDouble();
                        //   productDatum.ReservePrice = (values[28].ToString() == "") ? 0 : values[28].ToString().ParseToDouble();
                        //   productDatum.ClaimPrice = 0;
                        //   productDatum.MinimumSellingPrice = 0;
                        //   productDatum.SellerCity = "0";
                        //   productDatum.SellerState = "0";
                        //  productDatum.SellerCountry = "0";
                        //  productDatum.BatchNumber = batchno + "";



                        var productDatum = new ProductInformationImportViewModel()
                        {
                            ProductTitle = values[0].ToString(),

                            _Category = values[2],
                            OtherCategory = values[3].ToString(),

                            _ProductBrands = values[7].ToString() == "",

                            OtherManufacturer = values[8].ToString(),
                            DeadLineInDays = (values[11].ToString() == "") ? 0 : values[11].ToString().ParseToInt(),
                            PricingType = (values[25].ToString() != "" && values[25].ToString().ToLower().Equals("auctionable") ? "Auctionable" : "Non-Auctionable"),
                            BidDifference = (values[29].ToString() == "") ? 0 : values[29].ToString().ParseToDouble(),
                            Quantity = values[30].ToString(),
                            AvailableQuantity = (values[30].ToString() == "") ? 0 : values[30].ToString().ParseToInt(),
                            SellerMaterialId = (values[44].ToString() == "") ? "0" : values[44].ToString(),
                            EndUser = values[41].ToString(),
                            SellerName = Sellername,
                            SupplierName = values[43].ToString(),
                            CertificatesOrDataSheet = values[34].ToString(),
                            WINAssement = values[35].ToString(),
                            WINAssementby = values[36].ToString(),
                            Repairable = values[37].ToString(),
                            UsedEquipmentOrApplication = values[38].ToString(),
                            Age = (values[39].ToString() == "") ? 0 : values[39].ToString().ParseToDouble(),

                            

                            ProductLocation = "",
                            DeadLine = (values[11].ToString() == "") ? DateTime.Now.AddDays(-1) : DateTime.Now.AddDays(values[11].ToString().ParseToInt()),

                            ProductImage = getproductimage(values[45].ToString(), Sellername),
                            BidCount = 0,
                            Price = (values[28].ToString() == "") ? 0 : values[28].ToString().ParseToDouble(),
                            CreateDate = DateTime.Now,
                            Username = Sellername,
                            isActive = false,
                            IsWinActive = false,
                            SuggestionByWin = "",
                            Description = values[1].ToString(),
                            Condition = values[12].ToString(),
                            SpecialInstructions = values[13].ToString(),
                            Contact = values[14].ToString(),
                            Company = companyname, //values[15].ToString(),
                            Phone = values[16].ToString(),
                            Fax = values[17].ToString(),
                            Email = values[18].ToString(),
                            Address = values[19].ToString(),
                            lat = (values[20].ToString() == "") ? 0 : values[20].ToString().ParseToDouble(),
                            lon = (values[21].ToString() == "") ? 0 : values[21].ToString().ParseToDouble(),
                            _Country = values[22],
                            State = values[23].ToString(),
                            City = values[24].ToString(),
                            WholesaleValue = (values[26].ToString() == "") ? 0 : values[26].ToString().ParseToDouble(),
                            RetailValue = (values[27].ToString() == "") ? 0 : values[27].ToString().ParseToDouble(),
                            ReservePrice = (values[28].ToString() == "") ? 0 : values[28].ToString().ParseToDouble(),
                            ClaimPrice = 0,
                            MinimumSellingPrice = (values[28].ToString() == "") ? 0 : values[28].ToString().ParseToDouble(),
                            SellerCity = "0",
                            SellerState = "0",
                            SellerCountry = "0",
                            BatchNumber = batchno + "",
                             Keywords = values[48].ToString()
                        };


                        productData.Add(productDatum);

                    }

                }
                if (productData != null && productData.Count > 0)
                {
                    //   var iMapper = MapperHelper.IMapper;
                    //var config = new MapperConfiguration(cfg =>
                    //{

                    //    cfg.CreateMap<ProductInformationViewModel, ProductInformationImportViewModel>().ReverseMap();
                    //    cfg.CreateMap<ProductDetailViewModel, ProductDetailImportViewModel>().ReverseMap();
                    //});

                    // var IMapper = config.CreateMapper();

                    // var data= productData.AsQueryable().ProjectTo<ProductInformationViewModel>(IMapper.ConfigurationProvider).ToList();
                    DataTable Informationdt = productData.ToDataTable();

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["obsDBNative"].ToString());
                    SqlCommand cmd = new SqlCommand("ImportProductInfromation");
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@data", Informationdt);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    message = "Bulk Upload Done Successfully!- All Data  Saved ... your upload batchno is -" + batchno + "-";


                    //  var result=  new ProductInformationManager().AddRange(data);
                    // OBSDataManager.AddBulkProductData(productData, Session["Username"].ToString(), Session["User"].ToString());
                }

            }
            catch (Exception ex)
            {
                message = " Bulk Upload Failed!  Some thing wrong please contact system admin to check that   ";

                //  throw ex;

            }
            return message;
        }

        public string UpdateBulkAssetData(DataTable dt, string companyname, string Sellername, int batchno)
        {
            // var ProductIds = new List<int>();
            //var productcategory =new CategoryManager().Get();
            //var manufacturerlist = new ManufacturerDetailsManager().Get();
            string message = "";
            //try
            //{
                var productData = new List<ProductInformationImportViewModel>();

                foreach (DataRow row in dt.Rows)
                {
                    var values = row.ItemArray;
                    var isEmpty = row.ItemArray.All(c => c.ToString() == "");

                    if (!isEmpty)
                    {
                        //var list = new List<ProductDetailImportViewModel>();

                        //list.Add(new ProductDetailImportViewModel()
                        ////{
                        //    Description = values[1].ToString(),
                        //    Condition = values[12].ToString(),
                        //    SpecialInstructions = values[13].ToString(),
                        //    Contact = values[14].ToString(),
                        //    Company = values[15].ToString(),
                        //    Phone = values[16].ToString(),
                        //    Fax = values[17].ToString(),
                        //    Email = values[18].ToString(),
                        //    Address = values[19].ToString(),
                        //    lat = (values[20].ToString() == "") ? 0 : values[20].ToString().ParseToDouble(),
                        //    lon = (values[21].ToString() == "") ? 0 : values[21].ToString().ParseToDouble(),
                        //    _Country = values[22],
                        //    State = values[23].ToString(),
                        //    City = values[24].ToString(),
                        //    WholesaleValue = (values[26].ToString() == "") ? 0 : values[26].ToString().ParseToDouble(),
                        //    RetailValue = (values[27].ToString() == "") ? 0 : values[27].ToString().ParseToDouble(),
                        //    ReservePrice = (values[28].ToString() == "") ? 0 : values[28].ToString().ParseToDouble(),
                        //    SellerCity = "",
                        //    SellerState=""

                        // }) ;

                        var productDatum = new ProductInformationImportViewModel();
                        //{
                            productDatum.ProductTitle = values[0].ToString();
                            // ProductDetails = list;
                            productDatum._Category = values[2];//.ToString() == "") ? 0 : productcategory.SingleOrDefault(x=>x.CategoryName==values[2].ToString()).CategoryId;
                            productDatum.OtherCategory = values[3].ToString();
                            //ProductDocument = values[46].ToString();
                            productDatum._ProductBrands = values[7].ToString() == "";//) ? 0 : manufacturerlist.SingleOrDefault(x=>x.ManufacturersName==values[7].ToString()).ManufacturersId;

                            productDatum.OtherManufacturer = values[8].ToString();
                            productDatum.DeadLineInDays = (values[11].ToString() == "") ? 0 : values[11].ToString().ParseToInt();
                            productDatum.PricingType = (values[25].ToString() != "" && values[25].ToString().ToLower().Equals("auctionable") ? "Auctionable" : "Non-Auctionable");
                            productDatum.BidDifference = (values[29].ToString() == "") ? 0 : values[29].ToString().ParseToDouble();
                            productDatum.Quantity = values[30].ToString();
                    //ProductImage = (values[45].ToString() != "") ? Session["User"].ToString() + "/" + values[45].ToString() : values[45].ToString();
                    productDatum.AvailableQuantity = (values[30].ToString() == "") ? 0 : values[30].ToString().ParseToInt();
                            productDatum.SellerMaterialId = (values[44].ToString() == "") ? "0" : values[44].ToString();
                            productDatum.EndUser = values[41].ToString();
                            productDatum.SellerName = Sellername; //values[42].ToString();
                            productDatum.SupplierName = values[43].ToString();
                            productDatum.CertificatesOrDataSheet = values[34].ToString();
                            productDatum.WINAssement = values[35].ToString();
                            productDatum.WINAssementby = values[36].ToString();
                            productDatum.Repairable = values[37].ToString();
                            productDatum.UsedEquipmentOrApplication = values[38].ToString();
                            productDatum.Age = (values[39].ToString() == "") ? 0 : values[39].ToString().ParseToDouble();



                            productDatum.ProductLocation = "";
                            productDatum.DeadLine = (values[11].ToString() == "") ? DateTime.Now.AddDays(-1) : DateTime.Now.AddDays(values[11].ToString().ParseToInt());
                            // ProductImage = getproductimage(values[45].ToString(); Sellername);
                            productDatum.ProductImage = getproductimage(values[45].ToString(), Sellername);
                            productDatum.BidCount = 0;
                            productDatum.Price = (values[28].ToString() == "") ? 0 : values[28].ToString().ParseToDouble();
                            productDatum.CreateDate = DateTime.Now;
                            productDatum.Username = Sellername;
                            productDatum.isActive = false;
                            productDatum.IsWinActive = false;
                            productDatum.SuggestionByWin = "";
                            //{


                            productDatum.Description = values[1].ToString();
                            productDatum.Condition = values[12].ToString();
                            productDatum.SpecialInstructions = values[13].ToString();
                            productDatum.Contact = values[14].ToString();
                            productDatum.Company = companyname; //values[15].ToString();
                            productDatum.Phone = values[16].ToString();
                            productDatum.Fax = values[17].ToString();
                            productDatum.Email = values[18].ToString();
                            productDatum.Address = values[19].ToString();
                            productDatum.lat = (values[20].ToString() == "") ? 0 : values[20].ToString().ParseToDouble();
                            productDatum.lon = (values[21].ToString() == "") ? 0 : values[21].ToString().ParseToDouble();
                            productDatum._Country = values[22];
                            productDatum.State = values[23].ToString();
                            productDatum.City = values[24].ToString();
                            productDatum.WholesaleValue = (values[26].ToString() == "") ? 0 : values[26].ToString().ParseToDouble();
                            productDatum.RetailValue = (values[27].ToString() == "") ? 0 : values[27].ToString().ParseToDouble();
                            productDatum.ReservePrice = (values[28].ToString() == "") ? 0 : values[28].ToString().ParseToDouble();
                            productDatum.ClaimPrice = 0;
                            productDatum.MinimumSellingPrice = (values[28].ToString() == "") ? 0 : values[28].ToString().ParseToDouble(); 
                            productDatum.SellerCity = "0";
                            productDatum.SellerState = "0";
                            productDatum.SellerCountry = "0";
                            productDatum.BatchNumber = batchno + "";
                    productDatum.Keywords = values[49].ToString();
                    productDatum.ProductId = Convert.ToInt64(values[48].ToString());

                        //};


                        productData.Add(productDatum);

                    }

                }
                if (productData != null && productData.Count > 0)
                {
                    //   var iMapper = MapperHelper.IMapper;
                    //var config = new MapperConfiguration(cfg =>
                    //{

                    //    cfg.CreateMap<ProductInformationViewModel, ProductInformationImportViewModel>().ReverseMap();
                    //    cfg.CreateMap<ProductDetailViewModel, ProductDetailImportViewModel>().ReverseMap();
                    //});

                    // var IMapper = config.CreateMapper();

                    // var data= productData.AsQueryable().ProjectTo<ProductInformationViewModel>(IMapper.ConfigurationProvider).ToList();
                    DataTable Informationdt = productData.ToDataTable();

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["obsDBNative"].ToString());
                    SqlCommand cmd = new SqlCommand("ImportProductInfromationUpdate");
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@data", Informationdt);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    message = "Bulk Upload Done Successfully!  All Data  Saved ... your upload batchno is -" + batchno + "-";


                    //  var result=  new ProductInformationManager().AddRange(data);
                    // OBSDataManager.AddBulkProductData(productData, Session["Username"].ToString(), Session["User"].ToString());
                }

            //}
            //catch (Exception ex)
            //{
            //    message = "Bulk Upload Failed! Some thing wrong please contact system admin to check that";

            //    //  throw ex;

            //}
            return message;
        }

        [HttpPost]
        public string UploadFilesTemp(HttpPostedFileBase fileUploader)
        {
            if (fileUploader != null)
            {
                fileUploader.SaveAs(Server.MapPath("~/Content/TempFiles/" + fileUploader.FileName));
                return "File Uploaded";
            }
            else
            {
                return "File Uploaded Failed";
            }
        }


        [HttpPost]
        public JsonResult delete_file(string file_name)
        {
            if (file_name != null)
            {
                System.IO.File.Delete(Server.MapPath("~/Content/TempFiles/" + file_name));
                return Json(new { msg = "File Deleted" });
            }
            else
            {
                return Json(new { msg = "File Deleted failed" });
            }
        }


        public string getproductimage(string value, string sellername)
        {
            string image = "";

            if (!string.IsNullOrEmpty(value))
            {
                string[] stringSeparators = new string[] { "(&)" };
                string[] allimages = value.Split(stringSeparators, StringSplitOptions.None);

                // int counter = 0;
                foreach (string img in allimages)
                {
                    string imagename = Path.GetFileName(img);
                    imagename = sellername + "/" + imagename;

                    image += imagename + "(&)";
                    // counter++;
                }
                //if (counter == 1)
                //{
                //    image.Replace("(&)", "");
                //}

            }

            return image;
        }


        public void DownloadFile(FileInfo file)
        {
            if (file.Exists)
            {
                Response.Clear();
                Response.ClearHeaders();
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file.Name + "\"");
                Response.AddHeader("Content-Type", "application/Excel");
                Response.ContentType = "application/x-msexcel";
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.Flush();
                Response.WriteFile(file.FullName);
                Response.End();
            }
            else
            {
                Response.Write("This file does not exist -> " + file.FullName);
            }
        }
    }
}