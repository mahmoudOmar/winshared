﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Seller.Controllers
{
    public class PrintInvoiceController : Controller
    {
        // GET: Admin/PrintInvoice
        public ActionResult DownloadOrderInvoice(int? id, string selleremail)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BuyerOrderViewModel buyerOrder = new BuyerOrderManager().GetByid(Convert.ToInt32(id));
            ViewBag.SellerID = new AspNetUsersManager().GetByEmail(selleremail).Sellers[0].ID;
            if (buyerOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.Bank = new BankAccountsManager().Get().FirstOrDefault();
            return View(buyerOrder);
            // return new PdfResult(buyerOrder, "DownloadOrderInvoice");
        }

        public ActionResult GenerateInvoicePDF(int id, string selleremail)
        {
            return new Rotativa.ActionAsPdf("DownloadOrderInvoice", new {  id = id, selleremail = selleremail });
            //return new PdfResult( "Index");
        }
    }
}