﻿using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ps.haweya.win.obs.web.Areas.Seller.Controllers
{
    public class MyPreferencesController : SellerBaseController
    {
        int buyerReportId = 2;
        // GET: Seller/MyPreferences
        public ActionResult Index()
        {
            string aspnetid = User.Identity.GetUserId();

            ApplicationUser u = new SecurityManager().UserManager.FindById(aspnetid);

            BuyerViewModel b = new BuyerViewModel() { Email = u.Email, Name = u.FullName, Country = Convert.ToInt32(u.CountryId), State = Convert.ToInt32(u.StateId), City = Convert.ToInt32(u.CityId), POBox = u.POBox, ASPNetUserID = User.Identity.GetUserId(), ID = "0", Mobile = u.PhoneNumber };

            ViewBag.CUser = new AspNetUsersManager().GetByUserWithIncludes(User.Identity.GetUserId());
            ViewBag.Countries = CacheManager.Countries;
            ViewBag.Categories = new CategoryManager().Get();
            ViewBag.Manufacturers = new ManufacturerDetailsManager().Get();

            return View(b);
        }
        [HttpPost]
        public ActionResult Index(AspNetUserViewModel model)
        {
            try
            {
                var aspNetUsersManager = new AspNetUsersManager();
                var reportId = new UserReportsManager().AddOrUpdate(User.Identity.GetUserId(), buyerReportId, model.Frequency);
                new UserPreferedCategoryManager().AddRange(model.CategoriesPreferences, User.Identity.GetUserId());
                new UserPreferedManufacturerManager().AddRange(model.ManufacturersPreferences, User.Identity.GetUserId());
                new UserPreferedCountryManager().AddRange(model.CountryPreferences, User.Identity.GetUserId());

                TempData["msg"] = "s: Data Updated Successfully";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["msg"] = "e: Please Check Data You Entered";
                return RedirectToAction("Index");
            }
        }
    }
}