using Microsoft.AspNet.Identity;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.common;
using ps.haweya.win.obs.viewModels;
using ps.haweya.win.obs.web.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace ps.haweya.win.obs.web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
           
            // register mapping between models and view models globally
            MapperHelper.RegisterMapper();

            CacheManager.OBSManufacturers = StordProcedurManager.ManufacturersHomeData();

            CacheManager.Countries = new CountryManager().Get().ToList();
            CacheManager.States = new StateManager().Get().ToList();
            CacheManager.Cities = new CityManager().Get().ToList();

            CacheManager.Categories = new CategoryManager().Get().ToList();
            CacheManager.Links = new LinkManager().Get().ToList();
            CacheManager.ProductsImageRoles = new TypicalImageRoleManager().Get().ToList();

           CacheManager.MenuItems = new MenuItemManager().Gettosite().ToList();
            CacheManager.Setting = new SettingsManager().GetByid(1);
           CacheManager.Banars= new AdvertismentBanerManager().GetActive(true).OrderBy(x => x.OrderNo).ToList();
            CacheManager.HomeCategory= StordProcedurManager.CategoryHomeData();
            CacheManager.SlidersHome = new SliderManager().GetActive().ToList();
            CacheManager.HomeNews = new NewsManager().GetTopLayout(3).ToList();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            ErrorLogService.LogError(ex);
        }
    }

}
public static class ErrorLogService
{
    public static void LogError(Exception ex)
    {
        string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (string.IsNullOrEmpty(ip))
        {
            ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        string userid = null;
        if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated) {
            userid = System.Web.HttpContext.Current.User.Identity.GetUserId();
        }

        new ErrorLogManager().Add(new ErrorLogViewModel() {
            Titel = ex.GetType().Name,
            InsertDate = DateTime.Now,
            Url = HttpContext.Current.Request.RawUrl,
            Details = ex.Message.ToString(),
            IpAddress = ip,
            UserID = userid,
            StackTrace = ex.StackTrace,
            TargetSite = ex.TargetSite.Name
        });
    }
}