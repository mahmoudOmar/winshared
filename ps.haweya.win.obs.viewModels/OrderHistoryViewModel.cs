using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.viewModels
{
    
    public partial class OrderHistoryViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int ID { get; set; }
    
    		
        public Nullable<int> OrderID { get; set; }
    
    		
        public Nullable<int> OldStatusID { get; set; }
    
    		
        public Nullable<int> NewStatusID { get; set; }
    
    		
        public string Notes { get; set; }
    
    		
        public Nullable<System.DateTime> InsertDate { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string AddedBy { get; set; }
    }
}
