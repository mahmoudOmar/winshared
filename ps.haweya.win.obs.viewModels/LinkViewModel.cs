using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.viewModels
{
    
    public partial class LinkViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int Id { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string Area { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string Controller { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string Action { get; set; }
    
    		
        public Nullable<int> ParentId { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string ActionDisplayName { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string ControllerDisplayName { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string AreaDisplayName { get; set; }
    }
}
