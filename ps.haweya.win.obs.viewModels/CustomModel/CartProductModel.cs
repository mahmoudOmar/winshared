﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ps.haweya.win.obs.viewModels.CustomModel
{
    public class CartProductModel
    {
        public long ProductId { get; set; }
        public int Quantity { get; set; }
        public double BasePrice { get; set; }

    }
}