﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.viewModels
{
   public partial class CartItemViewModel
    {
        //public  BuyerViewModel Buyer { get; set; }
        //public  BuyerOrderViewModel BuyerOrder { get; set; }
        //public  ProductDetailViewModel ProductDetail { get; set; }
        //public  C_UserCartViewModel C_UserCart { get; set; }
        //public  ProductInformationViewModel ProductInformation { get; set; }
        public virtual C_UserCartViewModel C_UserCart { get; set; }
        public virtual BuyerViewModel Buyer { get; set; }
        public virtual BuyerOrderViewModel BuyerOrder { get; set; }
        //public virtual ProductDetailViewModel ProductDetail { get; set; }
        public virtual ProductInformationViewModel ProductInformation { get; set; }
    }
}
