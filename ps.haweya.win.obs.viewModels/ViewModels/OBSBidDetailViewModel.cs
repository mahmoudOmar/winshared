﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.viewModels
{
    public partial class OBSBidDetailViewModel
    {

        public  ProductInformationViewModel ProductInformation { get; set; }
       
        public  ICollection<OBSBidLiveTrackingViewModel> OBSBidLiveTrackings { get; set; }
        public AspNetUserViewModel AspNetUser { get; set; }
    }
}
