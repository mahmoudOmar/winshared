﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.viewModels
{
    public partial class AspNetUserViewModel
    {
        public  ICollection<AspNetRoleViewModel> AspNetRoles { get; set; }
        public  ICollection<BuyerOrderViewModel> BuyerOrders { get; set; }
        public  ICollection<AspNetUserClaimViewModel> AspNetUserClaims { get; set; }
    }
}
