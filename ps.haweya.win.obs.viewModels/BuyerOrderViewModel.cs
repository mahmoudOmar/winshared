using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.viewModels
{
    
    public partial class BuyerOrderViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int ID { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string UserID { get; set; }
    
    		
        public Nullable<System.DateTime> InsertDate { get; set; }
    
    		
        public Nullable<int> ShippingDataID { get; set; }
    
    		
        public Nullable<int> InvoiceDataID { get; set; }
    
    		
        public Nullable<int> VisaDataID { get; set; }
    
    		
        public Nullable<int> StageID { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public bool CloseStatus { get; set; }
    
    		
        public Nullable<double> TotalPrice { get; set; }
    
    		
        public Nullable<System.DateTime> ClosedDate { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string payment_method { get; set; }
    
    		
        public Nullable<int> StatusID { get; set; }
    
    		
        public Nullable<bool> IsNeedInspection { get; set; }
    
    		
        public Nullable<bool> IsNeedWinDelivery { get; set; }
    }
}
