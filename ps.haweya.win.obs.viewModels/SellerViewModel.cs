using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.viewModels
{
    
    public partial class SellerViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string ID { get; set; }
    
    		
        public Nullable<int> CompanyID { get; set; }
    
    		
        public Nullable<System.DateTime> InsertDate { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string AspNetUserID { get; set; }
    
    		
        public Nullable<int> TypeID { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string DocumentID { get; set; }
    
        [MaxLength(10,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string agreement { get; set; }
    
    		
        public Nullable<int> BusinessTypeid { get; set; }
    }
}
