﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class MaterialPickupLocationManager :BaseManager<MaterialPickupLocationSeller, MaterialPickupLocationSellerViewModel>
    {

        public IEnumerable<MaterialPickupLocationSellerViewModel> Get()
    {
        return repository.Get();
    }

    public MaterialPickupLocationSellerViewModel GetByid(int id)
    {
        return repository.Get(id);
    }
        public MaterialPickupLocationSellerViewModel GetBysellerid(string id)
        {
            return repository.Get(x=>x.UserId==id).FirstOrDefault();
        }
        public MaterialPickupLocationSellerViewModel Add(MaterialPickupLocationSellerViewModel model)
        {
            return repository.Add(model);
        }
        public MaterialPickupLocationSellerViewModel Update(MaterialPickupLocationSellerViewModel model)
        {
            return repository.Update(model);
        }
    }
}
