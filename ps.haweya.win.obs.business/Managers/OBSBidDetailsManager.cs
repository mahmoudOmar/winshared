﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class OBSBidDetailsManager :BaseManager<OBSBidDetail, OBSBidDetailViewModel>
    {

        public IEnumerable<OBSBidDetailViewModel> Get()
        {
            return repository.Get();
        }
        public OBSBidDetailViewModel Get(long id)
        {
            return repository.Get(id);
        }
  
        public OBSBidDetailViewModel GetLastByProductID(long ProductID)
        {
            return repository.QueryablResult(repository.QueryableReference("ProductInformation","").Where(x => x.ProductId == ProductID).OrderByDescending(x => x.BidPrice)).FirstOrDefault();
        }

        public OBSBidDetailViewModel GetLastByUserid(long ProductID,string UserId)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.ProductId == ProductID&&x.UserDetailsId==UserId).OrderByDescending(x => x.AddedOn)).FirstOrDefault();
        }
        public List<OBSBidDetailViewModel> GetByUserid(string UserId)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x =>x.UserDetailsId == UserId).OrderByDescending(x => x.AddedOn)).ToList();
        }
        public OBSBidDetailViewModel AddToBuyer(long ProductID,double Amount,string UserId,bool inspection,int currencyid,double currencyrate,double maincurrancyvalue,string bidoffer,string otherfiles)
        {
           
            var model = new OBSBidDetailsManager().GetLastByUserid(ProductID, UserId);
            if (model == null)
            {
                model = new OBSBidDetailViewModel();
                model.AddedBy = 1;
                model.AddedOn = DateTime.Now;
                model.BidCount = 1;
                model.BidPrice = Amount;
                model.ProductId = ProductID;
                model.UserDetailsId = UserId;
                model.IsSold = false;
                model.IsShipment = false;
                model.NeedInspection = inspection;
                model.StatusID = 1;
                model.CurrencyId = currencyid;
                model.CurrencyRate = currencyrate;
                model.MainCurrencyValue = maincurrancyvalue;
                model.BidOffer = bidoffer;
                model.OthersFiles = otherfiles;
                model = repository.Add(model);

            }

            else
            {
                model.AddedBy = 1;
                model.AddedOn = DateTime.Now;
                model.BidCount = model.BidCount + 1;
                if (Amount > 0)
                    model.BidPrice = Amount;
                model.ProductId = ProductID;
                model.UserDetailsId = UserId;
                model.IsSold = false;
                model.IsShipment = false;
                model.NeedInspection = inspection;
                model.StatusID = 1;
                model.CurrencyId = currencyid;
                model.CurrencyRate = currencyrate;
                model.MainCurrencyValue = maincurrancyvalue;
                model.BidOffer = bidoffer;
                model.OthersFiles = otherfiles;
                model = repository.Update(model, "BidId");
            }

            var a = new BidHistoryManager().Add(new BidsHistoryViewModel()
            {
                AddedBy = UserId,
                NewStatusID = 1,
                OldStatusID = 1,
                BidID = model.BidId,
                Notes = "New Bid count no= " + model.BidCount + ", Ammount= "+(model.CurrencyRate!=1?(model.BidPrice+" "+ model.Currency.Sign +"="+model.MainCurrencyValue +"SAR"): model.BidPrice+"SAR"),
                InsertDate = DateTime.Now
            }); ;

            return model;
        }
        /////////
        public IEnumerable<OBSBidDetailViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q, int ProductId=0)
        {
            q = q.ToLower();

            IQueryable<OBSBidDetail> query = null;

            if (string.IsNullOrEmpty(q))
                query = repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage);

            query = repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.ProductInformation.ProductTitle.ToLower().Contains(q) || x.ProductInformation.SellerName.ToLower().Contains(q) || x.AspNetUser.Email.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage);

            if (ProductId != 0) query = query.Where(t => t.ProductId == ProductId);

            return repository.QueryablResult(query);
        }

        public int Count(string q, int ProductId = 0)
        {
            q = q.ToLower();

            var query = repository.QueryableReference();

            if (string.IsNullOrEmpty(q) && ProductId == 0)
                return query.Count();

            query = query.Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.ProductInformation.ProductTitle.ToLower().Contains(q) || x.ProductInformation.SellerName.ToLower().Contains(q) || x.AspNetUser.Email.Contains(q));

            if (ProductId != 0) query = query.Where(t => t.ProductId == ProductId);

            return query.Count();
        }

        public IEnumerable<object> GetWithGroupingProducts(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();
            if (string.IsNullOrEmpty(q))
            return repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod))
                             .GroupBy(x => x.ProductInformation).OrderByDescending(g=>g.Key.CreateDate).Skip((Page - 1) * PerPage).Take(PerPage)
                             .Select(g => new { Key = g.Key.ProductId, g.Key.ProductTitle, Value = g.Count(), LastBid = g.Key.OBSBidDetails.OrderByDescending(x=>x.AddedOn).FirstOrDefault().BidPrice }).AsEnumerable();


            return repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod))
                             .GroupBy(x => x.ProductInformation).Where(x => x.Key.ProductTitle.ToLower().Contains(q) ||  x.Key.SellerName.ToLower().Contains(q)).OrderByDescending(g => g.Key.CreateDate).Skip((Page - 1) * PerPage).Take(PerPage)
                             .Select(g => new { Key = g.Key.ProductId, g.Key.ProductTitle, Value = g.Count(), LastBid = g.Key.OBSBidDetails.OrderByDescending(x => x.AddedOn).FirstOrDefault().BidPrice }).AsEnumerable();


        }

        public IEnumerable<bidtableadminhome> GetWithGroupingProductsmodel(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();
            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod))
                                 .GroupBy(x => x.ProductInformation).OrderByDescending(g => g.Key.CreateDate).Skip((Page - 1) * PerPage).Take(PerPage)
                                 .Select(g => new bidtableadminhome { Key = g.Key.ProductId, ProductTitle= g.Key.ProductTitle, Value = g.Count(), LastBid = g.Key.OBSBidDetails.OrderByDescending(x => x.AddedOn).FirstOrDefault().BidPrice }).AsEnumerable();


            return repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod))
                             .GroupBy(x => x.ProductInformation).Where(x => x.Key.ProductTitle.ToLower().Contains(q) || x.Key.SellerName.ToLower().Contains(q)).OrderByDescending(g => g.Key.CreateDate).Skip((Page - 1) * PerPage).Take(PerPage)
                             .Select(g => new bidtableadminhome {  Key = g.Key.ProductId, ProductTitle= g.Key.ProductTitle, Value = g.Count(), LastBid = g.Key.OBSBidDetails.OrderByDescending(x => x.AddedOn).FirstOrDefault().BidPrice }).AsEnumerable();


        }

        public IEnumerable<object> GetWithGroupingProductsToSeller(string sellername,string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();
            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x=>x.ProductInformation.IsDelete!=true&& x.ProductInformation.Username.ToLower().Equals(sellername.ToLower()))
                                 .GroupBy(x => x.ProductInformation).OrderByDescending(g => g.Key.DeadLine).Skip((Page - 1) * PerPage).Take(PerPage)
                                 .Select(g => new { Key = g.Key.ProductId, g.Key.ProductTitle, Value = g.Count(), LastBid = g.Key.OBSBidDetails.OrderByDescending(x => x.AddedOn).FirstOrDefault().BidPrice }).AsEnumerable();


            return repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.ProductInformation.IsDelete != true && x.ProductInformation.Username.ToLower().Equals(sellername.ToLower()))
                             .GroupBy(x => x.ProductInformation).Where(x => x.Key.ProductTitle.ToLower().Contains(q) || x.Key.SellerName.ToLower().Contains(q)).OrderByDescending(g => g.Key.DeadLine).Skip((Page - 1) * PerPage).Take(PerPage)
                             .Select(g => new { Key = g.Key.ProductId, g.Key.ProductTitle, Value = g.Count(), LastBid = g.Key.OBSBidDetails.OrderByDescending(x => x.AddedOn).FirstOrDefault().BidPrice }).AsEnumerable();


        }

        public IEnumerable<object> GetBidByStatus()
        {
            return repository.QueryableReference()
                              .GroupBy(x => x.Status)
                              .Select(g => new { Key = g.Key.Name, Value = g.Count(), Id = g.Key.ID }).AsEnumerable();
        }

        public IEnumerable<OBSBidDetailViewModel> ActiveBids() {

            var query = repository.QueryablResult(repository.QueryableReference()
                                                        .Where(x =>  
                                                                     !x.IsSold && 
                                                                     x.ProductInformation.BidStartDate <= DateTime.Now && 
                                                                     x.ProductInformation.DeadLine >= DateTime.Now)
                                                        .Select(t => t));

            return query.Select(t=>t);
        }

        public long UpdateBidStatus(long Bidid, int statusid)
        {
            OBSBidDetailViewModel entity = Get(Bidid);
            entity.StatusID = statusid;
            entity = repository.Update(entity, "BidId");
            return  entity.BidId;
        }

        public long UpdateBid(OBSBidDetailViewModel entity)
        {
            entity = repository.Update(entity,"BidId");
            return entity.BidId;
        }







        public IEnumerable<OBSBidDetailViewModel> GetWithIncloudsFinance(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            //List<int?> Financestatusides =  new List<int?>(){ 1, 2, 3, 4, 5, 10 ,18};
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x=>x.StatusID<=5).Skip((Page - 1) * PerPage).Take(PerPage));
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x=>x.IsSold==true).Skip((Page - 1) * PerPage).Take(PerPage));

            //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)  ).Skip((Page - 1) * PerPage).Take(PerPage));
            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.IsSold == true).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public IEnumerable<OBSBidDetailViewModel> GetWithIncloudsTechnical(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            List<int?> Technicalstatusides = new List<int?>() { 3, 7, 8, 9 };
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x=>x.StatusID<=5).Skip((Page - 1) * PerPage).Take(PerPage));
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => Technicalstatusides.Contains(x.StatusID)).Skip((Page - 1) * PerPage).Take(PerPage));

            //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)  ).Skip((Page - 1) * PerPage).Take(PerPage));
            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => Technicalstatusides.Contains(x.StatusID)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public IEnumerable<OBSBidDetailViewModel> GetWithIncloudsLogistics(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            List<int?> Logisticsstatusides = new List<int?>() { 6, 11, 12 };
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x=>x.StatusID<=5).Skip((Page - 1) * PerPage).Take(PerPage));
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => Logisticsstatusides.Contains(x.StatusID)).Skip((Page - 1) * PerPage).Take(PerPage));

            //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)  ).Skip((Page - 1) * PerPage).Take(PerPage));
            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => Logisticsstatusides.Contains(x.StatusID)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }


        public int GetWithIncloudsFinanceCount(string q)
        {
            //List<int?> Financestatusides = new List<int?>() { 1, 2, 3, 4, 5, 10 };
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                //return repository.QueryableReference().Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)  ).Count();
                return repository.QueryableReference().Where(x => x.IsSold == true).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)  ).Count();

            //return repository.QueryableReference().Where(x => x.StatusID <= 5).Count();
            return repository.QueryableReference().Where(x => x.IsSold == true).Count();

        }
        public int GetWithIncloudsTechnicalCount(string q)
        {
            List<int?> Technicalstatusides = new List<int?>() { 3, 7, 8, 9 };
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                //return repository.QueryableReference().Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)  ).Count();
                return repository.QueryableReference().Where(x => Technicalstatusides.Contains(x.StatusID)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)).Count();

            //return repository.QueryableReference().Where(x => x.StatusID <= 5).Count();
            return repository.QueryableReference().Where(x => Technicalstatusides.Contains(x.StatusID)).Count();

        }
        public int GetWithIncloudsLogisticsCount(string q)
        {
            List<int?> Logisticsstatusides = new List<int?>() { 11, 12, 6 };
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                //return repository.QueryableReference().Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)  ).Count();
                return repository.QueryableReference().Where(x => Logisticsstatusides.Contains(x.StatusID)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q)).Count();

            //return repository.QueryableReference().Where(x => x.StatusID <= 5).Count();
            return repository.QueryableReference().Where(x => Logisticsstatusides.Contains(x.StatusID)).Count();

        }

        



        public IEnumerable<object> GetTrackingDetails()
        {
            var details = repository.QueryablResult(repository.QueryableReference(""));
            var x = (from p in details
                     select new
                     {
                         BidId = p.BidId,
                         ProductTitle = p.ProductInformation.ProductTitle,
                         ProductImage = p.ProductInformation.ProductImg,
                         Location = p.ProductInformation.ProductLocation,
                         SellerUsername = p.ProductInformation.Username,
                         Quantity = p.ProductInformation.Quantity,
                         Price = p.BidPrice,
                         BuyerUsername = p.AspNetUser.Email,
                         IsSold = p.IsSold,
                         TrackingData = p.OBSBidLiveTrackings.ToList()
                     }).ToList();
            return x;
        }

        public IEnumerable<object> GetTrackingDetailsByUser(string Username)
        {
            var details = repository.QueryablResult(repository.QueryableReference("OBSBidLiveTrackings", ""));
            var d = (from p in details
                     select new
                     {
                         BidId = p.BidId,
                         ProductTitle = p.ProductInformation.ProductTitle,
                         ProductImage = p.ProductInformation.ProductImg,
                         Location = p.ProductInformation.ProductLocation,
                         SellerUsername = p.ProductInformation.Username,
                         Quantity = p.ProductInformation.Quantity,
                         Price = p.BidPrice,
                         BuyerUsername = p.AspNetUser.Email,
                         IsSold = p.IsSold,
                         TrackingData = p.OBSBidLiveTrackings.ToList()
                     }).Where(x => x.SellerUsername == Username || x.BuyerUsername == Username);
            return d;
        }

        // TODO: add the username to the query condition
        public IEnumerable<object> GetInvoiceDetailsByBuyer(string BuyerUserName)
        {
            var details = repository.QueryablResult(repository.QueryableReference(""));
            var invoices = (from p in details
                            select new
                            {
                                BidId = p.BidId,
                                Location = p.ProductInformation.ProductLocation,
                                BidCount = p.BidCount,
                                BidPrice = p.BidPrice,
                                UserDetailsId = p.UserDetailsId,
                                ProductId = p.ProductId,
                                AddedBy = p.AddedBy,
                                AddedOn = p.AddedOn,
                                ProductInformation = new
                                                      {
                                                          ProductTitle = p.ProductInformation.ProductTitle,
                                                          ProductImage = p.ProductInformation.ProductImg,
                                                          ProductLocation = p.ProductInformation.ProductLocation
                                                      },
                                OBSUserDetail = p.AspNetUser,
                                IsSold = p.IsSold
                            }).ToList().Where(x => x.IsSold == true /*&& x.OBSUserDetail.Email == BuyerUserName*/).OrderByDescending(x => x.ProductId); ;
            return invoices;
        }
    }
}
