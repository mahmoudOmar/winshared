﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;

namespace ps.haweya.win.obs.business.Managers
{
    public class ChangePasswordRandomLinkManager : BaseManager<ChangePasswordRandomLink, ChangePasswordRandomLinkViewModel>
    {
        public  int  CheckValidcode(string UserID, bool IsUsed, bool isValid)
        {
           
                 return repository.QueryableReference().Where(x=>x.UserID== UserID && x.IsUsed == IsUsed && x.isValid == isValid).Count();

           
        }

        public ChangePasswordRandomLinkViewModel GetByKey(string code)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.Link == code)).FirstOrDefault();


        }
        public int Add(ChangePasswordRandomLinkViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.ID;
        }
        public int Updated(ChangePasswordRandomLinkViewModel entity)
        {
            entity = repository.Update(entity);
            return entity.ID;
        }
    }
}
