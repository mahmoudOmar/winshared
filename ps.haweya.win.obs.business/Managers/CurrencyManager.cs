﻿using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class CurrencyManager : BaseManager<Currency, CurrencyViewModel>
    {
        public IEnumerable<CurrencyViewModel> Get()
        {
            return repository.Get();
        }

        public CurrencyViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public IEnumerable<CurrencyViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Name.ToLower().Contains(q)||x.ShortName.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public int Count(string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.Name.ToLower().Contains(q) || x.ShortName.ToLower().Contains(q)).Count();
        }
        public int AddOrUpdate(int Id, string Name, string ShortName, string Sign, double RateToRiyal, DateTime LastUpdate, bool IsMain, string Flag,string userid)
        {
            CurrencyViewModel entity = new CurrencyViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.Name = Name;
                entity.ShortName = ShortName;
                entity.Sign = Sign;
                entity.RateToRiyal = RateToRiyal;
                entity.LastUpdate = DateTime.Now;
                entity.IsMain = IsMain;
                entity.Flag = Flag != "" ? Flag : entity.Flag;
                entity.UpdatedBy = userid;
                repository.Update(entity, "Id");
                return entity.Id;
            }
            entity = new CurrencyViewModel()
            {
                Name = Name,
                ShortName = ShortName,
                Sign = Sign,
                RateToRiyal = RateToRiyal,
                IsMain = IsMain,
                Flag = Flag,
                UpdatedBy = userid,
                LastUpdate = DateTime.Now,
            };
            entity = repository.Add(entity);
            return entity.Id;
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}
