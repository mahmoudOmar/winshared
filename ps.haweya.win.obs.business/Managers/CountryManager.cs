﻿using Newtonsoft.Json;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ps.haweya.win.obs.business.Managers
{
    public class CountryManager
    {
        public List<CountryViewModel> Get()
        {
            var countryData = new List<CountryViewModel>();
            var path = HttpContext.Current.Server.MapPath("~/Content/Js/JsonData/countries.json");
            using (StreamReader sr = new StreamReader(path))
            {
                countryData = JsonConvert.DeserializeObject<List<CountryViewModel>>(sr.ReadToEnd());
                return countryData;
            }
        }

        public string GetCountryName(int id)
        {
            var countryData = new List<CountryViewModel>();
            var path = HttpContext.Current.Server.MapPath("~/Content/Js/JsonData/countries.json");
            using (StreamReader sr = new StreamReader(path))
            {
                countryData = JsonConvert.DeserializeObject<List<CountryViewModel>>(sr.ReadToEnd());
                CountryViewModel country=countryData.Where(x => x.id == id).FirstOrDefault();
                if (country != null)
                {
                    return country.name;
                }
                return "";
            }
        }
    }

    public class StateManager
    {
        public List<StateViewModel> Get()
        {
            var state = new List<StateViewModel>();
            var path = HttpContext.Current.Server.MapPath("~/Content/Js/JsonData/states.json");
            using (StreamReader sr = new StreamReader(path))
            {
                state = JsonConvert.DeserializeObject<List<StateViewModel>>(sr.ReadToEnd());
                return state;
            }
        }
    }

    public class CityManager
    {

        public List<CityViewModel> Get()
        {
            var cities = new List<CityViewModel>();
            var path = HttpContext.Current.Server.MapPath("~/Content/Js/JsonData/cities.json");
            using (StreamReader sr = new StreamReader(path))
            {
                cities = JsonConvert.DeserializeObject<List<CityViewModel>>(sr.ReadToEnd());
                return cities;
            }
        }
    }
}
