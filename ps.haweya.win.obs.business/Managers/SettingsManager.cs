﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;

namespace ps.haweya.win.obs.business.Managers
{
    public class SettingsManager : BaseManager<Setting, SettingViewModel>
    {
        public IEnumerable<SettingViewModel> Get()
        {
            return repository.Get();
        }
        public SettingViewModel GetByid(long id)
        {
            return repository.Get(id);
        }
        public int Add(SettingViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.Id;
        }
        public int Update(SettingViewModel entity)
        {
            entity = repository.Update(entity, "Id");
            return entity.Id;
        }
        public int AddOrUpdate(SettingViewModel model)
        {
            SettingViewModel entity = new SettingViewModel();
            if (model.Id != 0)
            {
                repository.Update(model, "Id");
                return entity.Id;
            }

            entity = repository.Add(model);
            return entity.Id;
        }
    }
}
