﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class VerficationPOTManager : BaseManager<VerficationPOT, VerficationPOTViewModel>
    {

        public VerficationPOTViewModel GetByUserId(string Userid,string OTP)
        {
            return repository.Get(x => x.UserID.Equals(Userid)&&x.POT.Equals(OTP)&&x.IsValid==true).FirstOrDefault();
        }
        public VerficationPOTViewModel Add(string Userid, string OTP)
        {
            VerficationPOTViewModel v = new VerficationPOTViewModel();
            v.UserID = Userid;
            v.POT = OTP;
            v.IsValid = true;
            return repository.Add(v);


        }
    }
}
