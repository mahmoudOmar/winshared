﻿using Newtonsoft.Json;
using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using ps.haweya.win.obs.viewModels.CustomModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class UserCartManager : BaseManager<UserCart, UserCartViewModel>
    {
        public UserCartViewModel Add(UserCartViewModel model)
        {
            return repository.Add(model);
        }
        public UserCartViewModel Update(UserCartViewModel model)
        {
            return repository.Update(model,"Id");
        }
        public bool Delete(int cartid)
        {
            return repository.Delete(cartid);
        }

        public bool DeleteByUserID(string USerID)
        {
            return repository.DeleteRange(GetByUserId(USerID).Select(x=>x.Id));
        }
        public IEnumerable<UserCartViewModel> GetByUserId(string UserId)
        {
        return repository.Get(x => x.UserId.Equals(UserId) && x.IsPending == true, "ProductInformation", "");
          //return repository.Get(x => x.UserId.Equals(UserId) && x.IsPending == true);
        }

        public void UpdateUserCart(string userid, List<CartProductModel> products)
        {

          

            var userCart = GetByUserId(userid);

         
           
            AspNetUserViewModel aspuser = new AspNetUsersManager().GetByUserId(userid);
            ProductInformationManager productmanager = new ProductInformationManager();
            foreach (var product in products)
            {
                if (userCart.Where(x => x.ItemId == product.ProductId).Count() == 0)
                    Add(new UserCartViewModel()
                    {
                        ItemId = product.ProductId,
                        Quantity = product.Quantity,
                        IsPending = true,
                        LastModifiedAt = DateTime.Now,
                        UserId = userid
                    });

            }
        
         
        }

    }
}
