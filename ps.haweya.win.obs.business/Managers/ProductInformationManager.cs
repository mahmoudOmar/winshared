﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace ps.haweya.win.obs.business.Managers
{
    public class ProductInformationManager : BaseManager<ProductInformation, ProductInformationViewModel>
    {

        public List<FullProductInformation_ResultViewModel> FullProductInformation(string User)
        {
            return StordProcedurManager.FullProductInformation(User);
        }


        public int GetCreatedProduct(DateTime date)
        {
            return repository.Get(x=>(EntityFunctions.TruncateTime(x.CreateDate) == DateTime.Today )&& x.isActive == false && x.IsWinActive == false).Count();
        }


        public int GetRejectedProduct(DateTime date)
        {
            return repository.Get(x => (EntityFunctions.TruncateTime(x.CreateDate) == DateTime.Today) && x.isActive == true && x.IsWinActive == false).Count();
        }

        public int GetApprovalProduct(DateTime date)
        {
            return repository.Get(x => (EntityFunctions.TruncateTime(x.CreateDate) == DateTime.Today) && x.isActive == true && x.IsWinActive == true).Count();
        }

        public int GetClosedProduct(DateTime date)
        {
            return repository.Get(x => (EntityFunctions.TruncateTime(x.DeadLine) == DateTime.Today)).Count();
        }


        public IEnumerable<ProductInformationViewModel> Get()
        {
            return repository.Get();
        }

        public ProductInformationViewModel GetByid(int Id)
        {
            return repository.Get(Id);
        }
        public ProductInformationViewModel GetByid(long Id)
        {
            return repository.Get(Id);
        }
        public IEnumerable<ProductInformationViewModel> GetClosingBids()
        {
            DateTime startDate = DateTime.Now.AddHours(-24);
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.DeadLine >= startDate && x.PricingType == "Auctionable")).Take(10);
        }
        public ProductInformationViewModel Add(ProductInformationViewModel entity)
        {
            return repository.Add(entity);
        }
        public List<ProductInformationViewModel> AddRange(List<ProductInformationViewModel> entities)
        {
            //  var data = productData.ProjectTo<ViewModel>(iMapper.ConfigurationProvider).ToList();


            return repository.AddRange(entities);
        }
        public DataTable AddRangeTable(DataTable dt)
        {
            //  var data = productData.ProjectTo<ViewModel>(iMapper.ConfigurationProvider).ToList();


            return dt;
        }

        public int GetLastBatchNo(string sellerName)
        {
            var query = repository.QueryableReference().Where(x => x.Username.ToLower().Equals(sellerName.ToLower())).Max(x => x.BatchNumber);
            int batchnno = 1;

            if (int.TryParse(query, out batchnno))
                batchnno++;
            return batchnno;
        }
        public List<string> GetBatchNoBySellerID(string sellerName)
        {
            var query = repository.QueryableReference().Where(x => x.Username.ToLower().Equals(sellerName.ToLower())).GroupBy(x => x.BatchNumber).Where(x=>!string.IsNullOrEmpty(x.Key)).Select(x => x.Key);

            return query.ToList();
        }

        public ProductInformationViewModel Update(ProductInformationViewModel entity)
        {
            var product = GetByid(entity.ProductId);

            if (product == null) throw new Exception("Invalid Id");

            product.IsWinActive = entity.IsWinActive;
            product.isActive = true;
            return repository.Update(product, "ProductId");
        }

        public ProductInformationViewModel UpdateFull(ProductInformationViewModel entity)
        {
            var product = GetByid(entity.ProductId);

            if (entity.ProductId == null||entity.ProductId==0) throw new Exception("Invalid Id");

           // product.IsWinActive = entity.IsWinActive;

            return repository.Update(entity, "ProductId");
        }
        public ProductInformationViewModel Delete(ProductInformationViewModel entity)
        {

            if (entity == null) throw new Exception("Invalid Id");
            return repository.Update(entity, "ProductId");
        }
        public ProductInformationViewModel Updateqty(ProductInformationViewModel entity)
        {
         

            

            return repository.Update(entity, "ProductId");
        }
        public int GetCountstoAdmin(string sellername = "")
        {
            int count = repository.QueryableReference().Where(x=> (sellername!=""?x.Username.Equals(sellername):true)&&x.IsDelete!=true).Count();
            return count;
        }
        public IEnumerable<object> GetWithGroupingProductsByCategory(string sellername="")
        {

            var query = repository.QueryableReference().Where(x=>x.IsDelete!=true)
                              .GroupBy(x => x.Category).OrderByDescending(g => g.Count())
                                  .Select(g => new { Category = g.Key, ProductCount = g.Count() });

            if (!string.IsNullOrEmpty(sellername))
            {
                 query = repository.QueryableReference().Where(x=>x.IsDelete != true&&x.Username.ToLower().Equals(sellername.ToLower()))
                                 .GroupBy(x => x.Category).OrderByDescending(g => g.Count())
                                     .Select(g => new { Category = g.Key, ProductCount = g.Count() });

            }

            var allobjects = from q in query.AsEnumerable()
                             join c in CacheManager.Categories
                             on q.Category equals c.CategoryId
                             select (new
                             {
                                 category = c.CategoryName,
                                 //CountryID=c.id,
                                 ProductCount = q.ProductCount

                             });
            return allobjects.AsEnumerable();

        }

        public IEnumerable<object> GetWithbidcounttocahrt(string sellername = "")
        {
            if (!string.IsNullOrEmpty(sellername))
            {
                return repository.QueryableReference().Where(x =>x.IsDelete!=true&&x.Username.ToLower().Equals(sellername.ToLower())&& x.PricingType.Equals("Auctionable")).OrderByDescending(x => x.CreateDate).Take(10)
                                .Select(g => new { g.ProductTitle, BidCount = g.OBSBidDetails.Count(), BidsfromallBuyers =g.OBSBidDetails.Count()>0? g.OBSBidDetails.Sum(x=>x.BidCount):0 }).AsEnumerable();

            }
                return repository.QueryableReference().Where(x=> x.IsDelete != true &&x.PricingType.Equals("Auctionable")).OrderByDescending(x=>x.CreateDate).Take(10)
                                  .Select(g => new {g.ProductTitle,BidCount = g.OBSBidDetails.Count(), BidsfromallBuyers = g.OBSBidDetails.Count() > 0 ? g.OBSBidDetails.Sum(x => x.BidCount) : 0 }).AsEnumerable();

            //var allobjects = from q in query
            //                 join c in CacheManager.Categories
            //                 on q.Category equals c.CategoryId
            //                 select (new
            //                 {
            //                     category = c.CategoryName,
            //                     //CountryID=c.id,
            //                     ProductCount = q.ProductCount

            //                 });
            //return allobjects.AsEnumerable();

        }


        public int GetBidsProductWithseller(string type, string sellername = "")
        {
            int count = repository.QueryableReference().Where(x => x.IsDelete!=true&&x.PricingType.Equals(type)&&(sellername != "" ? x.Username.Equals(sellername) : true)).Count();
            return count;
        }

        public bool isValidQuantity(int qty, int productquntity)
        {

            if (qty <= productquntity&&qty>0)
                return true;
            else
                return false;
        }

      
    }
}
