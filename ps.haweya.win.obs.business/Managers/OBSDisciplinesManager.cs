﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class OBSDisciplinesManager : BaseManager<OBSDiscipline, OBSDisciplineViewModel>
    {
        public IEnumerable<OBSDisciplineViewModel> Get()
        {
            return repository.Get();
        }

        public OBSDisciplineViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
    }
}
