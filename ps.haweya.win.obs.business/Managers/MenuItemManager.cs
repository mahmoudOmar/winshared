﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;

namespace ps.haweya.win.obs.business.Managers
{
    public class MenuItemManager : BaseManager<MenuItem, MenuItemViewModel>
    {
        public IEnumerable<MenuItemViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<MenuItemViewModel> Gettosite()
        {
            return repository.Get(x=>x.IsActive==true);
        }
        public MenuItemViewModel GetByid(long id)
        {
            return repository.Get(id);
        }
        public IEnumerable<MenuItemViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Title.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public long Add(MenuItemViewModel entity)
        {
            entity = repository.Add(entity);
            CacheManager.MenuItems = new MenuItemManager().Gettosite().ToList();
            return entity.Id;
        }
        public long Update(MenuItemViewModel entity)
        {
            entity = repository.Update(entity);
            CacheManager.MenuItems = new MenuItemManager().Gettosite().ToList();
            return entity.Id;
        }
        public bool Delete(int id)
        {
            var delete= repository.Delete(id);
            CacheManager.MenuItems = new MenuItemManager().Gettosite().ToList();
            return delete;
        }
        public int AddOrUpdate(int Id, int MenuId, string Title, string Link, bool IsActive)
        {
            MenuItemViewModel entity = new MenuItemViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.MenuId = MenuId;
                entity.Title = Title;
                entity.Link = Link;
                entity.IsActive = IsActive;
                repository.Update(entity, "Id");
                return entity.Id;
            }
            entity = new MenuItemViewModel()
            {
                MenuId = MenuId,
                Title = Title,
                Link = Link,
                IsActive = IsActive
            };
            entity = repository.Add(entity);
            CacheManager.MenuItems = new MenuItemManager().Gettosite().ToList();
            return entity.Id;
        }
        public int UpdateIsActive(int id)
        {
            MenuItemViewModel entity = new MenuItemViewModel();
            if (id != 0)
            {
                entity = GetByid(id);
                if (entity != null)
                {
                    entity.IsActive = entity.IsActive != null ? (!entity.IsActive) : true;
                    repository.Update(entity, "ID");
                }
                CacheManager.MenuItems = new MenuItemManager().Gettosite().ToList();
                return entity.Id;
            }

            return entity.Id;
        }
    }
}
