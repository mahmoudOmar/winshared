﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class BuyerOrderManager : BaseManager<BuyerOrder, BuyerOrderViewModel>
    {
        public IEnumerable<BuyerOrderViewModel> Get()
        {
            return repository.Get();
        }
      

        public IEnumerable<BuyerOrderViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public IEnumerable<BuyerOrderViewModel> GetInvoices(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        //GetInvoicesSeller
        public IEnumerable<BuyerOrderViewModel> GetInvoicesSeller(string column, int Page, int PerPage, string SortBy, string SortMethod, string q,string sellername)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x=>x.CartItems.Where(c=>c.ProductInformation.Username.Equals(sellername)).Count()>0).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.CartItems.Where(c => c.ProductInformation.Username.Equals(sellername)).Count() > 0).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public IEnumerable<BuyerOrderViewModel> GetWithIncloudsFinance(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            //List<int?> Financestatusides =  new List<int?>(){ 1, 2, 3, 4, 5, 10 ,18};
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x=>x.StatusID<=5).Skip((Page - 1) * PerPage).Take(PerPage));
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.AdminConfirmed == true).Skip((Page - 1) * PerPage).Take(PerPage));

            //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.AdminConfirmed == true).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public IEnumerable<BuyerOrderViewModel> GetWithIncloudsTechnical(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            List<int?> Technicalstatusides = new List<int?>() {3,  7, 8, 9 };
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x=>x.StatusID<=5).Skip((Page - 1) * PerPage).Take(PerPage));
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => Technicalstatusides.Contains(x.StatusID)).Skip((Page - 1) * PerPage).Take(PerPage));

            //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => Technicalstatusides.Contains(x.StatusID)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public IEnumerable<BuyerOrderViewModel> GetWithIncloudsLogistics(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            List<int?> Logisticsstatusides = new List<int?>() {6, 11, 12 };
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x=>x.StatusID<=5).Skip((Page - 1) * PerPage).Take(PerPage));
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => Logisticsstatusides.Contains(x.StatusID)).Skip((Page - 1) * PerPage).Take(PerPage));

            //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => Logisticsstatusides.Contains(x.StatusID)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public int CountINvoices(string q)
        {
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                return repository.QueryableReference().Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Count();

            return repository.QueryableReference().Count();

        }
        public int CountINvoicesSeller(string q, string sellername)
        {
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                return repository.QueryableReference().Where(x => x.CartItems.Where(c => c.ProductInformation.Username.Equals(sellername)).Count() > 0).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Count();

            return repository.QueryableReference().Where(x => x.CartItems.Where(c => c.ProductInformation.Username.Equals(sellername)).Count() > 0).Count();

        }
        public int Count(string q)
        {
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                return repository.QueryableReference().Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Count();

            return repository.QueryableReference().Count();

        }
        public int GetWithIncloudsFinanceCount(string q)
        {
            //List<int?> Financestatusides = new List<int?>() { 1, 2, 3, 4, 5, 10 };
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                //return repository.QueryableReference().Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Count();
                return repository.QueryableReference().Where(x => x.AdminConfirmed == true).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Count();

            //return repository.QueryableReference().Where(x => x.StatusID <= 5).Count();
            return repository.QueryableReference().Where(x => x.AdminConfirmed == true).Count();

        }
        public int GetWithIncloudsTechnicalCount(string q)
        {
            List<int?> Technicalstatusides = new List<int?>() { 3, 7, 8, 9 };
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                //return repository.QueryableReference().Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Count();
                return repository.QueryableReference().Where(x => Technicalstatusides.Contains(x.StatusID)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Count();

            //return repository.QueryableReference().Where(x => x.StatusID <= 5).Count();
            return repository.QueryableReference().Where(x => Technicalstatusides.Contains(x.StatusID)).Count();

        }
        public int GetWithIncloudsLogisticsCount(string q)
        {
            List<int?> Logisticsstatusides = new List<int?>() { 11, 12, 6 };
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                //return repository.QueryableReference().Where(x => x.StatusID <= 5).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Count();
                return repository.QueryableReference().Where(x => Logisticsstatusides.Contains(x.StatusID)).Where(x => x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.Email.ToLower().Contains(q) || x.Status.Name.ToLower().Contains(q) || x.payment_method.Contains(q)).Count();

            //return repository.QueryableReference().Where(x => x.StatusID <= 5).Count();
            return repository.QueryableReference().Where(x => Logisticsstatusides.Contains(x.StatusID)).Count();

        }

        public BuyerOrderViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public IEnumerable<BuyerOrderViewModel> GetByBuyerID(string Userid)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.UserID == Userid).OrderByDescending(x => x.InsertDate));
        }

        

        public int GetCountstoAdmin(string Userid="")
        {
            int count= repository.QueryableReference().Where(x =>Userid!=""?x.UserID == Userid:true).Count();
            return count;
        }

        public int GetCountstoSeller(string selleremail="")
        {
            int count = repository.QueryableReference().Where(x => selleremail != "" ? (x.CartItems.Where(s=>s.ProductInformation.Username !=null&&s.ProductInformation.Username.ToLower().Equals(selleremail.ToLower())).Count()>0):false).Count();
            return count;
        }

        public BuyerOrderViewModel GetLastByBuyerid(string Userid)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.UserID == Userid).OrderByDescending(x => x.InsertDate)).FirstOrDefault();
        }
        public BuyerOrderViewModel GetLastPinddingByBuyerid(string Userid)
        {
            return repository.QueryablResult(repository.QueryableReference("CartItems", "").Where(x => x.UserID == Userid && x.ClosedDate == null && x.IsSentToAdmin == false).OrderByDescending(x => x.InsertDate)).FirstOrDefault();
        }
        public IEnumerable<object> GetOrdersByStatus()
        {
            return repository.QueryableReference()
                              .GroupBy(x => x.Status)
                              .Select(g => new { Key = g.Key.Name, Value = g.Count(), Id = g.Key.ID }).AsEnumerable();
        }

        public int UpdateOrder(BuyerOrderViewModel entity)
        {
            entity = repository.Update(entity);
            return entity.ID;
        }
        public int AddOrder(BuyerOrderViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.ID;
        }

        public int UpdateOrderStatus(int orderid, int statusid)
        {
            BuyerOrderViewModel entity = GetByid(orderid);
            entity.StatusID = statusid;
            if (statusid == 18)
            {
                entity.AdminConfirmed = true;
            }

            if (statusid == 19)
            {
                entity.AdminConfirmed = false;
            }
            entity = repository.Update(entity);
            return entity.ID;
        }
        public DataTable ExportOrders()
        {
            var query = repository.QueryableReference();

            var CountryList = new CountryManager().Get();
            var queryitems = query.Select(x => new
            {
                OrderNO="ORD-"+x.ID,
                FullName = x.AspNetUser != null ? x.AspNetUser.FullName : "",
                ProfilePhoto = x.AspNetUser != null ? x.AspNetUser.ProfilePhoto : "",
                Email = x.AspNetUser != null ? x.AspNetUser.Email : "",
                CountryId = x.AspNetUser != null ? x.AspNetUser.CountryId : "0",
                ItemsCount = x.CartItems.Count(),
                TotalPrice = ((x.CartItems.Sum(c => (c.ProductInformation.UnitPrice * c.Qty) + (c.ShippingValue ?? 0) + (c.InspectionValue ?? 0))) + (x.CartItems.Sum(c => (c.ProductInformation.UnitPrice * c.Qty) + (c.ShippingValue ?? 0) + (c.InspectionValue ?? 0))) * 0.05)+""+ System.Configuration.ConfigurationManager.AppSettings["System_Currency"],
                payment_method = x.payment_method ?? "",
                StatusID = x.StatusID,
                StatusText = x.Status.Name,
                x.InsertDate,
               // DeliveryType = x.DeliveryType ?? 0,
                AdminConfirmed=x.AdminConfirmed==true? "Confirmed": x.AdminConfirmed == false? "Refused": "pending",
                Country = ""
            }).ToList();


            var list = queryitems.Select(x => new
            {
                x.OrderNO,
                x.FullName,
                x.ProfilePhoto,
                x.Email,
                x.ItemsCount,
                x.TotalPrice,
                x.payment_method,
                x.StatusText,
                x.InsertDate,
                x.AdminConfirmed,
                Country = CountryList.Where(t => t.id == int.Parse(x.CountryId ?? "0")).FirstOrDefault()!=null? CountryList.Where(t => t.id == int.Parse(x.CountryId ?? "0")).FirstOrDefault().name:""

            });
          

            DataTable dt = ps.haweya.win.obs.common.Helper.ToDataTable(list.ToList());

            return dt;

        }

    }
}
