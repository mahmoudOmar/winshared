﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class ManufacturerDetailsManager:BaseManager<OBSManufacturer,OBSManufacturerViewModel>
    {

        //public IEnumerable<OBSManufacturerViewModel> GetManufacturersListHome() {

        //    return repository.Get(include: "ProductInformations").OrderByDescending(x => x.ProductInformations.Count());

        //   // return repository.Get();
        //}

        //public IEnumerable<OBSManufacturerViewModel> GetManufacturersListHome(string q)
        //{
        //    return repository.Get(x => x.ManufacturersName.Contains(q), "ProductInformations").OrderByDescending(x=>x.ProductInformations.Count());
        //   // return repository.Get(x => x.ManufacturersName.Contains(q));
        //}
        public IEnumerable<OBSManufacturerViewModel> Get()
        {
            return repository.Get().OrderByDescending(x => x.DateCreated);
        }
     
        //public  Dictionary<int, string> GetManufacturersList(string q)
        //{
        //    var manufacturers = repository.Get(x => x.ManufacturersName.Contains(q),"ProductInformations"); 
        //    var lstManufacturers = manufacturers.ToDictionary(x => x.ManufacturersId, y => y.ManufacturersName);
        //    return lstManufacturers;
        //}
    }
}
