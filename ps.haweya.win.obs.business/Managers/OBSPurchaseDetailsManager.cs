﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class OBSPurchaseDetailsManager : BaseManager<OBSPurchaseDetail, OBSPurchaseDetailViewModel>
    {
        public IEnumerable<object> Get()
        {
            var details = repository.QueryablResult(repository.QueryableReference(""));
            var x = (from p in details
                     select new
                           {
                               PurchaseId = p.PurchaseId,
                               ProductTitle = p.ProductInformation.ProductTitle,
                               ProductImage = p.ProductInformation.ProductImg,
                               Location = p.ProductInformation.ProductLocation,
                               SellerUsername = p.ProductInformation.Username,
                               Quantity = p.BuyQuantity,
                               Price = 0,
                               BuyerUsername = p.OBSUserDetail.Email,
                               IsPurchase = p.IsPurchased,
                               TrackingData = (from tracking in p.OBSPurchaseLiveTrackings
                                               select new
                                               {
                                                   TrackingId = tracking.TrackingId,
                                                   PurchaseId = tracking.PurchaseId,
                                                   Inspection = tracking.Inspection,
                                                   OutForDelivery = tracking.OutForDelivery,
                                                   Payment = tracking.Payment,
                                                   ProductConfirmation = tracking.ProductConfirmation,
                                                   Delivered = tracking.Delivered
                                               })
                           }).ToList();
            return x;
        }
        public IEnumerable<OBSPurchaseDetailViewModel> GetAll()
        {
            return repository.Get();
        }

        public IEnumerable<object> GetByUser(string Username)
        {
            var details = repository.QueryablResult(repository.QueryableReference(""));
            var d = (from p in details
                     select new
                     {
                         PurchaseId = p.PurchaseId,
                         ProductTitle = p.ProductInformation.ProductTitle,
                         ProductImage = p.ProductInformation.ProductImg,
                         Location = p.ProductInformation.ProductLocation,
                         SellerUsername = p.ProductInformation.Username,
                         Quantity = p.BuyQuantity,
                         Price = 0,
                         BuyerUsername = p.OBSUserDetail.Email,
                         IsPurchase = p.IsPurchased,
                         TrackingData = (from tracking in p.OBSPurchaseLiveTrackings
                                         select new
                                         {
                                             TrackingId = tracking.TrackingId,
                                             PurchaseId = tracking.PurchaseId,
                                             Inspection = tracking.Inspection,
                                             OutForDelivery = tracking.OutForDelivery,
                                             Payment = tracking.Payment,
                                             ProductConfirmation = tracking.ProductConfirmation,
                                             Delivered = tracking.Delivered
                                         })
                     }).ToList().Where(x => x.SellerUsername == Username || x.BuyerUsername == Username);
            return d;
        }

        // TODO: add the username to the query condition
        public IEnumerable<object> GetInvoiceDetailsByBuyer(string BuyerUserName)
        {
            var details = repository.QueryablResult(repository.QueryableReference(""));
            var invoices = (from p in details
                     select new
                     {
                         PurchaseId = p.PurchaseId,
                         Location = p.ProductInformation.ProductLocation,
                         Quantity = p.BuyQuantity,
                         PurchaseOn = p.PurchaseOn,
                         UserDetailsId = p.UserDetailsId,
                         ProductId = p.ProductId,
                         BuyQuantity = p.BuyQuantity,
                         AddedOn = p.AddedOn,
                         ProductInformation = new
                         {
                             ProductTitle = p.ProductInformation.ProductTitle,
                             ProductImage = p.ProductInformation.ProductImg,
                             ProductLocation = p.ProductInformation.ProductLocation
                         },
                         OBSUserDetail = p.OBSUserDetail,
                         IsPurchase = p.IsPurchased
                     }).ToList().Where(x=>x.IsPurchase == true /*&& x.OBSUserDetail.Email == BuyerUserName*/).OrderByDescending(x => x.ProductId); ;
            return invoices;
        }

        public OBSPurchaseDetailViewModel GetInvoiceDetailsByPurchaseID(int ID)
        {
            var details = repository.QueryablResult(repository.QueryableReference(""));
            var invoice = (from p in details
                           select new OBSPurchaseDetailViewModel
                           {
                               IsShipment = p.IsShipment,
                               ShipmentCharges = p.ShipmentCharges,
                               PurchaseId = p.PurchaseId,
                               PurchaseOn = p.PurchaseOn,
                               UserDetailsId = p.UserDetailsId,
                               ProductId = p.ProductId,
                               BuyQuantity = p.BuyQuantity,
                               AddedOn = p.AddedOn,
                               OBSUserDetail = p.OBSUserDetail,
                               IsPurchased = p.IsPurchased,
                               ProductInformation = p.ProductInformation,
                           }).ToList().Where(x => x.IsPurchased == true && x.PurchaseId == ID).FirstOrDefault();
            return invoice;
        }

    }
}
