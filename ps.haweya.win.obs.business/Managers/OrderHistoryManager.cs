﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class OrderHistoryManager : BaseManager<OrderHistory, OrderHistoryViewModel>
    {
        public IEnumerable<OrderHistoryViewModel> GetByOrderId(int OrderID)
        {
            return repository.QueryablResult(repository.QueryableReference(IncludColumn: "Status|Status1").Where(x => x.OrderID == OrderID).OrderBy(x => x.InsertDate));
        }
        public int Add(OrderHistoryViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.ID;
        }
        public int Add(int orderId, int newStatus, string note,string addedby,int oldStatusid)
        {

            OrderHistoryViewModel entity = new OrderHistoryViewModel()
            {
                OrderID = orderId,
                OldStatusID = oldStatusid,
                NewStatusID = newStatus,
                Notes = note,
                AddedBy = addedby,
                InsertDate = DateTime.Now
            };
            entity = repository.Add(entity);
            return entity.ID;
        }
    }
}
