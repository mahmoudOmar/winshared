﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.viewModels;

namespace ps.haweya.win.obs.business.Managers
{

    public class AspNetUsersManager : BaseManager<AspNetUser, AspNetUserViewModel>
    {

        public IEnumerable<AspNetUserViewModel> Get()
        {
            return repository.Get();
        }

        //public string GetUserDEFAULT_ROLE(string UserID)
        //{

        //    return r;
        //}
        public IEnumerable<AspNetUserViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q,string role)
        {
            q = q.ToLower();
          
            if (!string.IsNullOrEmpty(role))
            {
                role = role.ToLower();
                if (string.IsNullOrEmpty(q))
                    return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.AspNetRoles.Where(a => a.Name.ToLower().Equals(role)).Count() > 0).Skip((Page - 1) * PerPage).Take(PerPage));

                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.AspNetRoles.Where(a => a.Name.ToLower().Equals(role)).Count() > 0).Where(x => x.FullName.ToLower().Contains(q) || x.Email.ToLower().Contains(q) || x.UserName.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
            }
            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.FullName.ToLower().Contains(q) || x.Email.ToLower().Contains(q) || x.UserName.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public IEnumerable<AspNetUserViewModel> GetByRole(string Role)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.AspNetRoles.Where(a => a.Name.ToLower().Contains(Role.ToLower())).Count() > 0));
        }
        public int GetBuyersCountstoAdmin(string Role)
        {
            return repository.QueryableReference().Where(x => x.AspNetRoles.Where(a => a.Name.ToLower().Contains(Role.ToLower())).Count() > 0).Count();
        }
        
        public IEnumerable<AspNetUserViewModel> GetByGetClosingBidsReport(bool Status)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.GetClosingBidsReport == Status));
        }
        public int Count(string q ,string role)
        {
            q = q.ToLower();
          
            if (!string.IsNullOrEmpty(role))
            {
                role = role.ToLower();
                if (!string.IsNullOrEmpty(q))
                    return repository.QueryableReference().Where(x => x.AspNetRoles.Where(a => a.Name.ToLower().Equals(role)).Count() > 0).Where(x => x.FullName.ToLower().Contains(q) || x.Email.ToLower().Contains(q) || x.UserName.ToLower().Contains(q)).Count();

                return repository.QueryableReference().Where(x => x.AspNetRoles.Where(a => a.Name.ToLower().Equals(role)).Count() > 0).Count();

            }
            if (!string.IsNullOrEmpty(q))
                return repository.QueryableReference().Where(x => x.FullName.ToLower().Contains(q) || x.Email.ToLower().Contains(q) || x.UserName.ToLower().Contains(q)).Count();

            return repository.QueryableReference().Count();

        }
        public AspNetUserViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public AspNetUserViewModel GetByUserId(string Userid)
        {
            return repository.Get(x => x.Id.Equals(Userid)).FirstOrDefault();
        }
        public AspNetUserViewModel GetByFullName(string FullName)
        {
            return repository.Get(x => x.FullName == FullName).FirstOrDefault();
        }
        public AspNetUserViewModel GetByUserIdWithRole(string Userid)
        {
            return repository.Get(x => x.Id.Equals(Userid) && (x.AspNetRoles.Where(a => a.Name.Equals("Seller") || a.Name.Equals("Buyer")).Count() > 0)).FirstOrDefault();
        }
        public AspNetUserViewModel GetByUserWithIncludes(string Userid)
        {
            var x = repository.QueryablResult(repository.QueryableReference("UserPreferedCategories|UserPreferedManufacturers|UserPreferedCountries|UserReports", "").Where(s => s.Id.Equals(Userid))).ToList().FirstOrDefault();
            return x;
        }


        public AspNetUserViewModel GetById(string UserId)
        {
            return repository.Get(x => x.Id == UserId).FirstOrDefault();
        }

        public AspNetUserViewModel GetByEmail(string Email)
        {
            var query = repository.QueryableReference().Where(x => x.Email == Email);
            return repository.QueryablResult(query).FirstOrDefault();

        }
        public AspNetUserViewModel Update(AspNetUserViewModel model)
        {

            var record = GetById(model.Id);

            record.FullName = model.FullName;
            record.PhoneNumber = model.PhoneNumber;
            record.POBox = model.POBox;
            record.CountryId = model.CountryId;
            record.StateId = model.StateId;
            record.CityId = model.CityId;

            return repository.Update(record, "Id");
        }

        public AspNetUserViewModel AddOrUpdateReport(AspNetUserViewModel model)
        {
            //var report = repository.repo
            return model;
        }

        public IEnumerable<object> GetBuyersGroupingByCountry(string seller="")
        {

            var querylist = repository.QueryableReference().Where(x => x.AspNetRoles.Where(r => r.Name.Equals("Buyer")).Count() > 0);
                            

            if(!string.IsNullOrEmpty(seller))
            {
                querylist = querylist.Where(x => x.BuyerOrders.Where(o => o.CartItems.Where(c => c.ProductInformation.Username.ToLower().Equals(seller.ToLower())).Count() > 0).Count() > 0);
            }

            var query= querylist.GroupBy(x => x.CountryId).OrderByDescending(g => g.Count())
                                  .Select(g => new { Country = g.Key, BuyerCount = g.Count() });
            var allobjects = from q in query.AsEnumerable()
                             join c in CacheManager.Countries
                             on Convert.ToInt32(q.Country??"0") equals c.id
                             select (new
                             {
                                 Country = c.name,
                                 //CountryID=c.id,
                                 BuyerCount = q.BuyerCount

                             });
            return allobjects.AsEnumerable();

        }

        public List<string> GetAllPOT()
        {


            var query = repository.QueryableReference().GroupBy(x => x.POT)
                                  .Select(g => new { pot =g.Key??"0" }).Select(x=>x.pot).ToList();
                            
            return query;

        }

        public int GetCreatedToday()
        {
             return repository.QueryableReference().Where(x => x.InsertDate.Year == DateTime.Now.Year
            && x.InsertDate.Month == DateTime.Now.Month
            && x.InsertDate.Day == DateTime.Now.Day).Count();
            //return 0;
        }
        //public AspNetUserViewModel AssignCategories(List<OBSProductCategoryViewModel> categoriesPreferences, string UserId)
        //{
        //    var user = GetByUserId(UserId);
        //    if(user != null)
        //    {
        //        //user.OBSProductCategories = categoriesPreferences;
        //        //repository.Update(user, "Id");
        //        foreach(var item in categoriesPreferences) { 

        //        }
        //    }
        //    return user;

        //}
    }
}
