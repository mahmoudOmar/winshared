﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;

namespace ps.haweya.win.obs.business.Managers
{
    public class SliderManager : BaseManager<Slider, SliderViewModel>
    {
        public IEnumerable<SliderViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<SliderViewModel> GetActive()
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x=>x.IsActive==true));
           // return repository.Get();
        }
        public IEnumerable<SliderViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                // return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));
                   return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Title.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public int Count(string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.Title.ToLower().Contains(q)).Count();
        }
        public SliderViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public int Add(SliderViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.Id;
        }

        public int AddOrUpdate(int Id, string Title,string Link, bool IsActive, string filename, string userid)
        {
            SliderViewModel entity = new SliderViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.Title = Title;
                entity.Link = Link;
                entity.IsActive = IsActive;
                entity.ImageFile = filename != "" ? filename : entity.ImageFile;
                entity.LastUpdate = DateTime.Now;
                entity.UpdatedBy = userid;
                repository.Update(entity, "Id");
                return entity.Id;
            }
            entity = new SliderViewModel()
            {
                Title = Title,
                Link = Link,
            IsActive = IsActive,
                ImageFile = filename,
                AddBy = userid,
                AddOn = DateTime.Now,
                LastUpdate = DateTime.Now,
                UpdatedBy = userid,
            };
            entity = repository.Add(entity);
            return entity.Id;
        }
        public int UpdateIsActive(int Id, string userid)
        {
            SliderViewModel entity = new SliderViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                if (entity != null)
                {
                    entity.UpdatedBy = userid;
                    entity.IsActive = entity.IsActive != null ? (!entity.IsActive) : true;
                    repository.Update(entity, "ID");
                }
                return entity.Id;
            }

            return entity.Id;
        }

        public int Update(SliderViewModel entity)
        {
            entity = repository.Update(entity);
            return entity.Id;
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}
