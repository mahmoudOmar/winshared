﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
   public class CompanyManager: BaseManager<Company, CompanyViewModel>
    {
        public IEnumerable<CompanyViewModel> Get()
        {
            return repository.Get();
        }

        public CompanyViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public CompanyViewModel GetByName(string Name)
        {
             var company= repository.QueryablResult(repository.QueryableReference().Where(x=>x.Name.ToLower().Equals(Name.ToLower()))).FirstOrDefault();
            return company;

        }

        public IEnumerable<CompanyViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Name.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public int Count(string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.Name.ToLower().Contains(q)).Count();
        }

        public int Add(CompanyViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.ID;
        }

        public int AddOrUpdate(int Id, string name)
        {
            CompanyViewModel entity = new CompanyViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.Name = name;
               
                repository.Update(entity, "ID");
                return entity.ID;
            }
            entity = new CompanyViewModel()
            {
                Name = name,
               
            };
            entity = repository.Add(entity);
            return entity.ID;
        }
     

        public int Update(CompanyViewModel entity)
        {
            entity = repository.Update(entity, "ID");
            return entity.ID;
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}
