﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;

namespace ps.haweya.win.obs.business.Managers
{
    public class CurrencyChangesManager: BaseManager<Currencychanging, CurrencychangingViewModel>
    {

        public IEnumerable<CurrencychangingViewModel> Get()
        {
            return repository.Get();
        }
        public CurrencychangingViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public IEnumerable<CurrencychangingViewModel> GetWithInclouds(int currencyId)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.CurrencyId==currencyId));
        }

        public int AddRate(int currencyId, double rate)
        {
            var cc = new CurrencychangingViewModel() {CurrencyId=currencyId,RateToRiyal=rate,InsertDate=DateTime.Now };

            var item = repository.Add(cc);
            return item.Id;
        }
    }
}
