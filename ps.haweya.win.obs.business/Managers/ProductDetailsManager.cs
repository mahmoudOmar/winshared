﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ps.haweya.win.obs.business.Managers
{
    public class ProductDetailsManager : BaseManager<ProductDetail, ProductDetailViewModel>
    {
        public ProductDetailViewModel Add(ProductDetailViewModel entity)
        {
            return repository.Add(entity);
        }
        public ProductDetailViewModel Update(ProductDetailViewModel entity)
        {
            return repository.Update(entity, "ProductDetailsId");
        }

        public IEnumerable<ProductDetailViewModel> Get()
        {
            return repository.Get();
        }

        public IEnumerable<ProductDetailViewModel> Get(int? id, string q, bool? AuctionCheck = null, bool? DirectCheck = null, List<int> brandsID = null, double? min = null, double? max = null, int? sorting = null, int? page = null, int? viewcount = null, string condetion = "", bool? active = null)
        {
            int productno = 0;
            int.TryParse(q, out productno);

            int pageNo = page != null ? Convert.ToInt32(page) : 0;
            int PageSize = viewcount != null ? Convert.ToInt32(viewcount) : 12;
            var query = repository.QueryableReference();


            //query = query.Where(x => x.ProductInformation.ProductImage != "" && x.ProductInformation.ProductImage != "No files selected");
            if (id != null)
                query = query.Where(x => x.ProductInformation.Category == id);




            if (!string.IsNullOrEmpty(q))
            {

                if (productno > 0)
                {
                    query = query.Where(x => x.ProductInformation.ProductId == productno || x.ProductInformation.ProductBrands == productno || x.ProductInformation.Keywords.Contains(productno + "") || x.ProductInformation.Keywords.Contains(productno + ""));
                    int c = query.Count();
                }
                else
                {
                    var product = query.Where(x => x.ProductInformation.ProductTitle.ToString().Contains(q));

                    if (product.Count() == 0)
                        product = product.Union(query.Where(x => x.Description.Contains(q) || x.ProductInformation.Keywords.ToLower().Contains(q.ToLower())));
                    //product = product.Union(query.Where(x =>x.ProductInformation.ProductBrands.ToString().Contains(q)));
                    //if (product.Count() > 0)
                        query = product;
                }
            }


            if (active != null)
                query = query.Where(x => x.ProductInformation.IsWinActive == active);

            if (AuctionCheck == true && DirectCheck != true)
                query = query.Where(x => x.ProductInformation.PricingType == "Auctionable");


            if (AuctionCheck != true && DirectCheck == true)
                query = query.Where(x => x.ProductInformation.PricingType != "Auctionable");


            if (brandsID != null && brandsID.Count() > 0)
                query = query.Where(x => brandsID.Contains(x.ProductInformation.ProductBrands));

            if (min != null)
                query = query.Where(x => x.ProductInformation.UnitPrice >= min);

            if (max != null)
                query = query.Where(x => x.ProductInformation.UnitPrice <= max);


            if (condetion != "")
                query = query.Where(x => condetion.Contains(x.Condition));


            if (string.IsNullOrEmpty(q))
                query = query.OrderBy(x => x.ProductInformation.OBSProductCategory.OrderID);
            else
                query = query.OrderBy(x => x.ProductInformation.ProductTitle.Contains(q));




            if (sorting != null && sorting == 1)
                query = query.OrderBy(x => x.ProductInformation.UnitPrice);

            if (sorting != null && sorting == 2)
                query = query.OrderByDescending(x => x.ProductInformation.UnitPrice);
            
            if (sorting != null && sorting == 3)
                query = query.OrderBy(x => x.ProductInformation.ProductTitle);

            if (sorting != null && sorting == 4)
                query = query.OrderByDescending(x => x.ProductInformation.ProductTitle);
            query = query.Skip(pageNo * PageSize).Take(PageSize);

            var result = repository.QueryablResult(query);

            return result;
        }
        public IQueryable<ProductDetail> Getqueryablemap(int? id, string q, bool? AuctionCheck = null, bool? DirectCheck = null, List<int> brandsID = null, double? min = null, double? max = null, int? sorting = null, int? page = null, int? viewcount = null, List<string> condetion = null, bool? active = null)
        {
            int productno = 0;
            int.TryParse(q, out productno);

            int pageNo = page != null ? Convert.ToInt32(page) : 0;
            int PageSize = viewcount != null ? Convert.ToInt32(viewcount) : 12;
            var query = repository.QueryableReference();


            //query = query.Where(x => x.ProductInformation.ProductImage != "" && x.ProductInformation.ProductImage != "No files selected");
            if (id != null)
                query = query.Where(x => x.ProductInformation.Category == id);
            if (active != null)
                query = query.Where(x => x.ProductInformation.IsWinActive == active);

            if (!string.IsNullOrEmpty(q))
                query = query.Where(x => x.ProductInformation.ProductTitle.Contains(q) || x.Description.Contains(q) || x.ProductId.ToString().Contains(q));


            if (AuctionCheck == true && DirectCheck != true)
                query = query.Where(x => x.ProductInformation.PricingType == "Auctionable");


            if (AuctionCheck != true && DirectCheck == true)
                query = query.Where(x => x.ProductInformation.PricingType != "Auctionable");


            if (brandsID != null && brandsID.Count() > 0)
                query = query.Where(x => brandsID.Contains(x.ProductInformation.ProductBrands));

            if (min != null)
                query = query.Where(x => x.ProductInformation.UnitPrice >= min);

            if (max != null)
                query = query.Where(x => x.ProductInformation.UnitPrice <= max);


            if (condetion != null && condetion.Count > 0)
                query = query.Where(x => condetion.Contains(x.Condition));

            query = query.OrderBy(x => x.ProductInformation.OBSProductCategory.OrderID);


            //if (sorting != null && sorting == 1)
            //    query = query.OrderBy(x => x.ProductInformation.UnitPrice);

            //if (sorting != null && sorting == 2)
            //    query = query.OrderByDescending(x => x.ProductInformation.UnitPrice);

            //query = query.Skip(pageNo * PageSize).Take(PageSize);

            //var result = repository.QueryablResult(query);

            return query;
        }
        public int GetCount(int? id, string q, bool? AuctionCheck, bool? DirectCheck, List<int> brandsID, double? min, double? max, int? sorting, int? page, int? viewcount, List<string> condetion = null, bool? active = null)
        {
            int productno = 0;
            int.TryParse(q, out productno);

            int pageNo = page != null ? Convert.ToInt32(page) : 0;
            int PageSize = viewcount != null ? Convert.ToInt32(viewcount) : 12;
            var query = repository.QueryableReference();


            //query = query.Where(x => x.ProductInformation.ProductImage != "" && x.ProductInformation.ProductImage != "No files selected");
            if (id != null)
                query = query.Where(x => x.ProductInformation.Category == id);




            if (!string.IsNullOrEmpty(q))
            {

                if (productno > 0)
                {
                    query = query.Where(x => x.ProductInformation.ProductId == productno || x.ProductInformation.ProductBrands == productno);
                    int c = query.Count();
                }
                else
                {
                    var product = query.Where(x => x.ProductInformation.ProductTitle.ToString().Contains(q));

                    if (product.Count() == 0)
                        product = product.Union(query.Where(x => x.Description.Contains(q)));
                    //product = product.Union(query.Where(x =>x.ProductInformation.ProductBrands.ToString().Contains(q)));
                    if (product.Count() > 0)
                        query = product;
                }
            }


            if (active != null)
                query = query.Where(x => x.ProductInformation.IsWinActive == active);

            if (AuctionCheck == true && DirectCheck != true)
                query = query.Where(x => x.ProductInformation.PricingType == "Auctionable");


            if (AuctionCheck != true && DirectCheck == true)
                query = query.Where(x => x.ProductInformation.PricingType != "Auctionable");


            if (brandsID != null && brandsID.Count() > 0)
                query = query.Where(x => brandsID.Contains(x.ProductInformation.ProductBrands));

            if (min != null)
                query = query.Where(x => x.ProductInformation.UnitPrice >= min);

            if (max != null)
                query = query.Where(x => x.ProductInformation.UnitPrice <= max);


            if (condetion != null && condetion.Count > 0)
                query = query.Where(x => condetion.Contains(x.Condition));


            if (string.IsNullOrEmpty(q))
                query = query.OrderBy(x => x.ProductInformation.OBSProductCategory.OrderID);
            else
                query = query.OrderBy(x => x.ProductInformation.ProductTitle.Contains(q));


            //if (sorting != null && sorting == 1)
            //    query = query.OrderBy(x => x.ProductInformation.UnitPrice);

            //if (sorting != null && sorting == 2)
            //    query = query.OrderByDescending(x => x.ProductInformation.UnitPrice);
           // var qcount = query.Select(x => x.ProductId);
           // int count = query.Count();
            return query.Select(x=>x.ProductId).Count();
        }

        public ProductDetailViewModel GetByid(int id)
        {
            return repository.Get(t => t.ProductId == id, include: "ProductInformation.OBSUOM").FirstOrDefault();
        }
        public IEnumerable<ProductDetailViewModel> GetMainAuctionsTop(int top = 1)
        {
            var query = repository.QueryableReference();
            query = query.Where(x => x.ProductInformation.PricingType == "Auctionable"
           && x.ProductInformation.BidStartDate != null && x.ProductInformation.BidStartDate.Value >= DateTime.Now
            //&& x.ProductInformation.BidStartDate.Value.Month >= DateTime.Now.Month
            //&& x.ProductInformation.BidStartDate.Value.Day >= DateTime.Now.Day


            && x.ProductInformation.IsWinActive == true);
            query = query.OrderBy(x => x.ProductInformation.BidStartDate.Value).Skip(0).Take(top);
            // return repository.QueryablResult(query);
            return repository.QueryablResult(query);
        }
        public IEnumerable<ProductDetailViewModel> GetMainEndingAuctionsTop(int top = 1)
        {
            var query = repository.QueryableReference();
            query = query.Where(x => x.ProductInformation.PricingType == "Auctionable"
&& x.ProductInformation.BidStartDate != null && x.ProductInformation.BidStartDate.Value <= DateTime.Now
           && x.ProductInformation.IsSold != true
           && x.ProductInformation.OBSBidDetails.Where(s => s.IsSold == true).Count() == 0
           && (x.ProductInformation.DeadLine == null || x.ProductInformation.DeadLine >= DateTime.Now)


            && x.ProductInformation.IsWinActive == true);
            query = query.OrderBy(x => x.ProductInformation.DeadLine != null).Skip(0).Take(top);
            // return repository.QueryablResult(query);
            var result = repository.QueryablResult(query);
            return result;
        }
        public IEnumerable<ProductDetailViewModel> GetToMap(int? country, int? state, int? city, int count = 10)
        {
            var query = repository.QueryableReference().Where(x => x.ProductInformation.IsWinActive == true);
            if (country != null && country != 0)
                query = query.Where(x => x.Country == country);
            if (state != null && state != 0)
                query = query.Where(x => x.State == state + "");
            if (city != null && city != 0)
                query = query.Where(x => x.City == city + "");
            query = query.OrderBy(x => x.ProductInformation.DeadLine).Skip(0).Take(count);
            // return repository.QueryablResult(query);
            return repository.QueryablResult(query);
        }
        public IEnumerable<ProductDetailViewModel> GetToMaphome()
        {
            var query = repository.QueryableReference().Where(x => x.ProductInformation.IsWinActive == true && x.ProductInformation.ShowInMap == true && x.lat != null && x.lat > 0 && x.lon != null && x.lon > 0);

            query = query.OrderBy(x => x.ProductInformation.DeadLine).Skip(0).Take(100);
            // return repository.QueryablResult(query);
            return repository.QueryablResult(query);
        }

        public List<ProductMapData> GetHomeProductForMap()
        {
            // x.ProductInformation.DeadLine >= DateTime.Now && x.ProductInformation.CreateDate >= DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)) && 
            var query = repository.QueryableReference().Where(x => x.ProductInformation.IsWinActive == true && x.ProductInformation.ShowInMap == true && x.lat != null && x.lat != 0 && x.lon != null && x.lon != 0).OrderByDescending(x => x.ProductInformation.CreateDate).Skip(0).Take(100);
            var productData = repository.QueryablResult(query); ;

            List<ProductMapData> data = productData.Select(x => new ProductMapData()
            {
                lat = x.lat,
                lon = x.lon,
                title = x.ProductInformation.ProductTitle,
                html = "<img src='" + x.ProductInformation.ProductImg[0] + "?w=250" + "' alt='" + x.ProductInformation.ProductTitle + "'/><br/><p style='width:250px'><a href='/ProductDetails/Details/" + x.ProductId + "/" + GenerateSlug(x.ProductInformation.ProductTitle) + "'><b>" + x.ProductInformation.ProductTitle + "</b></a></p>",
                //icon = "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
                show_infowindow = true
            }).ToList();
            List<ProductMapData> d1 = new List<ProductMapData>();
            string samelocationcontant = "";


            foreach (var itm in data)
            {
                ProductMapData dataitm = d1.Where(x => x.lat == itm.lat && x.lon == itm.lon).FirstOrDefault();
                if (dataitm != null)
                {
                    dataitm.lat = itm.lat;
                    dataitm.lon = itm.lon;
                    dataitm.title = itm.title;
                    dataitm.html += "<div class='mapitem'><hr/>" + itm.html + "</div>";
                    dataitm.show_infowindow = true;

                }
                else
                {
                    dataitm = new ProductMapData();

                    dataitm.lat = itm.lat;
                    dataitm.lon = itm.lon;
                    dataitm.title = itm.title;
                    dataitm.html = "<div class='mapitem'>" + itm.html + "</div>";
                    dataitm.show_infowindow = true;
                    d1.Add(dataitm);

                }

            }
            //= new List<ProductMapData>();

            return d1;



        }
        public IEnumerable<ProductDetailViewModel> Getrelated(int categoryid, int productid)
        {
            var model = new ProductInformationManager().GetByid(productid);
            var query = repository.QueryableReference().Where(x => x.ProductInformation.IsWinActive == true&&x.ProductInformation.PricingType==model.PricingType);
            query = query.Where(x => x.ProductInformation.Category == categoryid && x.ProductId != productid);
            string[] keys = model.Keywords!=null?model.Keywords.Split(','):"".Split(',');


            
            foreach (string key in keys)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    query.Union(query.Where(x => x.ProductInformation.Keywords.Contains(key)));
                }
            }


            query = query.OrderByDescending(x => x.ProductInformation.ProductId).Take(4);
            return repository.QueryablResult(query.OrderBy(x => x.ProductInformation.ProductTitle));
        }

        /* public ProductDetailViewModel GetMainAuction()
         {
             return repository.Get(x => x.ProductInformation.PricingType == "Auctionable" && x.ProductInformation.ExpiryDate >= DateTime.Now).OrderBy(x => x.ProductInformation.ExpiryDate).FirstOrDefault();

         }*/

        public int ManufacturerProductsCount(int id)
        {
            return repository.QueryableReference().Where(x => x.ProductInformation.ProductBrands == id && x.ProductInformation.isActive == true).Count();

        }

        public double GetMinPrice()
        {
            var query = repository.QueryableReference().Where(x => x.ProductInformation.IsWinActive == true);
            return query.Min(x => x.ProductInformation.UnitPrice);
        }


        public double GetMaxPrice()
        {
            var query = repository.QueryableReference().Where(x => x.ProductInformation.IsWinActive == true);
            return query.Max(x => x.ProductInformation.UnitPrice);
        }

        public List<ProductHomeViewModel> GetProductajaxhome(int? id, string q, bool? AuctionCheck, bool? DirectCheck, string allVal, string amount, int? sorting, int? viewcount, string condetion, int page = 1,string currencysign="SR",double currencyrate=1)
        {

            List<int> brndides = new List<int>();
            if (allVal != "" && allVal != null)
            {
                string[] checkboxnames = allVal.Split(',');
                foreach (string c in checkboxnames)
                {
                    try
                    {
                        int cbxid = Convert.ToInt32(c.Split('_')[1]);
                        brndides.Add(cbxid);
                    }
                    catch { }
                }
            }


            //List<string> condetions = new List<string>();
            //if (condetion != "" && condetion != null)
            //{
            //    string[] checkboxnames = condetion.Split(',');
            //    foreach (string c in checkboxnames)
            //    {
            //        try
            //        {
            //            string cbxid = c.Split('_')[1];
            //            condetions.Add(cbxid);
            //        }
            //        catch { }
            //    }
            //}
            
            string[] minmax = amount.Split('-');
            double min = 0;
            double max = 0;
            if (minmax.Length != 0)
            {
                try
                {
                    min = Convert.ToDouble(minmax[0].Replace(currencysign, "")) * currencyrate;
                    max = Convert.ToDouble(minmax[1].Replace(currencysign, "")) * currencyrate;
                }
                catch { }
            }

            List<ProductHomeViewModel> itemslist = new List<ProductHomeViewModel>();
            var items = Get(id, q, AuctionCheck, DirectCheck, brndides, min, max, sorting,page, viewcount,condetion,true).ToList();
            {
                foreach (var x in items)
                {
                    ProductHomeViewModel p = new ProductHomeViewModel();
                    p.ProductImage =  x.ProductInformation.ProductImg[0] ;
                    p.CategoryName = new CategoryManager().GetByid(x.ProductInformation.Category).CategoryName;
                    p.ProductId = Convert.ToInt32(x.ProductId);
                    p.ProductTitle = x.ProductInformation.ProductTitle;
                    p.PurchaseTimes = x.ProductInformation.PricingType == "Auctionable" ? "Bids N0 :" : "";
                    p.bidscount = x.ProductInformation.PricingType == "Auctionable" ? x.ProductInformation.BidCount + "" : "";
                    p.Plabel = x.ProductInformation.PricingType == "Auctionable" ? "bids" : "";
                    p.AvailableQuantity = x.ProductInformation.AvailableQuantity;
                    p.PriceLabel = x.ProductInformation.PricingType == "Auctionable" ? "LBA" : "Price";
                    p.TotalPrice = x.ProductInformation.PricingType == "Auctionable" ? (x.ProductInformation.OBSBidDetails.Count()>0? (x.ProductInformation.OBSBidDetails.OrderByDescending(a=>a.AddedOn).FirstOrDefault().MainCurrencyValue!=null? x.ProductInformation.OBSBidDetails.OrderByDescending(a => a.AddedOn).FirstOrDefault().MainCurrencyValue : x.ProductInformation.OBSBidDetails.OrderByDescending(a => a.AddedOn).FirstOrDefault().BidPrice): x.ProductInformation.Price):x.ProductInformation.UnitPrice;
                    p.ProductLocation = x.ProductInformation.ProductLocation != null && x.ProductInformation.ProductLocation != "" ? x.ProductInformation.ProductLocation : "";
                    p.Purchasebtn = x.ProductInformation.PricingType == "Auctionable" ? "Place a bid" : "Add to Cart";
                    p.Lat = x.lat;
                    p.Lon = x.lon;
                    p.PricingType = x.ProductInformation.PricingType;
                    p.BidStartDate = x.ProductInformation.BidStartDate;
                    p.DeadLine = x.ProductInformation.DeadLine;
                    p.IsBidSold = x.ProductInformation.IsSold;
                    p.SoldBidPrice = x.ProductInformation.OBSBidDetails.Where(s => s.IsSold).Count()>0?( x.ProductInformation.OBSBidDetails.Where(s => s.IsSold).FirstOrDefault().MainCurrencyValue!=null? x.ProductInformation.OBSBidDetails.Where(s => s.IsSold).FirstOrDefault().MainCurrencyValue: x.ProductInformation.OBSBidDetails.Where(s => s.IsSold).FirstOrDefault().BidPrice) : 0;
                  p.ShowBidAmmount=(x.ProductInformation.PricingType == "Auctionable"?x.ProductInformation.ShowBidAmmount:true);
                    itemslist.Add(p);
                }

                return itemslist;
            }




        }
        public List<GetHOMEPRODUCTS_ResultViewModel> GetProductajaxhomeproc(int? id, string q, bool? AuctionCheck, bool? DirectCheck, string allVal, string amount, int? sorting, int? viewcount, string condetion, int page = 1, string currencysign = "SR", double currencyrate = 1)
        {

            string brndides = "";
            if (allVal != "" && allVal != null)
            {
                string[] checkboxnames = allVal.Split(',');
                foreach (string c in checkboxnames)
                {
                    try
                    {
                        int cbxid = Convert.ToInt32(c.Split('_')[1]);
                        brndides += cbxid + ",";
                    }
                    catch { }
                }
            }
            if (string.IsNullOrEmpty(amount))
                amount = "";
            string[] minmax = amount.Split('-');
            double? min = null;
            double ?max = null;
            if (minmax.Length != 0)
            {
                try
                {
                    min = Convert.ToDouble(minmax[0].Replace(currencysign, "")) * currencyrate;
                    max = Convert.ToDouble(minmax[1].Replace(currencysign, "")) * currencyrate;
                }
                catch { }
            }

            //List<ProductHomeViewModel> itemslist = new List<ProductHomeViewModel>();
            bool active = true;
            int? productno = null;
            var items = StordProcedurManager.GetHOMEPRODUCTS(id, q, AuctionCheck, DirectCheck, brndides, min, max, sorting, page, viewcount, condetion, active, productno).ToList();
            //{
            //    foreach (var x in items)
            //    {
            //        ProductHomeViewModel p = new ProductHomeViewModel();
            //        p.ProductImage = x.ProductInformation.ProductImg[0];
            //        p.CategoryName = new CategoryManager().GetByid(x.ProductInformation.Category).CategoryName;
            //        p.ProductId = Convert.ToInt32(x.ProductId);
            //        p.ProductTitle = x.ProductInformation.ProductTitle;
            //        p.PurchaseTimes = x.ProductInformation.PricingType == "Auctionable" ? "Bids N0 :" : "";
            //        p.bidscount = x.ProductInformation.PricingType == "Auctionable" ? x.ProductInformation.BidCount + "" : "";
            //        p.Plabel = x.ProductInformation.PricingType == "Auctionable" ? "bids" : "";
            //        p.AvailableQuantity = x.ProductInformation.AvailableQuantity;
            //        p.PriceLabel = x.ProductInformation.PricingType == "Auctionable" ? "LBA" : "Price";
            //        p.TotalPrice = x.ProductInformation.PricingType == "Auctionable" ? (x.ProductInformation.OBSBidDetails.Count() > 0 ? (x.ProductInformation.OBSBidDetails.OrderByDescending(a => a.AddedOn).FirstOrDefault().MainCurrencyValue != null ? x.ProductInformation.OBSBidDetails.OrderByDescending(a => a.AddedOn).FirstOrDefault().MainCurrencyValue : x.ProductInformation.OBSBidDetails.OrderByDescending(a => a.AddedOn).FirstOrDefault().BidPrice) : x.ProductInformation.Price) : x.ProductInformation.UnitPrice;
            //        p.ProductLocation = x.ProductInformation.ProductLocation != null && x.ProductInformation.ProductLocation != "" ? x.ProductInformation.ProductLocation : "";
            //        p.Purchasebtn = x.ProductInformation.PricingType == "Auctionable" ? "Place a bid" : "Add to Cart";
            //        p.Lat = x.lat;
            //        p.Lon = x.lon;
            //        p.PricingType = x.ProductInformation.PricingType;
            //        p.BidStartDate = x.ProductInformation.BidStartDate;
            //        p.DeadLine = x.ProductInformation.DeadLine;
            //        p.IsBidSold = x.ProductInformation.IsSold;
            //        p.SoldBidPrice = x.ProductInformation.OBSBidDetails.Where(s => s.IsSold).Count() > 0 ? (x.ProductInformation.OBSBidDetails.Where(s => s.IsSold).FirstOrDefault().MainCurrencyValue != null ? x.ProductInformation.OBSBidDetails.Where(s => s.IsSold).FirstOrDefault().MainCurrencyValue : x.ProductInformation.OBSBidDetails.Where(s => s.IsSold).FirstOrDefault().BidPrice) : 0;
            //        p.ShowBidAmmount = (x.ProductInformation.PricingType == "Auctionable" ? x.ProductInformation.ShowBidAmmount : true);
            //        itemslist.Add(p);
            //    }

            return items;
            // }




        }
        
        public int GetProductajaxhomeprocCount(int? id, string q, bool? AuctionCheck, bool? DirectCheck, string allVal, string amount, int? sorting, int? viewcount, string condetion, int page = 1, string currencysign = "SR", double currencyrate = 1)
        {

            string brndides = "";
            if (allVal != "" && allVal != null)
            {
                string[] checkboxnames = allVal.Split(',');
                foreach (string c in checkboxnames)
                {
                    try
                    {
                        int cbxid = Convert.ToInt32(c.Split('_')[1]);
                        brndides += cbxid + ",";
                    }
                    catch { }
                }
            }
            if (string.IsNullOrEmpty(amount))
                amount = "";
            string[] minmax = amount.Split('-');
            double? min = null;
            double? max = null;
            if (minmax.Length != 0)
            {
                try
                {
                    min = Convert.ToDouble(minmax[0].Replace(currencysign, "")) * currencyrate;
                    max = Convert.ToDouble(minmax[1].Replace(currencysign, "")) * currencyrate;
                }
                catch { }
            }

            //List<ProductHomeViewModel> itemslist = new List<ProductHomeViewModel>();
            bool active = true;
            int? productno = null;
            
            var items = StordProcedurManager.GetHOMEPRODUCTSCount(id, q, AuctionCheck, DirectCheck, brndides, min, max, sorting, page, viewcount, condetion, active, productno);
            

            return items;
            // }




        }
        public int GetProductajaxhomecount(int? id, string q, bool? AuctionCheck, bool? DirectCheck, string allVal, string amount, int? sorting, int? viewcount, string condetion,int page=1,string currencysign="SR")
        {

            List<int> brndides = new List<int>();
            if (allVal != "" && allVal != null)
            {
                string[] checkboxnames = allVal.Split(',');
                foreach (string c in checkboxnames)
                {
                    try
                    {
                        int cbxid = Convert.ToInt32(c.Split('_')[1]);
                        brndides.Add(cbxid);
                    }
                    catch { }
                }
            }


            List<string> condetions = new List<string>();
            if (condetion != "" && condetion != null)
            {
                string[] checkboxnames = condetion.Split(',');
                foreach (string c in checkboxnames)
                {
                    try
                    {
                        string cbxid = c.Split('_')[1];
                        condetions.Add(cbxid);
                    }
                    catch { }
                }
            }
            string[] minmax = amount.Split('-');
            double min = 0;
            double max = 0;
            if (minmax.Length != 0)
            {
                try
                {
                    min = Convert.ToDouble(minmax[0].Replace(currencysign, ""));
                    max = Convert.ToDouble(minmax[1].Replace(currencysign, ""));
                }
                catch { }
            }


            int count = GetCount(id, q, AuctionCheck, DirectCheck, brndides, min, max, sorting, page, viewcount, condetions,true);


            return count;


        }

        public IEnumerable<object> GetProductResultmap(int? id, string q, bool? AuctionCheck, bool? DirectCheck, string allVal, string amount, int? sorting, int? viewcount, string condetion)
        {

            List<int> brndides = new List<int>();
            if (allVal != "" && allVal != null)
            {
                string[] checkboxnames = allVal.Split(',');
                foreach (string c in checkboxnames)
                {
                    try
                    {
                        int cbxid = Convert.ToInt32(c.Split('_')[1]);
                        brndides.Add(cbxid);
                    }
                    catch { }
                }
            }


            List<string> condetions = new List<string>();
            if (condetion != "" && condetion != null)
            {
                string[] checkboxnames = condetion.Split(',');
                foreach (string c in checkboxnames)
                {
                    try
                    {
                        string cbxid = c.Split('_')[1];
                        condetions.Add(cbxid);
                    }
                    catch { }
                }
            }
            string[] minmax = amount.Split('-');
            double min = 0;
            double max = 0;
            if (minmax.Length != 0)
            {
                try
                {
                    min = Convert.ToDouble(minmax[0].Replace("SR", ""));
                    max = Convert.ToDouble(minmax[1].Replace("SR", ""));
                }
                catch { }
            }

           
            var items = Getqueryablemap(id, q, AuctionCheck, DirectCheck, brndides, min, max, sorting, null, viewcount, condetions, true)
                .Select(x=>new {
                    //x.ProductId,
                    //x.ProductInformation.ProductTitle,
                    x.lat,
                    lng = x.lon})
                .OrderByDescending(x=>x.lat)
                .Skip(0)
                .Take(1000)
                .AsEnumerable();



            return items;

        }

        public DataTable ExportProductsByBatch(string sellerName, string companyname, int batchno)
        {
            var query = repository.QueryableReference().Where(x => x.ProductInformation.SellerName.Equals(sellerName) && x.ProductInformation.BatchNumber == batchno + "");
       
                var queryitems = query.Select(x => new
                {
                    x.ProductInformation.ProductTitle,
                    x.Description,
                    x.ProductInformation.OBSProductCategory.CategoryName,
                    x.ProductInformation.OtherCategory,
                    x.ProductInformation.OBSDiscipline.Discipline,
                    x.ProductInformation.OBSCommodity.Commodity,
                    x.ProductInformation.OBSSubCommodity.SubCommodity,
                    x.ProductInformation.OBSManufacturer.ManufacturersName,
                    x.ProductInformation.OtherManufacturer,
                    x.ProductInformation.ManufacturingDate,
                    x.ProductInformation.OBSUOM.UOM,
                    x.ProductInformation.DeadLineInDays,
                    x.Condition,
                    ShortDescription = "",
                    x.Contact,
                    x.Company,
                    x.Phone,
                    x.Fax,
                    x.Email,
                    x.Address,
                    x.lat,
                    x.lon,
                    x.Country,
                    x.State,
                    x.City,
                    x.ProductInformation.PricingType,
                    x.WholesaleValue,
                    x.RetailValue,
                    MinimumBidAmount = "0",
                    x.ProductInformation.BidDifference,
                    x.ProductInformation.Quantity,
                    x.ProductInformation.UnitPrice,
                    x.ProductInformation.TotalPrice,
                    x.ProductInformation.MarketPrice,
                    x.ProductInformation.CertificatesOrDataSheet,
                    x.ProductInformation.WINAssement,
                    x.ProductInformation.WINAssementby,
                    x.ProductInformation.Repairable,
                    x.ProductInformation.UsedEquipmentOrApplication,
                    x.ProductInformation.Age,
                    x.ProductInformation.ExpiryDate,
                    x.ProductInformation.EndUser,
                    x.ProductInformation.SellerName,
                    x.ProductInformation.SupplierName,
                    x.ProductInformation.SellerMaterialId,
                    x.ProductInformation.ProductImage,
                    x.ProductInformation.ProductDocument,
                    PermissionUsername = "",
                    x.ProductId,
                    x.ProductInformation.Keywords
                    
                });


                DataTable dt = ps.haweya.win.obs.common.Helper.ToDataTable(queryitems.ToList());

                return dt;
            
        }


        public IEnumerable<object> GetWithGroupingProductsByCountry()
        {

           var query=  repository.QueryableReference()
                             .GroupBy(x => x.Country).OrderByDescending(g => g.Count())
                                 .Select(g => new {Country=g.Key ,ProductCount= g.Count()}).AsEnumerable();

            var allobjects = from q in query
                             join c in CacheManager.Countries
                             on q.Country equals c.id
                             select (new {
                                 country = c.name,
                                 //CountryID=c.id,
                                 ProductCount = q.ProductCount

                             });
            return allobjects.AsEnumerable();

        }
        //public IEnumerable<object> GetCondetionsGrouped()
        //{
        //    var query = repository.QueryableReference()
        //                      .GroupBy(x => x.Condition).OrderByDescending(g => g.Count())
        //                      .Select(g => new { Condition = g.Key, ProductCount = g.Count() }).AsEnumerable();
        //    return query;
        //}
        
        public IEnumerable<object> GetWithGroupingProductsByCountryToSeller(string sellername)
        {

            var query = repository.QueryableReference().Where(x=> x.ProductInformation.IsDelete!=true&&x.ProductInformation.Username.ToLower().Equals(sellername.ToLower()))
                              .GroupBy(x => x.Country).OrderByDescending(g => g.Count())
                                  .Select(g => new { Country = g.Key, ProductCount = g.Count() }).AsEnumerable();

            var allobjects = from q in query
                             join c in CacheManager.Countries
                             on q.Country equals c.id
                             select (new
                             {
                                 country = c.name,
                                 //CountryID=c.id,
                                 ProductCount = q.ProductCount

                             });
            return allobjects.AsEnumerable();

        }
        public string GenerateSlug(string phrase)
        {
            string str = phrase.ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-zأ-ي0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }
    }
}
