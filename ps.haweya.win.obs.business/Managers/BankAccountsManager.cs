﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;

namespace ps.haweya.win.obs.business.Managers
{
    public class BankAccountsManager : BaseManager<BankAccount, BankAccountViewModel>
    {
        public IEnumerable<BankAccountViewModel> Get()
        {
            return repository.Get().Where(x=>x.IsActive==true);
        }
        public IEnumerable<BankAccountViewModel> GetActive()
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.IsActive == true));
        }
        public List<BankAccountViewModel> GetActiveList()
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.IsActive == true)).ToList();
        }
        public BankAccountViewModel GetByid(long id)
        {
            return repository.Get(id);
        }
        public IEnumerable<BankAccountViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Name.ToLower().Contains(q) || x.No.ToLower().Contains(q) || x.IBAN.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public long Add(BankAccountViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.Id;
        }
        public long Update(BankAccountViewModel entity)
        {
            entity = repository.Update(entity);
            return entity.Id;
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
        public int AddOrUpdate(int Id, string filename, string Name, string No, string IBAN,
            string CompanyName,
            string Beneficiary_Account_Name,
            string C_Company,
            string VAT_Number,
            string Telephone_number,
            string Vendor_Location,
            string Currency,
            string SWIFT_CODE,
            string Routing_number,
            string Bank_Address_Country,

            bool IsActive)
        {
            BankAccountViewModel entity = new BankAccountViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.Logo = filename != "" ? filename : entity.Logo;
                entity.Name = Name;
                entity.No = No;
                entity.IBAN = IBAN;
                entity.CompanyName = CompanyName;
                entity.Beneficiary_Account_Name = Beneficiary_Account_Name;
                entity.C_Company = C_Company;
                entity.VAT_Number = VAT_Number;
                entity.Telephone_number = Telephone_number;
                entity.Vendor_Location = Vendor_Location;
                entity.Currency = Currency;
                entity.SWIFT_CODE = SWIFT_CODE;
                entity.Routing_number = Routing_number;
                entity.Bank_Address_Country = Bank_Address_Country;
                entity.IsActive = IsActive;
                repository.Update(entity, "Id");
                return entity.Id;
            }
            entity = new BankAccountViewModel()
            {
                Logo = filename,
                Name = Name,
                No = No,
                IBAN = IBAN,
                IsActive = IsActive
            };
            entity = repository.Add(entity);
            return entity.Id;
        }
        public int UpdateIsActive(int id)
        {
            BankAccountViewModel entity = new BankAccountViewModel();
            if (id != 0)
            {
                entity = GetByid(id);
                if (entity != null)
                {
                    entity.IsActive = entity.IsActive != null ? (!entity.IsActive) : true;
                    repository.Update(entity, "ID");
                }
                return entity.Id;
            }

            return entity.Id;
        }

        public string BankDatatoEmail()
        {
            string banktable = "";
            var banks = new BankAccountsManager().GetActive();
            if(banks.Count()>0)
                {
                banktable = "<table style='margin-top:20px;'>";

                foreach (var itm in banks)
                {
                    banktable += "<tr><td colspan='2' style='border: 1px solid; padding:0 3px;'>" + itm.Name + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>Company Name</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.CompanyName + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>Beneficiary Account Name</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.Beneficiary_Account_Name + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>C/R# for Company</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.C_Company + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>VAT Number</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.VAT_Number + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>Telephone number</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.Telephone_number + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>Bank Name</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.Name + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>Vendor’s Location</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.Vendor_Location + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>Account Number</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.No + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>IBAN Number</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.IBAN + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>Currency</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.Currency + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>SWIFT CODE</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.SWIFT_CODE + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>Routing number (Sort Code)</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.Routing_number + "</td></tr>";
                    banktable += "<tr><td  style='border: 1px solid; padding:0 3px;'>Bank Address and Country</td><td  style='border: 1px solid; padding:0 3px;'>" + itm.Bank_Address_Country + "</td></tr>";
                    banktable += "<tr><td colspan='2' style='border: 1px solid; padding:0 3px;'></td></tr>";
                }
                banktable += "<table>";
            }

            return banktable;
        }
    }
}
