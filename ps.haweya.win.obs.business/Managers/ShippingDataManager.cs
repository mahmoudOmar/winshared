﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class ShippingDataManager : BaseManager<ShippingData, ShippingDataViewModel>
    {
        public IEnumerable<ShippingDataViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<ShippingDataViewModel> GetbyBuyerID(string userid)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.UserID == userid));
        }
        public IEnumerable<ShippingDataViewModel> GetbyBuyerIDPrimary(string userid)
        {
            var query = repository.QueryableReference().Where(x => x.UserID == userid && x.IsPrimary == true).OrderByDescending(x=>x.ID);
            if (query.Count() == 0)
                query = repository.QueryableReference().Where(x => x.UserID == userid).OrderByDescending(x => x.ID);
            return repository.QueryablResult(query).Take(1);
        }
        public ShippingDataViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public int UpdateShipData(ShippingDataViewModel entity)
        {
            entity = repository.Update(entity,"ID");
            return entity.ID;
        }
        public int AddShipingData(ShippingDataViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.ID;
        }


    }
}
