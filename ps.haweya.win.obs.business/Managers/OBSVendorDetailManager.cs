﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
   public class OBSVendorDetailManager : BaseManager<OBSVendorDetail, OBSVendorDetailViewModel>
    {

        public IEnumerable<OBSVendorDetailViewModel> Get()
        {
            return repository.Get();
        }

        public OBSVendorDetailViewModel GetByid(int id)
        {
            return repository.Get(id);
        }

        public List<string> GetCodes()
        {
            return repository.QueryableReference().Select(x=>x.VendorCode).ToList();
        }
        public OBSVendorDetailViewModel GetBysellerid(string id)
        {
            return repository.Get(x => x.UserId== id).FirstOrDefault();
        }
        public OBSVendorDetailViewModel Add(OBSVendorDetailViewModel model)
        {
            return repository.Add(model);
        }
        public OBSVendorDetailViewModel Update(OBSVendorDetailViewModel model)
        {
            return repository.Update(model,"VendorId");
        }
    }
}
