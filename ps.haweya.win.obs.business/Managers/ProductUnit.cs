﻿using ps.haweya.win.obs.viewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class ProductUnit
    {
        private ProductInformationManager piManager;

        public ProductUnit()
        {
            piManager = new ProductInformationManager();
        }

        public object GetBidByStatus(string Seller = null)
        {
            var OBSBidDetailsManager = new OBSBidDetailsManager();

            return OBSBidDetailsManager.repository.QueryableReference().Where(t=> Seller == null || t.AspNetUser.UserName.Equals(Seller))
                                              .GroupBy(t => t.IsSold)
                                              .Select(g => new { Key = g.Key, Value = g.Count(), Id = g.Key }).AsEnumerable().ToList();
        }


        public object GetCartItemsByStatus(string Seller=null)
        {
            var cartManager = new CartItemsManager();

            return cartManager.repository.QueryableReference().Where(t => Seller == null || t.BuyerOrder.AspNetUser.UserName.Equals(Seller))
                                              .GroupBy(t=>t.IsBought)
                                              .Select(g => new { Key = g.Key, Value = g.Count(), Id = g.Key }).AsEnumerable().ToList();
        }

        public object GetByActiveStatus(string Seller = null)
        {
            var ProductInformationManager = new ProductInformationManager();

            var result= ProductInformationManager.repository.QueryableReference().Where(x => x.IsDelete != true).Where(t => Seller == null || t.Username.Equals(Seller))
                                              .GroupBy(t => t.IsWinActive)
                                              .Select(g => new { Key = g.Key, Value = g.Count(), Id = g.Key }).AsEnumerable().ToList();
            return result;
        }
        public object GetBidPRoductsActiveStatus(string Seller = null)
        {
            var ProductInformationManager = new ProductInformationManager();

            return ProductInformationManager.repository.QueryableReference().Where(x => x.IsDelete != true).Where(t => t.PricingType == "Auctionable" &&(Seller == null || t.Username.Equals(Seller)))
                                              .GroupBy(t => t.IsWinActive)
                                              .Select(g => new { Key = g.Key, Value = g.Count(), Id = g.Key }).AsEnumerable().ToList();
        }

        public int GetPurchaseItemsCount(string Seller, string q)
        {
            var cartManager = new CartItemsManager();

            var query = cartManager.repository.QueryableReference().Where(t => Seller == null || t.ProductInformation.Username.Equals(Seller));

            if (!string.IsNullOrEmpty(q))
            {
                query = query.Where(t => t.ProductInformation.ProductTitle.Contains(q) ||
                                       t.BuyerOrder.AspNetUser.FullName.Contains(q) ||
                                       t.ProductInformation.Quantity.ToString().Contains(q) ||
                                       t.BuyerOrder.AspNetUser.Email.Contains(q) ||
                                       t.BuyerOrder.AspNetUser.PhoneNumber.Contains(q));
            }

            return query.Count();
        }

        public int GetByActiveStatusCount(string Seller, string q)
        {
            var ProductInformationManager = new ProductInformationManager();

            var query = ProductInformationManager.repository.QueryableReference().Where(t =>  t.IsDelete != true && ( Seller == null || t.Username.Equals(Seller)));

            if (!string.IsNullOrEmpty(q))
            {
                query = query.Where(x => x.IsDelete != true).Where(t => t.ProductTitle.Contains(q) ||
                                       t.SellerName.Contains(q) ||
                                       t.Quantity.ToString().Contains(q) ||
                                       t.Username.Contains(q) ||
                                         t.IsWinActive.ToString().Contains(q) ||
                                       t.PricingType == q
                                       || 
                                       (t.PricingType == "Non-Auctionable" && t.CartItems.Count(x => x.IsBought) != 0 && q == "purchased") ||

                                       (t.PricingType == "Non-Auctionable" && t.CartItems.Count() == 0 && q == "un-purchased")||
                                        (t.PricingType == "Auctionable" && t.OBSBidDetails.Count(x => x.IsSold) != 0 && q == "sold") ||

                                       (t.PricingType == "Auctionable" && t.OBSBidDetails.Count(x => x.IsSold) == 0 && q == "unsold")
                                       );
            }

            return query.Count();
        }
        public int GetBidsProductCount(string Seller, string q)
        {
            var ProductInformationManager = new ProductInformationManager();

            var query = ProductInformationManager.repository.QueryableReference().Where(x => x.IsDelete != true).Where(t => t.PricingType == "Auctionable" && (Seller == null || t.Username.Equals(Seller)));

            if (!string.IsNullOrEmpty(q))
            {
                query = query.Where(t => t.ProductTitle.Contains(q) ||
                                       t.SellerName.Contains(q) ||
                                       t.Quantity.ToString().Contains(q) ||
                                       t.Username.Contains(q) ||
                                       (t.OBSBidDetails.Count(x => x.IsSold) != 0 && q == "sold") ||

                                       (t.OBSBidDetails.Count(x => x.IsSold) == 0 && q == "unsold")
                                       );

            }

            return query.Count();
        }

        public IEnumerable<CartItemViewModel> GetPurchaseItems(string Seller, string q, string SortBy, string SortMethod, int page, int pages, int perPage)
        {
            var cartManager = new CartItemsManager();

            var query = cartManager.repository.QueryableReference(IncludColumn: "ProductInformation|BuyerOrder", SortBy + " " + SortMethod).Where(t => Seller == null || t.ProductInformation.Username.Equals(Seller));

            if (!string.IsNullOrEmpty(q))
            {
                query = query.Where(t => t.ProductInformation.ProductTitle.Contains(q) ||
                                       t.BuyerOrder.AspNetUser.FullName.Contains(q) ||
                                       t.ProductInformation.Quantity.ToString().Contains(q) ||
                                       t.BuyerOrder.AspNetUser.Email.Contains(q) ||
                                       t.BuyerOrder.AspNetUser.PhoneNumber.Contains(q) ||
                                       t.IsBought == q.Equals("true") || 
                                       t.IsBought == q.Equals("false"));
            }

            query = query.Skip((page - 1) * perPage).Take(perPage);

            return cartManager.repository.QueryablResult(query);
        }


        public int GetBidItemsCount(string Seller, string q)
        {
            var bidManager = new OBSBidDetailsManager();

            var query = bidManager.repository.QueryableReference().Where(t => Seller == null || t.ProductInformation.Username.Equals(Seller));

            if (!string.IsNullOrEmpty(q))
            {
                query = query.Where(t => t.ProductInformation.ProductTitle.Contains(q) ||
                                       t.AspNetUser.FullName.Contains(q) ||
                                       t.ProductInformation.Quantity.ToString().Contains(q) ||
                                       t.AspNetUser.Email.Contains(q) ||
                                       t.AspNetUser.PhoneNumber.Contains(q) || t.IsSold == q.Equals("true") || 
                                       t.IsSold == q.Equals("false"));
            }

            return query.Count();
        }

        public IEnumerable<OBSBidDetailViewModel> GetBidItems(string Seller, string q, string SortBy, string SortMethod, int page, int pages, int perPage)
        {
            var bidManager = new OBSBidDetailsManager();

            var query = bidManager.repository.QueryableReference(IncludColumn: "ProductInformation", SortBy + " " + SortMethod).Where(t => Seller == null || t.ProductInformation.Username.Equals(Seller));

            if (!string.IsNullOrEmpty(q))
            {
                query = query.Where(t => t.ProductInformation.ProductTitle.Contains(q) ||
                                       t.AspNetUser.FullName.Contains(q) ||
                                       t.ProductInformation.Quantity.ToString().Contains(q) ||
                                       t.AspNetUser.Email.Contains(q) ||
                                       t.AspNetUser.PhoneNumber.Contains(q) || t.IsSold == q.Equals("true") ||
                                       t.IsSold == q.Equals("false") );
            }

            query = query.Skip((page - 1) * perPage).Take(perPage);

            return bidManager.repository.QueryablResult(query);
        }


        public IEnumerable<ProductInformationViewModel> GetByActiveStatus(string Seller, string q, string SortBy, string SortMethod, int page, int pages, int perPage)
        {
            var ProductInformationManager = new ProductInformationManager();

            var query = ProductInformationManager.repository.QueryableReference( OrderBy: SortBy + " " + SortMethod).Where(t =>t.IsDelete!=true&&(Seller == null || t.Username.Equals(Seller)));

            if (!string.IsNullOrEmpty(q))
            {

                int qint = 0;
                
                query = query.Where(x=>x.IsDelete!=true).Where(t => t.ProductTitle.Contains(q) ||
                                       t.SellerName.Contains(q) ||
                                       t.Quantity.ToString().Contains(q) ||
                                       t.Username.Contains(q) ||
                                       t.PricingType == q ||
                                       t.IsWinActive.ToString().Contains(q)||
                                       ("WIN-"+t.ProductId).Contains(q.Trim())||

                                       (t.PricingType == "Non-Auctionable" && t.CartItems.Count(x=>x.IsBought) != 0 && q == "purchased") ||
                                       
                                       (t.PricingType == "Non-Auctionable" && t.CartItems.Count() == 0 && q == "un-purchased") ||

                                       (t.PricingType == "Auctionable" && t.OBSBidDetails.Count(x => x.IsSold) != 0 && q == "sold") ||

                                       (t.PricingType == "Auctionable" && t.OBSBidDetails.Count(x => x.IsSold) == 0 && q == "unsold")
                                        
                                       );

            }


            query = query.Skip((page - 1) * perPage).Take(perPage);

            return ProductInformationManager.repository.QueryablResult(query);
        }
        public IEnumerable<ProductInformationViewModel> GetByActiveStatusExport_Admin(string Seller)
        {
            var ProductInformationManager = new ProductInformationManager();

            var query = ProductInformationManager.repository.QueryableReference().Where(t => t.IsDelete != true && (Seller == null || t.Username.Equals(Seller)));


            return ProductInformationManager.repository.QueryablResult(query);
        }
        public IEnumerable<ProductInformationViewModel> GetDirectByActiveStatus(string Seller, string q, string SortBy, string SortMethod, int page, int pages, int perPage)
        {
            var ProductInformationManager = new ProductInformationManager();

            var query = ProductInformationManager.repository.QueryableReference(OrderBy: SortBy + " " + SortMethod).Where(t => t.IsDelete != true && t.PricingType == "Non-Auctionable" && (Seller == null || t.Username.Equals(Seller)));

            if (!string.IsNullOrEmpty(q))
            {
                query = query.Where(t => t.ProductTitle.Contains(q) ||
                                       t.SellerName.Contains(q) ||
                                       t.Quantity.ToString().Contains(q) ||
                                       t.Username.Contains(q) ||
                                       t.PricingType == q ||

                                       (t.PricingType == "Non-Auctionable" && t.CartItems.Count(x => x.IsBought) != 0 && q == "purchased") ||

                                       (t.PricingType == "Non-Auctionable" && t.CartItems.Count() == 0 && q == "un-purchased")||


                                       (t.PricingType == "Non-Auctionable" && t.IsWinActive==true && q == "true") ||

                                       (t.PricingType == "Non-Auctionable" && t.IsWinActive==false&& q == "false")

                                       //(t.PricingType == "Auctionable" && t.OBSBidDetails.Count(x => x.IsSold) != 0 && q == "sold") ||

                                       //(t.PricingType == "Auctionable" && t.OBSBidDetails.Count(x => x.IsSold) == 0 && q == "unsold")
                                       );

            }


            query = query.Skip((page - 1) * perPage).Take(perPage);

            return ProductInformationManager.repository.QueryablResult(query);
        }
        public int GetDirectByActiveStatusCount(string Seller, string q)
        {
            var ProductInformationManager = new ProductInformationManager();

            var query = ProductInformationManager.repository.QueryableReference().Where(t => t.PricingType == "Non-Auctionable" && t.IsDelete != true && (Seller == null || t.Username.Equals(Seller)));

            if (!string.IsNullOrEmpty(q))
            {
                query = query.Where(t => t.ProductTitle.Contains(q) ||
                                       t.SellerName.Contains(q) ||
                                       t.Quantity.ToString().Contains(q) ||
                                       t.Username.Contains(q) ||
                                       t.PricingType == q
                                       ||
                                       (t.PricingType == "Non-Auctionable" && t.CartItems.Count(x => x.IsBought) != 0 && q == "purchased") ||

                                       (t.PricingType == "Non-Auctionable" && t.CartItems.Count() == 0 && q == "un-purchased")||


                                       (t.PricingType == "Non-Auctionable" && t.IsWinActive == true && q == "true") ||

                                       (t.PricingType == "Non-Auctionable" && t.IsWinActive == false && q == "false")

                                       );
            }

            return query.Count();
        }
        public IEnumerable<ProductInformationViewModel> GetBidsProduct(string Seller, string q, string SortBy, string SortMethod, int page, int pages, int perPage)
        {
            var ProductInformationManager = new ProductInformationManager();

            var query = ProductInformationManager.repository.QueryableReference(OrderBy: SortBy + " " + SortMethod).Where(t => t.PricingType == "Auctionable" &&(Seller == null || t.Username.Equals(Seller)));

            if (!string.IsNullOrEmpty(q))
            {
                query = query.Where(t => t.ProductTitle.Contains(q) ||
                                       t.SellerName.Contains(q) ||
                                       t.Quantity.ToString().Contains(q) ||
                                       t.Username.Contains(q) ||
                                       (t.IsWinActive==true&&q=="true")||
                                       (t.IsWinActive == false && q == "false")||
                                       (t.OBSBidDetails.Where(x=>x.IsSold==true).Count() > 0 && q.ToLower().Trim() == "sold") ||

                                       ((t.OBSBidDetails.Where(x => x.IsSold == true).Count() == 0|| t.OBSBidDetails.Count()==0) && q.ToLower().Trim() == "unsold")
                                       );

                if (q.ToLower().Trim().Equals("sold"))
                {
                    query = query.Where(t => t.OBSBidDetails.Count()>0&& t.OBSBidDetails.Where(x => x.IsSold == true).Count() > 0);
                }
                if (q.ToLower().Trim().Equals("unsold"))
                {
                    query = query.Where(t => t.OBSBidDetails.Where(x => x.IsSold == true).Count() == 0|| t.OBSBidDetails.Count() == 0);
                }

            }


            query = query.Skip((page - 1) * perPage).Take(perPage);

            return ProductInformationManager.repository.QueryablResult(query);
        }

        public ProductInformationViewModel AddProduct(ProductInformationViewModel entity)
        {
            var images = (entity.Images ?? "").Split(',');
            var documents = (entity.Documents ?? "").Split(',');
            
            return piManager.Add(entity);
        }


        public ProductInformationViewModel UpdateProduct(ProductInformationViewModel entity)
        {
            var images = (entity.Images ?? "").Split(',');
            var documents = (entity.Documents ?? "").Split(',');
            return piManager.Update(entity);
        }
        public ProductInformationViewModel UpdateFull(ProductInformationViewModel entity)
        {
            var images = (entity.Images ?? "").Split(',');
            var documents = (entity.Documents ?? "").Split(',');
            return piManager.UpdateFull(entity);
        }
    }
}
