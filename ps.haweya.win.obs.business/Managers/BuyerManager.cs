﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{

    public class BuyerManager : BaseManager<Buyer, BuyerViewModel>
    {

        public IEnumerable<BuyerViewModel> Get()
        {
            return repository.Get();
        }

        public BuyerViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public BuyerViewModel GetByAspId(string aspid)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.ASPNetUserID == aspid)).FirstOrDefault();
        }

        public BuyerViewModel Update(BuyerViewModel  model)
        {
            return repository.Update(model,"ID");
        }
    }
}
