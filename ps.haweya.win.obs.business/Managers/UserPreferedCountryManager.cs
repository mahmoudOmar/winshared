﻿using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class UserPreferedCountryManager : BaseManager<UserPreferedCountry, UserPreferedCountryViewModel>
    {
        public UserPreferedCountryViewModel Add(UserPreferedCountryViewModel model)
        {
            return repository.Add(model);
        }
        public bool DeleteByUserId(string UserId)
        {
            return repository.DeleteRange(x => x.UserId == UserId);
        }
        public bool AddRange(List<int> Ids, string UserId)
        {
            DeleteByUserId(UserId);
            foreach (var id in Ids)
            {
                try
                {
                    UserPreferedCountryViewModel model = new UserPreferedCountryViewModel()
                    {
                        UserId = UserId,
                        CountryId = id,
                        InsertDate = DateTime.Now
                    };
                    Add(model);
                }
                catch (Exception)
                {

                }
            }
            return true;
        }
    }
}
