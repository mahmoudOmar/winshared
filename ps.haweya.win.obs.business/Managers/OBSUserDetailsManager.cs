﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;

namespace ps.haweya.win.obs.business.Managers
{
    class OBSUserDetailsManager : BaseManager<OBSUserDetail, OBSUserDetailViewModel>
    {
        public int GetCreatedToday()
        {
            return repository.QueryableReference().Where(x => x.date_created.Year == DateTime.Now.Year
            && x.date_created.Month == DateTime.Now.Month
            && x.date_created.Day == DateTime.Now.Day).Count();
        }
    }
}
