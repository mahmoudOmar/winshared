﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class AspNetRolesManager: BaseManager<AspNetRole, AspNetRoleViewModel>
    {
        public IEnumerable<AspNetRoleViewModel> Get()
        {
            return repository.Get();
        }
    }
}
