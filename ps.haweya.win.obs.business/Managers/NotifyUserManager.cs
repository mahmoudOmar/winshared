﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{

    public class NotifyUserManager : BaseManager<NotifyUser, NotifyUserViewModel>
    {

        public IEnumerable<NotifyUserViewModel> Get()
        {
            return repository.Get();
        }

        public NotifyUserViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public int AddNotify(NotifyUserViewModel entity)
        {
          entity=  repository.Add(entity);
            return entity.ID;
        }
        public int GetCount(int productid, string userid)
        {
            return repository.QueryableReference().Where(x => x.ProductID == productid && x.AspNetUserID == userid).Count();
             
        }

    }
}
