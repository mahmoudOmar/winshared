﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
   public class OrderActionManager : BaseManager<OrderAction, OrderActionViewModel>
    {

        public IEnumerable<OrderActionViewModel> Get()
        {
            return repository.Get();
        }
        public int Add(int orderId, double InspectionValue, string note,string addedby,int statusid,int deliverytype=0)
        {

            OrderActionViewModel entity = new OrderActionViewModel()
            {
                OrderID = orderId,
                StatusID = statusid,
                ActionType=1,
                Notes = note,
                AddedBy = addedby,
                Price=InspectionValue,
                InsertDate = DateTime.Now,
               DeliveryType=deliverytype
            };
            entity = repository.Add(entity);
            return entity.ID;
        }


    }
    
}
