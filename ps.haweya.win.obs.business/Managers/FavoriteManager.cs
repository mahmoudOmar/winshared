﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
   public class FavoriteManager : BaseManager<FavoriteItem, FavoriteItemViewModel>
    {

        public IEnumerable<FavoriteItemViewModel> Get()
        {
            return repository.Get();
        }

        public FavoriteItemViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public FavoriteItemViewModel GetByProductId(string aspuserid,long productid)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.AspNetUserId == aspuserid&&x.ProductID==productid)).FirstOrDefault();
        }
        public IEnumerable<FavoriteItemViewModel> GetByUserId(string aspuserid)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.AspNetUserId == aspuserid));
        }

        public List<long> GetFavoriteIdsByUserID(string aspuserid)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.AspNetUserId == aspuserid)).Select(x=>x.ProductID).ToList();
        }
        public int AddByUserID(string userid, long productid)
        {
            if (!GetFavoriteIdsByUserID(userid).Contains(productid))
            {
                FavoriteItemViewModel F = new FavoriteItemViewModel();
                F.AspNetUserId = userid;
                F.ProductID = productid;
                F.Insertdate = DateTime.Now;
                return repository.Add(F).ID;
            }

            return 0;
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
     
    }
}
