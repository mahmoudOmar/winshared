﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class OBSPurchaseLiveTrackingManager : BaseManager<OBSPurchaseLiveTracking, OBSPurchaseLiveTrackingViewModel>
    {
        public IEnumerable<OBSPurchaseLiveTrackingViewModel> Get()
        {
            return repository.Get();
        }


        public IEnumerable<object> GetTrackingDetails()
        {
            var trackingDetails = repository.QueryablResult(repository.QueryableReference("OBSPurchaseDetail", ""));
            var details = (from p in trackingDetails
                                   select new
                                   {
                                       PurchaseId = p.PurchaseId,
                                       ProductTitle = p.OBSPurchaseDetail.ProductInformation.ProductTitle,
                                       ProductImage = p.OBSPurchaseDetail.ProductInformation.ProductImg,
                                       Location = p.OBSPurchaseDetail.ProductInformation.ProductLocation,
                                       SellerUsername = p.OBSPurchaseDetail.ProductInformation.Username,
                                       Quantity = p.OBSPurchaseDetail.BuyQuantity,
                                       Price = 0,
                                       BuyerUsername = p.OBSPurchaseDetail.OBSUserDetail.Email,
                                       IsPurchase = p.OBSPurchaseDetail.IsPurchased,
                                       TrackingData = (from tracking in p.OBSPurchaseDetail.OBSPurchaseLiveTrackings
                                                       select new
                                                       {
                                                           TrackingId = tracking.TrackingId,
                                                           PurchaseId = tracking.PurchaseId,
                                                           Inspection = tracking.Inspection,
                                                           OutForDelivery = tracking.OutForDelivery,
                                                           Payment = tracking.Payment,
                                                           ProductConfirmation = tracking.ProductConfirmation,
                                                           Delivered = tracking.Delivered
                                                       })
                                   }).ToList();
            return details;
        }

        public IEnumerable<object> GetTrackingDetailsByUser(string Username)
        {
            var trackingDetails = repository.QueryablResult(repository.QueryableReference("OBSPurchaseDetail", ""));
            var details = (from p in trackingDetails
                           select new
                           {
                               PurchaseId = p.PurchaseId,
                               ProductTitle = p.OBSPurchaseDetail.ProductInformation.ProductTitle,
                               ProductImage = p.OBSPurchaseDetail.ProductInformation.ProductImg,
                               Location = p.OBSPurchaseDetail.ProductInformation.ProductLocation,
                               SellerUsername = p.OBSPurchaseDetail.ProductInformation.Username,
                               Quantity = p.OBSPurchaseDetail.BuyQuantity,
                               Price = 0,
                               BuyerUsername = p.OBSPurchaseDetail.OBSUserDetail.Email,
                               IsPurchase = p.OBSPurchaseDetail.IsPurchased,
                               TrackingData = (from tracking in p.OBSPurchaseDetail.OBSPurchaseLiveTrackings
                                               select new
                                               {
                                                   TrackingId = tracking.TrackingId,
                                                   PurchaseId = tracking.PurchaseId,
                                                   Inspection = tracking.Inspection,
                                                   OutForDelivery = tracking.OutForDelivery,
                                                   Payment = tracking.Payment,
                                                   ProductConfirmation = tracking.ProductConfirmation,
                                                   Delivered = tracking.Delivered
                                               })
                           }).ToList().Where(x=>x.SellerUsername == Username || x.BuyerUsername == Username);
            return details;
        }
    }
}
