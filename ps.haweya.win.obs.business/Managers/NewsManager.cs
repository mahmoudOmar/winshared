﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;

namespace ps.haweya.win.obs.business.Managers
{
    public class NewsManager : BaseManager<News, NewsViewModel>
    {
        public IEnumerable<NewsViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<NewsViewModel> Get(int page=1)
        {
            int pageNo = page;
            int PageSize =12;
            var query = repository.QueryableReference();
            query = query.OrderByDescending(x=>x.Id).Skip((pageNo-1) * PageSize).Take(PageSize);

            var result = repository.QueryablResult(query);

            return result;
        }
        public int GetCount()
        {

            return repository.QueryableReference().Count();
        }
        public IEnumerable<NewsViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Title.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public int Count(string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.Title.ToLower().Contains(q)).Count();
        }
        public NewsViewModel GetByid(int id)
        {
            return repository.Get(id);
        }

        public IEnumerable<NewsViewModel> GetTop(int count)
        {
            var query = repository.QueryableReference();
            query = query.OrderByDescending(x => x.AddOn).Take(count);
            return repository.QueryablResult(query);
        }
        public IEnumerable<NewsLayout> GetTopLayout(int count)
        {
            var query = repository.QueryableReference();
            query = query.OrderByDescending(x => x.AddOn).Take(count);
            return repository.QueryablResult(query).Select(x => new NewsLayout{Id=x.Id,AddOn=x.AddOn,Brief= x.Brief,Title=x.Title });
            
        }
        public IEnumerable<NewsViewModel> GetRelated(int id)
        {
            var query = repository.QueryableReference().Where(x => x.Id != id);
            query = query.OrderByDescending(x => x.AddOn);
            return repository.QueryablResult(query);
        }

        public int Add(NewsViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.Id;
        }

        public int AddOrUpdate(int Id, string Title, string Brief,string Details,string Keywords, bool IsActive, string filename, string userid)
        {
            NewsViewModel entity = new NewsViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.Title = Title;
                entity.Details = Details;
                entity.Brief = Brief;
                entity.Keywords = Keywords;
                entity.IsActive = IsActive;
                entity.ImageFile = filename != "" ? filename : entity.ImageFile;
                entity.LastUpdate = DateTime.Now;
                entity.UpdatedBy = userid;
                repository.Update(entity, "Id");
                return entity.Id;
            }
            entity = new NewsViewModel()
            {
                Title = Title,
                Brief=Brief,
                Details = Details,
                Keywords=Keywords,
                IsActive = IsActive,
                ImageFile = filename,
                AddBy = userid,
                AddOn = DateTime.Now,
                LastUpdate = DateTime.Now,
                UpdatedBy = userid,
            };
            entity = repository.Add(entity);
            return entity.Id;
        }
        public int UpdateIsActive(int Id, string userid)
        {
            NewsViewModel entity = new NewsViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                if (entity != null)
                {
                    entity.UpdatedBy = userid;
                    entity.IsActive = entity.IsActive != null ? (!entity.IsActive) : true;
                    repository.Update(entity, "ID");
                }
                return entity.Id;
            }

            return entity.Id;
        }

        public int Update(NewsViewModel entity)
        {
            entity = repository.Update(entity);
            return entity.Id;
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}
