﻿using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace ps.haweya.win.obs.business.Managers
{
    public class TicketManager : BaseManager<Ticket, TicketViewModel>
    {
        enum Status
        {
            New = 1,
            Pending = 2,
            Closed = 3
        }

        public TicketViewModel Add(TicketViewModel entity)
        {
            return repository.Add(entity);
        }

        public TicketViewModel Get(int Id)
        {
            return repository.Get(Id);
        }

        public IEnumerable<TicketViewModel> Get()
        {
            return repository.Get();
        }

        public string GetStatusName(int StatusId)
        {
            return ((Status)StatusId).ToString();
        }


        public IEnumerable<TicketViewModel> GetTicketHistory(int TicketId)
        {
            return repository.Get(x => x.Id == TicketId, "TicketRows", "Date desc");
        }

        public IEnumerable<object> GetByStatus()
        {
            return repository.QueryableReference()
                    .GroupBy(x => x.Status)
                    .Select(g => new { Key = ((Status)g.Key).ToString(), Value = g.Count(), Id = g.Key }).AsEnumerable();
        }

        public TicketViewModel Update(TicketViewModel entity)
        {
            var _entity = repository.Get(entity.Id);

            if (_entity == null) throw new Exception("Invalid Ticket Id " + entity.Id);

            _entity.Status = entity.Status;
            _entity.IsMovedToTechnical = entity.IsMovedToTechnical;

            return repository.Update(_entity, "Id");
        }

        public IEnumerable<TicketViewModel> Get(string column, int Page, int PerPage, string SortBy, string SortMethod, string q,bool? ismovedtechnical=null)
        {
            q = q.ToLower();

            var statusId = 0;
            statusId = q.ToLower().Equals("new") ? 1 : (q.ToLower().Equals("pending") ? 2 : (q.ToLower().Equals("closed") ? 3 : 0));

            if (!string.IsNullOrEmpty(q))
            {
                var query = repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod))
                    .Where(x => x.IsMovedToTechnical == (ismovedtechnical == true ? true : x.IsMovedToTechnical))
                                      .Where(t => t.TicketRows.Count(x => x.From.Contains(q)) > 0 ||
                                                    t.Subject.Contains(q) ||
                                                    t.Date.ToString().Contains(q) || 
                                                    t.Status == statusId);

                //if (statusId != 0)
                //{
                //    query = query.Where(t => t.Status == statusId);
                //}

                query = query.Skip((Page - 1) * PerPage)
                             .Take(PerPage);

                return repository.QueryablResult(query);
            }

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.IsMovedToTechnical == (ismovedtechnical == true ? true : x.IsMovedToTechnical))).Skip((Page - 1) * PerPage).Take(PerPage);
        }

        public int Count()
        {
            return repository.Count();
        }

        public int Count(string q,bool? ismoved=null)
        {
            return repository.Count(t=>t.IsMovedToTechnical==(ismoved==true?true:t.IsMovedToTechnical)&&(t.TicketRows.Count(x => x.From.Contains(q)) > 0 || t.Subject.Contains(q) || t.Source.Contains(q) || t.TicketRows.Count(x => x.Body.Contains(q)) > 0 || t.Date.ToString().Contains(q)));
        }

    }

    public class TicketRowManager : BaseManager<TicketRow, TicketRowViewModel>
    {


        public TicketRowViewModel Add(TicketRowViewModel entity, string From)
        {
            entity.From = From;
            entity.System = true;
            entity.Date = DateTime.Now;

            var ticket = new TicketManager().Get(entity.TicketId);

            new MailManager().SendMessage(ticket.From, "RE: TICKET[#" + ticket.Id.ToString("D5") + "] << " + ticket.Subject + " >>", entity.Body, "support@win.com.sa", "WIN%^&20sa");

            return repository.Add(entity);
        }
    }
}
