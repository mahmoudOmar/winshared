﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;

namespace ps.haweya.win.obs.business.Managers
{
   public class ErrorLogManager : BaseManager<ErrorLog, ErrorLogViewModel>
    {

        public IEnumerable<ErrorLogViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<ErrorLogViewModel> Get(int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference("",string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference("", string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Titel.ToLower().Contains(q) || x.Details.ToLower().Contains(q) || x.AspNetUser.FullName.ToLower().Contains(q) || x.IpAddress.Contains(q) || x.StackTrace.Contains(q) || x.TargetSite.Contains(q) || x.Url.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public int Count(string q)
        {
            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.Titel.ToLower().Contains(q) || x.Details.ToLower().Contains(q) || x.AspNetUser.FullName.ToLower().Contains(q) || x.IpAddress.Contains(q) || x.StackTrace.Contains(q) || x.TargetSite.Contains(q) || x.Url.Contains(q)).Count();
        }
        public ErrorLogViewModel Add(ErrorLogViewModel entity)
        {
            return repository.Add(entity);
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}
