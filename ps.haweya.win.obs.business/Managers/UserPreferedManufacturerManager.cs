﻿using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class UserPreferedManufacturerManager : BaseManager<UserPreferedManufacturer, UserPreferedManufacturerViewModel>
    {
        public UserPreferedManufacturerViewModel Add(UserPreferedManufacturerViewModel model)
        {
            return repository.Add(model);
        }
        public bool DeleteByUserId(string UserId)
        {
            return repository.DeleteRange(x => x.UserId == UserId);
        }
        public bool AddRange(List<int> Ids, string UserId)
        {
            DeleteByUserId(UserId);
            foreach (var id in Ids)
            {
                try
                {
                    UserPreferedManufacturerViewModel model = new UserPreferedManufacturerViewModel()
                    {
                        UserId = UserId,
                        ManufacturerId = id,
                        InsertDate = DateTime.Now
                    };
                    Add(model);
                }
                catch (Exception)
                {

                }
            }
            return true;
        }
    }
}
