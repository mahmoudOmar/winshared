﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;
namespace ps.haweya.win.obs.business.Managers
{
    public class VatHistoryManager:BaseManager<VATHistory,VATHistoryViewModel>
    {
        public VATHistoryViewModel GetById(int ID)
        {
            return repository.Get(ID);
        }
        public VATHistoryViewModel GetByText_value(string vattext,double? vatvalue)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.VATText == vattext&&x.VATValue==vatvalue).OrderBy(x => x.InsertDate)).FirstOrDefault();
        }
        public VATHistoryViewModel GetLast()
        {
            return repository.QueryablResult(repository.QueryableReference().OrderByDescending(x => x.InsertDate)).FirstOrDefault();
        }
        public IEnumerable<VATHistoryViewModel> Get()
        {
            return repository.Get();
        }
        public int Add(VATHistoryViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.Id;
        }
        
    }
}
