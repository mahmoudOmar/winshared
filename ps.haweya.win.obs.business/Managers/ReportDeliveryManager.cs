﻿using Hangfire;
using ps.haweya.win.obs.business.ViewModels;
using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ps.haweya.win.obs.business.Managers.OBSJobSchedulerManager;

namespace ps.haweya.win.obs.business.Managers
{
    public class ReportDeliveryManager : BaseManager<OBSJobScheduler, OBSJobSchedulerViewModel>
    {
        int buyerReportId = 2;
        public DateTime GetDateByFrequencty(ScheduleFrequency frequency)
        {
            DateTime date = DateTime.Now;
            switch (frequency)
            {
                case ScheduleFrequency.Daily:
                    return date.AddHours(-24);
                case ScheduleFrequency.Weekly:
                    return date.AddDays(-7);
                case ScheduleFrequency.Monthly:
                    return date.AddMonths(-1);
                case ScheduleFrequency.Quaterly:
                    return date.AddMonths(-3);
                case ScheduleFrequency.Yearly:
                    return date.AddMonths(-12);
            }
            return date;
        }
        public void SendStatusReport(OBSJobSchedulerViewModel schedule)
        {
            var UsersCreated = new AspNetUsersManager().GetCreatedToday();

            //var ProductData = repository.ProductInformations.ToList();

            var ProductData = new ProductInformationManager();

            var CreatedProduct = ProductData.GetCreatedProduct(DateTime.Now);
            var RejectedProduct = ProductData.GetRejectedProduct(DateTime.Now);
            var ApprovalProduct = ProductData.GetApprovalProduct(DateTime.Now);
            var ClosedProduct = ProductData.GetClosedProduct(DateTime.Now);

            MessageDataViewModel msgData = new MessageDataViewModel();
            msgData.ToEmailAddress = "winsurplussa@gmail.com,info@win.com.sa,wingait19@gmail.com,"+schedule.EmailIds;
            msgData.Subject = String.Format("Daily Report "+ schedule.ReportId);
            msgData.Message = String.Format("Number of Users Created: {0}<br/>Number of Created Product: {1}<br/>Number of Product Approvals from WIN Admin: {2}<br/>Number of Product Rejections from WIN Admin: {3}<br/>Number of Closed Product: {4}", UsersCreated, CreatedProduct,  ApprovalProduct, RejectedProduct, ClosedProduct);

            new MailManager().sendschedule(msgData.ToEmailAddress,msgData.Subject,msgData.Message);

        }

        public void SendBuyerPerfrencesNewsletter(int Frequency)
        {
            var reports = new UserReportsManager().GetByReportAndFrequency(buyerReportId, Frequency);
            foreach (var report in reports)
            {
                ScheduleFrequency scheduleFrequency;
                Enum.TryParse(Frequency.ToString(), true, out scheduleFrequency);
                var insertDate = GetDateByFrequencty(scheduleFrequency);
              
               // var items = new BuyerReportByUserManager().Get(report.UserId, insertDate);
               var items = StordProcedurManager.BuyerReportByUser(report.UserId, insertDate);
                
                if (items.Count > 0)
                {
                    new MailManager().SendMessage(items, report.AspNetUser.Email);
                }
            }
        }
        public void SendClosingBids()
        {
            var items = new ProductInformationManager().GetClosingBids().ToList();
            string[] emails = new AspNetUsersManager().GetByGetClosingBidsReport(true).Select(x=>x.Email).ToArray();
            new MailManager().SendClosingBidsMessage(items, emails);
        }
        public void SendBidClosedReport(long id)
        {
            var item = new ProductInformationManager().GetByid(id);
            var user = new AspNetUsersManager().GetByFullName(item.SellerName);
            new MailManager().SendBidClosedReport(item, user.Email);
        }

        public void SendBidwinnerClosedReport(long id)
        {
            var item = new ProductInformationManager().GetByid(id);
            if (item != null)
            {
                var max = item.OBSBidDetails.Count() > 0 ? item.OBSBidDetails.Max(x => x.BidPrice) : 0;
                var biddetails = item.OBSBidDetails.Where(x => x.BidPrice == max).SingleOrDefault();
                if (biddetails != null)
                {
                    var user = biddetails.AspNetUser;
                    new MailManager().SendBidwinnerClosedReport(item, user,max);
                }
            }
        }

        public void SendBidwinnerClosedReportafterrecipt(long id)
        {
            string bccemails = "Saboor@win.com.sa,Omer.arif@win.com.sa";

            ProductInformationViewModel item = new ProductInformationManager().GetByid(id);
            //var item = new ProductInformationManager().GetByid(id);
            var max = item.OBSBidDetails.Max(x => x.BidPrice);
            var maxdetails = item.OBSBidDetails.Where(x => x.BidPrice == max).FirstOrDefault();
            if (maxdetails != null && maxdetails.PercentRceiptCopy == null)
            {
                var nextmax = item.OBSBidDetails.Where(x => x.BidPrice != max).Max(x => x.BidPrice);
                var nextmaxdetails = item.OBSBidDetails.Where(x => x.BidPrice == nextmax).FirstOrDefault();
                var user = item.OBSBidDetails.Where(x => x.BidPrice == nextmax).SingleOrDefault().AspNetUser;
                new MailManager().SendBidwinnerClosedReport(item, user,nextmax,bccemails);
                new MailManager().SendMessage(user.Email, "Your Percent Bid Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We would like to inform you that your application has been registered in our system <br/> and that it is in the process of preparing and waiting for the "+ nextmaxdetails.ProductInformation.SecurityDepositPercent + "% from BidPrice to complete the process ... <br/> Please check with the company to complete the procedures.<br/> Your Percent Value is "+ nextmaxdetails.ProductInformation.SecurityDepositPercent + "%.<br/> ", "Warehouse Integration Company", user.FullName, (nextmaxdetails.BidPrice * ((nextmaxdetails.ProductInformation.SecurityDepositPercent??0)/(100))) + " "+ (nextmaxdetails.Currency!=null?nextmaxdetails.Currency.Sign:"SAR"),bccemails);

                var adminuser = new AspNetUsersManager().GetByRole("Admin").FirstOrDefault().Id;
                
                int ID = new BidHistoryManager().Add(Convert.ToInt32(maxdetails.BidId), 13, "The system cancelled the bid because the buyer doesn't send the percent 15% in the leagel period", adminuser,Convert.ToInt32(maxdetails.StatusID));
                //maxdetails.StatusID = 13;
                new OBSBidDetailsManager().UpdateBidStatus(maxdetails.BidId, 13);
                int IDnext = new BidHistoryManager().Add(Convert.ToInt32(nextmaxdetails.BidId), 13, "The system change  the bid to you because the buyer doesn't send the percent 15% in the leagel period", adminuser, Convert.ToInt32(nextmaxdetails.StatusID));
                //nextmaxdetails.StatusID = 13;
                new OBSBidDetailsManager().UpdateBidStatus(nextmaxdetails.BidId, 21);

                
            }
        }
        public void SendBidwinnerwait_tonextbider(long id)
        {
            string bccemails = "Saboor@win.com.sa,Omer.arif@win.com.sa";

            ProductInformationViewModel item = new ProductInformationManager().GetByid(id);
            //var item = new ProductInformationManager().GetByid(id);
            var max = item.OBSBidDetails.Max(x => x.BidPrice);
            var maxdetails = item.OBSBidDetails.Where(x => x.BidPrice == max).FirstOrDefault();
            if (maxdetails != null && maxdetails.PercentRceiptCopy == null)
            {
                var nextmax = item.OBSBidDetails.Where(x => x.BidPrice != max).Max(x => x.BidPrice);
                var nextmaxdetails = item.OBSBidDetails.Where(x => x.BidPrice == nextmax).FirstOrDefault();
                var user = item.OBSBidDetails.Where(x => x.BidPrice == nextmax).SingleOrDefault().AspNetUser;
                new MailManager().SendBidwinnerClosedReport(item, user, nextmax,bccemails);
                new MailManager().SendMessage(user.Email, "Your Percent Bid Value", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We would like to inform you that your application has been registered in our system <br/> and that it is in the process of preparing and waiting for  BidPrice to complete the process and we will wait for "+item.FullPaymetDays+"dayes to full payment then we will cancel bid ... <br/> Please check with the company to complete the procedures.<br/> Your Percent Value.<br/> ", "Warehouse Integration Company", user.FullName, (nextmaxdetails.BidPrice-(nextmaxdetails.BidPrice * ((nextmaxdetails.ProductInformation.SecurityDepositPercent??0)/(100)))) + " "+(nextmaxdetails.Currency!=null?nextmaxdetails.Currency.Sign:"SAR ") ,bccemails);

                var adminuser = new AspNetUsersManager().GetByRole("Admin").FirstOrDefault().Id;

                int ID = new BidHistoryManager().Add(Convert.ToInt32(maxdetails.BidId), 13, "The system cancelled the bid because the buyer doesn't send full payment in the leagel period", adminuser, Convert.ToInt32(maxdetails.StatusID));
                //maxdetails.StatusID = 13;
                new OBSBidDetailsManager().UpdateBidStatus(maxdetails.BidId, 13);
                int IDnext = new BidHistoryManager().Add(Convert.ToInt32(nextmaxdetails.BidId), 13, "The system change  the bid to you because the buyer doesn't send full payment  in the leagel period", adminuser, Convert.ToInt32(nextmaxdetails.StatusID));
                //nextmaxdetails.StatusID = 13;
                new OBSBidDetailsManager().UpdateBidStatus(nextmaxdetails.BidId, 21);


            }
        }
        public void SendEmailNotifyMe(int id)
        {

            NotifyUserViewModel item = new NotifyUserManager().GetByid(id);

            // AspNetUserViewModel user = new AspNetUsersManager().GetById(item.AspNetUserID);
            //var item = new ProductInformationManager().GetByid(id);
            string bccemails = "Saboor@win.com.sa,Omer.arif@win.com.sa";
            int period = ((item.NotifyDate ?? DateTime.Now) - DateTime.Now).Minutes;
            ScheduleFrequency scheduleFrequency;
            Enum.TryParse("0", true, out scheduleFrequency);
            BackgroundJob.Schedule(() => new MailManager().SendBidNotifyMe(item.ID,bccemails), TimeSpan.FromMinutes(period));
           
        }
        public void SetSendBidClosedReport(ProductInformationViewModel model)
        {if (model.DeadLine != null)
            {
                int period = (Convert.ToDateTime(model.DeadLine) - DateTime.Now).Minutes;
                ScheduleFrequency scheduleFrequency;
                Enum.TryParse("0", true, out scheduleFrequency);
                BackgroundJob.Schedule(() => SendBidClosedReport(model.ProductId), TimeSpan.FromMinutes(period));
            }
        }

        public void SetSendWinnerBidClosedReport(ProductInformationViewModel model)
        {
            //int period = model.DeadLine.Minutes;
            ScheduleFrequency scheduleFrequency;
            Enum.TryParse("0", true, out scheduleFrequency);
            BackgroundJob.Schedule(() => SendBidwinnerClosedReport(model.ProductId), TimeSpan.FromDays(Convert.ToDateTime(model.DeadLine).Day));
        }

        public void SendWinnerBidClosedReportafterrecipt(ProductInformationViewModel model) {
            //int period = model.DeadLine.Minutes;
            ScheduleFrequency scheduleFrequency;
            Enum.TryParse("0", true, out scheduleFrequency);
          
            BackgroundJob.Schedule(() => SendBidwinnerClosedReportafterrecipt(model.ProductId), TimeSpan.FromMinutes(DateTime.Now.AddMinutes(model.SecurityDepositDays??0*24*60).Minute));
        }
        public void SendWinnerBidwaitingvalue(ProductInformationViewModel model)
        {
            //int period = model.DeadLine.Minutes;
            ScheduleFrequency scheduleFrequency;
            Enum.TryParse("0", true, out scheduleFrequency);

            BackgroundJob.Schedule(() => SendBidwinnerwait_tonextbider(model.ProductId), TimeSpan.FromMinutes(DateTime.Now.AddMinutes((model.FullPaymetDays ?? 0) * 24 * 60).Minute));
        }
        public void SendBidsSummaryForSellerJob()
        {
            ScheduleFrequency scheduleFrequency;
            Enum.TryParse("1", true, out scheduleFrequency);
            
            BackgroundJob.Schedule(() => SendBidsSummaryForSeller(), TimeSpan.FromMinutes(DateTime.Now.AddMinutes(-35).Minute));
        }

        public void SendBidsSummaryForSeller()
        {
            var allBids = new OBSBidDetailsManager().ActiveBids();

            var sellers = allBids.GroupBy(t => t.ProductInformation.Username).Select(t => t.Key);

            foreach (var seller in sellers)
            {
                var products = allBids.Where(t => t.ProductInformation.Username.Equals(seller)).GroupBy(t => t.ProductId).Select(t => t.Key);

                var message = "";
                foreach (var product in products)
                {
                    var bids = allBids.Where(t => t.ProductInformation.Username.Equals(seller) && t.ProductId == product);

                    // send email
                    var _product = allBids.Where(t => t.ProductId == product).First();
                    var totals = bids.Sum(t => t.BidPrice);
                    var count = bids.Count();
                    var lastBidFrom = allBids.OrderByDescending(t => t.AddedOn).First().AspNetUser.FullName;

                    message = "Product : " + _product.ProductInformation.ProductTitle;
                    message += "==============================================================";
                    message += "Bids Count : " + count + "<br/>";
                    message += "Total Bids Price : " + totals + "<br/>";
                    message += "Last Bid From : " + lastBidFrom + "<br/>";

                    message += "<hr />";
                }

                new MailManager().SendMessage(seller, "Bids Summary", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", message, "Warehouse Integration Company", "", "");
            }
        }
        public void SendBidsSummaryForSellerbyproduct(long productid)
        {
            var product = new ProductInformationManager().GetByid(productid);

            string email = product.Username;

                var message = "";
               
                   
                    var totals = product.OBSBidDetails.Sum(t => t.BidPrice);
            var count = product.BidCount;
                    var lastBidFrom = product.OBSBidDetails.OrderByDescending(t => t.AddedOn).First().AspNetUser.FullName;
            var lastBidPrice = product.OBSBidDetails.OrderByDescending(t => t.AddedOn).First().BidPrice;

            message = "Product : " + product.ProductTitle;
                    message += "==============================================================";
                    message += "Bids Count : " + count + "<br/>";
                    message += "Total Bids Price : " + totals + "<br/>";
                    message += "Last Bid From : " + lastBidFrom + "<br/>";
            message += "Last Bid Price : " + lastBidPrice + "<br/>";
            message += "<hr />";
            
            string bccemails = "Saboor@win.com.sa,Omer.arif@win.com.sa";
            new MailManager().SendMessage("m.aldossary@gait.com.sa", "Bids Summary", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", message, "Warehouse Integration Company", "", "", bccemails);
            new MailManager().SendMessage(email, "Bids Summary", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", message, "Warehouse Integration Company", "", "");
            
        }
        public void StartCronJobs()
        {
            var scheduledAt = DateTime.Today.Date;
            for (int frequency = 2; frequency < 7; frequency++)
            {
                string scheduleName = "BuyerPerfrencesNewsletter => " + frequency;
                ScheduleFrequency Frequency;
                Enum.TryParse(frequency.ToString(), true, out Frequency);
                RecurringJob.AddOrUpdate(scheduleName, () => SendBuyerPerfrencesNewsletter(frequency), 
                                                             GetCronExpression(Frequency, scheduledAt.Hour, scheduledAt.Minute, false), 
                                                             TimeZoneInfo.FindSystemTimeZoneById("Arabic Standard Time"));
//TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
            }

            //ScheduleFrequency scheduleFrequency;
            //Enum.TryParse("3", true, out scheduleFrequency);
           // RecurringJob.AddOrUpdate("WeeklyAdminReport => 3", () => SendStatusReport(new OBSJobSchedulerViewModel()), GetCronExpression(scheduleFrequency, scheduledAt.Hour, scheduledAt.Minute, false), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
        }
    }
}
