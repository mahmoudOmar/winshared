﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;

namespace ps.haweya.win.obs.business.Managers
{
   public class AdvertismentBanerManager: BaseManager<AdvertismentBaner, AdvertismentBanerViewModel>
    {

        public IEnumerable<AdvertismentBanerViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<AdvertismentBanerViewModel> GetActive(bool? isactive)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.IsActive == isactive));
        }
        public IEnumerable<AdvertismentBanerViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Title.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public int Count(string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.Title.ToLower().Contains(q)).Count();
        }
        public AdvertismentBanerViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public int Add(AdvertismentBanerViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.Id;
        }

        public int AddOrUpdate(int Id, string Title,string Link, bool IsActive, string photo,int orderno)
        {
            AdvertismentBanerViewModel entity = new AdvertismentBanerViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.Title = Title;
                entity.IsActive = IsActive;
                entity.Link = Link;
                entity.Photo = photo != "" ? photo : entity.Photo;
                entity.OrderNo = orderno;
                repository.Update(entity, "Id");
                return entity.Id;
            }
            entity = new AdvertismentBanerViewModel()
            {
                Title = Title,
            IsActive = IsActive,
            Link = Link,
            Photo = photo != "" ? photo : entity.Photo,
            OrderNo = orderno,
            InsertDate = DateTime.Now,
            };
            entity = repository.Add(entity);
            return entity.Id;
        }
        public int UpdateIsActive(int Id)
        {
            AdvertismentBanerViewModel entity = new AdvertismentBanerViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                if (entity != null)
                {
                    entity.IsActive = entity.IsActive != null ? (!entity.IsActive) : true;
                    repository.Update(entity, "Id");
                }
                return entity.Id;
            }

            return entity.Id;
        }

        public int Update(AdvertismentBanerViewModel entity)
        {
            entity = repository.Update(entity,"Id");
            return entity.Id;
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}
