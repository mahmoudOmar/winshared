﻿using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class TransactionsLogManager : BaseManager<TransactionsLog, TransactionsLogViewModel>
    {

        public IEnumerable<TransactionsLogViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<TransactionsLogViewModel> Get(int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference("", string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference("", string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Action.ToLower().Contains(q) || x.Model.ToLower().Contains(q) || x.AspNetUser.FullName.ToLower().Contains(q) || x.RecordID.Contains(q) || x.UserID.Contains(q) || x.AspNetUser.Email.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public int Count(string q)
        {
            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.Action.ToLower().Contains(q) || x.Model.ToLower().Contains(q) || x.AspNetUser.FullName.ToLower().Contains(q) || x.RecordID.Contains(q) || x.UserID.Contains(q) || x.AspNetUser.Email.Contains(q)).Count();
        }
      
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }

      
}
