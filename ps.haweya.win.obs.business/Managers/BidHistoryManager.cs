﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;
namespace ps.haweya.win.obs.business.Managers
{
    public class BidHistoryManager : BaseManager<BidsHistory, BidsHistoryViewModel>
    {
        public IEnumerable<BidsHistoryViewModel> GetByBidId(int BidID)
        {
            return repository.QueryablResult(repository.QueryableReference(IncludColumn: "Status|Status1").Where(x => x.BidID == BidID).OrderBy(x => x.InsertDate));
        }
        public int Add(BidsHistoryViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.ID;
        }
        public int Add(int bidid, int newStatus, string note, string addedby, int oldStatusid)
        {

            BidsHistoryViewModel entity = new BidsHistoryViewModel()
            {
                BidID = bidid,
                OldStatusID = oldStatusid,
                NewStatusID = newStatus,
                Notes = note,
                AddedBy = addedby,
                InsertDate = DateTime.Now
            };
            entity = repository.Add(entity);
            return entity.ID;
        }
        public int UpdateBid(BidsHistoryViewModel entity)
        {
            entity = repository.Update(entity);
            return entity.ID;
        }

    }
}
 

