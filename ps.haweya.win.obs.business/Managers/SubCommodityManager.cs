﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
   public class SubCommodityManager : BaseManager<OBSSubCommodity, OBSSubCommodityViewModel>
    {

        public IEnumerable<OBSSubCommodityViewModel> Get()
        {
            return repository.Get();
        }
        
             public IEnumerable<OBSSubCommodityViewModel> GetByCategory(int categoryid)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.CategoryID == categoryid));
        }
        public OBSSubCommodityViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public OBSSubCommodityViewModel GetByName(string Name)
        {
            var _subCommodity = repository.QueryablResult(repository.QueryableReference().Where(x => x.SubCommodity.ToLower().Equals(Name.ToLower()))).FirstOrDefault();
            return _subCommodity;

        }

        public IEnumerable<OBSSubCommodityViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.SubCommodity.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public int Count(string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.SubCommodity.ToLower().Contains(q)).Count();
        }

        public int Add(OBSSubCommodityViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.SubCommodityId;
        }

        public int AddOrUpdate(int Id, string name,int CommodityId,int categoryid)
        {
            OBSSubCommodityViewModel entity = new OBSSubCommodityViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.SubCommodity = name;
                entity.CommodityId = CommodityId;
                entity.CategoryID = categoryid;
                repository.Update(entity, "SubCommodityId");
                return entity.SubCommodityId;
            }
            entity = new OBSSubCommodityViewModel()
            {
                SubCommodity = name,
                CommodityId = CommodityId,
                CategoryID = categoryid,
                DateCreated = DateTime.Now,
                CreatedBy = 1
            };
            entity = repository.Add(entity);
            return entity.SubCommodityId;
        }


        public int Update(OBSSubCommodityViewModel entity)
        {
            entity = repository.Update(entity, "SubCommodityId");
            return entity.SubCommodityId;
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}
