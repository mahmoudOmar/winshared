﻿using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit.Security;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class EmailManager
    {
        public void FetchTickets()
        {
            try
            {

                using (var client = new ImapClient())
                {
                    var username = ConfigurationManager.AppSettings["EMAIL_ADDRESS"].ToString();
                    var password = ConfigurationManager.AppSettings["EMAIL_PASSWORD"].ToString();

                    client.ServerCertificateValidationCallback = (s, c, h, eee) => true;

                    client.Connect("imap.gmail.com", 993, true);

                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    client.Authenticate(ConfigurationManager.AppSettings["EMAIL_ADDRESS"].ToString(),
                                        ConfigurationManager.AppSettings["EMAIL_PASSWORD"].ToString());

                    var inbox = client.Inbox.Open(FolderAccess.ReadWrite);

                    var unReadEmails = client.Inbox.Search(SearchQuery.NotSeen);
                    var messages = client.Inbox.Fetch(unReadEmails, MessageSummaryItems.Full | MessageSummaryItems.BodyStructure);


                    foreach (IMessageSummary message in messages)
                    {
                        ProcessEmail(client.Inbox, message);
                        client.Inbox.AddFlags(message.UniqueId, MessageFlags.Seen, true);
                    }

                    client.Disconnect(true);
                }
            }
            catch (Exception ee)
            {
                throw new Exception("Error in connecting to email", ee.InnerException);
            }
        }

        private void ProcessEmail(IMailFolder folder, IMessageSummary Message)
        {
            var ticketManager = new TicketManager();

            var ticketId = 0;
            TicketViewModel ticket = null;

            try
            {
                Match match = Regex.Match(Message.Envelope.Subject, @"(TICKET\[#\d{5}\])");
                if (match.Success)
                {
                    int.TryParse(Regex.Match(match.Groups[1].Value, @"\d{5}").ToString(), out ticketId);
                    ticket = ticketManager.Get(ticketId);
                }
            }
            catch (Exception e)
            {
                ticketId = 0;
            }

            if (ticketId == 0)
            {
                ticket = new TicketViewModel()
                {
                    From = string.Join(",", Message.Envelope.From),
                    Date = DateTime.Now,
                    Source = "Email",
                    Status = 1,
                    Subject = Message.Envelope.Subject
                };

                ticketManager.Add(ticket);
            }


            new TicketRowManager().Add(new TicketRowViewModel
            {
                Body = ((TextPart)folder.GetBodyPart(Message.UniqueId, Message.HtmlBody)).Text,
                Date = Message.Date.DateTime,
                From = string.Join(",", Message.Envelope.From),
                System = false,
                TicketId = ticket.Id
            }, "");

        }
    }
}
