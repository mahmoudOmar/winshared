﻿using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class UserPreferedCategoryManager : BaseManager<UserPreferedCategory, UserPreferedCategoryViewModel>
    {
        public UserPreferedCategoryViewModel Add(UserPreferedCategoryViewModel model)
        {
            return repository.Add(model);
        }
        public bool DeleteByUserId(string UserId)
        {
            return repository.DeleteRange(x => x.UserId == UserId);
        }
        public bool AddRange(List<int> Ids, string UserId)
        {
            DeleteByUserId(UserId);
            foreach (var id in Ids)
            {
                try
                {
                    UserPreferedCategoryViewModel model = new UserPreferedCategoryViewModel()
                    {
                        UserId = UserId,
                        CategoryId = id,
                        InsertDate = DateTime.Now
                    };
                    Add(model);
                }
                catch (Exception)
                {

                }
            }
            return true;
        }
    }
}
