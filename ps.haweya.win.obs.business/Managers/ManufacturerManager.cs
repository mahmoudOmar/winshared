﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ps.haweya.win.obs.models;
//using ps.haweya.win.obs.viewModels;

namespace ps.haweya.win.obs.business.Managers
{
    public class ManufacturerManager : BaseManager<Manufacturer, ManufacturerViewModel>
    {
        public IEnumerable<ManufacturerViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<ManufacturerViewModel> GetActive(bool?ismain)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x=>x.IsActive==true&&(ismain!=null?x.IsMain==ismain:true)));
        }
        public IEnumerable<ManufacturerViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Title.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public int Count(string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.Title.ToLower().Contains(q)).Count();
        }
        public ManufacturerViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public int Add(ManufacturerViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.Id;
        }

        public int AddOrUpdate(int Id, string Title, bool IsActive, bool IsMain, string filename, string userid)
        {
            ManufacturerViewModel entity = new ManufacturerViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.Title = Title;
                entity.IsActive = IsActive;
                entity.IsMain = IsMain;
                entity.ImageFile = filename != "" ? filename : entity.ImageFile;
                entity.LastUpdate = DateTime.Now;
                entity.UpdatedBy = userid;
                repository.Update(entity, "Id");
                return entity.Id;
            }
            entity = new ManufacturerViewModel()
            {
                Title = Title,
                IsActive = IsActive,
                ImageFile = filename,
                IsMain=IsMain,
                AddBy = userid,
                AddOn = DateTime.Now,
                LastUpdate = DateTime.Now,
                UpdatedBy = userid,
            };
            entity = repository.Add(entity);
            return entity.Id;
        }
        public int UpdateIsActive(int Id, string userid)
        {
            ManufacturerViewModel entity = new ManufacturerViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                if (entity != null)
                {
                    entity.UpdatedBy = userid;
                    entity.IsActive = entity.IsActive != null ? (!entity.IsActive) : true;
                    repository.Update(entity, "ID");
                }
                return entity.Id;
            }

            return entity.Id;
        }

        public int Update(ManufacturerViewModel entity)
        {
            entity = repository.Update(entity);
            return entity.Id;
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}