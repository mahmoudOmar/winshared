﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class OBSBidLiveTrackingManager : BaseManager<OBSBidLiveTracking, OBSBidLiveTrackingViewModel>
    {
        public IEnumerable<OBSBidLiveTrackingViewModel> Get()
        {
            return repository.Get();
        }


        //public IEnumerable<object> GetTrackingDetails()
        //{
        //    var trackingDetails = repository.QueryableReference("OBSBidLiveTrackings", "");
        //    var details = (from p in trackingDetails
        //                   select new
        //                   {
        //                       BidId = p.BidId,
        //                       ProductTitle = p.OBSBidDetail.ProductInformation.ProductTitle,
        //                       ProductImage = p.OBSBidDetail.ProductInformation.ProductImage,
        //                       Location = p.OBSBidDetail.ProductInformation.ProductLocation,
        //                       SellerUsername = p.OBSBidDetail.ProductInformation.Username,
        //                       Quantity = p.OBSBidDetail.ProductInformation.Quantity,
        //                       Price = p.OBSBidDetail.BidPrice,
        //                       BuyerUsername = p.OBSBidDetail.AspNetUser.Email,
        //                       IsSold = p.OBSBidDetail.IsSold,
        //                       TrackingData = (from tracking in p.OBSBidDetail.OBSBidLiveTrackings
        //                                       select new
        //                                       {
        //                                           TrackingId = tracking.BidTrackingId,
        //                                           PurchaseId = tracking.BidId,
        //                                           Inspection = tracking.Inspection,
        //                                           OutForDelivery = tracking.OutForDelivery,
        //                                           Payment = tracking.Payment,
        //                                           ProductConfirmation = tracking.ProductConfirmation,
        //                                           Delivered = tracking.Delivered
        //                                       }).ToList()
        //                   }).ToList();
        //    return details;
        //}

        //public IEnumerable<object> GetTrackingDetailsByUser(string Username)
        //{
        //    var trackingDetails = repository.QueryablResult(repository.QueryableReference("OBSPurchaseDetail", ""));
        //    var details = (from p in trackingDetails
        //                   select new
        //                   {
        //                       PurchaseId = p.BidId,
        //                       ProductTitle = p.OBSBidDetail.ProductInformation.ProductTitle,
        //                       ProductImage = p.OBSBidDetail.ProductInformation.ProductImg,
        //                       Location = p.OBSBidDetail.ProductInformation.ProductLocation,
        //                       SellerUsername = p.OBSBidDetail.ProductInformation.Username,
        //                       Quantity = p.OBSBidDetail.BidCount,
        //                       Price = 0,
        //                       BuyerUsername = p.OBSBidDetail.AspNetUser.Email,
        //                       IsPurchase = p.OBSBidDetail.IsShipment,
        //                       TrackingData = (from tracking in p.OBSBidDetail.OBSBidLiveTrackings
        //                                       select new
        //                                       {
        //                                           TrackingId = tracking.BidTrackingId,
        //                                           PurchaseId = tracking.BidId,
        //                                           Inspection = tracking.Inspection,
        //                                           OutForDelivery = tracking.OutForDelivery,
        //                                           Payment = tracking.Payment,
        //                                           ProductConfirmation = tracking.ProductConfirmation,
        //                                           Delivered = tracking.Delivered
        //                                       })
        //                   }).ToList().Where(x=>x.SellerUsername == Username || x.BuyerUsername == Username);
        //    return details;
        //}
    }
}
