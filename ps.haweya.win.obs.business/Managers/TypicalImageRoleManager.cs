﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class TypicalImageRoleManager : BaseManager<TypicalImageRole, TypicalImageRoleViewModel>
    {

        public IEnumerable<TypicalImageRoleViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<TypicalImageRoleViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.OBSProductCategory.CategoryName.ToLower().Contains(q) || x.OBSDiscipline.Discipline.ToLower().Contains(q) || x.OBSCommodity.Commodity.ToLower().Contains(q) || x.OBSSubCommodity.SubCommodity.Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public int Count( string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.OBSProductCategory.CategoryName.ToLower().Contains(q) || x.OBSDiscipline.Discipline.ToLower().Contains(q) || x.OBSCommodity.Commodity.ToLower().Contains(q) || x.OBSSubCommodity.SubCommodity.Contains(q)).Count();
        }
        public TypicalImageRoleViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public int Add(TypicalImageRoleViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.ID;
        }

        public int AddOrUpdate(int roleid,int CategoryID,int DisciplineID,int CommodityID,int SubCommodityID,string filename,string userid)
        {
            TypicalImageRoleViewModel entity = new TypicalImageRoleViewModel();
            if (roleid != 0)
            {
                entity = GetByid(roleid);
                entity.CategoryID = CategoryID;
                entity.SubCommodityID = SubCommodityID;
                entity.CommodityID = CommodityID;
                entity.DisciplineID = DisciplineID;
                entity.ImageFile = filename != "" ? filename : entity.ImageFile;
                entity.LastUpdate = DateTime.Now;
                entity.UpdatedBy = userid;
                repository.Update(entity, "ID");
                return entity.ID;
            }
 entity =new TypicalImageRoleViewModel() {
                CategoryID = CategoryID,
                CommodityID = CommodityID,
                SubCommodityID = SubCommodityID,
                DisciplineID = DisciplineID,
                ImageFile = filename,
                IsRun = true,
                AddBy = userid,
                AddOn = DateTime.Now,
                LastUpdate = DateTime.Now,
                UpdatedBy = userid,
            };
            entity = repository.Add(entity);
            return entity.ID;
        }
        public int UpdateRun(int roleid,string userid)
        {
            TypicalImageRoleViewModel entity = new TypicalImageRoleViewModel();
            if (roleid != 0)
            {
                entity = GetByid(roleid);
                if (entity != null)
                {
                    entity.UpdatedBy = userid;
                    entity.IsRun = entity.IsRun!=null?(!entity.IsRun):true;
                    repository.Update(entity, "ID");
                }
                return entity.ID;
            }
          
            return entity.ID;
        }
        
        public int Update(TypicalImageRoleViewModel entity)
        {
            entity = repository.Update(entity);
            return entity.ID;
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}
