﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class CacheManager
    {
        public static List<ManufacturersHomeData_ResultViewModel> OBSManufacturers { get; set; }
       
        public static List<CountryViewModel> Countries { get; set; }
        public static List<StateViewModel> States { get; set; }
        public static List<CityViewModel> Cities { get; set; }

        public static List<OBSProductCategoryViewModel> Categories { get; set; }

        public static List<LinkViewModel> Links { get; set; }

        public static List<TypicalImageRoleViewModel> ProductsImageRoles { get; set; }

        public static List<MenuItemViewModel> MenuItems { get; set; }
        public static SettingViewModel Setting { get; set; }

        public static List<AdvertismentBanerViewModel> Banars { get; set; }
        public static List<CategoryHomeData_ResultViewModel> HomeCategory { get; set; }
        public static List<SliderViewModel> SlidersHome { get; set; }
        public static List<NewsLayout> HomeNews { get; set; }
        

    }
}

