﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class SellerManager : BaseManager<Seller, SellerViewModel>
    {
        public SellerViewModel Add(SellerViewModel model)
        {

            return repository.Add(model);
        }
        public SellerViewModel GetByUserId(string Userid)
        {

            return repository.Get(x=>x.AspNetUserID.Equals(Userid)).FirstOrDefault();
        }

        public SellerViewModel updateVerfication(SellerAccountVerification reg,string userid)
        {

            var seller = GetByUserId(userid);
            seller.TypeID = reg.Typeid;
            seller.DocumentID = reg.DocumentID;
            seller.agreement = reg.agreement + "";
            return repository.Update(seller);
        }
        public SellerViewModel Update(SellerViewModel model)
        {

            return repository.Update(model);
        }
    }
}
