﻿using ps.haweya.win.obs.business.ViewModels;
using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class UserReportsManager : BaseManager<UserReport, UserReportViewModel>
    {
        public UserReportViewModel GetByUserAndReport(string UserId, int ReportId)
        {
            return repository.Get(x => x.UserId == UserId && x.ReportId == ReportId).FirstOrDefault();
        }
        public IEnumerable<UserReportViewModel> GetByReportAndFrequency(int ReportId, int Frequency)
        {
            return repository.Get(x => x.ReportId == ReportId && x.Frequency == Frequency);
        }
        public int AddOrUpdate(string UserId, int ReportId, int Frequency)
        {
            UserReportViewModel entity = GetByUserAndReport(UserId, ReportId);
            if (entity == null)
            {
                entity = new UserReportViewModel()
                {
                    UserId = UserId,
                    ReportId = ReportId,
                    Frequency = Frequency
                };
                entity = repository.Add(entity);
                return entity.Id;
            }

            entity.UserId = UserId;
            entity.ReportId = ReportId;
            entity.Frequency = Frequency;
            repository.Update(entity, "Id");
            return entity.Id;
        }
    }
}
