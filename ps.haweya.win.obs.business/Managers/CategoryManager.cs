﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class CategoryManager : BaseManager<OBSProductCategory, OBSProductCategoryViewModel>
    {
        public OBSProductCategoryViewModel Update(OBSProductCategoryViewModel entity)
        {
            return repository.Update(entity, "CategoryId");
        }

        public bool Delete(int Id)
        {
            return repository.Delete(Id);
        }

        public OBSProductCategoryViewModel Add(OBSProductCategoryViewModel entity)
        {
            var current = repository.Get(t => t.CategoryName.ToLower() == entity.CategoryName.ToLower()).FirstOrDefault();
            if (current != null) return current;
            return repository.Add(entity);
        }

        public IEnumerable<OBSProductCategoryViewModel> Get(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod))
                .Where(x => x.CategoryName.ToLower().Contains(q))
                .Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public int Count(string q)
        {
            return repository.Count(t => t.CategoryName.Contains(q));
        }
        public IEnumerable<OBSProductCategoryViewModel> Get()
        {
            return repository.Get();
        }
        public OBSProductCategoryViewModel GetByid(long id)
        {
            return repository.Get(id);
        }
        public List<OBSProductCategoryViewModel> Get(List<int> Ids)
        {
            List<OBSProductCategoryViewModel> categoriesList = new List<OBSProductCategoryViewModel>();
            foreach (var id in Ids)
            {
                categoriesList.Add(GetByid(id));
            }
            return categoriesList;
        }

        public OBSProductCategoryViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
    }
}
