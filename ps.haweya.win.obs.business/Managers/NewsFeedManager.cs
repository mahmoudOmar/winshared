﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
   public class NewsFeedManager: BaseManager<OBSNewsFeed, OBSNewsFeedViewModel>
    {
        public IEnumerable<OBSNewsFeedViewModel> Get()
        {
            return repository.Get();
        }
        public OBSNewsFeedViewModel GetByid(int id)

        {
            return repository.Get(id);
        }
        public IEnumerable<OBSNewsFeedViewModel> GetTop(int count)
        {
            var query = repository.QueryableReference();
            query = query.OrderByDescending(x => x.AddedOn).Take(count);
            return repository.QueryablResult(query);
        }
        public IEnumerable<OBSNewsFeedViewModel> GetRelated(int id)
        {
            var query = repository.QueryableReference().Where(x=>x.NewsId!=id);
            query = query.OrderByDescending(x => x.AddedOn);
            return repository.QueryablResult(query);
        }
        
    }
}
