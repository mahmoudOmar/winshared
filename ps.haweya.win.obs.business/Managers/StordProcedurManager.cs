﻿using System;
using ps.haweya.win.obs.models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Data.Entity.Core.Objects;
using AutoMapper;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace ps.haweya.win.obs.business.Managers
{
    public class StordProcedurManager
    {

        public static List<T> ExecuteStoredProcedure<T>(string name, object Parameters = null) where T : class
        {

            List<T> results = new List<T>();
            DataTable DT = new DataTable();

            var connectionString = ConfigurationManager.ConnectionStrings["obsDBNative"].ToString();

            using (SqlDataAdapter DA = new SqlDataAdapter(name, connectionString))
            {
                DA.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (Parameters != null)
                {
                    var props = Parameters.GetType().GetProperties();

                    foreach (var item in props)
                    {
                        var x = Parameters.GetType().GetProperty(item.Name);
                        if (x != null)
                        {
                            var _value = x.GetValue(Parameters, null);
                            DA.SelectCommand.Parameters.AddWithValue("@" + item.Name, _value ?? DBNull.Value);
                        }
                    }
                }
                DA.Fill(DT);
            }

            results = AutoMapper.Mapper.DynamicMap<IDataReader, List<T>>(DT.CreateDataReader());

            return results;
        }

        
        public static DataTable ExecuteStoredProcedureAsDataTable(string name, object Parameters)
        {

            DataTable DT = new DataTable();

            var connectionString = ConfigurationManager.ConnectionStrings["obsDBNative"].ToString();

            using (SqlDataAdapter DA = new SqlDataAdapter(name, connectionString))
            {
                DA.SelectCommand.CommandType = CommandType.StoredProcedure;

                var props = Parameters.GetType().GetProperties();

                foreach (var item in props)
                {
                    var x = Parameters.GetType().GetProperty(item.Name);
                    if (x != null)
                    {
                        var _value = x.GetValue(Parameters, null);
                        DA.SelectCommand.Parameters.AddWithValue("@" + item.Name, _value ?? DBNull.Value);
                    }
                }
                DA.Fill(DT);
            }

            return DT;

        }

        public static void ExecuteStoredProcedure(string name, object Parameters)
        {

            DataTable DT = new DataTable();

            var connectionString = ConfigurationManager.ConnectionStrings["HayatkomEntities"].ToString();

            using (SqlDataAdapter DA = new SqlDataAdapter(name, connectionString))
            {
                DA.SelectCommand.CommandType = CommandType.StoredProcedure;

                var props = Parameters.GetType().GetProperties();

                foreach (var item in props)
                {
                    var x = Parameters.GetType().GetProperty(item.Name);
                    if (x != null)
                    {
                        var _value = x.GetValue(Parameters, null);
                        DA.SelectCommand.Parameters.AddWithValue("@" + item.Name, _value ?? DBNull.Value);
                    }
                }
                DA.Fill(DT);
            }

        }


        public static List<GetHOMEPRODUCTS_ResultViewModel> GetHOMEPRODUCTS(int? id, string q, bool? AuctionCheck = null, bool? DirectCheck = null, 
            string brandsID = null, double? min = null, double? max = null, int? sorting = null, int? page = null, int? viewcount = null, string condetion = "",
            bool? active = null,int ?productno=null)
        {
            //List<object> param = new List<object>();
            //param.Add(id);
            //param.Add(q);
            //param.Add(AuctionCheck);
            //param.Add(DirectCheck);
            //param.Add(brandsID);
            //param.Add(min);
            //param.Add(max);
            //param.Add(sorting);
            //param.Add(page);
            //param.Add(viewcount);
            //param.Add(condetion);
            //param.Add(active);
            //param.Add(productno);
            
            brandsID = brandsID == "" ? null : brandsID;
            condetion = condetion == "" ? null : condetion;
            return ExecuteStoredProcedure<GetHOMEPRODUCTS_ResultViewModel>("GetHOMEPRODUCTS", new { id = id,q=q, AuctionCheck= AuctionCheck,
                DirectCheck= DirectCheck,
                brndides = brandsID,
                min= min,
                max=max,
                sorting= sorting,
                page= page,
                viewcount= viewcount,
                condetion= condetion,
                active= active,
                productno= productno
            });
        }

        public static int GetHOMEPRODUCTSCount(int? id, string q, bool? AuctionCheck = null, bool? DirectCheck = null,
           string brandsID = null, double? min = null, double? max = null, int? sorting = null, int? page = null, int? viewcount = null, string condetion = "",
           bool? active = null, int? productno = null)
        {
            brandsID = brandsID == "" ? null : brandsID;
            condetion= condetion == "" ? null : condetion;

            return ExecuteStoredProcedureCounters("GetHOMEPRODUCTSCount", new
            {
                id = id,
                q = q,
                AuctionCheck = AuctionCheck,
                DirectCheck = DirectCheck,
                brndides = brandsID,
                min = min,
                max = max,
                sorting = sorting,
                page = page,
                viewcount = viewcount,
                condetion = condetion,
                active = active,
                productno = productno
            });
        }
        public static List<GetSearchPRODUCTS_ResultViewModel> GetSearchhomePRODUCTS( string q)
        {

            return ExecuteStoredProcedure<GetSearchPRODUCTS_ResultViewModel>("GetSearchPRODUCTS", new
            {
                q = q
            });
        }
        public static List<FullProductInformation_ResultViewModel> FullProductInformation(string User)
        {
            List<object> param = new List<object>();
            param.Add(User);
            return ExecuteStoredProcedure<FullProductInformation_ResultViewModel>("FullProductInformation", param);
        }

        public static List<ManufacturersHomeData_ResultViewModel> ManufacturersHomeData()
        {
            return ExecuteStoredProcedure<ManufacturersHomeData_ResultViewModel>("ManufacturersHomeData");
        }
        public static List<ManufacturersHomeData_ResultViewModel> ManufacturersHomeDataAuctions()
        {
            return ExecuteStoredProcedure<ManufacturersHomeData_ResultViewModel>("ManufacturersHomeData", new
            {
                Auction = 1,
            });
        }
        public static List<CategoryHomeData_ResultViewModel> CategoryHomeData()
        {
            return ExecuteStoredProcedure<CategoryHomeData_ResultViewModel>("CategoryHomeData");
        }
        public static List<CategoryHomeData_ResultViewModel> CategoryHomeDataAuctions()
        {
            return ExecuteStoredProcedure<CategoryHomeData_ResultViewModel>("CategoryHomeData", new
            {
                Auction = 1,
            });
        }



        
    public static List<Conditiontohome_ResultViewModel> Conditiontohome()
        {
            return ExecuteStoredProcedure<Conditiontohome_ResultViewModel>("Conditiontohome");
        }

        public static List<BuyerReportByUser_ResultViewModel> BuyerReportByUser(string UserId, DateTime insertDate)
        {
            return ExecuteStoredProcedure<BuyerReportByUser_ResultViewModel>("BuyerReportByUser", new  { UserId = UserId, insertDate = insertDate } );
        }

        public static int ExecuteStoredProcedureCounters(string name, object Parameters)
        {

            DataTable DT = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["obsDBNative"].ToString();

            using (SqlDataAdapter DA = new SqlDataAdapter(name, connectionString))
            {
                DA.SelectCommand.CommandType = CommandType.StoredProcedure;

                var props = Parameters.GetType().GetProperties();

                foreach (var item in props)
                {
                    var x = Parameters.GetType().GetProperty(item.Name);
                    if (x != null)
                    {
                        var _value = x.GetValue(Parameters, null);
                        DA.SelectCommand.Parameters.AddWithValue("@" + item.Name, _value ?? DBNull.Value);
                    }
                }
                DA.Fill(DT);
            }
            if (DT.Rows.Count > 0)
                return Convert.ToInt32(DT.Rows[0][0].ToString());

            return 0;
        }

        //public static List<KeyValuePair<string, object>> testOutput()
        //{
        //    var outParam = new List<ObjectParameter>();
        //    outParam.Add(new ObjectParameter("co", 0));


        //    var results = Repository.Repository<object, object>.ExecuteStoredProcedureAsync("testOut", objectParameters: outParam)[1] as List<ObjectParameter>;

        //    return results.Select(t=> new KeyValuePair<string, object> ( t.Name, t.Value )).ToList();
        //}
        //public static List<KeyValuePair<string, object>> SellerBoughtItemsCount(string UserId)
        //{
        //    var outParam = new List<ObjectParameter>();
        //    outParam.Add(new ObjectParameter("ItemsCount", 0));
        //    var inParam = new List<object>();
        //    inParam.Add(UserId);

        //    var results = Repository.Repository<object, object>.ExecuteStoredProcedureAsync("SellerBoughtItemsCount", inParam, objectParameters: outParam)[1] as List<ObjectParameter>;

        //    return results.Select(t => new KeyValuePair<string, object>(t.Name, t.Value)).ToList();
        //}
        //public static List<KeyValuePair<string, object>> SellerProductsCount(string UserId)
        //{
        //    var outParam = new List<ObjectParameter>();
        //    outParam.Add(new ObjectParameter("ItemsCount", 0));
        //    var inParam = new List<object>();
        //    inParam.Add(UserId);

        //    var results = Repository.Repository<object, object>.ExecuteStoredProcedureAsync("SellerProductsCount", inParam, objectParameters: outParam)[1] as List<ObjectParameter>;

        //    return results.Select(t => new KeyValuePair<string, object>(t.Name, t.Value)).ToList();
        //}
        //public static List<KeyValuePair<string, object>> SellerItemsBuyersCount(string UserId)
        //{
        //    var outParam = new List<ObjectParameter>();
        //    outParam.Add(new ObjectParameter("ItemsCount", 0));
        //    var inParam = new List<object>();
        //    inParam.Add(UserId);

        //    var results = Repository.Repository<object, object>.ExecuteStoredProcedureAsync("SellerItemsBuyersCount", inParam, objectParameters: outParam)[1] as List<ObjectParameter>;

        //    return results.Select(t => new KeyValuePair<string, object>(t.Name, t.Value)).ToList();
        //}
        //public static List<KeyValuePair<string, object>> SellerItemsCitiesCount(string UserId)
        //{
        //    var outParam = new List<ObjectParameter>();
        //    outParam.Add(new ObjectParameter("ItemsCount", 0));
        //    var inParam = new List<object>();
        //    inParam.Add(UserId);

        //    var results = Repository.Repository<object, object>.ExecuteStoredProcedureAsync("SellerItemsCitiesCount", inParam, objectParameters: outParam)[1] as List<ObjectParameter>;

        //    return results.Select(t => new KeyValuePair<string, object>(t.Name, t.Value)).ToList();
        //}
        //public static List<KeyValuePair<string, object>> ProductsByCategory(string UserId)
        //{
        //    var outParam = new List<ObjectParameter>();
        //    outParam.Add(new ObjectParameter("ItemsCount", 0));
        //    var inParam = new List<object>();
        //    //if(UserId != "")
        //    //{
        //        inParam.Add(UserId);
        //    //}

        //    var results = Repository.Repository<object, object>.ExecuteStoredProcedureAsync("ProductsByCategory", inParam, objectParameters: outParam)[1] as List<ObjectParameter>;

        //    return results.Select(t => new KeyValuePair<string, object>(t.Name, t.Value)).ToList();
        //}

    }
}
