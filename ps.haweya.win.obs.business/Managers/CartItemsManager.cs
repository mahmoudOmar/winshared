﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
   public class CartItemsManager:BaseManager<CartItem, CartItemViewModel>
    {
        

        public IEnumerable<CartItemViewModel> Get()
        {
            return repository.Get();
        }

        public CartItemViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public IEnumerable<CartItemViewModel> GetByBuyerId(string Userid,bool IsBought)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.AspNetUserId == Userid && x.IsBought==IsBought));
        }

        public IEnumerable<CartItemViewModel> GetByOrderId(int id)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.OrderID == id));
        }
        public IEnumerable<CartItemViewModel> GetInspectionItemsByOrderId(int id)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.OrderID == id&&x.NeedInspection==true));
        }

        public IEnumerable<CartItemViewModel> GetShipmentItemsByOrderId(int id)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.OrderID == id && x.NeesShipment == true));
        }
        public IEnumerable<CartItemViewModel> GetByBuyerId(string Userid, bool IsBought,int orderid)
        {
            return repository.QueryablResult(repository.QueryableReference("ProductInformation","").Where(x => x.AspNetUserId == Userid && x.IsBought == IsBought&&x.OrderID==orderid));
        }

        public CartItemViewModel Update(CartItemViewModel model)
        {
            return repository.Update(model);
        }

        public CartItemViewModel Add(CartItemViewModel model)
        {
            return repository.Add(model);
        }

        public bool Delete(int ID)
        {
            List<int> test = new List<int>();
            test.Add(ID);

            return repository.DeleteRange(test);
        }
    }
}
