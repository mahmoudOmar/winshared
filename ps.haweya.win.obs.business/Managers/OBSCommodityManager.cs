﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public  class OBSCommodityManager :BaseManager<OBSCommodity, OBSCommodityViewModel>
    {
        public IEnumerable<OBSCommodityViewModel> Get()
    {
        return repository.Get();
    }
        public IEnumerable<OBSCommodityViewModel> GetByCategory(int categoryid)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x=>x.CategoryID== categoryid));
        }

       
        public OBSCommodityViewModel GetByid(int id)
        {
            return repository.Get(id);
        }
        public OBSCommodityViewModel GetByName(string Name)
        {
            var company = repository.QueryablResult(repository.QueryableReference().Where(x => x.Commodity.ToLower().Equals(Name.ToLower()))).FirstOrDefault();
            return company;

        }

        public IEnumerable<OBSCommodityViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.Commodity.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }

        public int Count(string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryableReference().Count();

            return repository.QueryableReference().Where(x => x.Commodity.ToLower().Contains(q)).Count();
        }

        public int Add(OBSCommodityViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.CommodityId;
        }

        public int AddOrUpdate(int Id, string name, int categoryid)
        {
            OBSCommodityViewModel entity = new OBSCommodityViewModel();
            if (Id != 0)
            {
                entity = GetByid(Id);
                entity.Commodity = name;
                entity.CategoryID = categoryid;
                repository.Update(entity, "CommodityId");
                return entity.CommodityId;
            }
            entity = new OBSCommodityViewModel()
            {
                Commodity = name,
                CategoryID = categoryid,
                DateCreated = DateTime.Now,
                CreatedBy = 1
            };
            entity = repository.Add(entity);
            return entity.CommodityId;
        }


        public int Update(OBSCommodityViewModel entity)
        {
            entity = repository.Update(entity, "CommodityId");
            return entity.CommodityId;
        }

        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}