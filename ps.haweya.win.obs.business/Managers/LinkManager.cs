﻿using ps.haweya.win.obs.models;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class LinkManager : BaseManager<Link, LinkViewModel>
    {
        public LinkViewModel Add(LinkViewModel entity)
        {
            return repository.Add(entity);
        }

        public LinkViewModel AddOrIgnore(LinkViewModel entity)
        {
            var link = repository.Get(t => t.Area.ToLower().Equals(entity.Area) &&
                                                                 t.Controller.ToLower().Equals(entity.Controller) &&
                                                                 t.Action.ToLower().Equals(entity.Action)).FirstOrDefault();
            if (link == null)
            {
                return repository.Add(entity);
            }

            return link;
        }

        public LinkViewModel GetById(int Id)
        {
            return repository.Get(x => x.Id == Id, include: "AspNetRoles").FirstOrDefault();
        }

        public IEnumerable<LinkViewModel> GetByArea(string Area)
        {
            return repository.Get(x => x.Area == Area);
        }
        public IEnumerable<LinkViewModel> GetByActiontoseo(string controller,string action)
        {
            var link= repository.Get(x => string.IsNullOrEmpty(x.Area)
            &&x.Action.ToLower().Equals(action.ToLower())
            &&x.Controller.ToLower().Equals(controller.ToLower()));
            return link;
        }
        public IEnumerable<LinkViewModel> Get()
        {
            return repository.Get();
        }
        public IEnumerable<LinkViewModel> GetSeoLinks()
        {
            return repository.Get(x => x.IsSeoLink == true);
        }
        public int Update(LinkViewModel entity)
        {
           
            if (entity.Id != 0)
            {
                repository.Update(entity, "Id");
                return entity.Id;
            }
            return 0;
        }
    }


    public class UserLinkManager : BaseManager<UserLink, UserLinkViewModel>
    {
        public UserLinkViewModel AddOrIgnore(UserLinkViewModel entity)
        {
            var link = repository.Get(t => t.UserId.Equals(entity.UserId) && t.LinkId == entity.LinkId).FirstOrDefault();
            if (link == null)
            {
                return repository.Add(entity);
            }

            return link;
        }

        public UserLinkViewModel Add(UserLinkViewModel entity)
        {
            return repository.Add(entity);
        }

        public List<UserLinkViewModel> AddLinkRangeToUser(List<UserLinkViewModel> entity)
        {
            return AddRange(entity);
        }

        public List<UserLinkViewModel> AddRange(List<UserLinkViewModel> entity)
        {
            return repository.AddRange(entity);
        }

        public bool DeleteByUserID(string userid)
        {
            return repository.DeleteRange(x => x.UserId == userid);
        }

        public UserLinkViewModel GetByUserId(string UserId, int LinkId)
        {
            return repository.Get(t => t.UserId.Equals(UserId) && t.LinkId == LinkId).FirstOrDefault();
        }

        public IEnumerable<UserLinkViewModel> GetByUserId(string UserId)
        {
            return repository.Get(t => t.UserId.Equals(UserId), include: "Link");
        }
    }
}
