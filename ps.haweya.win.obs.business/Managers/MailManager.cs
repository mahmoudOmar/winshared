﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class MailManager
    {

        //test no replyemail
        public void sendmessagenoreply()
        
        {
            
                  SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EMAIL_ADDRESS"], ConfigurationManager.AppSettings["EMAIL_PASSWORD"]);
            client.UseDefaultCredentials = true;
            client.Credentials = credentials;

            MailMessage msgprop = new MailMessage();
            msgprop.From = new MailAddress(ConfigurationManager.AppSettings["EMAIL_ADDRESS"]);
            msgprop.To.Add(new MailAddress("mahmoudomry@gmail.com"));

            msgprop.Subject = "test";
            msgprop.IsBodyHtml = true;
            msgprop.Body = "test";
            // client.Send(msgprop);
            try
            {
                client.Send(msgprop);
                //lblMsg.Text = "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
                throw new Exception("failed sending  email", ex.InnerException);
            }
        }

        public void sendschedule(string emails,string subject,string details)

        {

            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EMAIL_ADDRESS"], ConfigurationManager.AppSettings["EMAIL_PASSWORD"]);
            client.UseDefaultCredentials = true;
            client.Credentials = credentials;

            MailMessage msgprop = new MailMessage();
            msgprop.From = new MailAddress(ConfigurationManager.AppSettings["EMAIL_ADDRESS"]);

            List<string> emailslist = emails.Split(',').ToList();
            if (emailslist.Contains("omer.arif@win.com.sa") == false) {
                MailAddress bcc = new MailAddress("omer.arif@win.com.sa");
                msgprop.Bcc.Add(bcc);
//                emailslist.Add("omer.arif@win.com.sa");
            }
            foreach (string s in emailslist)
            {
                if(! string.IsNullOrEmpty(s))
                msgprop.To.Add(new MailAddress(s));
            }
            

            msgprop.Subject = subject;
            msgprop.IsBodyHtml = true;
            msgprop.Body = details;
            // client.Send(msgprop);
            try
            {
                client.Send(msgprop);
                //lblMsg.Text = "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
                throw new Exception("failed sending  email", ex.InnerException);
            }
        }
        public void SendMessage(string Email, string Subject, string Message, string email = "", string password = "")
        {
            
            email=email ==""? ConfigurationManager.AppSettings["EMAIL_ADDRESS"].ToString():email;
            password = password==""? ConfigurationManager.AppSettings["EMAIL_PASSWORD"].ToString():password;
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(email, password);
            client.UseDefaultCredentials = true;
            client.Credentials = credentials;

            MailMessage msgprop = new MailMessage();
            msgprop.From = new MailAddress(email);

            List<string> emails = Email.Split(',').ToList();
            if (emails.Contains("omer.arif@win.com.sa") == false)
            {
                //emails.Add("omer.arif@win.com.sa");
                MailAddress bcc = new MailAddress("omer.arif@win.com.sa");
                msgprop.Bcc.Add(bcc);
            }
            foreach (string m in emails)
            {
                msgprop.To.Add(new MailAddress(m));
            }
            

            msgprop.Subject = Subject;
            msgprop.IsBodyHtml = true;
            msgprop.Body = Message;
            // client.Send(msgprop);
            try
            {
                client.Send(msgprop);
                // lblMsg.Text = "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
               throw new Exception("failed sending  email", ex.InnerException);
            }


        }
        public void SendMessage(string Email, string Subject, string logofile, string logolink, string buttonlink, string details, string sitename, string recivername, string buttontext,string bccemails="")
        {
            //try
            //{
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EMAIL_ADDRESS"], ConfigurationManager.AppSettings["EMAIL_PASSWORD"]);
            client.UseDefaultCredentials = true;
            client.Credentials = credentials;

            MailMessage msgprop = new MailMessage();
            msgprop.From = new MailAddress(ConfigurationManager.AppSettings["EMAIL_ADDRESS"]);
            //  msgprop.To.Add(new MailAddress(Email));
            List<string> emails = Email.Split(',').ToList();
            if (emails.Contains("omer.arif@win.com.sa") == false)
            {
                //  emails.Add("omer.arif@win.com.sa");
                MailAddress bcc = new MailAddress("omer.arif@win.com.sa");
                msgprop.Bcc.Add(bcc);
            }
            foreach (string m in bccemails.Split(','))
            {
                if (!string.IsNullOrEmpty(m))
                {
                    MailAddress bcc1 = new MailAddress(m);
                    msgprop.Bcc.Add(bcc1);
                }
            }

            foreach (string m in bccemails.Split(','))
            {
                if (!string.IsNullOrEmpty(m))
                {
                    MailAddress bcc1 = new MailAddress(m);
                    msgprop.Bcc.Add(bcc1);
                }
            }
            foreach (string m in emails)
            {
                msgprop.To.Add(new MailAddress(m));
            }

            msgprop.Subject = Subject;
            msgprop.IsBodyHtml = true;
            msgprop.Body = messageboody(logofile, logolink, buttonlink, details, sitename, recivername, buttontext);

            try
            {
                client.Send(msgprop);
                // lblMsg.Text = "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
               throw new Exception("failed sending  email", ex.InnerException);
            }

        }
        public void SendMessage(List<BuyerReportByUser_ResultViewModel> items, string Email)
        {
            //try
            //{
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EMAIL_ADDRESS"], ConfigurationManager.AppSettings["EMAIL_PASSWORD"]);
            client.UseDefaultCredentials = true;
            client.Credentials = credentials;

            MailMessage msgprop = new MailMessage();
            msgprop.From = new MailAddress(ConfigurationManager.AppSettings["EMAIL_ADDRESS"]);
            //msgprop.To.Add(new MailAddress("dev.mghazali@gmail.com"));
            //msgprop.Bcc.Add(new MailAddress("mahmoudomry@gmail.com"));
            //msgprop.Bcc.Add(new MailAddress("mqudaih@haweya.ps"));
            // msgprop.To.Add(new MailAddress(Email));
            List<string> emails = Email.Split(',').ToList();
            if (emails.Contains("omer.arif@win.com.sa") == false)
            {
                //  emails.Add("omer.arif@win.com.sa");
                MailAddress bcc = new MailAddress("omer.arif@win.com.sa");
                msgprop.Bcc.Add(bcc);
            }
            foreach (string m in emails)
            {
                msgprop.To.Add(new MailAddress(m));
            }
            msgprop.Subject = "Preferences Report";
            msgprop.IsBodyHtml = true;
            msgprop.Body = buyerPreferencesBody(items, Email);

            try
            {
                client.Send(msgprop);
                // lblMsg.Text = "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
                ////throw new Exception("failed sending  email", ex.InnerException);
            }

        }
        private string messageboody(string logofile, string logolink, string buttonlink, string details, string sitename, string recivername, string buttontext)
        {
            string messagebody = @"<style>.shippingitems td {border:1px solid #808080;padding:0 3px;}</style><table border='0' cellspacing='0' cellpadding='0' align='center' width='520' bgcolor='#ffffff' style='background:#ffffff;max-width:520px'>
        <tbody>
            <tr>
                <td width='20' bgcolor='#eeeeee' style='background:#eeeeee'></td>
                <td width='480'>
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tbody>
                            <tr>
                                <td height='20' bgcolor='#eeeeee' style='background:#eeeeee'></td>
                            </tr>
                            <tr>
                                <td>
                                    <table border='0' cellspacing='5' cellpadding='5' align='center' width='100%' style='border-bottom:1px solid #eeeeee'>
                                        <tbody>
                                            <tr>
                                                <td align='center'>
                                                    <a href='" + logolink + @"' style='color:#4285f4;text-decoration:underline' target='_blank' >
                                                        <img  border='0' src='" + logofile + @"' alt='" + sitename + @"' style='text-align:center;border:none' class='CToWUd'>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height='49'></td>
                                            </tr>
                                            <tr>
                                                <td align='center'  style='color:#4285f4;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:32px;font-weight:300;line-height:46px;margin:0;padding:0 25px 0 25px;text-align:center'>Welcome  " + recivername + @"</td>
                                            </tr>

                                            <tr>
                                                <td height='20'>" + details + @"</td>
                                            </tr>



                                            <tr>
                                                <td align='center'  style='color:#757575;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:17px;font-weight:300;line-height:24px;margin:0;padding:0 25px 0 25px;text-align:center'>
<br/> <a href='"+buttonlink+@"' style='padding:20px;background:#999999;color:#ffffff;font-size:18px'>" + buttontext + @"</a> <br/> </td>
                                            </tr>
<tr>
                                                <td height='30'></td>
                                            </tr>
                                            <tr>
                                                <td align='right' style='color:#757575;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:17px;font-weight:300;line-height:24px;margin:0;padding:0 25px 0 25px;text-align:right'>" + sitename + @"  </td>
                                            </tr>
                                           

                                       </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height='20' bgcolor='#eeeeee' style='background:#eeeeee'></td>
                            </tr>
                       
                    
                        </tbody>
                    </table>
                </td>
                <td width='20' bgcolor='#eeeeee' style='background:#eeeeee'></td>
            </tr>
        </tbody>
    </table>";
            return messagebody;
        }

        private string buyerPreferencesBody(List<BuyerReportByUser_ResultViewModel> items, string Email)
        {
            string itemsHtml = "";
            for (int i = 1; i < items.Count; i++)
            {
                if (i % 2 == 1)
                {
                    itemsHtml += "<tr>";
                }
                var item = items[i - 1];
                itemsHtml += @"
                                <td width='50%' style='padding:15px;vertical-align: top;' >
                                    <div style='background: #fff;width:300px'>
                                        <div style='background-position: center center;
                                        background-repeat: no-repeat;
                                        background-size: cover;
                                        -moz-background-size: cover;
                                        -webkit-background-size: cover;background-image: url(https://win.com.sa/content/UploadData/UploadImages/" + item.ProductImage + @");'>
                                            <a href='#' style='display: block;padding-top: 74%;'></a>
                                        </div>
                                        <div style='padding:15px;border:1px solid #ebebeb;'>
                                            <div style='color: #f4a32d;font-size: 13px;    margin-bottom: 8px;'>" + item.CategoryName + @"</div>
                                            <h3 style='font-size: 14px;margin-bottom: 10px;padding-bottom: 10px;min-height: 38px;border-bottom: 1px solid #ebebeb;'><a style='color: #403c34;text-decoration: none;' href='#'>TERMINAL BLOCK, ELECTRICAL TERMINAL BLOCK</a></h3>
                                            <div style='font-size: 13px;color: #878787;margin:0 0 15px'>
                                                <span style='inline-block;margin-right:10px;'>
                                                    Qty:
                                                    <span style='color: #403c34;'> " + item.Quantity + @" item</span>
                                                </span>
                                                <span style='inline-block;'>
                                                    Price:
                                                    <span style='color: #403c34;'> " + item.UnitPrice + @" "+ System.Configuration.ConfigurationManager.AppSettings["System_Currency"] + @"</span>
                                                </span>
                                            </div>
                                            <div style='text-align: center;'>
                                                <a href='https://win.com.sa/ProductDetails/Details/" + item.ProductId + @"' style='display:inline-block;border: 1px solid #f4a32d;color:#f4a32d;text-decoration: none;
                                                padding: 5px 15px;
                                                border-radius: 3px;'>Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>";
                if (i % 2 == 0)
                {
                    itemsHtml += "</tr>";
                }
            }
            if (!itemsHtml.EndsWith("</tr>"))
            {
                itemsHtml += "</tr>";
            }
            string html = @"
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title></title>
</head>
<body>
    <table border='0' cellspacing='0' cellpadding='0' align='center'  width='630' bgcolor='#ffffff' style='background:#ffffff;max-width:600px'>
        <tbody>
            <tr>
                <td bgcolor='#eeeeee' style='background:#eeeeee;padding:15px;'>
                    <table border='0' cellspacing='0' cellpadding='0'  width='600' bgcolor='#ffffff' style='background:#ffffff;'>
                        <tr>
                            <td align='center' style='padding:15px'>
                                <a href='#' style='color:#4285f4;text-decoration:underline' target='_blank'>
                                    <img border='0' src='https://win.com.sa/Content/FrontEnd/images/logo.png' alt='' style='text-align:center;border:none;height:50px' class='CToWUd'>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td align='center' style='padding:0 15px;text-align:center;'>
                                <div style='font-weight: 700;font-size: 20px;color: #027DB4;margin:0 0 15px;'>Welcome Buyer, Here's the new about your preferences</div>
                            </td>
                        </tr>
                        " + itemsHtml + @"
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style='padding:15px;'><hr style='margin:0;border:1px solid #ebebeb' /></td>
                        </tr>
                        <tr>
                            <td style='padding:0 15px 20px;text-align:center'>
                                <a href='https://win.com.sa/' style='text-decoration: none;color:#999;display:inline-block;padding:0 15px;'>Home</a> | 
                                <a href='https://win.com.sa/Home/About' style='text-decoration: none;color:#999;display:inline-block;padding:0 15px;'>About</a> | 
                                <a href='https://win.com.sa/Home/Contact' style='text-decoration: none;color:#999;display:inline-block;padding:0 15px;'>Contact</a> 
                            </td>
                        </tr>
                        <tr>
                            <td style='font-size:13px;color:#aaa;line-height:22px;text-align:center;padding: 0 15px 20px;'>
                                    Chamber of Commerce Building, 6th Floor, Jubail Industrial City Saudi Arabia
                                    <br/>
                                    info@win.com.sa
                                    <br/>
                                    00966133479300
                            </td>
                        </tr>
                        <tr>
                            <td style='padding:0 0 30px;text-align:center;'>
                                <a href='https://www.facebook.com/winogm.sa/' style='text-decoration: none;color:#999;display:inline-block;margin:0 5px;'>
                                    <img src='https://image.flaticon.com/icons/svg/145/145802.svg' style='width:22px'>
                                </a> 
                                <a href='https://twitter.com/WINOnlineGloba1' style='text-decoration: none;color:#999;display:inline-block;margin:0 5px;'>
                                    <img src='https://image.flaticon.com/icons/svg/145/145812.svg' style='width:22px'>
                                </a> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
";
            return html;
        }
        public void SendClosingBidsMessage(List<ProductInformationViewModel> items, string[] Emails)
        {
            //try
            //{
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EMAIL_ADDRESS"], ConfigurationManager.AppSettings["EMAIL_PASSWORD"]);
            client.UseDefaultCredentials = true;
            client.Credentials = credentials;

            MailMessage msgprop = new MailMessage();
            msgprop.From = new MailAddress(ConfigurationManager.AppSettings["EMAIL_ADDRESS"]);
            //  msgprop.To.Add(new MailAddress("dev.mghazali@gmail.com"));
            List<string> emails = Emails.ToList();
            if (emails.Contains("omer.arif@win.com.sa") == false)
            {
                //emails.Add("omer.arif@win.com.sa");
                MailAddress bcc = new MailAddress("omer.arif@win.com.sa");
                msgprop.Bcc.Add(bcc);
            }
           
           
            foreach (var email in emails)
            {
                msgprop.To.Add(new MailAddress(email));
            }

            msgprop.Subject = "Preferences Report";
            msgprop.IsBodyHtml = true;
            msgprop.Body = closingBidsBody(items);

            try
            {
                client.Send(msgprop);
                // lblMsg.Text = "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
             throw new Exception("failed sending  email", ex.InnerException);
            }

        }
        private string closingBidsBody(List<ProductInformationViewModel> items)
        {
            string itemsHtml = "";
            for (int i = 1; i < items.Count; i++)
            {
                if (i % 2 == 1)
                {
                    itemsHtml += "<tr>";
                }
                var item = items[i - 1];
                itemsHtml += @"
                                <td width='50%' style='padding:15px;vertical-align: top;' >
                                    <div style='background: #fff;width:300px'>
                                        <div style='background-position: center center;
                                        background-repeat: no-repeat;
                                        background-size: cover;
                                        -moz-background-size: cover;
                                        -webkit-background-size: cover;background-image: url(https://win.com.sa/Content/UploadData/UploadImages/" + item.ProductImage + @");'>
                                            <a href='#' style='display: block;padding-top: 74%;'></a>
                                        </div>
                                        <div style='padding:15px;border:1px solid #ebebeb;'>
                                            <div style='color: #f4a32d;font-size: 13px;    margin-bottom: 8px;'>" + item.OBSProductCategory.CategoryName + @"</div>
                                            <h3 style='font-size: 14px;margin-bottom: 10px;padding-bottom: 10px;min-height: 38px;border-bottom: 1px solid #ebebeb;'><a style='color: #403c34;text-decoration: none;' href='#'>TERMINAL BLOCK, ELECTRICAL TERMINAL BLOCK</a></h3>
                                            <div style='font-size: 13px;color: #878787;margin:0 0 15px'>
                                                <span style='inline-block;margin-right:10px;'>
                                                    Qty:
                                                    <span style='color: #403c34;'> " + item.Quantity + @" item</span>
                                                </span>
                                                <span style='inline-block;'>
                                                    Price:
                                                    <span style='color: #403c34;'> " + item.UnitPrice + @" "+ System.Configuration.ConfigurationManager.AppSettings["System_Currency"] + @"</span>
                                                </span>
                                            </div>
                                            <div style='text-align: center;'>
                                                <a href='https://win.com.sa/ProductDetails/Details/" + item.ProductId + @"' style='display:inline-block;border: 1px solid #f4a32d;color:#f4a32d;text-decoration: none;
                                                padding: 5px 15px;
                                                border-radius: 3px;'>Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>";
                if (i % 2 == 0)
                {
                    itemsHtml += "</tr>";
                }
            }
            if (!itemsHtml.EndsWith("</tr>"))
            {
                itemsHtml += "</tr>";
            }
            string html = @"
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <title></title>
</head>
<body>
    <table border='0' cellspacing='0' cellpadding='0' align='center'  width='630' bgcolor='#ffffff' style='background:#ffffff;max-width:600px'>
        <tbody>
            <tr>
                <td bgcolor='#eeeeee' style='background:#eeeeee;padding:15px;'>
                    <table border='0' cellspacing='0' cellpadding='0'  width='600' bgcolor='#ffffff' style='background:#ffffff;'>
                        <tr>
                            <td align='center' style='padding:15px'>
                                <a href='#' style='color:#4285f4;text-decoration:underline' target='_blank'>
                                    <img border='0' src='https://win.com.sa/Content/FrontEnd/images/logo.png' alt='' style='text-align:center;border:none;height:50px' class='CToWUd'>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td align='center' style='padding:0 15px;text-align:center;'>
                                <div style='font-weight: 700;font-size: 20px;color: #027DB4;margin:0 0 15px;'>Welcome Buyer, Here are the closing bids</div>
                            </td>
                        </tr>
                        " + itemsHtml + @"
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style='padding:15px;'><hr style='margin:0;border:1px solid #ebebeb' /></td>
                        </tr>
                        <tr>
                            <td style='padding:0 15px 20px;text-align:center'>
                                <a href='https://win.com.sa/' style='text-decoration: none;color:#999;display:inline-block;padding:0 15px;'>Home</a> | 
                                <a href='https://win.com.sa/Home/About' style='text-decoration: none;color:#999;display:inline-block;padding:0 15px;'>About</a> | 
                                <a href='https://win.com.sa/Home/Contact' style='text-decoration: none;color:#999;display:inline-block;padding:0 15px;'>Contact</a> 
                            </td>
                        </tr>
                        <tr>
                            <td style='font-size:13px;color:#aaa;line-height:22px;text-align:center;padding: 0 15px 20px;'>
                                    Chamber of Commerce Building, 6th Floor, Jubail Industrial City Saudi Arabia
                                    <br/>
                                    info@win.com.sa
                                    <br/>
                                    00966133479300
                            </td>
                        </tr>
                        <tr>
                            <td style='padding:0 0 30px;text-align:center;'>
                                <a href='https://www.facebook.com/winogm.sa/' style='text-decoration: none;color:#999;display:inline-block;margin:0 5px;'>
                                    <img src='https://image.flaticon.com/icons/svg/145/145802.svg' style='width:22px'>
                                </a> 
                                <a href='https://twitter.com/WINOnlineGloba1' style='text-decoration: none;color:#999;display:inline-block;margin:0 5px;'>
                                    <img src='https://image.flaticon.com/icons/svg/145/145812.svg' style='width:22px'>
                                </a> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
";
            return html;
        }
        public void SendBidClosedReport(ProductInformationViewModel item, string Email)
        {
            //try
            //{
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EMAIL_ADDRESS"], ConfigurationManager.AppSettings["EMAIL_PASSWORD"]);
            client.UseDefaultCredentials = true;
            client.Credentials = credentials;

            MailMessage msgprop = new MailMessage();
            msgprop.From = new MailAddress(ConfigurationManager.AppSettings["EMAIL_ADDRESS"]);
            //   msgprop.To.Add(new MailAddress(Email));
            List<string> emails = Email.Split(',').ToList();
            if (emails.Contains("omer.arif@win.com.sa") == false)
            {
                // emails.Add("omer.arif@win.com.sa");
                MailAddress bcc = new MailAddress("omer.arif@win.com.sa");
                msgprop.Bcc.Add(bcc);
            }
            foreach (string m in emails)
            {
                msgprop.To.Add(new MailAddress(m));
            }

            msgprop.Subject = "Preferences Report";
            msgprop.IsBodyHtml = true;
            msgprop.Body = BidClosedReport(item);
            try
            {
                client.Send(msgprop);
                // lblMsg.Text = "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
             throw new Exception("failed sending  email", ex.InnerException);
            }

        }
        public void SendBidwinnerClosedReport(ProductInformationViewModel item, AspNetUserViewModel user,double? price,string bccemails="")
        {
            //try
            //{
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EMAIL_ADDRESS"], ConfigurationManager.AppSettings["EMAIL_PASSWORD"]);
            client.UseDefaultCredentials = true;
            client.Credentials = credentials;

            MailMessage msgprop = new MailMessage();
            msgprop.From = new MailAddress(ConfigurationManager.AppSettings["EMAIL_ADDRESS"]);
            msgprop.To.Add(new MailAddress(user.Email));
            //  msgprop.To.Add(new MailAddress("omer.arif@win.com.sa"));
            MailAddress bcc = new MailAddress("omer.arif@win.com.sa");
            msgprop.Bcc.Add(bcc);

            msgprop.Subject = "Bid Winner Report";
            msgprop.IsBodyHtml = true;
            msgprop.Body = BidwinerClosedReport(item, user,price);
            try
            {
                client.Send(msgprop);
                // lblMsg.Text = "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
             throw new Exception("failed sending  email", ex.InnerException);
            }

        }
        string BidClosedReport(ProductInformationViewModel item)
        {
            string html = "Item with name : " + item.ProductTitle + " closed and has " + item.BidCount + " bidders count";
            return html;
        }

        string BidwinerClosedReport(ProductInformationViewModel item, AspNetUserViewModel user,double? price)
        {
            if (price == null)
                price = Convert.ToDouble(item.OBSBidDetails.Max(x => x.BidPrice));
            string html = messageboody("https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "you are the winner in this bid  Item <br/> Item with name : " + item.ProductTitle + " closed and has " + item.ProductDetails.Count() + " bidders count", "Warehouse Integration Company", user.FullName, price.ToString());
            return html;
        }

        public void SendBidNotifyMe(int id,string bccemails="")
        {
            //try
            //{
            NotifyUserViewModel item = new NotifyUserManager().GetByid(id);

            AspNetUserViewModel user = new AspNetUsersManager().GetById(item.AspNetUserID);
            ProductInformationViewModel model = new ProductInformationManager().GetByid( item.ProductID??0);
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EMAIL_ADDRESS"], ConfigurationManager.AppSettings["EMAIL_PASSWORD"]);
            client.UseDefaultCredentials = true;
            client.Credentials = credentials;

            MailMessage msgprop = new MailMessage();
            msgprop.From = new MailAddress(ConfigurationManager.AppSettings["EMAIL_ADDRESS"]);
            msgprop.To.Add(new MailAddress(user.Email));
            // msgprop.To.Add(new MailAddress("omer.arif@win.com.sa"));
            MailAddress bcc = new MailAddress("omer.arif@win.com.sa");
            msgprop.Bcc.Add(bcc);

            foreach (string m in bccemails.Split(','))
            {
                if (!string.IsNullOrEmpty(m))
                {
                    MailAddress bcc1 = new MailAddress(m);
                    msgprop.Bcc.Add(bcc1);
                }
            }

            msgprop.Subject = "Notify Bid";
            msgprop.IsBodyHtml = true;
            msgprop.Body = messageboody("https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We  are notify you about bid its with start soon <br/> Item with name : " + model.ProductTitle + "  <br/> its will start at: " + model.BidStartDate + ",  <br/> and it will end at" + model.DeadLine, "Warehouse Integration Company", user.FullName,model.BidStartDate.ToString());
            try
            {
                client.Send(msgprop);
                // lblMsg.Text = "Your message has been successfully sent.";
            }
            catch (Exception ex)
            {
             throw new Exception("failed sending  email", ex.InnerException);
            }

        }

        public string messageboodyProductSigesstion( string details)
        {
            string logofile = "https://www.win.com.sa//Content/FrontEnd/images/logo.png";
            string logolink = "https://www.win.com.sa/";
            string sitename = "Warehouse Integration Company";
            string messagebody = @"<style>.shippingitems td {border:1px solid #808080;padding:0 3px;}</style><table border='0' cellspacing='0' cellpadding='0' align='center' width='520' bgcolor='#ffffff' style='background:#ffffff;max-width:520px'>
        <tbody>
            <tr>
                <td width='20' bgcolor='#eeeeee' style='background:#eeeeee'></td>
                <td width='480'>
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tbody>
                            <tr>
                                <td height='20' bgcolor='#eeeeee' style='background:#eeeeee'></td>
                            </tr>
                            <tr>
                                <td>
                                    <table border='0' cellspacing='5' cellpadding='5' align='center' width='100%' style='border-bottom:1px solid #eeeeee'>
                                        <tbody>
                                            <tr>
                                                <td align='center'>
                                                    <a href='" + logolink + @"' style='color:#4285f4;text-decoration:underline' target='_blank' >
                                                        <img  border='0' src='" + logofile + @"' alt='" + sitename + @"' style='text-align:center;border:none' class='CToWUd'>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height='49'></td>
                                            </tr>
                                            <tr>
                                                <td align='center'  style='color:#4285f4;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:32px;font-weight:300;line-height:46px;margin:0;padding:0 25px 0 25px;text-align:center'>Welcome WIN Admin </td>
                                            </tr>

                                            <tr>
                                                <td height='20'>" + details + @"</td>
                                            </tr>



                                           
<tr>
                                                <td height='30'></td>
                                            </tr>
                                            <tr>
                                                <td align='right' style='color:#757575;font-family:&quot;Roboto&quot;,OpenSans,&quot;Open Sans&quot;,Arial,sans-serif;font-size:17px;font-weight:300;line-height:24px;margin:0;padding:0 25px 0 25px;text-align:right'>" + sitename + @"  </td>
                                            </tr>
                                           

                                       </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height='20' bgcolor='#eeeeee' style='background:#eeeeee'></td>
                            </tr>
                       
                    
                        </tbody>
                    </table>
                </td>
                <td width='20' bgcolor='#eeeeee' style='background:#eeeeee'></td>
            </tr>
        </tbody>
    </table>";
            return messagebody;
        }
    }
}
