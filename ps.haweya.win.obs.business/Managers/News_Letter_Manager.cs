﻿using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
 public   class News_Letter_Manager : BaseManager<News_letter_Emails, News_letter_EmailsViewModel>

    {
        public IEnumerable<News_letter_EmailsViewModel> Get()
        {
            return repository.Get();
        }
        public News_letter_EmailsViewModel GetByEmail(string email)
        {
            return repository.QueryablResult(repository.QueryableReference().Where(x => x.Email.ToLower().Equals(email.ToLower()))).FirstOrDefault();
        }
        public News_letter_EmailsViewModel Add(News_letter_EmailsViewModel model)
        {

            //if (GetByEmail(model.Email) == null)
                return repository.Add(model);
            //else
            //    return model;
        }
    }
}
