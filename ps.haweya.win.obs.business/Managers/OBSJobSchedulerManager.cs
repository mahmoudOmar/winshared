﻿using Hangfire;
using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Managers
{
    public class OBSJobSchedulerManager : BaseManager<OBSJobScheduler, OBSJobSchedulerViewModel>
    {
        public IEnumerable<OBSJobSchedulerViewModel> Get()
        {
            return repository.Get();
        }
        public OBSJobSchedulerViewModel GetByid(long id)
        {
            return repository.Get(id);
        }
        public IEnumerable<OBSJobSchedulerViewModel> GetWithInclouds(string column, int Page, int PerPage, string SortBy, string SortMethod, string q)
        {
            q = q.ToLower();

            if (string.IsNullOrEmpty(q))
                return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Skip((Page - 1) * PerPage).Take(PerPage));

            //return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.UpdatedBy.ToLower().Contains(q) || x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.UserName.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
            return repository.QueryablResult(repository.QueryableReference(IncludColumn: column, string.Format("{0} {1}", SortBy, SortMethod)).Where(x => x.UpdatedBy.ToLower().Contains(q)).Skip((Page - 1) * PerPage).Take(PerPage));
        }
        public int Count(string q)
        {
            q = q.ToLower();
            if (!string.IsNullOrEmpty(q))
                //return repository.QueryableReference().Where(x => x.UpdatedBy.ToLower().Contains(q) || x.AspNetUser.FullName.ToLower().Contains(q) || x.AspNetUser.UserName.ToLower().Contains(q)).Count();
            return repository.QueryableReference().Where(x => x.UpdatedBy.ToLower().Contains(q)).Count();

            return repository.QueryableReference().Count();

        }
        public long Add(OBSJobSchedulerViewModel entity)
        {
            entity = repository.Add(entity);
            return entity.JobSchedulerId;
        }
        public long AddOrUpdate(long JobSchedulerId, TimeSpan SchedulerTime, DateTime AsOfUpdated, string UpdatedBy, string EmailIds
            , bool IsActive , int ReportId, int Frequency,string UserId)
        {
            OBSJobSchedulerViewModel entity = new OBSJobSchedulerViewModel();
            if (JobSchedulerId != 0)
            {
                entity = GetByid(JobSchedulerId);
                entity.SchedulerTime = SchedulerTime;
                entity.AsOfUpdated = AsOfUpdated;
                entity.UpdatedBy = UpdatedBy;
                entity.EmailIds = EmailIds;
                entity.IsActive = IsActive;
                entity.ReportId = ReportId;
                entity.Frequency = Frequency.ToString();
           //     entity.UserId = UserId;
                repository.Update(entity, "JobSchedulerId");
                //RecurringJob.RemoveIfExists(GetName(entity.ReportId, entity.JobSchedulerId));
                AddSchedule(entity);
                return entity.JobSchedulerId;
            }

            entity = new OBSJobSchedulerViewModel()
            {
                SchedulerTime = SchedulerTime,
                AsOfUpdated = AsOfUpdated,
                UpdatedBy = UpdatedBy,
                EmailIds = EmailIds,
                IsActive = IsActive,
                ReportId = ReportId,
                Frequency = Frequency.ToString(),
               // UserId = UserId
            };
            entity = repository.Add(entity);
            AddSchedule(entity);
            return entity.JobSchedulerId;
        }
        public long Update(OBSJobSchedulerViewModel entity)
        {
            entity = repository.Update(entity);
            return entity.JobSchedulerId;
        }

        public bool Delete(int id)
        {
            var entity=repository.Get(id);
            RecurringJob.RemoveIfExists(GetName(entity.ReportId, entity.JobSchedulerId));
            return repository.Delete(id);
            
        }
        #region Admin Report
        public static void Execute(OBSJobSchedulerViewModel schedule)
        {
            new ReportDeliveryManager().SendStatusReport(schedule);
        }
        public static void AddSchedule(OBSJobSchedulerViewModel schedule)
        {
            var scheduledAt = new DateTime();
            scheduledAt = scheduledAt.Add(schedule.SchedulerTime);
            var scheduleName = GetName(schedule.ReportId, schedule.JobSchedulerId);
            ScheduleFrequency scheduleFrequency;
            Enum.TryParse(schedule.Frequency, true, out scheduleFrequency);
            RecurringJob.AddOrUpdate(scheduleName, () => Execute(schedule), GetCronExpression(scheduleFrequency, scheduledAt.Hour, scheduledAt.Minute, false), TimeZoneInfo.FindSystemTimeZoneById("Arabic Standard Time"));
        }
        public enum ScheduleFrequency
        {
            Minutely, Hourly, Daily, Weekly, Monthly, Quaterly, Yearly
        }
        public static string GetCronExpression(ScheduleFrequency frequency, int hour, int minute,
            bool shouldExecuteOnlyOnWeekdays = true)
        {
            switch (frequency)
            {
                case ScheduleFrequency.Minutely:
                    return string.Format("*/{0} * * * *", 1);
                case ScheduleFrequency.Hourly:
                    return string.Format("* */{0} * * *", 1);
                case ScheduleFrequency.Daily:
                default:
                    return shouldExecuteOnlyOnWeekdays ? string.Format("{1} {0} * * 1-5", hour, minute) : Cron.Daily(hour, minute);
                case ScheduleFrequency.Weekly:
                    return Cron.Weekly(DayOfWeek.Saturday, hour, minute);
                case ScheduleFrequency.Monthly:
                    return Cron.Monthly(30, hour, minute);
                case ScheduleFrequency.Quaterly:
                    return string.Format("{1} {0} * */3 *", hour, minute);
                case ScheduleFrequency.Yearly:
                    return Cron.Yearly(12, 31, hour, minute);

            }
        }
        private static string GetName(long reportId, long scheduleId)
        {
            return string.Format("ReportId:{0} > schId:{1}", reportId, scheduleId);
        }
        #endregion
    }
}
