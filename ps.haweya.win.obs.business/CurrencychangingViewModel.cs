using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class CurrencychangingViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int Id { get; set; }
    
    		
        public Nullable<int> CurrencyId { get; set; }
    
    		
        public Nullable<double> RateToRiyal { get; set; }
    
    		
        public Nullable<System.DateTime> InsertDate { get; set; }
    }
}
