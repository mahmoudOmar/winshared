﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.Repository
{
    public interface IRepository<T> : IDisposable
    {
        T Get(int id);
        IEnumerable<T> Get(string OrderBy);
        
        IEnumerable<T> Get(string include, string OrderBy);

        T Add(T entity);
        T Update(T entity,string key);
        bool Delete(int id);
    }
}