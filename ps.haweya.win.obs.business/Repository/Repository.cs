﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using System.Web;
using System.Security;
using System.Collections;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.Web.Mvc;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace ps.haweya.win.obs.business.Repository
{
    public class Repository<Model, ViewModel> : IRepository<ViewModel> where Model : class where ViewModel : class
    {

        private obsDB _entities = new obsDB();
        private bool disposed = false;

        public Repository()
        {
          //  Mapper.CreateMap<ViewModel, Model>(MemberList.Source).ReverseMap().MaxDepth(1);
            _entities.Database.CommandTimeout = 999999999;
        }


        public int Count()
        {
            return _entities.Set<Model>().Count();
        }

        public int Count(Expression<Func<Model, bool>> exp)
        {
            return _entities.Set<Model>().Count(exp);
        }

        public IEnumerable SqlQuery<T>(string sql, object[] param)
        {
            return (IEnumerable)_entities.Database.SqlQuery<IEnumerable>(sql, param);
        }

        public virtual IEnumerable<ViewModel> Get(string OrderBy = "")
        {

            var query = _entities.Set<Model>();

            if (string.IsNullOrEmpty(OrderBy))
                return Mapper.Map<IEnumerable<Model>, IEnumerable<ViewModel>>(query);

            return Mapper.Map<IEnumerable<Model>, IEnumerable<ViewModel>>(query.OrderBy(OrderBy));

        }

        public virtual IEnumerable<ViewModel> Get(Expression<Func<Model, bool>> exp, string include, string OrderBy = "")
        {
            var query = _entities.Set<Model>().Include(include).Where(exp);

            if (string.IsNullOrEmpty(OrderBy))
            {
                return Mapper.Map<IEnumerable<Model>, IEnumerable<ViewModel>>(query).ToList();
            }

            return Mapper.Map<IEnumerable<Model>, IEnumerable<ViewModel>>(query.Include(include).Where(exp).OrderBy(OrderBy)).ToList();
        }

        public virtual IEnumerable<ViewModel> Get(string include, string OrderBy = "")
        {
            var query = _entities.Set<Model>().Include(include);

            if (string.IsNullOrEmpty(OrderBy))
                return Mapper.Map<IEnumerable<Model>, IEnumerable<ViewModel>>(query).ToList();

            return Mapper.Map<IEnumerable<Model>, IEnumerable<ViewModel>>(query.OrderBy(OrderBy)).ToList();
        }

        public virtual DbSet<Model> GetModel()
        {
            return _entities.Set<Model>();
        }

        public virtual IQueryable<Model> QueryableReference(string OrderBy = "")
        {


            if (string.IsNullOrEmpty(OrderBy))
            {
             
                var property = typeof(Model).GetProperties()
          .FirstOrDefault(p => p.GetCustomAttributes(false)
         .Any(a => a.GetType() == typeof(KeyAttribute)));
                if (property != null)
                    return _entities.Set<Model>().OrderBy(property.Name + " desc");

                return _entities.Set<Model>();
            }

            // return _entities.Set<Model>().OrderBy(OrderBy);



            else
            {
                string[] orderlist = OrderBy.Split(' ');
                if (orderlist.Count() != 0)
                {

                    var listOfFieldNames = typeof(Model).GetProperties().Select(f => f.Name).ToList();

                    if (listOfFieldNames.Contains(orderlist[0]))
                        return _entities.Set<Model>().OrderBy(OrderBy);
                    else
                    {
                        var property = typeof(Model).GetProperties()
               .FirstOrDefault(p => p.GetCustomAttributes(false)
                   .Any(a => a.GetType() == typeof(KeyAttribute)));

                        return _entities.Set<Model>().OrderBy(property!=null?property.Name +"desc" :"");
                    }
                }
                else
                {
                    return _entities.Set<Model>();

                }
            }
        }

        public virtual IQueryable<Model> QueryableReference(string IncludColumn, string OrderBy = "")
        {

            var data = _entities.Set<Model>().AsQueryable<Model>();

            var columnsToInclude = (IncludColumn ?? "").Split('|').Where(t => !string.IsNullOrEmpty(t)).ToList();
            foreach (var col in columnsToInclude)
            {
                if (!string.IsNullOrEmpty(col))
                    data = data.Include(col);
            }

            if (string.IsNullOrEmpty(OrderBy.Trim()))
            {

                    var property = typeof(Model).GetProperties()
              .FirstOrDefault(p => p.GetCustomAttributes(false)
             .Any(a => a.GetType() == typeof(KeyAttribute)));
                    if (property != null)
                        return data.OrderBy(property.Name + " desc");

                return data;
                }

            //return _entities.Set<Model>().OrderBy(OrderBy);


            //  return data.OrderBy(OrderBy.Trim());
            else
            {

                string[] orderlist = OrderBy.Split(' ');
                if (orderlist.Count() != 0)
                {

                    var listOfFieldNames = typeof(Model).GetProperties().Select(f => f.Name).ToList();

                    if (listOfFieldNames.Contains(orderlist[0]))
                        return data.OrderBy(OrderBy);
                    else
                    {

                        var property = typeof(Model).GetProperties()
            .FirstOrDefault(p => p.GetCustomAttributes(false)
           .Any(a => a.GetType() == typeof(KeyAttribute)));
                        if (property != null)
                            return data.OrderBy(property != null?property.Name + " desc":"");
                        return data;
                    }
                }
                else
                {
                    return data;
                }
            }
        }

        public virtual IEnumerable<ViewModel> QueryablResult(IQueryable<Model> query)
        {
            var results = query.Select(t => t).AsEnumerable();
            return Mapper.Map<IEnumerable<Model>, IEnumerable<ViewModel>>(query);
        }

        public ViewModel Get(int id)
        {
            Model query = _entities.Set<Model>().Find(id);
            var results = Mapper.Map<Model, ViewModel>(query);
            return results;
        }
        public ViewModel Get(long id)
        {
            Model query = _entities.Set<Model>().Find(id);
            var results = Mapper.Map<Model, ViewModel>(query);
            return results;
        }

        public List<ViewModel> Get(Expression<Func<Model, bool>> exp, string OrderBy = "")
        {
            var _entity = _entities.Set<Model>().Where(exp);

            if (string.IsNullOrEmpty(OrderBy))
                return Mapper.Map<IEnumerable<Model>, IEnumerable<ViewModel>>(_entity).ToList();

            return Mapper.Map<IEnumerable<Model>, IEnumerable<ViewModel>>(_entity.OrderBy(OrderBy)).ToList();
        }

        public ViewModel Add(ViewModel entity)
        {
            var CreatedOn = entity.GetType().GetProperty("CreatedOn");
            if (CreatedOn != null)
                CreatedOn.SetValue(entity, DateTime.Now);

            var model = Mapper.Map<ViewModel, Model>(entity);
            _entities.Set<Model>().Add(model);

            _entities.SaveChanges();
            var result = Mapper.Map<Model, ViewModel>(model, entity);


            logingrecord(result, "Insert");



            return result;
        }

        public List<ViewModel> AddRange(List<ViewModel> entities)
        {
            var model = Mapper.Map<List<ViewModel>, List<Model>>(entities);
            _entities.Set<Model>().AddRange(model);

            _entities.SaveChanges();

            var resutlt = Mapper.Map<List<Model>, List<ViewModel>>(model, entities);
            foreach (ViewModel i in resutlt)
            {
                logingrecord(i, "Insert");
            }
            return resutlt;
        }

        public virtual bool DeleteRange(Expression<Func<Model, bool>> predicate)
        {
            var dbSet = _entities.Set<Model>();
            dbSet.RemoveRange(dbSet.Where(predicate));

            // logingrecord(i, "Insert");

            return _entities.SaveChanges() > 0;
        }

        public virtual bool DeleteRange(IEnumerable<int> Ids)
        {
            foreach (var item in Ids)
            {
                Delete(item);
            }
            return _entities.SaveChanges() > 0;
        }

        public virtual bool Delete(string id)
        {
            var _entity = _entities.Set<Model>().Find(id);
            if (_entity != null)
                _entities.Set<Model>().Remove(_entity);


            return _entities.SaveChanges() > 0;
        }
        public virtual bool Delete(ViewModel _entity)
        {
            var model = Mapper.Map<ViewModel, Model>(_entity);
            bool result = false;
            if (model != null)
            {
                _entities.Set<Model>().Remove(model);
            }
            return result;
        }

        public virtual bool Delete(int id)
        {
            try
            {
                var _entity = _entities.Set<Model>().Find(id);
                if (_entity != null)
                {

                    _entities.Set<Model>().Remove(_entity);
                }
                int result = _entities.SaveChanges();

                logingrecord(_entity, "Delete");
                return result > 0;
            }
            catch (Exception e)
            {
                //    if (e is DbEntityValidationException)
                //        ExceptionLogHelper.LogValidationError((DbEntityValidationException)e);

                throw new Exception(e.Message, e.InnerException);
            }
        }

        public virtual ViewModel Update(ViewModel entity, string key = "ID")
        {
            var model = Mapper.Map<ViewModel, Model>(entity);
            var type = entity.GetType();
            var property = type.GetProperty(key);

            int _value = -1;

            int.TryParse(property.GetValue(entity).ToString(), out _value);

            Model _entity = null;

            if (_value != 0)
            {
                _entity = _entities.Set<Model>().Find(int.Parse(property.GetValue(entity).ToString()));
            }
            else
            {
                _entity = _entities.Set<Model>().Find(property.GetValue(entity).ToString());
            }
            
            _entities.Entry(_entity).State = System.Data.Entity.EntityState.Modified;
            var ddd = _entities.Entry(_entity).State;
            _entities.Entry(_entity).CurrentValues.SetValues(entity);

            _entities.SaveChanges();


            var result = Mapper.Map<Model, ViewModel>(_entity, entity);
            logingrecord(result, "Update");
            return result;

        }


        public void logingrecord(ViewModel entity, string action)
        {

            if (!(entity.GetType().Name.ToLower().Trim().Equals("transactionslogviewmodel") || entity.GetType().Name.ToLower().Trim().Equals("errorlogviewmodel")))
            {
                try
                {
                    var keyName = entity.GetType().GetProperties().FirstOrDefault(x => x.Name.ToLower().Equals("id")) != null ? entity.GetType().GetProperties().FirstOrDefault(x => x.Name.ToLower().Equals("id")).Name : entity.GetType().GetProperties().FirstOrDefault(x => x.Name.ToLower().LastIndexOf("id") > 0) != null ? entity.GetType().GetProperties().FirstOrDefault(x => x.Name.ToLower().LastIndexOf("id") > 0).Name : entity.GetType().GetProperties().FirstOrDefault().Name;

                    var recored = "";
                    if (keyName != null)
                        recored = entity.GetType().GetProperty(keyName).GetValue(entity).ToString();
                    string userid = null;
                    if (HttpContext.Current.User.Identity.IsAuthenticated)
                        userid = new win.obs.business.Managers.AspNetUsersManager().GetByEmail(HttpContext.Current.User.Identity.Name).Id;

                    if (!string.IsNullOrEmpty(userid))
                    {
                        var t = new TransactionsLog()
                        {
                            DateTime = DateTime.Now,
                            Model = entity.GetType().Name,
                            Action = action,
                            UserID = userid,
                            RecordID = recored
                        };

                        _entities.TransactionsLogs.Add(t);
                        _entities.SaveChanges();
                    }
                }
                catch (Exception ex) { string exstring = ex.ToString(); }
            }
        }
        public void logingrecord(Model entity, string action)
        {
            string entityname = entity.GetType().Name;
            if (!(entity.GetType().Name.ToLower().Trim().Contains("transactionslog") || entity.GetType().Name.ToLower().Trim().Contains("errorlog")))
            {

                try
                {
                    var keyName = entity.GetType().GetProperties().FirstOrDefault(x => x.Name.ToLower().Equals("id")) != null ? entity.GetType().GetProperties().FirstOrDefault(x => x.Name.ToLower().Equals("id")).Name : entity.GetType().GetProperties().FirstOrDefault(x => x.Name.ToLower().LastIndexOf("id") > 0) != null ? entity.GetType().GetProperties().FirstOrDefault(x => x.Name.ToLower().LastIndexOf("id") > 0).Name : entity.GetType().GetProperties().FirstOrDefault().Name;

                    var recored = "";
                    if (keyName != null)
                        recored = entity.GetType().GetProperty(keyName).GetValue(entity).ToString();
                    string userid = "";
                    if (HttpContext.Current.User.Identity.IsAuthenticated)
                        userid = new win.obs.business.Managers.AspNetUsersManager().GetByEmail(HttpContext.Current.User.Identity.Name).Id;
                    var t = new TransactionsLog()
                    {
                        DateTime = DateTime.Now,
                        Model = entity.GetType().Name,
                        Action = action,
                        UserID = userid,
                        RecordID = recored
                    };

                    _entities.TransactionsLogs.Add(t);
                    _entities.SaveChanges();
                }
                catch (Exception ex) { }
            }
        }
      
      
        //public static async System.Threading.Tasks.Task<object[]> ExecuteStoredProcedureAsync(string SPName, List<object> parameters = null, List<ObjectParameter> objectParameters = null)
        //{
        //    using (var db = new obsDB())
        //    {
        //        object[] result = new object[2];

        //        var method = db.GetType().GetMethod(SPName);

        //        if (parameters == null)
        //            parameters = new List<object>();

        //        if (objectParameters != null)
        //        {
        //            foreach (var item in objectParameters)
        //            {
        //                parameters.Add(item);
        //            }

        //            method.Invoke(db, parameters.ToArray());

        //            foreach (var item in parameters)
        //            {
        //                if (item is ObjectParameter)
        //                {
        //                    var obj = item as ObjectParameter;
        //                    objectParameters.ElementAt(objectParameters.FindIndex(t => t.Name == obj.Name)).Value = obj.Value;
        //                }
        //            }

        //            result[1] = objectParameters;
        //        }
        //        else
        //        {
        //            IEnumerable results = (IEnumerable)method.Invoke(db, parameters.ToArray());
        //            var xx = await results.AsQueryable().ToListAsync();
        //            //result[0] = results.AsQueryable().ProjectTo<ViewModel>(MapperHelper.IMapper.ConfigurationProvider).ToList();
        //            result[0] = AutoMapper.Mapper.DynamicMap<IEnumerable<ViewModel>>(results);

        //            //= Mapper.Map<List<ViewModel>>(results.AsQueryable());
        //        }

        //        return result;
        //    }
        //}

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _entities.Dispose();
                }
            }
            this.disposed = true;
        }
   

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

       
    }

}
