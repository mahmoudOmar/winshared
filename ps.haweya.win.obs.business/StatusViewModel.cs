using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class StatusViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int ID { get; set; }
    
        [MaxLength(250,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string Name { get; set; }
    
    		
        public Nullable<int> SectionID { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string Class { get; set; }
    }
}
