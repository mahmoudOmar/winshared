using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class OBSBidDetailViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public long BidId { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public long ProductId { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public double BidPrice { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int BidCount { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public bool IsSold { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int AddedBy { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public System.DateTime AddedOn { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string UserDetailsId { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public bool IsShipment { get; set; }
    
    		
        public Nullable<bool> NeedInspection { get; set; }
    
    		
        public Nullable<bool> IsInspect { get; set; }
    
    		
        public Nullable<double> InspectionValue { get; set; }
    
    		
        public string InspictionReciptCopy { get; set; }
    
    		
        public Nullable<System.DateTime> InspictionReciptCopyTime { get; set; }
    
    		
        public Nullable<System.DateTime> ApprovedInspictionReciptCopyTime { get; set; }
    
    		
        public Nullable<bool> ApprovedInspictionReciptCopy { get; set; }
    
    		
        public Nullable<int> StatusID { get; set; }
    
    		
        public string PaymentRceiptCopy { get; set; }
    
    		
        public Nullable<System.DateTime> PaymentRceiptCopyTime { get; set; }
    
    		
        public Nullable<System.DateTime> ApprovedPaymentRceiptCopyTime { get; set; }
    
    		
        public Nullable<bool> ApprovedPaymentReciptCopy { get; set; }
    
    		
        public string PercentRceiptCopy { get; set; }
    
    		
        public Nullable<System.DateTime> PercentRceiptCopyTime { get; set; }
    
    		
        public Nullable<System.DateTime> ApprovedPercentRceiptCopyTime { get; set; }
    
    		
        public Nullable<bool> ApprovedPercentReciptCopy { get; set; }
    
    		
        public Nullable<int> CurrencyId { get; set; }
    
    		
        public Nullable<double> CurrencyRate { get; set; }
    
    		
        public Nullable<double> MainCurrencyValue { get; set; }
    
    		
        public string BidOffer { get; set; }
    
    		
        public string OthersFiles { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string VatText { get; set; }
    
    		
        public Nullable<double> VatValue { get; set; }
    }
}
