using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class AdvertismentBanerViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int Id { get; set; }
    
        [MaxLength(250,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string Title { get; set; }
    
    		
        public string Link { get; set; }
    
    		
        public string Photo { get; set; }
    
    		
        public Nullable<System.DateTime> InsertDate { get; set; }
    
    		
        public Nullable<bool> IsActive { get; set; }
    
    		
        public Nullable<int> OrderNo { get; set; }
    }
}
