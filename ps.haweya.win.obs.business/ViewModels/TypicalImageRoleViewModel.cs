﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
     public partial class TypicalImageRoleViewModel
    {

        public  OBSCommodityViewModel OBSCommodity { get; set; }
        public OBSDisciplineViewModel OBSDiscipline { get; set; }
        public OBSProductCategoryViewModel OBSProductCategory { get; set; }
        public OBSSubCommodityViewModel OBSSubCommodity { get; set; }
    }
}
