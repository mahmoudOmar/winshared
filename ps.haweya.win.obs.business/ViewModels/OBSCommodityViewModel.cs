﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
    public partial class OBSCommodityViewModel
    {

        public  OBSProductCategoryViewModel OBSProductCategory { get; set; }
        
        public  List<OBSSubCommodityViewModel> OBSSubCommodities { get; set; }
    }
}
