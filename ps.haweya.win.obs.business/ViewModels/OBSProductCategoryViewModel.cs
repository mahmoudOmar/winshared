﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
    public partial class OBSProductCategoryViewModel
    {
        public OBSProductCategoryViewModel Parent { get; set; }
        public  List<OBSCommodityViewModel> OBSCommodities { get; set; }
       
        public List<OBSSubCommodityViewModel> OBSSubCommodities { get; set; }
    }
}
