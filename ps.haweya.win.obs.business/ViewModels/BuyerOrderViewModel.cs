﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
    public partial class BuyerOrderViewModel
    {
        public  ICollection<CartItemViewModel> CartItems { get; set; }
        public InvoiceOrderDataViewModel InvoiceOrderData { get; set; }
        public  ShippingDataViewModel ShippingData { get; set; }
        public StatusViewModel Status { get; set; }
        public  AspNetUserViewModel AspNetUser { get; set; }
        public  ICollection<OrderActionViewModel> OrderActions { get; set; }
        
        public  ICollection<OrderHistoryViewModel> OrderHistories { get; set; }
        public  CurrencyViewModel Currency { get; set; }
    }
}
