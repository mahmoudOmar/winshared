﻿using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using ps.haweya.win.obs.

namespace ps.haweya.win.obs.business
{
    public partial class ProductInformationViewModel
    {

        public List<ProductDetailViewModel> ProductDetails { get; set; }
        public List<OBSBidDetailViewModel> OBSBidDetails { get; set; }
        public OBSManufacturerViewModel OBSManufacturer { get; set; }
        public OBSCommodityViewModel OBSCommodity { get; set; }
        public OBSDisciplineViewModel OBSDiscipline { get; set; }
        public OBSSubCommodityViewModel OBSSubCommodity { get; set; }
        public OBSUOMViewModel OBSUOM { get; set; }
        public ICollection<UserCartViewModel> UserCarts { get; set; }

        public ICollection<CartItemViewModel> CartItems { get; set; }
        public OBSProductCategoryViewModel OBSProductCategory { get; set; }
        public string Images { get; set; }

        public string Documents { get; set; }
        public string DocumentTitle { get; set; }
      
        public string[] ProductImg
        {
            get
            {
                if (!string.IsNullOrEmpty(this.ProductImage))
                {

                    string[] stringSeparators = new string[] { "(&)" };
                    return this.ProductImage.Split(stringSeparators, StringSplitOptions.None)
                                            .Where(t=>!string.IsNullOrEmpty(t))
                                            .Select(t=> {
                                                return Helper.IMAGE_PATH +"/"+ t;
                                            }).ToArray();
                }

                var result = CacheManager.ProductsImageRoles.ToList();

                if (Category != 0)
                {
                    result = result.Where(t => t.CategoryID == Category).ToList();
                }

                if (DisciplineId != 0)
                {
                    result = result.Where(t => t.DisciplineID == DisciplineId).ToList();
                }

                if (CommodityId != 0)
                {
                    result = result.Where(t => t.CommodityID == CommodityId).ToList();
                }

                if (SubCommodityId != 0)
                {
                    result = result.Where(t => t.SubCommodityID == SubCommodityId).ToList();
                }

                return new string[] { Helper.IMAGE_PATH + "/" + (result.Count > 0 ? result.FirstOrDefault().ImageFile : "img_not_available.png") };

            }

        }


        public CountryViewModel Country { get; set; }
        public StateViewModel State { get; set; }
        public CityViewModel City { get; set; }

      //  public double UnitPrice { get { return Math.Ceiling(this._price); } set { this._price = value; } }
    }
}
