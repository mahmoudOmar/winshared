﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
  public partial  class OBSPurchaseDetailViewModel
    {


        public ProductInformationViewModel ProductInformation { get; set; }
      
        public ICollection<OBSPurchaseLiveTrackingViewModel> OBSPurchaseLiveTrackings { get; set; }
        public OBSUserDetailViewModel OBSUserDetail { get; set; }
    }
}
