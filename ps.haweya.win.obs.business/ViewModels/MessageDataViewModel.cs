﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.ViewModels
{
    public class MessageDataViewModel
    {
        public string ToEmailAddress { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Html { get; set; }
    }
}
