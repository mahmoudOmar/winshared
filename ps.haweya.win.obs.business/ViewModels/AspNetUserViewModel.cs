﻿using ps.haweya.win.obs.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
    public partial class AspNetUserViewModel
    {
        public  ICollection<AspNetRoleViewModel> AspNetRoles { get; set; }
        public  ICollection<BuyerOrderViewModel> BuyerOrders { get; set; }
        public  ICollection<AspNetUserClaimViewModel> AspNetUserClaims { get; set; }
        public int Frequency { get; set; }
        public List<int> CountryPreferences { get; set; }
        public List<int> CategoriesPreferences { get; set; }
        public List<int> ManufacturersPreferences { get; set; }
        public List<UserReportViewModel> UserReports { get; set; }
        public List<OBSProductCategoryViewModel> OBSProductCategories { get; set; }
        public List<OBSManufacturerViewModel> OBSManufacturers { get; set; }
        public List<UserPreferedCategoryViewModel> UserPreferedCategories { get; set; }
        public List<UserPreferedCountryViewModel> UserPreferedCountries { get; set; }
        public List<UserPreferedManufacturerViewModel> UserPreferedManufacturers { get; set; }
        public  List<SellerViewModel> Sellers { get; set; }
        public  List<OBSVendorDetailViewModel> OBSVendorDetails { get; set; }

        public List<MaterialPickupLocationSellerViewModel> MaterialPickupLocationSellers { get; set; }

        public List<ShippingDataViewModel> ShippingDatas { get; set; }

        //public CountryViewModel Country { get; set; }
        //public StateViewModel State { get; set; }
        //public CityViewModel City  { get; set; }

    }
}
