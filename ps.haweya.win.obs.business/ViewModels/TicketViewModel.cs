﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
    public partial class TicketViewModel
    {
        public string StatusLabel { get; set; }
        public List<TicketRowViewModel> TicketRows { get; set; }

    }
}
