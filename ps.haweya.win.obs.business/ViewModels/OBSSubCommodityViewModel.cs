﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
   public partial class OBSSubCommodityViewModel
    {
        public OBSCommodityViewModel OBSCommodity { get; set; }
        public OBSProductCategoryViewModel OBSProductCategory { get; set; }
    }
}
