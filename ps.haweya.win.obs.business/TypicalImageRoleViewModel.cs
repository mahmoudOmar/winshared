using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class TypicalImageRoleViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int ID { get; set; }
    
    		
        public Nullable<int> CategoryID { get; set; }
    
    		
        public Nullable<int> DisciplineID { get; set; }
    
    		
        public Nullable<int> CommodityID { get; set; }
    
    		
        public Nullable<int> SubCommodityID { get; set; }
    
        [MaxLength(250,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string ImageFile { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string AddBy { get; set; }
    
    		
        public Nullable<System.DateTime> AddOn { get; set; }
    
    		
        public Nullable<System.DateTime> LastUpdate { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string UpdatedBy { get; set; }
    
    		
        public Nullable<bool> IsRun { get; set; }
    }
}
