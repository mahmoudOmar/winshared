using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class ChangePasswordRandomLinkViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int ID { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string UserID { get; set; }
    
    		
        public string Link { get; set; }
    
    		
        public Nullable<bool> isValid { get; set; }
    
    		
        public Nullable<System.DateTime> MaxValidDateTime { get; set; }
    
    		
        public Nullable<bool> IsUsed { get; set; }
    }
}
