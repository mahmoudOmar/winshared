using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class ErrorLogViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int ID { get; set; }
    
        [MaxLength(250,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string Titel { get; set; }
    
    		
        public Nullable<System.DateTime> InsertDate { get; set; }
    
    		
        public string Url { get; set; }
    
    		
        public string Details { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string IpAddress { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string UserID { get; set; }
    
    		
        public string StackTrace { get; set; }
    
    		
        public string TargetSite { get; set; }
    }
}
