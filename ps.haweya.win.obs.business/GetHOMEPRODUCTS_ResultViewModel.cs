using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    public partial class GetHOMEPRODUCTS_ResultViewModel
    {
        public long ProductId { get; set; }
        public string ProductTitle { get; set; }
        public string ProductImage { get; set; }
        public string CategoryName { get; set; }
        public string PurchaseTimes { get; set; }
        public Nullable<int> bidscount { get; set; }
        public string Plabel { get; set; }
        public string PricingType { get; set; }
        public int BidCount { get; set; }
        public int AvailableQuantity { get; set; }
        public string PriceLabel { get; set; }
        public Nullable<double> TotalPrice { get; set; }
        public double UnitPrice { get; set; }
        public string ProductLocation { get; set; }
        public string Purchasebtn { get; set; }
        public Nullable<double> lat { get; set; }
        public Nullable<double> lon { get; set; }
        public Nullable<System.DateTime> BidStartDate { get; set; }
        public Nullable<System.DateTime> DeadLine { get; set; }
        public bool IsSold { get; set; }
        public Nullable<int> SoldBidPrice { get; set; }
        public Nullable<int> ShowBidAmmount { get; set; }
    }
}
