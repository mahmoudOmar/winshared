﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
    public class BaseViewModel
    {
    }
    public class ManufacturerDetails
    {
        public int ManufacturersId { get; set; }
        public string ManufacturersName { get; set; }
        public int ProductCount { get; set; }
    }

    public class ProductHomeViewModel
    {
      public String ProductImage { get; set; }
        public String CategoryName { get; set; }
        public int ProductId { get; set; }
        public String ProductTitle { get; set; }
        public String PurchaseTimes { get; set; }
        public String bidscount { get; set; }
        public String Plabel { get; set; }
        public double? AvailableQuantity { get; set; }
        public String PriceLabel { get; set; }
        public double? TotalPrice { get; set; }
        public String ProductLocation { get; set; }
        public String Purchasebtn { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }

        public Nullable<System.DateTime> BidStartDate { get; set; }
        public System.DateTime? DeadLine { get; set; }
        public string PricingType { get; set; }
        public bool IsBidSold { get; set; }
        public double? SoldBidPrice { get; set; }
        public bool? ShowBidAmmount { get; set; }
    }
    public class ContactViewModel
    {
        [Required(ErrorMessage = "Plese Enter Your Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Plese Enter Your Email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please Enter Valid Email Address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Plese Enter Your Phone")]
        public string phone { get; set; }
        [Required(ErrorMessage = "Plese Enter Your Message")]
        public string Message { get; set; }
    }
    public class SellerAccountVerification
    {
        [Required]
        [Range(1, 2, ErrorMessage = "Please Select Account Type")]
        public int Typeid { get; set; }


        //[Range(1, 2, ErrorMessage = "Please Select Account Type")]
        public int? BusinessTypeid { get; set; }
        public string DocumentID { get; set; }
        [Required]
        public int agreement { get; set; }
    }
    public class LoginViewModel
    {
        [Required(ErrorMessage = "This field is required")]

        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public string returnurl { get; set; }
    }
    public class RegisterViewModel
    {

        [Required]

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Please Select Your Country")]
        public int Counrty { get; set; }
        [Required(ErrorMessage = "Please Select Your State")]
        public int State { get; set; }
        [Required(ErrorMessage = "Please Select Your City")]
        public int City { get; set; }
        [Required]
        public string POBox { get; set; }
        [Required]
        public string Mobile { get; set; }
        [Required]
        //[Range(typeof(bool), "True", "True", ErrorMessage = "You Must Agree on the Terms & Policies!")]
        public bool Agreepolicies { get; set; }
    }

    public class VerifyViewModel
    {
        [Required]
        [MinLength(6), MaxLength(6)]
        public string OTP { get; set; }
        [Required]
        public string UserId { get; set; }

        public string TypeID { get; set; }
    }

    public class VendorNameSellerViewModel
    {
        [Required]
        public string Name { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class PagingViewModel {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int RecordsCount { get; set; }
    }

    public class CategoryViewModelwithCount {
        
public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int productcount { get; set; }

    }
    public class AttachmentsModel
    {
        public long AttachmentID { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public long size { get; set; }
    }
    public class BidersLocations
    {
        public string Coutry { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public DateTime time { get; set; }
        public double price { get; set; }
    }
    public class NewsLayout
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Brief { get; set; }
        public DateTime? AddOn { get; set; }
    }
    public class ProductMapData
    {
        public double lat { get; set; }
        public double lon { get; set; }
        public string title { get; set; }
        public string html { get; set; }
        public string icon { get; set; }

        public bool show_infowindow { get; set; }
    }
    public class bidtableadminhome { 
    public long Key { get; set; }
        public string ProductTitle { get; set; }
        public int Value { get; set; }
        public double LastBid { get; set; }
        
    }
    public class TokenResponseLinkedin
    {
        [JsonProperty(PropertyName = "access_token")]
        public string Access_token { get; set; }

        [JsonProperty(PropertyName = "expires_in")]
        public int Expires_in { get; set; }

    }


    public class UserInfoLinkedoin
    {


        [JsonProperty(PropertyName = "emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }
        [JsonProperty(PropertyName = "countrycode")]
        public string Countrycode { get; set; }

      
    }
    public class GmailToken
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public long ExpiresIn { get; set; }

        [JsonProperty("id_token")]
        public string IdToken { get; set; }
    }
    public partial class UserProfile_Google
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("verified_email")]
        public bool VerifiedEmail { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("given_name")]
        public string GivenName { get; set; }

        [JsonProperty("family_name")]
        public string FamilyName { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("picture")]
        public string Picture { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }
    }
}

