using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class CartItemViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int ID { get; set; }
    
    		
        public Nullable<int> CartID { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string AspNetUserId { get; set; }
    
    		
        public Nullable<System.DateTime> InsertDate { get; set; }
    
    		
        public Nullable<int> Qty { get; set; }
    
    		
        public Nullable<long> ProductID { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public bool IsBought { get; set; }
    
    		
        public Nullable<int> OrderID { get; set; }
    
    		
        public Nullable<bool> NeedInspection { get; set; }
    
    		
        public Nullable<bool> NeesShipment { get; set; }
    
    		
        public Nullable<double> InspectionValue { get; set; }
    
    		
        public Nullable<double> ShippingValue { get; set; }
    }
}
