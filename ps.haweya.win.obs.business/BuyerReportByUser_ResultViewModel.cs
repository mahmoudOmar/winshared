using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    public partial class BuyerReportByUser_ResultViewModel
    {
        public long ProductId { get; set; }
        public string ProductTitle { get; set; }
        public string ProductLocation { get; set; }
        public string Quantity { get; set; }
        public System.DateTime DeadLine { get; set; }
        public string ProductImage { get; set; }
        public int BidCount { get; set; }
        public double Price { get; set; }
        public int Category { get; set; }
        public int ProductBrands { get; set; }
        public System.DateTime CreateDate { get; set; }
        public double BidDifference { get; set; }
        public string PricingType { get; set; }
        public string Username { get; set; }
        public bool isActive { get; set; }
        public bool IsWinActive { get; set; }
        public string SuggestionByWin { get; set; }
        public int AvailableQuantity { get; set; }
        public int DeadLineInDays { get; set; }
        public bool isPrivate { get; set; }
        public bool IsSold { get; set; }
        public double ShipmentCharges { get; set; }
        public bool IsShipment { get; set; }
        public bool IsInspect { get; set; }
        public string OtherCategory { get; set; }
        public string OtherManufacturer { get; set; }
        public Nullable<System.DateTime> ExtendDeadline { get; set; }
        public bool isExtendDeadline { get; set; }
        public bool IsDelete { get; set; }
        public bool IsBulkUpload { get; set; }
        public int DisciplineId { get; set; }
        public int CommodityId { get; set; }
        public int SubCommodityId { get; set; }
        public Nullable<System.DateTime> ManufacturingDate { get; set; }
        public int UOMId { get; set; }
        public string EndUser { get; set; }
        public string SupplierName { get; set; }
        public string CertificatesOrDataSheet { get; set; }
        public string WINAssement { get; set; }
        public string WINAssementby { get; set; }
        public string Repairable { get; set; }
        public string UsedEquipmentOrApplication { get; set; }
        public double Age { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public string SellerName { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public double MarketPrice { get; set; }
        public string SellerMaterialId { get; set; }
        public Nullable<System.DateTime> PreviousDeadline { get; set; }
        public bool IsExtendOverwrite { get; set; }
        public string BatchNumber { get; set; }
        public string SecurityDeposit { get; set; }
        public string ProductDocument { get; set; }
        public string CategoryName { get; set; }
    }
}
