using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    public partial class Conditiontohome_ResultViewModel
    {
        public string Condition { get; set; }
        public Nullable<int> ProductCount { get; set; }
    }
}
