﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business.PageModel
{
    public class NewListingItemPageModel
    {
        public IEnumerable<OBSProductCategoryViewModel> Categories { get; set; }
        public IEnumerable<OBSManufacturerViewModel> Manufacturers { get; set; }
        public IEnumerable<CountryViewModel> Countries { get; set; }
        public IEnumerable<CompanyViewModel> Companies { get; set; }
        public IEnumerable<OBSSubCommodityViewModel> SubCommodities { get; set; }
        public ProductInformationViewModel ProductInformationViewModel { get; set; }

    }
}
