using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class BuyerOrderViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int ID { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string UserID { get; set; }
    
    		
        public Nullable<System.DateTime> InsertDate { get; set; }
    
    		
        public Nullable<int> ShippingDataID { get; set; }
    
    		
        public Nullable<int> InvoiceDataID { get; set; }
    
    		
        public Nullable<int> VisaDataID { get; set; }
    
    		
        public Nullable<int> StageID { get; set; }
    
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public bool CloseStatus { get; set; }
    
    		
        public Nullable<double> TotalPrice { get; set; }
    
    		
        public Nullable<System.DateTime> ClosedDate { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string payment_method { get; set; }
    
    		
        public Nullable<int> StatusID { get; set; }
    
    		
        public Nullable<bool> IsNeedInspection { get; set; }
    
    		
        public Nullable<bool> IsNeedDelivery { get; set; }
    
    		
        public Nullable<bool> IsSentToAdmin { get; set; }
    
    		
        public Nullable<int> DeliveryType { get; set; }
    
    		
        public Nullable<bool> IsPaid { get; set; }
    
    		
        public string PaymentRceiptCopy { get; set; }
    
    		
        public Nullable<System.DateTime> PaidDate { get; set; }
    
    		
        public Nullable<bool> AdminConfirmed { get; set; }
    
    		
        public Nullable<System.DateTime> PaymentRceiptCopyTime { get; set; }
    
    		
        public Nullable<System.DateTime> ApprovedPaymentRceiptCopyTime { get; set; }
    
    		
        public Nullable<bool> ApprovedPaymentReciptCopy { get; set; }
    
    		
        public string InspictionReciptCopy { get; set; }
    
    		
        public Nullable<System.DateTime> InspictionReciptCopyTime { get; set; }
    
    		
        public Nullable<System.DateTime> ApprovedInspictionReciptCopyTime { get; set; }
    
    		
        public Nullable<bool> ApprovedInspictionReciptCopy { get; set; }
    
    		
        public string DeliveryReciptCopy { get; set; }
    
    		
        public Nullable<System.DateTime> DeliveryReciptCopyTime { get; set; }
    
    		
        public Nullable<bool> ApprovedDeliveryReciptCopy { get; set; }
    
    		
        public Nullable<System.DateTime> ApprovedDeliveryReciptCopyTime { get; set; }
    
    		
        public Nullable<int> CurrencyId { get; set; }
    
    		
        public Nullable<double> CurrencyRate { get; set; }
    
        [MaxLength(50,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string VatText { get; set; }
    
    		
        public Nullable<double> VatValue { get; set; }
    
    		
        public string ShipmentIdentificationNumber { get; set; }
    
    		
        public string LabelImage { get; set; }
    
    		
        public string TrakingNOs { get; set; }
    }
}
