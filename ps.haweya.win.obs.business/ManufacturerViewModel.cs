using ps.haweya.win.obs.viewModels.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ps.haweya.win.obs.business
{
    
    public partial class ManufacturerViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Validation))]
    		
        public int Id { get; set; }
    
    		
        public string Title { get; set; }
    
        [MaxLength(250,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string ImageFile { get; set; }
    
    		
        public Nullable<bool> IsActive { get; set; }
    
    		
        public Nullable<bool> IsMain { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string AddBy { get; set; }
    
    		
        public Nullable<System.DateTime> AddOn { get; set; }
    
    		
        public Nullable<System.DateTime> LastUpdate { get; set; }
    
        [MaxLength(128,ErrorMessageResourceName = "MaxLengthError", ErrorMessageResourceType = typeof(Validation))]
    		
        public string UpdatedBy { get; set; }
    }
}
