﻿
using AutoMapper;
using ps.haweya.win.obs.models;
//using ps.haweya.win.obs.;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.business
{
    public class MapperHelper
    {

        public static void RegisterMapper()
        {

            Mapper.CreateMap<ProductInformation, ProductInformationViewModel>()
                 //.ForMember(d => d.ProductDetails, opt => opt.Ignore())
                 //.ForMember(d => d.OBSBidDetails, opt => opt.Ignore())
                 //.ForMember(d => d.OBSManufacturer, opt => opt.Ignore())
                 //.ForMember(d => d.OBSCommodity, opt => opt.Ignore())
                 //.ForMember(d => d.OBSDiscipline, opt => opt.Ignore())
                 //.ForMember(d => d.OBSSubCommodity, opt => opt.Ignore())
                 //.ForMember(d => d.OBSUOM, opt => opt.Ignore())
                 //.ForMember(d => d.UserCarts, opt => opt.Ignore())
                 //.ForMember(d => d.CartItems, opt => opt.Ignore())
                 //.ForMember(d => d.OBSProductCategory, opt => opt.Ignore())
                 //.ForMember(d => d.ProductImg, opt => opt.Ignore())
                 .ReverseMap().MaxDepth(1);

            Mapper.CreateMap<ProductDetail, ProductDetailViewModel>().ReverseMap();
            Mapper.CreateMap<OBSProductCategory, OBSProductCategoryViewModel>().ReverseMap();
            Mapper.CreateMap<OBSManufacturer, OBSManufacturerViewModel>().ReverseMap();
            Mapper.CreateMap<OBSBidDetail, OBSBidDetailViewModel>().ReverseMap();
            Mapper.CreateMap<OBSNewsFeed, OBSNewsFeedViewModel>().ReverseMap();
            Mapper.CreateMap<NotifyUser, NotifyUserViewModel>().ReverseMap();
            Mapper.CreateMap<Buyer, BuyerViewModel>().ReverseMap();
            Mapper.CreateMap<AspNetUser, AspNetUserViewModel>().ReverseMap();
            Mapper.CreateMap<OBSCommodity, OBSCommodityViewModel>().ReverseMap();
            Mapper.CreateMap<OBSDiscipline, OBSDisciplineViewModel>().ReverseMap();
            Mapper.CreateMap<OBSSubCommodity, OBSSubCommodityViewModel>().ReverseMap();
            Mapper.CreateMap<OBSUOM, OBSUOMViewModel>().ReverseMap();
            Mapper.CreateMap<ShippingData, ShippingDataViewModel>().ReverseMap();
            Mapper.CreateMap<AspNetRole, AspNetRoleViewModel>().ReverseMap();
            Mapper.CreateMap<VerficationPOT, VerficationPOTViewModel>().ReverseMap();
            Mapper.CreateMap<Seller, SellerViewModel>().ReverseMap();
            Mapper.CreateMap<UserCart, UserCartViewModel>()
                .ForMember(dest => dest.ProductInformation, opts => opts.MapFrom(x => x.ProductInformation))
                .MaxDepth(3)
                .ReverseMap();
            Mapper.CreateMap<BuyerOrder, BuyerOrderViewModel>().ReverseMap();
            Mapper.CreateMap<C_UserCart, C_UserCartViewModel>().ReverseMap();
            Mapper.CreateMap<FavoriteItem, FavoriteItemViewModel>().ReverseMap();
            Mapper.CreateMap<CartItem, CartItemViewModel>().ReverseMap();
            Mapper.CreateMap<InvoiceOrderData, InvoiceOrderDataViewModel>().ReverseMap();
            Mapper.CreateMap<ShippingData, ShippingDataViewModel>().ReverseMap();
            Mapper.CreateMap<Status, StatusViewModel>().ReverseMap();
            Mapper.CreateMap<OrderHistory, OrderHistoryViewModel>().ReverseMap();
            Mapper.CreateMap<OrderAction, OrderActionViewModel>().ReverseMap();
            Mapper.CreateMap<OBSJobScheduler, OBSJobSchedulerViewModel>().ReverseMap();

            Mapper.CreateMap<Link, LinkViewModel>().ReverseMap();
            Mapper.CreateMap<UserLink, UserLinkViewModel>().ReverseMap();

            Mapper.CreateMap<OBSBidDetail, OBSBidDetailViewModel>().ReverseMap();
            Mapper.CreateMap<OBSBidLiveTracking, OBSBidLiveTrackingViewModel>().ReverseMap();
            Mapper.CreateMap<AspNetUserClaim, AspNetUserClaimViewModel>().ReverseMap();

            Mapper.CreateMap<MaterialPickupLocationSeller, MaterialPickupLocationSellerViewModel>().ReverseMap();
            Mapper.CreateMap<OBSVendorDetail, OBSVendorDetailViewModel>().ReverseMap();

            Mapper.CreateMap<Company, CompanyViewModel>().ReverseMap();

            Mapper.CreateMap<TypicalImageRole, TypicalImageRoleViewModel>().ReverseMap();

            Mapper.CreateMap<ErrorLog, ErrorLogViewModel>().ReverseMap();

            Mapper.CreateMap<TransactionsLog, TransactionsLogViewModel>().ReverseMap();

            Mapper.CreateMap<Ticket, TicketViewModel>().ReverseMap();
            Mapper.CreateMap<TicketRow, TicketRowViewModel>().ReverseMap();
            Mapper.CreateMap<UserReport, UserReportViewModel>().ReverseMap();
            Mapper.CreateMap<UserPreferedCategory, UserPreferedCategoryViewModel>().ReverseMap();
            Mapper.CreateMap<UserPreferedCountry, UserPreferedCountryViewModel>().ReverseMap();
            Mapper.CreateMap<UserPreferedManufacturer, UserPreferedManufacturerViewModel>().ReverseMap();
            Mapper.CreateMap<BuyerReportByUser_Result, BuyerReportByUser_ResultViewModel>().ReverseMap();
            Mapper.CreateMap<ManufacturersHomeData_Result, ManufacturersHomeData_ResultViewModel>().ReverseMap();
            Mapper.CreateMap<CategoryHomeData_Result, CategoryHomeData_ResultViewModel>().ReverseMap();
            
            Mapper.CreateMap<FullProductInformation_Result, FullProductInformation_ResultViewModel>().ReverseMap();
            Mapper.CreateMap<BidsHistory, BidsHistoryViewModel>().ReverseMap();
            Mapper.CreateMap<Conditiontohome_Result, Conditiontohome_ResultViewModel>().ReverseMap();
            Mapper.CreateMap<Setting, SettingViewModel>().ReverseMap();
            Mapper.CreateMap<BankAccount, BankAccountViewModel>().ReverseMap();
            Mapper.CreateMap<Slider, SliderViewModel>().ReverseMap();
            Mapper.CreateMap<Page, PageViewModel>().ReverseMap();
            Mapper.CreateMap<News, NewsViewModel>().ReverseMap();
            Mapper.CreateMap<Manufacturer, ManufacturerViewModel>().ReverseMap();
            Mapper.CreateMap<MenuItem, MenuItemViewModel>().ReverseMap();
            Mapper.CreateMap<ChangePasswordRandomLink, ChangePasswordRandomLinkViewModel>().ReverseMap();
            Mapper.CreateMap<AdvertismentBaner, AdvertismentBanerViewModel>().ReverseMap();
            Mapper.CreateMap<Currency, CurrencyViewModel>().ReverseMap();
            Mapper.CreateMap<Currencychanging, CurrencychangingViewModel>().ReverseMap();
            Mapper.CreateMap<VATHistory, VATHistoryViewModel>().ReverseMap();
            Mapper.CreateMap<GetHOMEPRODUCTS_Result, GetHOMEPRODUCTS_ResultViewModel>().ReverseMap();
            Mapper.CreateMap<GetSearchPRODUCTS_Result, GetSearchPRODUCTS_ResultViewModel>().ReverseMap();
            Mapper.CreateMap<News_letter_Emails, News_letter_EmailsViewModel>().ReverseMap();
        }

        //public class MapperProfile : Profile
        //{
        //    public MapperProfile()
        //    {

        //        Mapper.CreateMap<ProductInformation, ProductInformationViewModel>()
        //            //.ForMember(d => d.ProductDetails, opt => opt.Ignore())
        //            //.ForMember(d => d.OBSBidDetails, opt => opt.Ignore())
        //            //.ForMember(d => d.OBSManufacturer, opt => opt.Ignore())
        //            //.ForMember(d => d.OBSCommodity, opt => opt.Ignore())
        //            //.ForMember(d => d.OBSDiscipline, opt => opt.Ignore())
        //            //.ForMember(d => d.OBSSubCommodity, opt => opt.Ignore())
        //            //.ForMember(d => d.OBSUOM, opt => opt.Ignore())
        //            //.ForMember(d => d.UserCarts, opt => opt.Ignore())
        //            //.ForMember(d => d.CartItems, opt => opt.Ignore())
        //            //.ForMember(d => d.OBSProductCategory, opt => opt.Ignore())
        //            //.ForMember(d => d.ProductImg, opt => opt.Ignore())
        //            .ReverseMap().MaxDepth(1);

        //        Mapper.CreateMap<ProductDetail, ProductDetailViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSProductCategory, OBSProductCategoryViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSManufacturer, OBSManufacturerViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSBidDetail, OBSBidDetailViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSNewsFeed, OBSNewsFeedViewModel>().ReverseMap();
        //        Mapper.CreateMap<NotifyUser, NotifyUserViewModel>().ReverseMap();
        //        Mapper.CreateMap<Buyer, BuyerViewModel>().ReverseMap();
        //        Mapper.CreateMap<AspNetUser, AspNetUserViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSCommodity, OBSCommodityViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSDiscipline, OBSDisciplineViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSSubCommodity, OBSSubCommodityViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSUOM, OBSUOMViewModel>().ReverseMap();
        //        Mapper.CreateMap<ShippingData, ShippingDataViewModel>().ReverseMap();
        //        Mapper.CreateMap<AspNetRole, AspNetRoleViewModel>().ReverseMap();
        //        Mapper.CreateMap<VerficationPOT, VerficationPOTViewModel>().ReverseMap();
        //        Mapper.CreateMap<Seller, SellerViewModel>().ReverseMap();
        //        Mapper.CreateMap<UserCart, UserCartViewModel>()
        //            .ForMember(dest => dest.ProductInformation, opts => opts.MapFrom(x => x.ProductInformation))
        //            .MaxDepth(3)
        //            .ReverseMap();
        //        Mapper.CreateMap<BuyerOrder, BuyerOrderViewModel>().ReverseMap();
        //        Mapper.CreateMap<C_UserCart, C_UserCartViewModel>().ReverseMap();
        //        Mapper.CreateMap<FavoriteItem, FavoriteItemViewModel>().ReverseMap();
        //        Mapper.CreateMap<CartItem, CartItemViewModel>().ReverseMap();
        //        Mapper.CreateMap<InvoiceOrderData, InvoiceOrderDataViewModel>().ReverseMap();
        //        Mapper.CreateMap<ShippingData, ShippingDataViewModel>().ReverseMap();
        //        Mapper.CreateMap<Status, StatusViewModel>().ReverseMap();
        //        Mapper.CreateMap<OrderHistory, OrderHistoryViewModel>().ReverseMap();
        //        Mapper.CreateMap<OrderAction, OrderActionViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSJobScheduler, OBSJobSchedulerViewModel>().ReverseMap();

        //        Mapper.CreateMap<Link, LinkViewModel>().ReverseMap();
        //        Mapper.CreateMap<UserLink, UserLinkViewModel>().ReverseMap();

        //        Mapper.CreateMap<OBSBidDetail, OBSBidDetailViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSBidLiveTracking, OBSBidLiveTrackingViewModel>().ReverseMap();
        //        Mapper.CreateMap<AspNetUserClaim, AspNetUserClaimViewModel>().ReverseMap();

        //        Mapper.CreateMap<MaterialPickupLocationSeller, MaterialPickupLocationSellerViewModel>().ReverseMap();
        //        Mapper.CreateMap<OBSVendorDetail, OBSVendorDetailViewModel>().ReverseMap();

        //        Mapper.CreateMap<Company, CompanyViewModel>().ReverseMap();

        //        Mapper.CreateMap<TypicalImageRole, TypicalImageRoleViewModel>().ReverseMap();

        //        Mapper.CreateMap<ErrorLog, ErrorLogViewModel>().ReverseMap();

        //        Mapper.CreateMap<TransactionsLog, TransactionsLogViewModel>().ReverseMap();

        //        Mapper.CreateMap<Ticket, TicketViewModel>().ReverseMap();
        //        Mapper.CreateMap<TicketRow, TicketRowViewModel>().ReverseMap();
        //        Mapper.CreateMap<UserReport, UserReportViewModel>().ReverseMap();
        //        Mapper.CreateMap<UserPreferedCategory, UserPreferedCategoryViewModel>().ReverseMap();
        //        Mapper.CreateMap<UserPreferedCountry, UserPreferedCountryViewModel>().ReverseMap();
        //        Mapper.CreateMap<UserPreferedManufacturer, UserPreferedManufacturerViewModel>().ReverseMap();
        //        Mapper.CreateMap<BuyerReportByUser_Result, BuyerReportByUser_ResultViewModel>().ReverseMap();
        //        Mapper.CreateMap<ManufacturersHomeData_Result, ManufacturersHomeData_ResultViewModel>().ReverseMap();
        //        Mapper.CreateMap<FullProductInformation_Result, FullProductInformation_ResultViewModel>().ReverseMap();

        //    }
        //}

    }
}
