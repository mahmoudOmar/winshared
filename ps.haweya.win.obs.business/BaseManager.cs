﻿using ps.haweya.win.obs.business.Repository;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace ps.haweya.win.obs.business
{
    public abstract class BaseManager<DataModel, ViewModel> 
         where ViewModel : class
         where DataModel : class
    {

        public Repository<DataModel, ViewModel> repository;

        public BaseManager()
        {
            this.repository = new Repository<DataModel, ViewModel>();
        }

    }
}
