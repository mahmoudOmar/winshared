﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.common
{
   public static class Helper
    {
        public static  string IMAGE_PATH = "/Content/UploadData/UploadImages";
        public static DataTable ExcelToDataTable(this string path)
        {
            using (XLWorkbook workBook = new XLWorkbook(path))
            {
                //Read the first Sheet from Excel file.
                IXLWorksheet workSheet = workBook.Worksheet(1);

                //Create a new DataTable.
                DataTable dt = new DataTable();

                //Loop through the Worksheet rows.
                bool firstRow = true;
                foreach (IXLRow row in workSheet.Rows())
                {
                    //Use the first row to add columns to DataTable.
                    if (firstRow)
                    {
                        foreach (IXLCell cell in row.Cells())
                        {
                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                            {
                                dt.Columns.Add(cell.Value.ToString());
                            }
                            else
                            {
                                break;
                            }
                        }
                        firstRow = false;
                    }
                    else
                    {
                        int i = 0;
                        DataRow toInsert = dt.NewRow();
                        foreach (IXLCell cell in row.Cells(1, dt.Columns.Count))
                        {
                            try
                            {
                                toInsert[i] = cell.Value.ToString();
                            }
                            catch (Exception ex)
                            {

                            }
                            i++;
                        }
                        dt.Rows.Add(toInsert);
                    }
                }
      
                return dt;
                
            }
        }

        //public static List<T> ConvertTo<T>(this DataTable datatable) where T : new()
        //{
        //    var temp = new List<T>();
        //    try
        //    {
        //        var columnsNames = (from DataColumn dataColumn in datatable.Columns select dataColumn.ColumnName).ToList();
        //        temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => ConvertDataTable<T>(row, columnsNames));
        //        return temp;
        //    }
        //    catch
        //    {
        //        return temp;
        //    }

        //}
        public static List<T> ConvertDataTable<T>(this DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static DataTable ToDataTable<T>(this List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    try
                    {
                        values[i] = props[i].GetValue(item);
                    }
                    catch { values[i] = ""; }
                }
                table.Rows.Add(values);
            }
            return table;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        //public static T GetObject<T>(DataRow row, List<string> columnsName) where T : new()
        //{
        //    T obj = new T();
        //    try
        //    {
        //        string columnname = "";
        //        string value = "";
        //        PropertyInfo[] Properties;
        //        Properties = typeof(T).GetProperties();
        //        foreach (PropertyInfo objProperty in Properties)
        //        {
        //            columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
        //            if (!string.IsNullOrEmpty(columnname))
        //            {
        //                value = row[columnname].ToString();
        //                if (!string.IsNullOrEmpty(value))
        //                {
        //                    if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
        //                    {
        //                        value = row[columnname].ToString().Replace("$", "").Replace(",", "");
        //                        objProperty.SetValue(obj,
        //                            Convert.ChangeType(value,
        //                                Type.GetType(
        //                                    Nullable.GetUnderlyingType(
        //                                        objProperty.PropertyType).ToString())),
        //                            null);
        //                    }
        //                    else
        //                    {
        //                        value = row[columnname].ToString().Replace("%", "");
        //                        objProperty.SetValue(obj,
        //                            Convert.ChangeType(value,
        //                                Type.GetType(objProperty.PropertyType.ToString())),
        //                            null);
        //                    }
        //                }
        //            }
        //        }
        //        return obj;
        //    }
        //    catch
        //    {
        //        return obj;
        //    }
        //}

        public static double ParseToDouble(this string data)
        {
            double value;
            if (Double.TryParse(data, out value))
                return value;
            else
                return 0;

        }
        public static int ParseToInt(this string data)
        {
            int value;
            if (Int32.TryParse(data, out value))
                return value;
            else
                return 0;
        }
    }
}
