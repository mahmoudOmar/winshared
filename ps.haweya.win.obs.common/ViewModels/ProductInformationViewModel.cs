﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using ps.haweya.win.obs.

namespace ps.haweya.win.obs.viewModels
{
    public partial class ProductInformationViewModel
    {

        private string _ProductImage;
        public List<ProductDetailViewModel> ProductDetails { get; set; }
        public List<OBSBidDetailViewModel> OBSBidDetails { get; set; }
        public OBSManufacturerViewModel OBSManufacturer { get; set; }
        public OBSCommodityViewModel OBSCommodity { get; set; }
        public OBSDisciplineViewModel OBSDiscipline { get; set; }
        public OBSSubCommodityViewModel OBSSubCommodity { get; set; }
        public OBSUOMViewModel OBSUOM { get; set; }
        public ICollection<UserCartViewModel> UserCarts { get; set; }

        public ICollection<CartItemViewModel> CartItems { get; set; }
        public  OBSProductCategoryViewModel OBSProductCategory { get; set; }
        public string Images { get; set; }

        public string Documents { get; set; }
        public string DocumentTitle { get; set; }

        public string ProductImage
        {
            set
            {
                this._ProductImage = value;
              
            }
            get {

                //CacheManager.
                //value = value ?? "";
                //var countryobj = CacheManager.Countries.SingleOrDefault(x => x.name.ToLower().Equals(value.ToString().ToLower()));
                //this.Country = countryobj == null ? 0 : countryobj.id;
                return this._ProductImage;
            }
        }




    }
}
