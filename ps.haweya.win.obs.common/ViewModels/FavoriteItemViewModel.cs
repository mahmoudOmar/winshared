﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.viewModels
{
  public partial  class FavoriteItemViewModel
    {
        public  AspNetUserViewModel AspNetUser { get; set; }
        public  ProductDetailViewModel ProductDetail { get; set; }
    }
}
