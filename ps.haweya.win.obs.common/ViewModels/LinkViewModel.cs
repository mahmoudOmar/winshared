﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.viewModels
{
    public partial class LinkViewModel
    {
        public List<AspNetRoleViewModel> AspNetRoles { get; set; }
        public bool IsSelected { get; set; }
    }
}