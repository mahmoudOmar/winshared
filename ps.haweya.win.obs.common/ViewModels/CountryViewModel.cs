﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.viewModels
{
    public  partial class CountryViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string sortname { get; set; }
    }

    public partial class StateViewModel
    {
     
        public int id { get; set; }
        public string name { get; set; }
        public int country_id { get; set; }
    }

    public partial class CityViewModel
    {

        public int id { get; set; }
        public string text { get; set; }
        public int Stateid { get; set; }
    }

}
