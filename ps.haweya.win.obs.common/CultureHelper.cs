﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ps.haweya.win.obs.common
{

    public static class CultureHelper
    {

        private static readonly List<string> _expectedCultures = new List<string> { "ar", "ar-DZ", "ar-BH", "ar-EG", "ar-IQ", "ar-JO", "ar-KW", "ar-LB", "ar-LY", "ar-MA", "ar-OM", "ar-QA", "ar-SA", "ar-SY", "ar-TN", "ar-AE", "ar-YE", "en", "en-AU", "en-BZ", "en-CA", "en-029", "en-IN", "en-IE", "en-JM", "en-MY", "en-NZ", "en-PH", "en-SG", "en-ZA", "en-TT", "en-GB", "en-US", "en-ZW" };

        private static readonly List<string> _cultureSuffix = new List<string> {
            "ar",       // default
            "en"
        };

        public static Dictionary<string, string> DBSuffix = new Dictionary<string, string> { { "ar", "_Ar" }, { "en", "_En" } };


        public static string Singularize(string word)
        {
            PluralizationService ps = PluralizationService.CreateService(CultureInfo.GetCultureInfo("en-us"));
            return ps.Singularize(word);
        }

        public static bool IsRighToLeft()
        {
            return Thread.CurrentThread.CurrentCulture.TextInfo.IsRightToLeft;
        }

        public static string GetCulture(string name)
        {
            if (string.IsNullOrEmpty(name))
                return GetDefaultCulture();

            if (_expectedCultures.Where(c => c.Equals(name, StringComparison.InvariantCultureIgnoreCase)).Count() == 0)
                return GetDefaultCulture();

            if (_cultureSuffix.Where(c => c.Equals(name, StringComparison.InvariantCultureIgnoreCase)).Count() > 0)
                return name;

            var n = GetPrefixCulture(name);
            foreach (var c in _cultureSuffix)
                if (c.StartsWith(n))
                    return c;

            return GetDefaultCulture();
        }

        public static string GetDefaultCulture()
        {
            return _cultureSuffix[0]; // return Default culture
        }

        public static string GetCurrentThreadCulture()
        {
            //    var 
            return Thread.CurrentThread.CurrentCulture.Name;
        }

        public static string GetCurrentNeutralCulture()
        {
            return GetPrefixCulture(Thread.CurrentThread.CurrentCulture.Name);
        }

        public static string GetPrefixCulture(string name)
        {
            if (!name.Contains("-")) return name;
            return name.Split('-')[0];
        }

        public static string GetCultureValue(this object model, string name)
        {
            if (model == null)
            {
                return "";
            }
            Type type = model.GetType();
            string propertyName = string.Format("{0}{1}", name.Replace("get_", ""), CultureHelper.DBSuffix[CultureHelper.GetCurrentNeutralCulture()]);
            PropertyInfo value = type.GetProperty(propertyName);

            return (string)Convert.ChangeType(value.GetValue(model, null), typeof(string));
        }

        public static void SetTheardLanguage()
        {
            string lang = "ar";


            var cookie = HttpContext.Current.Request.Cookies["language"];
            if (cookie != null)
            {
                lang = cookie.Value.Equals("en") ? "en" : "ar";
            }
            else
            {
                cookie = new HttpCookie("language", "ar");
                HttpContext.Current.Response.Cookies.Add(cookie);
            }

            lang = string.Format("{0}-{1}", lang, lang.Equals("en") ? "US" : "eg");

            CultureInfo newCulture = new System.Globalization.CultureInfo(lang, false);
            newCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            newCulture.DateTimeFormat.DateSeparator = "/";
            newCulture.NumberFormat.DigitSubstitution = DigitShapes.NativeNational;

            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture = newCulture;
            Thread.CurrentThread.CurrentCulture = newCulture;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.DefaultThreadCurrentUICulture = newCulture;
        }

    }
}