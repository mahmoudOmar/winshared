﻿
using AutoMapper;
using AutoMapper.Configuration;
using ps.haweya.win.obs.models;
//using ps.haweya.win.obs.;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.common
{
    public class MapperHelper
    {

        public static IMapper IMapper { get; set; }

        public static void RegisterMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MapperProfile());
                cfg.ForAllMaps((m, n) => {
                    n.MaxDepth(1);
                });
            });

            IMapper = config.CreateMapper();
        }

        public class MapperProfile : Profile
        {
            public MapperProfile()
            {
                CreateMap<ProductInformation, ProductInformationViewModel>()
                   // .ForMember(d => d.ProductDetails, s => s.MapFrom(s => s.ProductDetails))
                    .ReverseMap();

                CreateMap<ProductDetail, ProductDetailViewModel>().ReverseMap();
                CreateMap<OBSProductCategory, OBSProductCategoryViewModel>().ReverseMap();
                CreateMap<OBSManufacturer, OBSManufacturerViewModel>().ReverseMap();
                CreateMap<OBSBidDetail, OBSBidDetailViewModel>().ReverseMap();
                CreateMap<OBSNewsFeed, OBSNewsFeedViewModel>().ReverseMap();
                CreateMap<NotifyUser, NotifyUserViewModel>().ReverseMap();
                CreateMap<Buyer, BuyerViewModel>().ReverseMap();
                CreateMap<AspNetUser, AspNetUserViewModel>().ReverseMap();
                CreateMap<OBSCommodity, OBSCommodityViewModel>().ReverseMap();
                CreateMap<OBSDiscipline, OBSDisciplineViewModel>().ReverseMap();
                CreateMap<OBSSubCommodity, OBSSubCommodityViewModel>().ReverseMap();
                CreateMap<OBSUOM, OBSUOMViewModel>().ReverseMap();
                CreateMap<ShippingData, ShippingDataViewModel>().ReverseMap();
                CreateMap<AspNetRole, AspNetRoleViewModel>().ReverseMap();
                CreateMap<VerficationPOT, VerficationPOTViewModel>().ReverseMap();
                CreateMap<Seller, SellerViewModel>().ReverseMap();
                CreateMap<UserCart, UserCartViewModel>().ReverseMap();
                CreateMap<BuyerOrder, BuyerOrderViewModel>().ReverseMap();
                CreateMap<C_UserCart, C_UserCartViewModel>().ReverseMap();
                CreateMap<FavoriteItem, FavoriteItemViewModel>().ReverseMap();
                CreateMap<CartItem, CartItemViewModel>().ReverseMap();
                CreateMap<InvoiceOrderData, InvoiceOrderDataViewModel>().ReverseMap();
                CreateMap<ShippingData, ShippingDataViewModel>().ReverseMap();
                CreateMap<Status, StatusViewModel>().ReverseMap();
                CreateMap<OrderHistory, OrderHistoryViewModel>().ReverseMap();
                CreateMap<OrderAction, OrderActionViewModel>().ReverseMap();

                CreateMap<Link, LinkViewModel>().ReverseMap();
                CreateMap<UserLink, UserLinkViewModel>().ReverseMap();

                CreateMap<OBSBidDetail, OBSBidDetailViewModel>().ReverseMap();
                CreateMap<OBSBidLiveTracking, OBSBidLiveTrackingViewModel>().ReverseMap();
                CreateMap<AspNetUserClaim, AspNetUserClaimViewModel>().ReverseMap();

                CreateMap<MaterialPickupLocationSeller, MaterialPickupLocationSellerViewModel>().ReverseMap();
                CreateMap<OBSVendorDetail, OBSVendorDetailViewModel>().ReverseMap();

                CreateMap<Company, CompanyViewModel>().ReverseMap();

                CreateMap<TypicalImageRole, TypicalImageRoleViewModel>().ReverseMap();

                CreateMap<ErrorLog, ErrorLogViewModel>().ReverseMap();


            }
        }

    }
}
