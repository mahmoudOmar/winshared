﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Web.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.web.Controllers;
using Moq;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using System.Collections.Generic;
using ps.haweya.win.obs.business.Repository;
using System.Linq;
using ps.haweya.win.obs.security;

namespace ps.haweya.win.obs.test
{
    [TestClass]
    public class HomeTest
    {
       
        //check login View
        [TestMethod]
        public void LoginViewWithoutuser()
        {
            //Arrange 
            HomeController hc = new HomeController();
            //Act
            var result = hc.Login() as ViewResult;
            //Assert
            Assert.IsNotNull(result);
        }

        // check prooduct  available quantity

        [TestMethod]
        public void CheckQuntityaddtocart()
        {
            Mock<ProductInformationManager> mockreposetry = new Mock<ProductInformationManager>();
            int cartitemquantity = 3;
            int productquantity = 5;
            var result = mockreposetry.Object.isValidQuantity(cartitemquantity, productquantity);
            Assert.IsTrue(result);

        }

        [TestMethod]
        public void checkdecreaseQuantity()
        {
            Mock<ProductInformationManager> mockreposetry = new Mock<ProductInformationManager>();
            int cartitemquantity = 1;
            int productquantity = 5;
            var result = mockreposetry.Object.isValidQuantity(cartitemquantity, productquantity);
            Assert.IsTrue(result);

        }

        // addto cart
        [TestMethod]
        public void Addtocart()
        {

            var usercart = new List<CartItemViewModel>();
            Mock<CartItemsManager> mockreposetry = new Mock<CartItemsManager>();

            CartController c = new CartController();
            CartItemViewModel model = new CartItemViewModel()
            {
                InsertDate = DateTime.Now,
                ProductID = 123456,
                Qty = 20,
                IsBought = false
            };


            var result = c.AddUnit(model);


            Assert.AreEqual(result, "Item Added Successfully to cart");

        }
        [TestMethod]
        public void Getcartitems()
        {

            var usercart = new List<CartItemViewModel>();
            Mock<IRepository<CartItemViewModel>> mockreposetry = new Mock<IRepository<CartItemViewModel>>();

            CartItemsManager manager = new CartItemsManager();
            CartItemViewModel model = new CartItemViewModel()
            {
                InsertDate = DateTime.Now,
                ProductID = 123456,
                Qty = 20,
                IsBought = false
            };
            usercart.Add(model);
            mockreposetry.Setup(x => x.Get("")).Returns(usercart);
            var result = mockreposetry.Object.Get("");
            Assert.IsNotNull(result);

        }

        [TestMethod]
        public void checkcartitems_Summary()
        {

            var usercart = new List<CartItemViewModel>();
            Mock<IRepository<CartItemViewModel>> mockreposetry = new Mock<IRepository<CartItemViewModel>>();

            CartItemsManager manager = new CartItemsManager();
            CartItemViewModel model = new CartItemViewModel()
            {
                InsertDate = DateTime.Now,
                ProductID = 123456,
                Qty = 20,
                ProductInformation = new ProductInformationViewModel() {
                    ProductId = 123456,
                    Price = 500,
                },
                IsBought = false,
            };
            usercart.Add(model);

            double subtotal = 20 * 500;
            double total = (20 * 500)*0.05;

            var result = subtotal == model.ProductInformation.Price * model.Qty && (total == (model.ProductInformation.Price * model.Qty) * 0.05) ? true : false;
            Assert.IsTrue(result);

        }

        [TestMethod]
        public void CheckUniqe_Email_Register()
        {
            var aspnetuserslist = new List<AspNetUserViewModel>();
            AspNetUserViewModel model = new AspNetUserViewModel()
            {
                InsertDate = DateTime.Now,
                Id = Guid.NewGuid().ToString(),
                Email = "email@email.email",
                EmailConfirmed = false,
                FullName = " New User",
                UserName = "email@email.email",
            };
            aspnetuserslist.Add(model);
            Mock<IRepository<AspNetUserViewModel>> mockreposetry = new Mock<IRepository<AspNetUserViewModel>>();
            AspNetUsersManager manager = new AspNetUsersManager();
            AspNetUserViewModel newmodel = new AspNetUserViewModel()
            {
                InsertDate = DateTime.Now,
                Id = Guid.NewGuid().ToString(),
                Email ="email1@email.email",
                 EmailConfirmed=false,
                  FullName=" New User",
             UserName="email@email.email",
              
            };
            mockreposetry.Setup(x => x.Get("")).Returns(aspnetuserslist.Where(x=>x.Email==newmodel.Email));
            var result = mockreposetry.Object.Get("").Count();
            Assert.AreEqual(result,0);
        }

        [TestMethod]
        public void Check_Add_OTP()
        {
            var otplist = new List<VerficationPOTViewModel>();
            Mock<IRepository<VerficationPOTViewModel>> mockreposetry = new Mock<IRepository<VerficationPOTViewModel>>();
            VerficationPOTManager manager = new VerficationPOTManager();
            Random r = new Random(100000);
            string pot = r.Next(100001, 999999).ToString();
            VerficationPOTViewModel otpmodel = new VerficationPOTViewModel()
            {
                EmailAddress = "email@email.email",
                insertDate = DateTime.Now,
                IsValid = true,
                POT = "12313",
                UserID = Guid.NewGuid().ToString(),
            };
            otplist.Add(otpmodel);
            List<string> potlist = new List<string>() { "123456","213456","345353", "12313" };
            while (potlist.Contains(pot))
            {
                pot = r.Next(100000, 999999).ToString();
            }
            VerficationPOTViewModel newmodel = new VerficationPOTViewModel()
            {
                 EmailAddress= "email2@email2.email",
                 insertDate=DateTime.Now,
                 IsValid=true,
                 POT=pot,
                 UserID= Guid.NewGuid().ToString(),
            };
            mockreposetry.Setup(x => x.Get("")).Returns(otplist.Where(x => x.POT == newmodel.POT));
            var result = mockreposetry.Object.Get("").Count();
            Assert.AreEqual(result, 0);
        }


        [TestMethod]
        public void LoginWithoutOTP()
        {
            var aspnetuserslist = new List<AspNetUserViewModel>();
            AspNetUserViewModel model = new AspNetUserViewModel()
            {
                InsertDate = DateTime.Now,
                Id = Guid.NewGuid().ToString(),
                Email = "mahmoudomry@gmail.com",
                EmailConfirmed = false,
                FullName = " New User",
                UserName = "email@email.email",
                LockoutEnabled=true,
                

            };
            aspnetuserslist.Add(model);
            Mock<IRepository<AspNetUserViewModel>> mockreposetry = new Mock<IRepository<AspNetUserViewModel>>();
            

            mockreposetry.Setup(x => x.Get("")).Returns(aspnetuserslist.Where(x => x.EmailConfirmed == false&&x.Email== "mahmoudomry@gmail.com"));
            var result = mockreposetry.Object.Get("").Count();
            Assert.AreEqual(result, 1);

        }

        [TestMethod]
        public void LoginWithOTP()
        {
            var aspnetuserslist = new List<AspNetUserViewModel>();
            AspNetUserViewModel model = new AspNetUserViewModel()
            {
                InsertDate = DateTime.Now,
                Id = Guid.NewGuid().ToString(),
                Email = "mahmoudomry@gmail.com",
                EmailConfirmed = true,
                FullName = " New User",
                UserName = "email@email.email",
                LockoutEnabled = true,
            };
            aspnetuserslist.Add(model);
            Mock<IRepository<AspNetUserViewModel>> mockreposetry = new Mock<IRepository<AspNetUserViewModel>>();

            mockreposetry.Setup(x => x.Get("")).Returns(aspnetuserslist.Where(x => x.EmailConfirmed == true&&x.LockoutEnabled==true && x.Email == "mahmoudomry@gmail.com"));
            var result = mockreposetry.Object.Get("").Count();
            Assert.AreEqual(result, 1);

        }

        [TestMethod]
        public void LoginWithAdminReject()
        {
            var aspnetuserslist = new List<AspNetUserViewModel>();
            AspNetUserViewModel model = new AspNetUserViewModel()
            {
                InsertDate = DateTime.Now,
                Id = Guid.NewGuid().ToString(),
                Email = "mahmoudomry@gmail.com",
                EmailConfirmed = true,
                FullName = " New User",
                UserName = "email@email.email",
                LockoutEnabled = false,
            };
            aspnetuserslist.Add(model);
            Mock<IRepository<AspNetUserViewModel>> mockreposetry = new Mock<IRepository<AspNetUserViewModel>>();

            mockreposetry.Setup(x => x.Get("")).Returns(aspnetuserslist.Where(x => x.EmailConfirmed == true && x.LockoutEnabled == true && x.Email == "mahmoudomry@gmail.com"));
            var result = mockreposetry.Object.Get("").Count();
            Assert.AreEqual(result, 0);

        }


        [TestMethod]
        public void addFirstBid()
        {

          
            var Bidslist = new List<OBSBidDetailViewModel>();
            OBSBidDetailViewModel model = new OBSBidDetailViewModel()
            {
              AddedOn=DateTime.Now,
              AspNetUser= new AspNetUserViewModel(),
              BidPrice =550,
              IsSold=false,
              BidCount=0,
              ProductInformation= new ProductInformationViewModel(),
              NeedInspection=true,
              StatusID=1,                
            };
            Bidslist.Add(model);
            Mock<IRepository<OBSBidDetailViewModel>> mockreposetry = new Mock<IRepository<OBSBidDetailViewModel>>();

            mockreposetry.Setup(x => x.Add(model)).Returns(model);
            var result = mockreposetry.Object.Add(model);
            Assert.IsNotNull(result);

        }

        [TestMethod]
        public void add_SecondBid_withDifference()
        {
            ProductInformationViewModel product = new ProductInformationViewModel()
            {
                ProductId = 12345,
                IsShipment = true,
                IsInspect = true,
                AvailableQuantity = 500,
                BidDifferenceType = 1,
                BidDifference = 50,
                CreateDate = DateTime.Now,
                DeadLine = DateTime.Now.AddDays(1),
            };
            var Bidslist = new List<OBSBidDetailViewModel>();
            OBSBidDetailViewModel model = new OBSBidDetailViewModel()
            {
                AddedOn = DateTime.Now,
                AspNetUser = new AspNetUserViewModel(),
                BidPrice = 550,
                IsSold = false,
                BidCount = 0,
                ProductInformation = product,
                NeedInspection = true,
                StatusID = 1,
            };
            Bidslist.Add(model);
            OBSBidDetailViewModel modelnew = new OBSBidDetailViewModel()
            {
                AddedOn = DateTime.Now,
                AspNetUser = new AspNetUserViewModel(),
                BidPrice = 620,
                IsSold = false,
                BidCount = 0,
                ProductInformation = product,
                NeedInspection = true,
                StatusID = 1,
            };
            Mock<IRepository<OBSBidDetailViewModel>> mockreposetry = new Mock<IRepository<OBSBidDetailViewModel>>();

            mockreposetry.Setup(x => x.Add(modelnew)).Returns(modelnew.BidPrice>=Bidslist.Max(x=>x.BidPrice)+product.BidDifference?modelnew:null);
            var result = mockreposetry.Object.Add(modelnew);
            Assert.IsNotNull(result);

        }


        [TestMethod]
        public void add_Product()
        {
            ProductInformationViewModel product = new ProductInformationViewModel()
            {
                ProductId = 12345,
                IsShipment = true,
                IsInspect = true,
                AvailableQuantity = 500,
                BidDifferenceType = 1,
                BidDifference = 50,
                CreateDate = DateTime.Now,
                DeadLine = DateTime.Now.AddDays(1),
            };
        
            Mock<IRepository<ProductInformationViewModel>> mockreposetry = new Mock<IRepository<ProductInformationViewModel>>();
            mockreposetry.Setup(x => x.Add(product)).Returns(product);
            var result = mockreposetry.Object.Add(product);
            Assert.IsNotNull(result);

        }
    }
}





