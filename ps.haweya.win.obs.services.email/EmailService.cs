﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Timers;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;
using MimeKit;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;

namespace ps.haweya.win.obs.services.email
{
    public partial class EmailService : ServiceBase
    {
        public string USERNAME = ConfigurationManager.AppSettings["EMAIL_ADDRESS"].ToString();
        public string PASSWORD = ConfigurationManager.AppSettings["EMAIL_PASSWORD"].ToString();

        public static int PROCESSID = 1;

        private Timer timer = new Timer();

        public EmailService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            TraceService("start service");

            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);

            int interval = Convert.ToInt32(ConfigurationManager.AppSettings["timerIntervalPerSec"]);

            timer.Interval = interval;
            timer.Enabled = true;
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            TraceService("Process (" + (PROCESSID) + ") Start at " + DateTime.Now);

            timer.Enabled = false;

            try
            {
                using (var client = new ImapClient())
                {

                    client.ServerCertificateValidationCallback = (s, c, h, eee) => true;

                    client.Connect("imap.gmail.com", 993, true);

                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    client.Authenticate(ConfigurationManager.AppSettings["EMAIL_ADDRESS"].ToString(),
                                        ConfigurationManager.AppSettings["EMAIL_PASSWORD"].ToString());

                    var inbox = client.Inbox.Open(FolderAccess.ReadWrite);

                    var unReadEmails = client.Inbox.Search(SearchQuery.NotSeen);
                    var messages = client.Inbox.Fetch(unReadEmails, MessageSummaryItems.Full | MessageSummaryItems.BodyStructure);

                    TraceService("Process " + messages.Count);

                    foreach (IMessageSummary message in messages)
                    {

                        ProcessEmail(client.Inbox, message);
                        client.Inbox.AddFlags(message.UniqueId, MessageFlags.Seen, true);
                    }

                    client.Disconnect(true);
                }
            }
            catch (Exception ee)
            {
                TraceService("Exception in process " + ee.Message ?? "");
                throw new Exception("Error in connecting to email", ee.InnerException);
            }

            timer.Enabled = true;

            TraceService("Process (" + (PROCESSID++) + ") End at " + DateTime.Now);
        }

        private void ProcessEmail(IMailFolder folder, IMessageSummary Message)
        {
            using (var db = new winprodTestDB1Entities())
            {

                var ticketId = 0;
                Ticket ticket = null;

                try
                {
                    Match match = Regex.Match(Message.Envelope.Subject, @"(TICKET\[#\d{5}\])");
                    if (match.Success)
                    {
                        int.TryParse(Regex.Match(match.Groups[1].Value, @"\d{5}").ToString(), out ticketId);
                        ticket = db.Tickets.Find(ticketId);
                    }
                }
                catch (Exception e)
                {
                    ticketId = 0;
                }

                if (ticketId == 0)
                {
                    ticket = new Ticket()
                    {
                        From = string.Join(",", Message.Envelope.From),
                        Date = DateTime.Now,
                        Source = "Email",
                        Status = 1,
                        Subject = Message.Envelope.Subject
                    };

                    db.Tickets.Add(ticket);
                    db.SaveChanges();
                }

                TraceService("added to ticket");

                try
                {
                    var body = ((TextPart)folder.GetBodyPart(Message.UniqueId, Message.HtmlBody)).Text;

                    db.TicketRows.Add(new TicketRow
                    {
                        Body = body,
                        Date = Message.Date.DateTime,
                        From = string.Join(",", Message.Envelope.From),
                        System = false,
                        TicketId = ticket.Id
                    });

                    db.SaveChanges();

                }catch(Exception eeee)
                {
                    TraceService(eeee.Message);
                    TraceService(eeee.InnerException == null ? "" : eeee.InnerException.Message);
                }
                TraceService("added to ticket rows");

            }

        }

        protected override void OnStop()
        {
            timer.Enabled = false;
            TraceService("stopping service");
        }

        private void TraceService(string content)
        {
            string codeBase = Assembly.GetExecutingAssembly().Location;
            UriBuilder uri = new UriBuilder(codeBase);

            string path = Uri.UnescapeDataString("c://logs/" + DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + ".txt");
            FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);

            StreamWriter sw = new StreamWriter(fs);
            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(content);

            sw.Flush();
            sw.Close();
        }

    }
}
