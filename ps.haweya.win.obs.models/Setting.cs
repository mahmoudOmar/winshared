//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ps.haweya.win.obs.models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Setting
    {
        public int Id { get; set; }
        public string WebLogoOrgName { get; set; }
        public string WebLogoName { get; set; }
        public string MobLogoOrgName { get; set; }
        public string MobLogoName { get; set; }
        public string Address { get; set; }
        public string MobileNo1 { get; set; }
        public string MobileNo2 { get; set; }
        public string Email { get; set; }
        public string shipping_Return { get; set; }
        public string Terms_and_Conditions { get; set; }
        public string Privacy_policy { get; set; }
        public string VatText { get; set; }
        public Nullable<double> VatValue { get; set; }
        public string Company_Profile { get; set; }
        public string WhatsappNO { get; set; }
        public string UNA { get; set; }
    }
}
