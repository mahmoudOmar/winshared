//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ps.haweya.win.obs.models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserCart
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public long ItemId { get; set; }
        public int Quantity { get; set; }
        public System.DateTime LastModifiedAt { get; set; }
        public bool IsPending { get; set; }
    
        public virtual ProductInformation ProductInformation { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
