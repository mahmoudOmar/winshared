//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ps.haweya.win.obs.models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductInformation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductInformation()
        {
            this.CartItems = new HashSet<CartItem>();
            this.FavoriteItems = new HashSet<FavoriteItem>();
            this.OBSBidDetails = new HashSet<OBSBidDetail>();
            this.OBSJobOnceSchedulers = new HashSet<OBSJobOnceScheduler>();
            this.OBSLogisticsDetails = new HashSet<OBSLogisticsDetail>();
            this.OBSMediaDetails = new HashSet<OBSMediaDetail>();
            this.OBSProductInspectDetails = new HashSet<OBSProductInspectDetail>();
            this.OBSProductUserMaps = new HashSet<OBSProductUserMap>();
            this.OBSPurchaseDetails = new HashSet<OBSPurchaseDetail>();
            this.ProductDetails = new HashSet<ProductDetail>();
            this.UserCarts = new HashSet<UserCart>();
        }
    
        public long ProductId { get; set; }
        public string ProductTitle { get; set; }
        public string ProductLocation { get; set; }
        public string Quantity { get; set; }
        public Nullable<System.DateTime> DeadLine { get; set; }
        public string ProductImage { get; set; }
        public int BidCount { get; set; }
        public double Price { get; set; }
        public int Category { get; set; }
        public int ProductBrands { get; set; }
        public System.DateTime CreateDate { get; set; }
        public double BidDifference { get; set; }
        public string PricingType { get; set; }
        public string Username { get; set; }
        public bool isActive { get; set; }
        public bool IsWinActive { get; set; }
        public string SuggestionByWin { get; set; }
        public int AvailableQuantity { get; set; }
        public int DeadLineInDays { get; set; }
        public bool isPrivate { get; set; }
        public bool IsSold { get; set; }
        public double ShipmentCharges { get; set; }
        public bool IsShipment { get; set; }
        public bool IsInspect { get; set; }
        public string OtherCategory { get; set; }
        public string OtherManufacturer { get; set; }
        public Nullable<System.DateTime> ExtendDeadline { get; set; }
        public bool isExtendDeadline { get; set; }
        public bool IsDelete { get; set; }
        public bool IsBulkUpload { get; set; }
        public int DisciplineId { get; set; }
        public int CommodityId { get; set; }
        public int SubCommodityId { get; set; }
        public Nullable<System.DateTime> ManufacturingDate { get; set; }
        public int UOMId { get; set; }
        public string EndUser { get; set; }
        public string SupplierName { get; set; }
        public string CertificatesOrDataSheet { get; set; }
        public string WINAssement { get; set; }
        public string WINAssementby { get; set; }
        public string Repairable { get; set; }
        public string UsedEquipmentOrApplication { get; set; }
        public double Age { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public string SellerName { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public double MarketPrice { get; set; }
        public string SellerMaterialId { get; set; }
        public Nullable<System.DateTime> PreviousDeadline { get; set; }
        public bool IsExtendOverwrite { get; set; }
        public string BatchNumber { get; set; }
        public Nullable<int> SecurityDeposit { get; set; }
        public string ProductDocument { get; set; }
        public Nullable<System.DateTime> BidStartDate { get; set; }
        public string Keywords { get; set; }
        public string SupplierId { get; set; }
        public Nullable<int> SecurityDepositDays { get; set; }
        public Nullable<int> BidDifferenceType { get; set; }
        public Nullable<int> FullPaymetDays { get; set; }
        public Nullable<double> SecurityDepositPercent { get; set; }
        public Nullable<bool> ShowInMap { get; set; }
        public Nullable<bool> hasMinimumSellingPrice { get; set; }
        public Nullable<bool> ShowBidAmmount { get; set; }
        public Nullable<bool> AllowShowBiddersLocations { get; set; }
        public Nullable<double> Length { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<double> Height { get; set; }
        public Nullable<double> Weight { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CartItem> CartItems { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FavoriteItem> FavoriteItems { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSBidDetail> OBSBidDetails { get; set; }
        public virtual OBSCommodity OBSCommodity { get; set; }
        public virtual OBSDiscipline OBSDiscipline { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSJobOnceScheduler> OBSJobOnceSchedulers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSLogisticsDetail> OBSLogisticsDetails { get; set; }
        public virtual OBSManufacturer OBSManufacturer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSMediaDetail> OBSMediaDetails { get; set; }
        public virtual OBSProductCategory OBSProductCategory { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSProductInspectDetail> OBSProductInspectDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSProductUserMap> OBSProductUserMaps { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OBSPurchaseDetail> OBSPurchaseDetails { get; set; }
        public virtual OBSSubCommodity OBSSubCommodity { get; set; }
        public virtual OBSUOM OBSUOM { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductDetail> ProductDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserCart> UserCarts { get; set; }
    }
}
