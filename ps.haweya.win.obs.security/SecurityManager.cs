﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using ps.haweya.win.obs.business;
using ps.haweya.win.obs.business.Managers;
using ps.haweya.win.obs.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace ps.haweya.win.obs.security
{

    public class SecurityManager : IDisposable
    {

        public static string[] AdminRoles
        {
            get
            {
                return new string[] { "3", "2" };
            }

        }
        public static string[] UserRoles
        {
            get
            {
                return new string[] { "1", "2" };
            }
        }

        private ApplicationDbContext dbContext;
        public UserManager<ApplicationUser> UserManager { get; private set; }
        public RoleManager<IdentityRole> RoleManager { get; private set; }

        public bool IsAuthenticated { get { return Thread.CurrentPrincipal.Identity.IsAuthenticated; } }
        public string CurrentUserName { get { return Thread.CurrentPrincipal.Identity.GetUserName(); } }
        public string GetCurrentUserId { get { return Thread.CurrentPrincipal.Identity.GetUserId(); } }

        public SecurityManager()
        {
            dbContext = new ApplicationDbContext();
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbContext));
            RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbContext));

            UserManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 4,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };
            UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager) { AllowOnlyAlphanumericUserNames = false };
        }

        public void Update() { dbContext.SaveChanges(); }
        public ApplicationUser GetCurrentUser() { return GetUser(CurrentUserName); }
        public ApplicationUser GetUser(string username) { return UserManager.FindByName(username); }
        public IdentityRole GetRole(string RoleId) { return RoleManager.FindById(RoleId); }
        public string GetUserId(string userName)
        {
            string id = string.Empty;
            ApplicationUser user = UserManager.FindByName(userName);
            if (user != null)
                id = user.Id;

            return id;
        }


        public static bool IsUserInRole(string UserId, string RoleName)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            return userManager.IsInRole(UserId, RoleName);
        }

        public ApplicationUser GetByUserId(string Id)
        {
            string id = string.Empty;
            ApplicationUser user = UserManager.FindById(Id);
            return user;
        }

        public string CreateUser(RegisterViewModel model, string type)
        {
            Random r = new Random(100000);

            string pot = r.Next(100001, 999999).ToString();
            List<string> potlist = new AspNetUsersManager().GetAllPOT();
            while(potlist.Contains(pot))
            {
                pot = r.Next(100000, 999999).ToString();
            }

            var u = new ps.haweya.win.obs.security.ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                CountryId = model.Counrty + "",
                StateId = model.State + "",
                CityId = model.City + "",
                EmailConfirmed = false,
                POBox = model.POBox,
                PhoneNumber = model.Mobile,
                POT = pot,
                IsPOTChanged = false,
                FullName = model.Name,
                ConfirmedEmail = false,
                Role = "",
                InsertDate = DateTime.Now,
                LockoutEnabled = type == "Seller" ? false : true

            };

            var result = UserManager.CreateAsync(u, model.Password).Result;

            if (result.Succeeded)
            {
                UserManager.AddToRoleAsync(u.Id, type);

                // default role
                u.Claims.Add(new IdentityUserClaim() { UserId = u.Id, ClaimType = "DEFAULT_ROLE", ClaimValue = type });

                //new MailManager().SendMessage(u.Email, "Verifying email address", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "To verify your email, we've sent a One Time Password (OTP) .<br/> Use below password to verify Your Account ", "Warehouse Integration Company", model.Name, u.POT);
                new MailManager().SendMessage(u.Email, "Verifying email address", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We have sent one Time Password (OTP) to your email for verification .<br/> Use below password to verify Your Account ", "Warehouse Integration Company", model.Name, u.POT);
                return u.Id;
            }

            return string.Join(", ", result.Errors);
        }
        public string CreateUserExternal(string name,string email,string mobile,string countryid, string type)
        {
            
            var u = new ps.haweya.win.obs.security.ApplicationUser
            {
                UserName =email,
                Email = email,
                CountryId = countryid,
               
                EmailConfirmed = true,
               
                PhoneNumber = mobile,
               
                IsPOTChanged = false,
                FullName = name,
                ConfirmedEmail = true,
                Role = "",
                InsertDate = DateTime.Now,
                LockoutEnabled = type == "Seller" ? false : true

            };

            var result = UserManager.CreateAsync(u, "external_pass@123").Result;

            if (result.Succeeded)
            {
                UserManager.AddToRoleAsync(u.Id, type);

                // default role
                u.Claims.Add(new IdentityUserClaim() { UserId = u.Id, ClaimType = "DEFAULT_ROLE", ClaimValue = type });

                //new MailManager().SendMessage(u.Email, "Verifying email address", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "To verify your email, we've sent a One Time Password (OTP) .<br/> Use below password to verify Your Account ", "Warehouse Integration Company", model.Name, u.POT);
                //   new MailManager().SendMessage(u.Email, "Verifying email address", "https://win.com.sa/Content/FrontEnd/images/logo.png", "https://win.com.sa", "#", "We have sent one Time Password (OTP) to your email for verification .<br/> Use below password to verify Your Account ", "Warehouse Integration Company", model.Name, u.POT);
                


                return u.Id;
            }

            return string.Join(", ", result.Errors);
        }
        public string UpdateBuyer(BuyerViewModel model)
        {
            Random r = new Random(100000);
            string pot = r.Next(100000, 999999).ToString();
            var u = UserManager.FindById(model.ASPNetUserID);
            {
                //UserName = model.Email,
                //Email = model.Email,
                u.FullName = model.Name;
                u.CountryId = model.Country + "";
                u.StateId = model.State + "";
                u.CityId = model.City + "";
                //u.EmailConfirmed = ;
                u.POBox = model.POBox;
                u.PhoneNumber = model.Mobile;

            };

            var result = UserManager.Update(u);

            if (result.Succeeded)
            {
                return u.Id;
            }

            return string.Format(", ", result.Errors);
        }
        public string UpdateSeller(RegisterViewModel model,string userid)
        {
            //Random r = new Random(100000);
            //string pot = r.Next(100000, 999999).ToString();
            var u = UserManager.FindById(userid);
            {
                //UserName = model.Email,
                //Email = model.Email,

                u.CountryId = model.Counrty + "";
                u.StateId = model.State + "";
                u.CityId = model.City + "";
                u.EmailConfirmed = false;
                u.POBox = model.POBox;
                u.PhoneNumber = model.Mobile;

            };

            var result = UserManager.Update(u);

            if (result.Succeeded)
            {
                return u.Id;
            }

            return string.Format(", ", result.Errors);
        }

        public SignInStatus SignIn(ApplicationSignInManager ApplicationSignInManager, string Email, string Password, bool isPersistent)
        {
            //            var identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            //      AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true, ExpiresUtc = DateTime.UtcNow.AddDays(5), IssuedUtc = DateTime.UtcNow }, identity);
            return ApplicationSignInManager.PasswordSignInAsync(Email, Password, isPersistent, shouldLockout: false).Result;
        }

        public void SignInauto(ApplicationSignInManager ApplicationSignInManager, string userid, bool isPersistent)
        {
            //            var identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            //      AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true, ExpiresUtc = DateTime.UtcNow.AddDays(5), IssuedUtc = DateTime.UtcNow }, identity);
            ApplicationUser user = GetByUserId(userid);
             ApplicationSignInManager.SignIn(user,isPersistent,true);
        }

        public void SignIn(IAuthenticationManager AuthenticationManager, string userid, bool? isPersistent)
        {
            ApplicationUser user = GetByUserId(userid);

            var identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent.HasValue ? isPersistent.Value : true, ExpiresUtc = DateTime.UtcNow.AddDays(5), IssuedUtc = DateTime.UtcNow }, identity);
        }

        public bool ResetPassword(string userName = null, string password = null)
        {
            ApplicationUser user = userName == null ? GetCurrentUser() : GetUser(userName);

            if (user == null)
                return false;

            UserManager.RemovePassword(user.Id);
            UserManager.AddPassword(user.Id, password);

            UserManager.Update(user);

            return true;
        }

        public void UpdateSecretQuestion(string question, string answer, string userName = null)
        {
            ApplicationUser user = userName == null ? GetCurrentUser() : GetUser(userName);

            if (user == null)
                return;

            dbContext.SaveChanges();
        }

        public void Logout(IAuthenticationManager AuthenticationManager)
        {
            AuthenticationManager.SignOut();
        }

        public void MapUserToRole(string userId, string roleName)
        {
            UserManager.AddToRole(userId, roleName);
        }

        public Task<ApplicationUser> FindByIdAsync(string userId)
        {
            return UserManager.FindByIdAsync(userId);
        }
        public ApplicationUser FindById(string userId)
        {
            return UserManager.FindById(userId);
        }

        public ApplicationUser FindByEmail(string email)
        {
            return UserManager.FindByEmail(email);
        }

        public Task<IdentityResult> ChangePasswordAsync(string userId, string currentPassword, string newPassword)
        {
            return UserManager.ChangePasswordAsync(userId, currentPassword, newPassword);
        }

        public Task<IdentityResult> AddPasswordAsync(string userId, string newPassword)
        {
            return UserManager.AddPasswordAsync(userId, newPassword);
        }


        public void Update(AspNetUserViewModel model)
        {

            ApplicationUser user = GetUser(model.UserName);
            if (user == null)
                return;
            user.EmailConfirmed = model.EmailConfirmed;
            dbContext.SaveChanges();

            //model.ty
        }
        public void VerifyUser(string UserId)
        {

            ApplicationUser user = GetByUserId(UserId);
            if (user == null)
                return;
            user.EmailConfirmed = true;
            dbContext.SaveChanges();


            //model.ty
        }


        public List<string> GetUserRolesById(string userid)
        {


            return UserManager.GetRoles(userid).ToList();
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
        }
    }

    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }

    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                //RequireNonLetterOrDigit = true,
                RequireDigit = true,
                //RequireLowercase = true,
                //RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(IRoleStore<IdentityRole, string> store)
            : base(store)
        {
        }
        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var roleStore = new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>());
            return new ApplicationRoleManager(roleStore);
        }
    }
}